---
lang: en
name: "2014"

title: "We celebrated our fifth anniversary with a leadership transition and refining our strategic approach."
description: "We developed tools to monitor elections, to compare bills and track legislative work, freedom of access to information, conflict of interests, to monitor government’s promises and campaign financing. The majority of these tools were first developed and tested in Chile, but this changed: they have been used in more than 10 cases and not only in Latin American countries, but also y countries far away such as Morocco."
---
