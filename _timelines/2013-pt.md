---
lang: pt
name: "2013"
title: "Buscamos reduzir as assimetrias da informação que separam a cidadania da política, o mercado e demais espaços sociais, como condição necessária para a integral participação."
description: "Trabalhamos na plataforma Vota Inteligente para informar tudo sobre as eleições. Impulsionamos a lei da Transparência, para que todas y todos reconheçam seu direito a informação. Ademais, criamos ferramentas para investigar os conflitos de interesse das autoridades legislativas."
---
