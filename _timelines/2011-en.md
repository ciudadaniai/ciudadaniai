---
lang: en
name: "2011"
title: "We compiled information on the public sector to transfer it in a friendly and simple manner to citizens."
description: "We opened up a new space with our projects to know, in percentages, how much our elected authorities fulfill their promises, and how parliamentarians legislate in Chile."
---
