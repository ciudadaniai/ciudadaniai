---
lang: es
name: "2016"

title: "Trabajamos en tecnología cívica para la promoción de la transparencia, la rendición de cuentas y el acceso a la información."
description: "Iniciamos la transición desde herramientas tecnológicas hacia un trabajo en tecnología cívica in situ. Comenzamos a explorar en tecnologías de denuncia ciudadana frente a irregularidades de las campañas políticas en Chile."

---
