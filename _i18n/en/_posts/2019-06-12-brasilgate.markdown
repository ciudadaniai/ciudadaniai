---
layout: post
title: 'Brazilgate'
intro: 'The intercept published conversations between Moro and the Operation Car Wash prosecutors. A blow to democracy.'
day: 12
month: Jun
date: 2019-06-12
categories: News
highlighted: false
share_message: "BRASILGATE: Sin libertad de prensa no hay democracia @ciudadaniai."
img: 12-06-19.jpg
single_image:
  -
    caption: Diario Río Negro
---
***Column written by Ana Carolina Lourenço, Regional Advocacy Coordinator at Ciudadanía Inteligente. Published on [Nodal](https://www.nodal.am/2019/06/brasilgate-sin-libertad-de-prensa-no-hay-democracia-por-ana-carolina-lourenco/)***

Intercept Brazil published three reports about a series of improper manipulations by the then-judge Sérgio Moro, now Minister of Justice, in the investigations of the Brazilian Operation Car Wash, which prevented the participation of the leader of the Workers' Party, Luiz Inácio Lula da Silva, in the presidential elections in 2018. The material: messages exchanged from 2015 to 2018, via Telegram, between Sérgio Moro and the prosecutor Deltan Dallagnol, which were obtained through an anonymous source. The conversations present irrefutable evidence of irregular actions, even illegal and politicized, by the prosecution in charge. According to the messages published, current Minister Sérgio Moro suggested witnesses to the Prosecutor's Office, gave his opinion on the progress of the findings, anticipated a decision by the accusers, and articulated movements that, if corroborated, permanently damage the impartiality of a judge.

Beyond an indication of actions outside the jurisdiction of a judge, the revelations are especially grave because of their effects on the Brazilian elections of 2018. Only four days after the filing of the Tríplex case against Lula in 2017, the coordinator of Operation Car Wash’s investigation, Deltan Dallagnol, had doubts about the soundness of the story he told Judge Moro. Once the accusations of passive corruption and money laundering against Lula had been made, Dallagnol sent messages directly to Moro, who should make the judgement of the accusations, to explain the terms of the accusations, their weaknesses, and their argumentative choices.

The revelations show a politicization of justice during different phases of the process. Today, facts that show a pattern of behavior over time by the Car Wash prosecutors to interfere in the election results are presented to the public. For example, in the exchange of messages, a political and illegal maneuver is used to successfully block an interview between Lula and the news publication Folha de São Paulo before the first round of the presidential elections. Why? The impact of the interview presented risks and a possible victory for Fernando Haddad, Workers’ Party presidential candidate. The supposed technical and impartial arguments that were shown in the national and international press, even on social media, are no longer valid with the publication of Intercept.

Since 2016, the politicization of the Operation Car Wash was at the center of the debate, polarizing Brazilian and Latin American society. One side of the discussion defended the impartiality of Brazilian justice operators, applauded for their fight against corruption beyond the political party involved, in this case the Workers’ Party. On the other hand, the notorious political tone of their actions was noted with concern. Given the new revelations, it is clear and irrefutable that the prosecutors of the Operation Car Wash are not nonpartisan or apolitical, but rather the opposite. They are people who act out of ideological convictions and who worked to prevent the return of the WP to power, through actions contrary to the rule of law, weakening democracy and the trust of citizens in their institutions.

Now, the most important question is: what happens after these revelations? Will an election be considered legitimate where there was interference, no longer from external actors, but from internal actors, from the judiciary branch itself, which actively, premeditatedly, and continuously harmed one of the political parties that aspired to a position of power? Were Brazilian voters deprived of the possibility of voting for the political candidate of their choice? Should Sérgio Moro remain in government, as a minister, especially if the Federal Police - under his command and control - is the entity that corresponds to investigating the contents of the conversations?

In this context, in Ciudadanía Inteligente, we reinforce our mission of support to whistleblowers and all the people who work every day to seek the truth: journalists. This media revelation unfortunately unveils a malicious capture of the legitimate fight against corruption, is an attack against the Brazilian Constitution, a violation of the separation of the powers of the Republic and is an insult to democracy itself.

Today's revelations, and the work that follows, reinforce the importance of investigative journalism and whistleblowers as counterparts to the arbitrariness of power, and as key players in the fight against corruption that serves the public interest rather than political vendettas.

Strong democracies exist only with freedom of the press. It could not be any other way, and Brazil is the example to prevent impunity from triumphing.

Let’s fight for strong democracies.

To The Intercept and Brazilian society, all of our solidarity.

Let’s continue.
