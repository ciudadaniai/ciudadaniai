---
layout: post
title: 'Por una Ciudadanía Conectada'
intro: '¿Qué hay detrás de internet? No te pierdas nuestro festival "Ciudadanía Conectada".'
day: 17
month: Feb
date: 2020-01-27 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Por una Ciudadanía Conectada by @ciudadaniai.'
img: ciudadaniaconectada.png
caption:
---
En Chile en los últimos años el acceso a Internet ha aumentado considerablemente. Hoy el 87,4% de los hogares tiene acceso a esta red. ¿Pero sabemos cómo funciona? a pesar de que el uso de internet ha permitido que las personas se mantengan conectadas y realicen acciones sociales casi inmediatas a través de diferentes plataformas, existe un porcentaje de la población que desconoce cómo usarlas y peor aún; no saben cuáles son los riesgos y amenazas de esta tecnología.

En el contexto social que nos encontramos es fundamental que la ciudadanía esté conectada y tenga las herramientas para desenvolverse de manera autónoma en las plataformas que ofrece la red, para generar sus propias decisiones y acciones ante los acontecimientos que nos estamos enfrentando. Es por eso que desde Ciudadanía Inteligente junto a la Municipalidad de Quilicura organizamos “Ciudadanía Conectada”, un festival tecnológico bajo tres temáticas: Brecha y violencia de género en espacios digitales, Inteligencia Artificial y Desinformación.

La violencia de género online es una realidad que deben pasar cientos de mujeres diariamente. Son ellas las que se deben privar de sus redes sociales para no ser acosadas, hostigadas, calumniadas, ni recibir contenido inapropiado por parte de sus agresores. En Chile aún no existe una legislación que castigue estas malas prácticas, lo que hace que muchas de las denuncias que ingresan por esta causa queden desestimadas. ¿Entonces qué debemos hacer si somos víctimas de violencia online?  En [Datos protegidos](https://datosprotegidos.org/dataprivacyday2019-conoce-nuestro-video-sobre-violencia-de-genero-en-internet/) han estudiado este tipo de violencia y han entregado recomendaciones para empujar por una Internet segura para las mujeres.

¿Y qué pasa cuando hablamos de Inteligencia Artificial? Mucho se desconoce de esta tecnología que hoy está muy inserta en nuestra sociedad. ¿Sabemos acaso cuánto interviene en nuestra rutina? ¿Cuáles son sus límites?. Este festival “Ciudadanía Contectada” busca justamente  concientizar a la ciudadanía respecto de los sesgos presentes y cómo pueden influir en diferentes procesos diarios como por ejemplos: en la contratación de un trabajo o en la automatización de procesos donde el ser humano toma el rol de fiscalizador y no de realizador.

Vamos un paso más atrás. Las Fake News. Este es uno de los más grandes desafíos que sufre nuestra era digital. De hecho, estudios recientes sobre estos fenómenos reafirman que los seres humanos tendemos a creer información falsa si confirma nuestras creencias previas, un fenómeno que crea un escenario todavía más favorable a la desinformación.

Todos los días las fake news se difunden rápidamente a través de redes sociales y debemos encontrar una fórmula para remediar esta plaga. En Ciudadanía Contectada tendremos talleres y charlas para cómo reconocerlas y compartir esa herramienta. Una noticia falsa puede hacer gran daño en nuestras democracias.

Creemos que como Ciudadanía Inteligente tenemos el compromiso de entregarles las herramientas y conocimientos que estén a nuestro alcance a la comuna de Quilicura y todo Chile para concientizar y sumar a la ciudadanía a estar conectados con la realidad, los cambios y procesos que está viviendo Chile y el mundo. ¡Súmate a Ciudadanía Conectada!  
