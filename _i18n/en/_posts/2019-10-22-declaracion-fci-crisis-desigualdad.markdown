---
layout: post
title: 'Declaración FCI Crisis Desigualdad'
intro: 'Chile protesta contra la desigualdad y tenemos que escucharlo'
day: 22
month: Oct
date: 2019-10-22 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Declaración FCI Crisis Desigualdad'
img: 22-10-19.png
caption:
---

***Nuestra organización lleva 10 años luchando por mejores democracias en Chile y América Latina. A lo largo de nuestra historia, han pasado por nuestro equipo decenas de líderes jóvenes, que se han dedicado con mucha pasión a crear tecnologías, narrativas y metodologías para cambiar las cosas, pero sabemos que no han cambiado lo suficiente.***

Enfrentamos crisis múltiples que convergen, para las cuales aún no tenemos respuesta, y que obligan a una profunda reflexión colectiva. Aumento de la desigualdad, pérdida de nuestros bienes comunes naturales, aumento en la violencia sexual, racial y policial. Precarización del trabajo y obsolescencia de los modelos educativos, que ya no garantizan movilidad social. Despojo. Deterioro de la calidad de la oferta política, elevando a plataformas electorales a candidatos de un populismo anti derechos, que basan sus discursos en el odio y el miedo. Migraciones forzadas masivas, por crisis políticas y climáticas. Billonarios aumentando sus ganancias, mientras el resto de la población es más pobre y precaria. Militarización normalizada, violencia y criminalización para neutralizar la protesta social, con las tácticas represivas de ayer que sufrió la región, pero con la tecnología de hoy.

**Las brechas sociales no son únicamente más visibles, sino más grandes.** El descontento social no es nuevo, pero sí lo es la falta de correspondencia con las plataformas políticas, mediáticas y sociales. Hoy es un momento de reconocer esa desconexión entre clamor social y acción política, desde Quito hasta Santiago, desde Detroit hasta Managua.

Debemos encarar de inmediato este reto que la ciudadanía, sobre todo los más jóvenes, nos han presentado: las organizaciones de la sociedad civil, quienes están en cargos de representación política, los gobiernos, **y todos y todas quienes tenemos privilegios construidos sobre un sistema injusto tenemos que hacer una autocrítica profunda que se transforme en acciones concretas que realmente correspondan con el clamor popular.** La de Chile no es una crisis del transporte público, es una crisis de desigualdad que el poder ha tratado con indolencia y desidia. Y es un esquema que se repite a lo largo del continente.

Desde nuestro espacio, usando nuestra experiencia, creatividad y redes **vamos a hacer todo lo que esté en nuestro poder** para que, primero, no se normalice la militarización de la política: la historia y la evidencia nos muestra que **simplemente no funciona**, y termina dañando precisamente a aquellos que el sistema ya castiga.

En segundo lugar, nuestro compromiso está puesto en contribuir a que este despertar ciudadano motive espacios de diálogo reales, concretos, **en los que no seamos las y los mismos privilegiados de siempre quienes tengamos la palabra**, y partan por generar acuerdos en torno a la vergonzosa desigualdad económica, social, racial y de acceso al poder de Chile. La pobreza es la forma más brutal de violencia. Hoy nos toca escuchar y aprender de la ciudadanía con la que trabajamos, especialmente con aquellos y aquellas que no tienen vehículos políticos, como el voto, para participar. Todavía no sabemos cómo se articula un diálogo con un movimiento social tan distinto a los que hemos visto antes, pero sí sabemos una cosa: que juntas y juntos somos más fuertes, y sólo colaborativamente vamos a construir una región mejor.
