---
layout: post
title: 'Kleptocracy: The Other Crisis that Hits Latin America in the Midst of COVID-19'
intro: 'The health emergency has not only left thousands of deaths, but has also shown the worst face of corruption.'
day: 13
month: July
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Kleptocracy: The Other Crisis that Hits Latin America'
img: cleptocracia.png
caption:
---
During the month of May, Peru’s Public Prosecution Office identified that the in Amazon region, one of the most affected by the pandemic, public officials [irregularly acquired oxygen cylinders that, on top of being 20% pricier than market value, were empty](https://comprasestatales.org/ministerio-publico-investiga-presuntas-compras-sobrevaloradas-de-tanques-de-oxigeno-en-la-diresa-de-iquitos/).

Cases such as this one have happened all over Latin America since COVID-19 reached the region. The pandemic not only has evidenced the poor development conditions of countries all around the world, and the rawness of structural inequalities among Latin American citizenship. It has also showed a second crisis that was already hitting our countries: the naturalization of corruption inside the public function.

We are now under vasts quarantines (one of the most efficient and used contention  strategies), a measurement that sadly has a drawback, limiting economic activity which, in turn, increases poverty and impedes the possibility to attend basic necessities, particularly among those most vulnerable.


Because of that, countries such as Perú, Chile, Ecuador, México, Guatemala and Colombia have promoted actions that mobilize huge amounts of public funds, trying to weather the storm, but vulnerated citizens are not always the ones receiving their benefits.

**Ecuador**

In Ecuador, for example, there were several irregularities regarding the identification of vulnerable population, as disability certificates were awarded to congresspeople, judges, high performance athletes, and even the president of the Council for Citizen Participation and Social Control. All of them have no real disabilities. The Health Minister pointed out that over 3.000 illegal certificates were emitted ilegally during the emergency, [of which 2.281 are in the process of being voided.](https://www.elcomercio.com/actualidad/destitucion-beneficiarios-carnes-discapacidad-vehiculos.html)

In the same country, the Institute of Social Security allegedly bought face masks with a markup of 150%, and in May 2020 it was made public that Los Ceibos Hospital, in Guayaquil, acquired 4.000 body bags at USD 148 each, a price 12 times higher than market price, [for a total of approximately USD 592.000.](https://www.elcomercio.com/datos/coronavirus-fundas-cadaveres-hospitales-compras.html)

Corruption also impacted the construction of the Pedernales hospital, in the Manabí province (a city still suffering the consequences of the 2013 earthquake), as the winner of the open call for tenders was a company linked to assemblymember Daniel Mendoza, accused of leading an organized crime network.

**Peru**

In the case of Peru, one of the main public strategies to counteract the effects of the pandemic were direct money transfers. These were initially targeted towards poor sectors of the population, and progressively became universal. Despite the attractiveness of the measurement, their execution left a lot to be desired: the [recipients were not those most vulnerable, and several irregularities were detected in their assignment.](https://www.gob.pe/defensoria-del-pueblo)

In the same countries, local governments were granted special powers to allocate [budget to the delivery of basic household](https://ojo-publico.com/1748/covid-19-en-regiones-investigan-malos-manejos-en-entrega-de-canastas) provisions to ensure effective quarantine. In several cases expenses were not transparent, and food was expired, showing irregularities in acquisition processes.

**Chile**

A similar situation occurred in Chile, where the process of allocating help [has not been efficient](https://www.elmostrador.cl/dia/2020/06/12/es-un-escandalo-diputada-natalia-castillo-recibe-caja-de-alimentos-para-chile-y-anuncia-que-recurrira-a-contraloria/) because, among other causes, there have been issues identifying what sectors of the population are [actually vulnerable](https://www.biobiochile.cl/noticias/nacional/chile/2020/06/21/alimentos-chile-gobierno-se-enfoca-la-entrega-pendiente-cajas-regiones.shtml).

**Mexico**

As in the aforementioned countries, the sanitary emergencia has allowed for bigger flexibilities in the management of public budgets, which has also allowed for bigger flexibility in transparency standards. In Mexico, the President appointed a special task force to analyze possible deviation of resources, as well as an [intervention of the Financial Intelligence Unit to monitor the money being destined to states](https://www.animalpolitico.com/2020/04/amlo-operativo-desvio-insumos-covid-19/).

An investigation made by the [organization Mexicans Against Corruption and Impunity detected that the regional delegation of the Mexican Institute of Social Security (IMSS)](https://www.forbes.com.mx/corrupcion-y-volatilidad-en-tiempos-de-covid-19-en-mexico-mcci/) in the state of Hidalgo bought in May 20 ventilators to a company owned the son of the Federal Electricity Comission. The company, Cyber Robotic Solutions, sold their machines at a premium of 50%.

**Guatemala**

In Guatemala, the government sacked two vice-ministers of Health and presented reports against 8 public servants who conspired to commit fraud. The country is known for its alliance between the Executive, Legislative and Judicial powers that allows the perpetuation of a corruption and impunity system, guaranteeing privileges to mafias incrusted in these three powers. For this reason, there have been maneuvers to co opt the election of magistrates that can rig justice to favour authors of corruption, creating a severe risk of narcokleptocracy.

Cases such as this one, in which transparency and public accountability has been flexibilized or postponed, has allowed the waste of public resources. There is another common factor, too: the complicity of public actors, who conspire with private sector actors who hold economic power.

As in Guatemala, the connection between political, economic, and even judicial powers, makes these cases not just isolated corruption acts. They are strategies to rig the market, modifying and abusing the rules of the game: in other words, Kleptocracy, institutionalized corruption.

**Kleptocracy** as a way to manage political power cannot continue. Its impact is not just macroeconomic, but affects fundamental rights of people, being more evident during this pandemic. Because of this, it is vital to establish policies that include vulnerated populations integrally, with a transversal and intersectional approach, also promoting to backup citizens who want to denounce practices that go against the development of democratic states.  

We demand a direct fight against the entrenched kleptocracy in Latin American states!

* Whistleblower protection: Let’s end the fear of reporting and alerting abuses!

* Support for the media and journalists who carry out investigative tasks in the search for the truth.

* Promotion of campaigns regarding how corruption works and how it damages entire societies.

* Creation of protocols at the national and supranational levels so that certain communities have support for their particular needs, blocking the embezzlement of public funds.
