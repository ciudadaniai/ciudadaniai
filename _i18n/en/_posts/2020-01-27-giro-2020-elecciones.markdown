---
layout: post
title: 'A turn to build more participatory and representative elections in Brazil'
intro: 'Coming soon GIRO2020. Our initiative that will be part of the entire electoral process in Brazil'
day: 27
month: Ene
date: 2020-01-27 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Coming soon GIRO2020. Our initiative that will be part of the entire electoral process in Brazil.'
img: giro_blog.png
caption:
---
We of the [Intelligent Citizenship](https://ciudadaniai.org/en/) arrived in Brazil in 2017 with the mission to contribute in one of the most complex and challenging democracies in Latin America. From our office in Rio de Janeiro, we have denounced and concentrated our efforts on the events of recent years that are a source of concern for democratic degradation around the world. Political violence, abuse of power, restriction to freedom of expression and increased social inequality contaminate the political environment and weaken the impact of citizenship on political processes. Faced with this, the year 2020 marks the beginning of a new municipal electoral cycle in Brazil and with it a new opportunity to build the future we want for our cities.

That is the reason why we created **GIRO 2020**, a proposal of [Intelligent Citizenship](https://ciudadaniai.org/en/) in partnership with [Casa Fluminense](https://casafluminense.org.br/a-casa-en/) to make elections more participatory and representative, with a territorialized public policy agenda aligned with the needs of citizenship. Far beyond the vote, our twist is a journey that begins in the electoral year, will cross the polling day and then move onwards after the results of the ballot, keeping an eye on the governing of our cities.  

In February we will launch a series of meetings with civil society in Rio de Janeiro where we will talk about the recent changes in our country and the challenges we must face to strengthen spaces for engagement and monitoring networks in our cities. With the learning from this movement and in dialogue with over 100 organizations and collectives we will consolidate an agenda with concrete proposals for the main challenges faced by the cities of Rio de Janeiro.

Committed to increasing the incidence of citizenship demands in the electoral and post-election period, we will carry out training processes with a further 100 candidacies, in addition to building a collective process of territorial mobilization and proposal monitoring focused on a trained network of 50 territorial leaders.

With **GIRO 202** we invite citizenship to remain united and strong, to reverse setbacks of rights and dispute more dignified lives in our cities. Why yes, we do not give up.
