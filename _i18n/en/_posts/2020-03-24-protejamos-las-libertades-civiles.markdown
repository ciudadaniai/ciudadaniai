---
layout: post
title: 'Protecting Civil Liberties During a Public Health Crisis'
intro: 'Five principles to take care of our data in times of health crisis.'
day: 24
month: Mar
date: 2020-01-27 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Protecting Civil Liberties During a Public Health Crisis.'
img: datacorona.jpg
caption:
---
*Information replicated from the [Manifesto](https://www.eff.org/deeplinks/2020/03/protecting-civil-liberties-during-public-health-crisis) of [Eletronic Frontier Foundation](https://www.eff.org/) written by Matthew Guariglia and Adam Schwartz.*

Across the world, public health authorities are working to contain the spread of COVID-19 (Coronavirus Disease 2019). In pursuit of this urgent and necessary task, many government agencies are collecting and analyzing personal information about large numbers of identifiable people, including their health, travel, and personal relationships. As our society struggles with how best to minimize the spread of this disease, we must carefully consider the way that “big data” containment tools impact our digital liberties.

Special efforts by public health agencies to combat the spread of COVID-19 are warranted. In the digital world as in the physical world, public policy must reflect a balance between collective good and civil liberties in order to protect the health and safety of our society from communicable disease outbreaks. It is important, however, that any extraordinary measures used to manage a specific crisis must not become permanent fixtures in the landscape of government intrusions into daily life. There is historical precedent for life-saving programs such as these, and their intrusions on digital liberties, to outlive their urgency.

Thus, any data collection and digital monitoring of potential carriers of COVID-19 should take into consideration and commit to these principles:

* **Privacy intrusions must be necessary and proportionate.** A program that collects, en masse, identifiable information about people must be scientifically justified and deemed necessary by public health experts for the purpose of containment. And that data processing must be proportionate to the need. For example, maintenance of 10 years of travel history of all people would not be proportionate to the need to contain a disease like COVID-19, which has a two-week incubation period.

* **Data collection based on science, not bias.** Given the global scope of communicable diseases, there is historical precedent for improper government containment efforts driven by bias based on nationality, ethnicity, religion, and race—rather than facts about a particular individual’s actual likelihood of contracting the virus, such as their travel history or contact with potentially infected people. Today, we must ensure that any automated data systems used to contain COVID-19 do not erroneously identify members of specific demographic groups as particularly susceptible to infection.

* **Expiration.** As in other major emergencies in the past, there is a hazard that the data surveillance infrastructure we build to contain COVID-19 may long outlive the crisis it was intended to address. The government and its corporate cooperators must roll back any invasive programs created in the name of public health after crisis has been contained.

* **Transparency**. Any government use of "big data" to track virus spread must be clearly and quickly explained to the public. This includes publication of detailed information about the information being gathered, the retention period for the information, the tools used to process that information, the ways these tools guide public health decisions, and whether these tools have had any positive or negative outcomes.

* **Due Process.** If the government seeks to limit a person’s rights based on this "big data" surveillance (for example, to quarantine them based on the system’s conclusions about their relationships or travel), then the person must have the opportunity to timely and fairly challenge these conclusions and limits.
