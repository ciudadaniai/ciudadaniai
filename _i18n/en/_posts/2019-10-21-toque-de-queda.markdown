---
layout: post
title: 'Chile: Toque de queda y militares en las calles, no más, nunca más.'
intro: 'En Ciudadanía Inteligente analizamos por qué la militarización no es la solución.'
day: 21
month: Oct
date: 2019-10-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'En Ciudadanía Inteligente analizamos por qué la militarización no es la solución.'
img: 21-10-19.png
caption:
---

***El Gobierno civil y democráticamente electo del Presidente Sebastián Piñera decidió recurrir a leyes excepcionales y a las Fuerzas Armadas para contener el descontento de la ciudadanía. El despliegue de miles de militares y la suspensión de derechos civiles y políticos en Chile, en las últimas cuarenta y ocho horas, es el golpe más duro a la democracia desde la época de la dictadura, perpetrado por las élites políticas. Desde ayer, el país opera bajo una Ley sancionada en tiempos del General Augusto Pinochet. Hoy domingo, ha decretado un toque de queda aún más severo para la Región Metropolitana.***

En un país en tiempos de paz, [con los más bajos índices de violencia en la región](https://www.eleconomistaamerica.cl/sociedad-eAm-chile/noticias/9015481/03/18/Chile-uno-de-los-paises-mas-seguros-de-America.html), cerraremos el fin de semana con más de nueve mil soldados patrullando las calles, con Estado de Emergencia declarado en la mayoría del país, y después de un traumático “toque de queda” que limitó la libertad de locomoción y la libertad de reunión de millones de ciudadanos y ciudadanas. Dicha suspensión de derechos se se renueva esta noche.

El despliegue en los barrios, en las áreas urbanas, frente a familias, de vehículos diseñados para batalla, para conflictos bélicos, ha generado temor y repite traumas de la dictadura, a aquellos que lucharon duramente por memoria, verdad, justicia y un retorno a la democracia. En un país que no ha curado aún las heridas de graves delitos de lesa humanidad, las decisiones del gobierno golpean a muchos más que las balas de goma con las que se atacó a protestantes, que desafiaban políticas de austeridad.

A los más jóvenes, los niños, niñas y adolescentes que salieron por primera vez a ejercer su derecho a la protesta, el Estado les devuelve un mensaje de represión y violencia. La jornada dejó una larga lista de menores golpeados y vulnerados por las autoridades que deberían protegerlos, asustados al ver armas automáticas letales apuntadas hacia ellos y ellas, y sin tener claridad a quién recurrir cuando es el mismo Estado que los y las vulnera.

##Gobernar con la gente, para la gente, no protegerse de la gente##

La línea que se cruzó no puede volverse, bajo circunstancia alguna, la nueva normalidad para la gestión de la población que se opone a políticas estatales. Se ha impedido a la ciudadanía el ejercicio de su derecho de reunión y manifestación por medio de un Estado de Emergencia y se ha expuesto a la población a riesgo de ser víctimas de mayor violencia y represión, al desplegar personal militar que no está preparados para tareas de seguridad ciudadana: ni su formación, ni las armas que portan, ni su normativa en materia de rendición de cuentas son adecuadas.

El Gobierno del Presidente Piñera, además, ha asumido importantes compromisos internacionales, al ser la sede de APEC y COP25. Parte de las demandas que elevamos es precisamente abstenerse a recurrir al Ejército de Chile, y a leyes de aplicación extraordinaria en una democracia, para garantizar el funcionamiento de dichos eventos. Una vez el gobierno abdica su deber de ofrecer seguridad interior a la ciudadanía, no solamente ésta queda más expuesta a abusos y a mayor violencia, el poder civil se debilita.

La tarea del gobierno inmediata, además de admitir sus errores, es rectificarlos. El país carece de  mecanismos y protocolos efectivos para escuchar las demandas de la población, especialmente las de los más vulnerables, y arreglarlo requiere de voluntad política pero también de recursos. De tiempo y dedicación de la más alta jerarquía de gobierno. Se debe diseñar colaborativamente estrategias de manejo de conflictos sociales adecuadas, que activen a distintas instancias públicas, privadas y sociales, incluyendo los medios, los centros educativos y las organizaciones de la sociedad civil organizada, para actuar a tiempo y evitar escalamientos de conflictividad social.

Al calificar la intervención militar como un error, los datos nos respaldan.  La reciente intervención de las fuerzas armadas en Brasil para tareas de seguridad ciudadana, en la ciudad de Río de Janeiro en 2018, ejemplifica la ineficacia de este tipo de medidas: en 10 meses de intervención, [la ciudad registró un aumento del 59% en tiroteos, más de 4.000 homicidios y  40% más de muertes por intervención policial  que en el año anterior](http://www.observatoriodaintervencao.com.br/dados/relatorios1/).

El gobierno debe disculparse públicamente con la ciudadanía por las decisiones recientes, que derivan de su falta de diligencia, celeridad, coordinación y agilidad para resolver las crisis sociales. La ciudadanía necesita líderes que la escuchen y sirvan a propósitos comunes.

El lugar y rol de los militares en una sociedad de paz no está limitando la protesta, ganando poder y protagonismo político, y reviviendo traumas históricos no superados ayer. Es imperativo que se reviertan de forma inmediata las medidas dictadas y que el gobierno suspenda todos los estados de excepción activados, y resuelva la situación de descontento social con los mecanismos democráticos a su alcance, no sin antes dar exhaustiva cuenta de las decisiones tomadas en los últimos días, así como sus consecuencias, aún la pérdida de vidas humanas.

La normalidad deseada es aquella en la que la fuerza pública obedece órdenes civiles y se guía por la protección a las personas, donde se activan oportunamente mecanismos de diálogo vinculante y de intercambio con la ciudadanía. Queremos ser la democracia ejemplar e inclusiva del futuro, rechazamos convertirnos en la sociedad que no aprendió, que repitió el pasado.
