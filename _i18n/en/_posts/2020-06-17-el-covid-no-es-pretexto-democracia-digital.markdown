---
layout: post
title: 'COVID is not an excuse to threaten freedom on the Internet'
intro: '8 proposals to configure post-pandemic Internet.'
day: 17
month: junio
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'COVID is not an excuse to threaten freedom on the Internet'
img: en-blog.png
caption:
---
Latin America is going through a period of uncertainty marked by deep social discontent and an accelerated and opaque rush of social and political changes driven by the COVID-19 health emergency. In 2019, countries such as Chile, Colombia or Mexico were experiencing various social protests demanding improvements and expansion of rights. Shared injustices threw people onto the streets to advocate for a transformation of the social order and solutions to structural problems such as corruption and violence.

But in 2020 the citizen action to which we were accustomed simply stopped being possible. Although we have seen that protests against racism and police abuse have not ceased the concentration of people in Mexico, Brazil or outside the region, the health emergency has shortened citizens' rights and increased the powers of the rulers to monitor, control and, in many cases, legislate by decree. National and local governments are adopting new technologies and data management systems to cope with the public health emergency and general economic trauma, increasing reliance on data-based systems to track disease and profile the population. COVID-19 expansion rhythms not only delimited collective action and the gathering of people on the streets, but also confined citizenship to the digital space, a largely private, surveilled and opaque domain.

These changes are not [temporary](https://www.technologyreview.com/2020/03/17/905264/coronavirus-pandemic-social-distancing-18-months/) nor [inconsequential](https://www.nesta.org.uk/blog/there-will-be-no-back-normal/). They radically change the way we understand the effective exercise of human and political rights. Perhaps it is our daily life that is already beginning to experience this new normality. We have resorted to virtuality for work, school, activism, and daily relationships. But with the increasing digitization of social interactions, civil society is questioning the false dilemma many governments are leading us to: that of health or privacy.

This occurs due to the undemocratic way in which social relations have been configured in the digital sphere. Therefore, controlling and facing the health crisis through technology has accentuated inequalities and has presented us with urgent challenges to reconfigure a digital space that truly serves democracy. One of them is recognizing the Internet as a public space that we inhabit and that allows us to exercise rights and freedoms: such as the freedoms of expression, assembly, and privacy. Questioning whether the Internet is currently a democratic space and identifying what conditions endanger our democratic life in digital spaces are other not minor challenges.

If we aim for a new normality, what structural changes do we need to extend to the digital environment? What discourses, ideas and conceptions about technology hinder or threaten an **Internet of rights and freedoms that allows collective action and citizen participation?**

There is no more urgent time to **reconfigure the prevailing narratives or discourses on the digital and democracy** and propose alternatives that point in another direction. However, the participation of civil society in the debates on digital rights is still limited to a few organizations, so it is vital to add diverse voices, from environmentalism, gender struggles, the defense of human rights, and many others.

These are some of the narratives that we see are being reproduced with the current health crisis, and to which we bring forward democratic, alternative visions.

* Today the Internet is seen as "a non-real world" (e.g. the real world and the digital world).

***We propose to recognize the internet as a public space that we share and inhabit. The network is also a real space, with tangible impacts on our lives.***

* The main use of the Internet and digital technologies has gone to businesses and political propaganda. This has opened the door to increased government surveillance and censorship of dissident voices. Currently, digital tracking facilitated by widely adopted systems is used by authoritarian governments to exercise control and repression.

***Let us defend the Internet as a common good, as a space to affirm and defend rights and collective action. Let us defend digital technologies as tools that support processes of social change, organization, meeting and participation. Let's defend an Internet that strengthens democracy!***

* A few major players have a monopoly on how the networks and systems we use work, how we interact, what we see, and what content is privileged.

***But we all create content and socio-digital relationships, identities and new ways of thinking-doing through these systems. We are not just users, a good part of our democratic life happens right there. It is therefore necessary to make the management of these systems more democratic and open.***

* Digital technologies are seen as a solution to all kinds of problems.

***However, it is key to submit the implementation of new technologies to democratic deliberation. What elements do they involve? How do they work? What are the related risks? Who gets benefited and who gets hurt? How do they become more beneficial? Are they truly necessary?***

* Surveillance seems to be justified on the grounds that "I have nothing to hide."

***We believe that democracy and privacy are closely related. A healthy democracy depends on the interaction of autonomous individuals. Individual autonomy depends on having privacy. Defending the Internet as a space where privacy matters benefits the entire society.***

* Currently there is a discourse of "savagery of the Internet" where violence is normalized and its impact is minimized.

***It is important not to normalize digital violence as part of Internet interactions, recognizing that it has real impacts on people's lives. Digital violence is an extension of “analogous” violence, which is evident and is carried out through technology.***

* Digital is seen as an unlimited world in which everything goes viral and reproduces infinitely.

***We must be aware that digital has a physical and social impact. Visits, likes, reproductions, and interactions imply more energy consumption, hardware waste generation and a greater deployment of infrastructures such as marine cables and data centers. For example, it has been [estimated](https://theshiftproject.org/article/climat-insoutenable-usage-video/) that, in 2020, only streaming video would emit as much CO2 as Spain.***


* In a health emergency context, technology has [posed](https://www.accessnow.org/tecnologias-de-vigilancia-para-controlar-el-covid-19-en-america-latina/) a dichotomy between health and privacy.

***Technology can support pandemic expansion control while respecting rights and freedoms. All initiatives based on technology and management of personal and health databases must be transparent, respectful of rights and comply with measures that protect our information. The COVID-19 emergency situation we face is not an excuse to collect unnecessary data, or to not improve the protection practices and ethical development of technology for the public interest. Short-term efficiency should not undermine human rights.***

Here you can check the [video](https://www.youtube.com/watch?v=FT-lQA4DRQc)

**Authors**


*Ricardo Zapata - El Derecho a No Obedecer (Colombia)*

*Beatriz Quesadas - SocialTIC (México)*

*Haydeé Quijano - SocialTIC (México)*

*Daniela Jordán - Frena la Curva (Chile)*
