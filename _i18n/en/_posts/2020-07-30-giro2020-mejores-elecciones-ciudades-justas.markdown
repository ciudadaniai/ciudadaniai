---
layout: post
title: 'GIRO2020: A change to build better elections and fairer cities'
intro: 'GIRO2020 is ready! Apply to the political formation of municipal candidates of Rio de Janeiro.'
day: 30
month: July
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Check our project Giro2020'
img: giro2020.png
caption:
---
Rio de Janeiro is one of the 10 most unequal metropolises in the world. The Metropolitan Region, with 13 million inhabitants distributed in 22 cities, has some of the worst indicators of human and social development in Brazil.The electoral context of 2020 brings many concerns and challenges, especially for social leaders, civil society and activist movements that have been mobilizing politics in their territories, trying to advance in overcoming these historical inequalities.

Political tensions and the insecurity of upcoming elections demand answers and solutions to contain the weakness of local democratic processes. At a time when citizenship is experiencing a low level of confidence in public management, it is necessary to prosper in the face of uncertainties.
The COVID-19 global humanitarian public health crisis we are facing intensifies this scenario, demonstrating the urgency of readjusting our priorities for the advancement of proactive agendas that prioritize social justice and the right to life, overcoming structural and emergency urban inequalities.

In recent years, we have observed cities, neighborhoods and territories suffer from neglect and decadence due to actions taken by elected representatives. In addition, we experience the increasing reduction and even total exclusion of citizen participation and protagonism in the construction of public policy agendas. The crisis we are currently experiencing, both in politics and health, is also a consequence of the historic abandonment of cities, their citizens, and their collective priorities.

The elections are a decisive moment in the creation of our future. The construction of a new possible future for society also depends on who we align ourselves with in the defense of our agendas and priorities.

To overcome this moment without making the same mistakes of the past, we need to bring to power those who are committed to the ones who live and act in the daily life of urban challenges. In this context of institutional fragility, political polarization and disfavor, of wear and tear of the social fabric, we observe that an imbalance in the electoral dispute is approaching.

For this reason, we have launched the [GIRO2020 Political Formation](https://giro2020.org/), a civil society initiative that aims to strengthen democracy before, during and after the elections in the Metropolitan Region of Rio de Janeiro, by delivering content, strategies and tools to support diverse candidacies.
The [Giro 2020](https://giro2020.org/) Training, an initiative resulting from the partnership between the [Intelligent Citizenship Foundation](https://ciudadaniai.org/en/) and [Casa Fluminense](https://casafluminense.org.br/a-casa-en/), is a training cycle that includes public policy content, trends on electoral challenges and political imagery, communication tools, and funding aimed at electoral campaigns.

The training program is free of charge and will be conducted in a closed learning environment within the GIRO2020 platform, with a virtual debate space.

The course material is based on two main content pillars:

* A detailed propositional agenda built on a diagnosis of data and public policy proposals for the Rio de Janeiro Metropolitan Region, based on the Rio Agenda and the Inequality Map organized by Casa Fluminense.

* A set of tools and strategies on trends of the 2020  electoral process, addressing the challenges to be faced by candidates during the campaign.

Considering the effects of the COVID-19 pandemic and the new arena of electoral dispute in our cities, GIRO2020 prioritizes strengthening candidacies which are aligned with our mission and values from a social-political perspective. For this reason, our training is focused on municipal pre-candidacies in the metropolitan region of Rio de Janeiro, preferably those of women, blacks, youth, lgbt+ in all its intersections and diversities, and people who are active in the debate to reduce inequalities and increase access to social rights.

We endorse and support proposals for advancement based on social participation, transparency, respect and appreciation for diversities, and public commitment to reducing inequalities and increasing opportunities as a way to foster a new political scenario in Brazilian society.

**We need to change the priorities and the protagonists in the politics of our cities!**
