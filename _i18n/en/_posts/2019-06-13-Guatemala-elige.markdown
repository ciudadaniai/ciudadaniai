---
layout: post
title: 'Guatemala chooses without political offer'
intro: 'Next weekend, the largest economy in Central America, Guatemala, holds general elections.'
day: 13
month: Jun
date: 2019-06-13
categories: News
highlighted: false
share_message: "Guatemala elige entre la estabilidad pactada y el colapso del Estado por @ciudadaniai."
img: 13-06-19.jpg
single_image:
  -
    caption: Carolina Saadeh
---
***Column written by Renata Ávila, executive director of Ciudadanía Inteligente. Published on [3500 millones, blog de El País](https://elpais.com/elpais/2019/06/13/3500_millones/1560381888_387141.html).***

Next weekend, the largest economy in Central America, Guatemala, holds general elections, defining the course of its sixteen million inhabitants. The candidacy menu promises little: there are no notable players and the polls offer a possible narcotic pax and stability agreement between corrupt groups, an escalation in state violence, social cleansing, and auction of the few remaining public services left to privatize.

There is no viable political bid that offers solutions to the two main problems today: extreme poverty and racism, and the capture of the state by parallel power groups.

The election is held in a tense political context at the regional level: the North American countries, by militarizing the only route of escape from hunger, violence, and misery suffered by Central Americans and deploying troops to the south of Mexico, are converting the country into the "Triangle North" in a Gaza-like strip of Latin America. Weapons for the war against drugs are used today for war against the poor, who prefer to risk dying in a desert to living like that.

It is the country with the most children dying in detention centers on the border to the north, and for every child that has died in captivity at the hands of ICE, many more died of hunger, a death as violent as that of a bullet. The extreme poverty inflicted on the indigenous population, especially children, is the most brutal form of violence, a scenario that will be exacerbated by the looming climate change crisis, with the country listed among the ten most vulnerable and without a plan.

Today, remittances are the only source of growth and social mobility for the poor. The figures of poverty are nothing more than evidence of an apartheid, a racism that cuts across all layers of the state. 79% of the poor are indigenous. In rural areas, 8 out of 10 people are in extreme poverty. Nearly 40% of indigenous women over 15 years of age do not know how to read and write, 58% of indigenous children under the age of five suffer from chronic malnutrition, and more than half of indigenous children will drop out of school before completing the years they would need to break free from the cycle of poverty (Source of Data: [World Bank](https://data.worldbank.org/?locations=GT-XN), [National Survey of Living Conditions 2014](http://www.ine.gob.gt/index.php/encuestas-de-hogares-y-personas/condiciones-de-vida) and of [the Human Development Report of UNDP in Guatemala](http://desarrollohumano.org.gt/), [UNESCO Database](http://uis.unesco.org/country/GT)). The response of the state is violence and dispossession: just in the past year, 2018, the state killed 26 indigenous leaders defending their territory. None of the electoral leaders represents them.

**CICIG, destroyed hope and its last chance**

To reverse the cycle of violence, poverty, and misery, it is essential to restore confidence in the institutions of the State, a task in which an experiment in Guatemala has exceeded the results in the Latin American region- the International Commission Against Impunity (CICIG in Spanish). Its investigators were able to bring charges against more than 200 officials, including two recent ex-presidents and several ministers, former police chiefs, high-ranking military officers and judges. This was achieved by strengthening the capacities of prosecutors, judges, and investigators, who presented more than a hundred cases, and promoted more than 34 legal reforms of institutional strengthening.

The Commission's impeccable work was restoring much-needed credibility in the institutions for Guatemalans. That is, until it touched the president who is currently in power, comedian Jimmy Morales, his brother, his son, and powerful interests in the Guatemalan Army. As a defensive strategy since 2017, the group of people affected by CICIG carried out a concerted effort to expel the Commission. The head of commission, Colombian Iván Velásquez, was declared persona *non grata* and can not enter the country, judges of the highest courts of Guatemala who have supported the Commission, are under threat, and most of the candidates have indicated that the mandate of the supporting body will not be renewed once it expires this September 2019.

The business elites and the country's most powerful families backed the president, who did not rely solely on them: he was flanked by the military, also under investigation, while disobeying express orders from the highest court in the country. Meanwhile, the Trump administration issued ambiguous comments and a weak condemnation of the actions of a president who seeks to stop the scrutiny of their actions. It is perhaps much more convenient for that government to have problematic countries run by fragile and vulnerable leaders, who offer absolute obedience in exchange for protection and immunity.

The last hope of saving CICIG, and thereby offering a possible future for millions, is at the polls. The president and congress which is elected on June 16 will define the future of the most effective model ever attempted in the region for institutional rescue, choosing to rescue or bury it altogether. But without an institutional rescue and a national project, the exodus to the North will only increase.
