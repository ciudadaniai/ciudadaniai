---
layout: post
title: 'Democracy is strengthened in practice'
intro: 'Chile experienced a historic moment: it’s citizens achieve the impossible to implement a real democracy.'
day: 15
month: Nov
date: 2019-11-15 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Democracy is strengthened in practice'
img: 15-11-19-en.png
caption: Photo by Carlos Figueroa
---

It began as a student protest in response to a metro fare increase. On Friday October 18th, 2019, in the face of a massive call for fare evasion by students and increasingly by citizens, things changed for Chile to a point of no return. The government reacted as it unfortunately tends to do: repressing protests, criminalizing those who exercised their right to protest, without the capacity to distinguish between those who demonstrated peacefully versus isolated acts of violence. Feeling a loss of control over the situation, and turning to “iron fist”-type catchphrases like “necessary restoration of order at all costs,” President Sebastián Piñera decided, in a way that hasn’t been done since the dictatorship, to use [violence and force](https://ciudadaniai.org/crisis)  as a solution to what is a [political problem](https://ciudadaniai.org/chile).

Four weeks have passed since then. 28 days of hope, solidarity, assembly, massivity, creativity, mobilization, insistence and resilience. And on a historic day, Friday October 25th, more than 1.5 million people gathered in the iconic Plaza Italia- which many are calling to be renamed as Plaza Dignidad (dignity) in light of the recent events and their historic transcendence-  as well in other regions in the country, in a national, peaceful, massive, family-friendly demonstration with one objective: to create a more just Chile, together.

However, these have also been weeks of profound darkness, fear, hopelessness, anguish, and uncertainty. We saw military forces deployed to the streets. We lived under the rigor of curfew for six days. For many, dark memories from the times of the dictatorship resurfaced. Many experienced for the first time what is lived in territories and municipalities that today do not enjoy the right to live in peace. Systemic violence against citizens was enforced at the hands of the State. The cost was high: hundreds of people were arrested, wounded, raped, tortured, and mutilated. 22 people lost their lives. A price too high to pay to sustain a clear and transversal demand: the current system does NOT work. It is unjust and it gives us a profoundly unequal and violent system, lacking dignity, where true democracy is impossible. 

The Executive and the political class took a long time to do what they are called to do: govern and solve problems from the politic, the real politic. The level of delegitimization and mistrust is already very high. Today, the government has a 15% approval rating. After lengthy negotiations, on the day that marks a year since the police-led assassination of the indigenous Mapuche activist Camilo Catrillanca, a possible agreement was reached. It was clear that citizens would not cease their demands, that violence as a political response does not work, and that it only attracts more violence, in an unprecedented escalation that left landmarks and commercial and residential areas destroyed throughout the country. 

Last night, almost in the early morning, we found out the outcome of these negotiations: an “Agreement for Social Peace and the New Constitution”. A historic accord, but above all, a historic achievement by citizens. At Ciudadanía Inteligente, we consider it essential for the process that has been initiated to be based on a framework of unrestricted respect for the fundamental rights and guarantees of people. This means ensuring that human rights violations in the past four weeks do not go unpunished. Only on this foundation of reparation can we begin to build a truly democratic process. 

How does this agreement work? First with a plebiscite that could lead to a constitutional change. This change can happen through two mechanisms:

a) Constituent Convention (equivalent to a Constituent Assembly), where citizens elect representatives who write a new Constitution and once that is complete, the convention is dissolved. 

b) Constituent Congress, composed of 50% citizens and 50% members of the current National Congress.

Decisions will be subject to a quorum of 2/3 (66.6%). If there is no agreement on a section, it will NOT be reverted to the 1980 Constitution. It will simply not be a matter of the new constitution but rather move to congress in the form of a proposed law. The process will culminate with a ratification plebiscite where the vote will be mandatory. 

The constituent referendum (to define the structure of the group of people who will write the new constitution) will take place in April 2020. That same year, in October, and along with municipal elections, there will be elections for delegates. Measures have not yet been established to make the constituent Convention or Congress truly inclusive and representative in its formation, i.e. to establish quotas for women, sexual minorities, indigenous peoples, among many others. The constituent body shall meet for nine months, and may be extended only once for three more months. 

Today more than ever, no one can "go home." There are millions of questions about how the process that has been initiated will be implemented. It is here that the role of citizens and civil society will be essential to monitor, participate, demand accountability, educate and mobilize. As an organization we will make all our tools available to support from our experience a process that can begin to transform Chile into a more just, inclusive, and sustainable country, into a true democracy for all.
