---
layout: post
title: 'ABRE Alcaldías: una estrategia para abrir las ciudades de América Latina'
intro: 'Lanzamos nuevo proyecto que busca poner a la ciudadanía en el centro de las decisiones'
day: 9
month: Dic
date: 2019-12-09 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'ABRE Alcaldías: una estrategia para abrir las ciudades de América Latina'
img: 9-12-19.jpg
caption:
---

En [Ciudadanía Inteligente](https://ciudadaniai.org/index) estamos comprometidxs con fortalecer ciudades inteligentes que utilicen la tecnología como un instrumento catalizador para los Derechos Humanos, la sostenibilidad y la justicia social. Con este propósito, buscamos crear una red de ciudades latinoamericanas que desarrollen políticas digitales que ubiquen a la ciudadanía en el centro de las decisiones y que promuevan gobiernos más abiertos, transparentes y participativos.

Creemos que el compromiso de las ciudades con la innovación, respetando  los derechos digitales y estándares éticos, es fundamental para el fortalecimiento de las democracias en Latinoamérica. Con este objetivo, hemos creado **ABRE ALCALDÍAS**. Un proyecto que entrega capacidades tecnológicas a los municipios para que puedan innovar en la creación de políticas públicas por medio de una participación ciudadana efectiva.

Esta iniciativa comenzará en enero del 2020 donde se impartirán escuelas formativas en 4 países de la región (El Salvador, Ecuador, Guatemala y México) y que formaran a 40 equipos municipales para que estos  puedan co-crear políticas públicas junto a la ciudadanía.

Para la ejecución de ABRE Alcaldías es indespensable la participación de la sociedad civil como agentes de articulación y por su capacidad de fortalecer el canal entre las autoridades con la ciudadanía. Es por esto que invitamos a todas las organizaciones locales de la sociedad civil a ser parte del proyecto y **empujar en conjunto gobiernos más abiertos, más democráticos, más transparenre y más inclusivos ¡No te quedes fuera!**

Revisa [acá](https://drive.google.com/file/d/1cmmY7jxeQFDuyfnWpjYD-o1M8AK9q750/view) todos los detalles de la postulación.
