---
layout: post
title: 'We have a new Executive Director!'
intro: 'The lawyer Octavio Del Favero comes to lead our organization.'
day: 08
month: ene
date: 2020-03-21 12:00:00 -0300)
categories: Invitación
highlighted: false
share_message: 'We have a new executive director'
img: octavioblog.png
caption:
---
We have good news! A new Executive Director joins our organization. The Chilean lawyer, **Octavio Del Favero**, comes to lead our organization with a vision that combines fundamental rights, citizen participation, and innovation to strengthen the democracies of Latin America’.

Octavio Del Favero is a lawyer from the University of Chile and MSc in Comparative Politics from the London School Economics, with vast experience working in civil society and the public sector. He worked for four years in Ciudadanía Inteligente, leading our legislative advocacy in Chile, contributing to reforms against corruption, promoting transparency, and democratizing political parties. He also coordinated our regional advocacy strategy working with activists and leading networks of Latin American organizations.

We know that our democracies face significant challenges because of long-term economic and social inequalities and dysfunctional political institutions. However, we are optimistic about the enormous power of change that the mobilizations and citizen organizations have shown at crucial moments for the region. Our task will be to promote profound changes in our democracies, giving citizens the power to participate in decisions that affect them and build societies based on the rights and the dignity of all individuals and groups.

A new cycle begins today for FCI. We are convinced that this new leadership will join our fantastic team to work for the challenges Latin America currently faces. In this new period, we will continue working as active agents in historical processes with new initiatives and putting all our experience at the service of greater citizen participation, accountability, and inclusion. Our goal is for everyone's voice to be heard.

We warmly welcome our new executive director, and we look forward to continuing working for better democracies together.
