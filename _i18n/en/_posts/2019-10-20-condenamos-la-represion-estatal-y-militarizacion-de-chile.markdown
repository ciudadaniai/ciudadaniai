---
layout: post
title: 'Ciudadanía Inteligente condemns State repression and rejects the militarization of Chile.'
intro: 'Ciudadanía Inteligente condemns State repression and rejects the militarization of Chile.'
day: 20
month: Oct
date: 2019-10-20 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Ciudadanía Inteligente condemns State repression and rejects the militarization of Chile.'
img: 20-10-19.jpg
caption:
---

***The Chilean government declared a State of Emergency on October 19th, 2019. Although it initially applied to only one region of the country, it quickly extended to three. In the face of this State of Emergency, [Ciudadanía Inteligente](https://ciudadaniai.org/) condemns state repression, and the deployment of military force. It is also calling for the government to revoke the suspension of rights and guarantees, restore the civil command and control of the country, and remove the armed forces from the streets to start a dialogue in response to the citizens’ demands.***

Today, Chile is experiencing the second day of the State of Emergency in six regions of the country and its extension is not out of the question, nor is the implementation of a curfew outside the realm of possibility. The Chilean Government’s Minister of the Interior announced that the armed forces would be given more authority to restrain citizens’ freedoms. They made this announcement without offering a concrete plan, and without referring to the request for dialogue announced by President Sebastián Piñera yesterday afternoon.

In this sense, Ciudadanía Inteligente, as a representative of Chile’s civil society, demands that the government invest in effective mechanisms and protocols in order to listen to the demands of the population. Furthermore, the organization believes that the government should assign the necessary resources to design strategies for managing social conflicts and avoiding their escalation, as well as strategies for involving multiple governmental sectors to resolve upcoming problems.

Similarly, the organization demands the activation of different public, private, and social requests, including the media, educational institutions, and organized civil society organizations in order to act in a timely manner and avoid escalation of social conflicts.

Renata Ávila, the Executive Director of Cuidadanía Inteligente, pointed out that “Cuidadanía Inteligente has sadly observed the errors made by Sebastián Piñera’s government and his Cabinet of Ministers, which Chilean citizens are no longer tolerating. Once the citizens are silenced and confined to their houses by a curfew enforced by the Armed Forces patrolling the city, the little dignity that the government had left will be profoundly diminished.” Additionally, the Director added that “the military deployment to contain and silence citizen protests in one of the safest countries in the world is a mistake that should be immediately admitted and revoked. It’s state violence, which brings with it international responsibility.”

The root of the demonstrations that have been happening over the past five days in Chile have is the citizens’ massive discontent for the social injustices and inequalities in healthcare, education, pensions, job opportunities, transportation, among many others. The rise of the cost of metro tickets was the catalyst for this societal unrest, but the citizens’ demands are much more profound and demand immediate governmental action.
