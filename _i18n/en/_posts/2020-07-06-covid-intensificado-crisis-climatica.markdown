---
layout: post
title: 'COVID has intensified the climate crisis'
intro: 'We propose 8 actions to mitigate the climate crisis, exacerbated by the pandemic.'
day: 06
month: Jul
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'COVID has intensified the climate crisis'
img: crisis_climatica.png
caption:
---
Before COVID-19, Latin America and the entire world were already facing a crisis; the climate crisis. It affects not only biodiversity, land, flora, and fauna, but also citizens. Today the inequalities and struggles that environmental activists and defenders have tried to address, have intensified by the pandemic, and many remain unanswered.

The effects of the overexploitation of our resources in one of the most unequal regions in the world are also uneven. In Latin America, about [37 million people do not have access to drinking water, and only 37% of the region has access to safe sanitation](https://www.elmostrador.cl/agenda-pais/2020/04/04/millones-en-america-latina-tienen-que-combatir-el-coronavirus-sin-agua-potable/).

Respiratory diseases associated with environmental pollutants and greenhouse gases are more prevalent in marginalized and impoverished sectors, causing nearly [7 million deaths annually](https://www.terram.cl/2020/04/el-impacto-del-desarrollo-sostenible-en-la-crisis-por-covid-19-y-viceversa/) in the world. Also, [significant natural disasters, such as droughts, floods, low temperatures, fires, and hurricanes, have bigger effects on groups that live and generate income directly from the land](https://blog.oxfamintermon.org/afecta-el-cambio-climatico-a-la-desigualdad-social/), such as indigenous communities and rural sectors.

Citizens are not the only ones affected by COVID-19. The explosive increase in the use of plastic material for sanitary reasons is also alarming, especially considering that before the pandemic, nearly 8 million tons of plastic per year reached the ocean. It will take between 450 and 1,000 years before the material es degraded.

An essential part of this plastic explosion is directly related to the care and contact with infected patients, such as plates, glasses, and plastic utensils. In this sense, research indicates that [the virus can survive(https://www.bbc.com/mundo/noticias-51955233) up to three days on plastic surfaces and the easiest way to destroy it is to disinfecting, instead of throwing plastic away and risking contamination.

All of this is alarming, but at the same time, it presents an opportunity to create a new society. One that attends the needs of our territories and nature, and internalizes that a sustainable and respectful relationship with the environment will also promote better living conditions.

Governments and society must implement changes now! We raise our voices to fight for a real sustainable region.
We demand:

* Public policies and contingency actions directed at vulnerable communities and, in the long term, the co-design of public policies with citizens to reduce social inequality.

* A real commitment of the States of Latin America with the objectives of sustainable development presented by the United Nations.

* Promote (and legislate for) sustainable actions and friendly alternatives with the territory, respecting cultural, economic, and social differences in urban and rural contexts.

* Establish legislation that addresses plastic waste, reduces its production, promotes reuse or efficient disposal, and promotes alternative materials.

* The creation of more open spaces for groups and communities seeks to inform and promote citizens around actions and habits that are friendly to the environment.

* Public Policies to protect communities, towns, and activists who defend their environment and their land.

* Public policies that promote resilience and awareness of caring for the environment.

* The creation of a supranational law to attribute the co-responsibility of companies of the industrial sector and the States of Latin America regarding the repercussions that their production has on the natural environment, flora, and fauna.

We hope that these actions, added to many other possible ones, will allow us to build and provide a new opportunity. We hope that current and future generations have the right to live in an equitable, healthy, and environmentally friendly society. May our 3Rs, in times of crisis, be rethinking, reacting, and reviving.
