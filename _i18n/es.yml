meta:
  description: Somos una organización latinoamericana que lucha por la transformación de las democracias.
  keywords: democracia, tecnología
  title: Fundación Ciudadanía Inteligente
  projects: Proyectos de FCI. Hola!

navbar:
  about_fci: Sobre Ciudadanía
  fci: Qué hacemos
  funding: Cómo nos financiamos
  documentation: Transparencia
  our_story: Nuestra Historia
  team: Nuestro Equipo
  board: Nuestro Directorio
  projects: Proyectos
  our_projects: Nuestros Proyectos
  our_documents: Nuestras Publicaciones
  our_campaign: Nuestras Campañas
  tools: Crea tu herramienta
  support: Colaboran
  blog: Blog
  contact: Contacto
  join: Únete al equipo
  donations: Apóyanos

home:
  hero:
    title: Desarrollamos tecnología para fortalecer
    title2: las democracias en América Latina
    subtitle_crisis: "Chile"
    subtitle_crisis2: "Del descontento a la acción"
    text_crisis: Si el sistema político de América Latina no se abre a las demandas de la ciudadanía, el descontento llegará a un límite.
    crisis_cta: Cronología
    crisis_chile_cta: Evidencia anti militarización
  about:
    title: Quiénes somos
    content: Somos una organización latinoamericana que lucha por la transformación de las democracias.
    button: Quiero saber más
  projects:
    title: Conoce nuestros proyectos
    item_todos: Todos
    item_incidencia: Incidencia
    item_planificacion: Planificación Estratégica
    item_transparenta: Transparenta
    item_compara: Compara
    item_denuncia: Denuncia
    item_fiscaliza: Fiscaliza
    item_participa: Participa
    item_articula: Articula
    item_informate: Infórmate
    item_rinde: Rinde Cuentas
  sec_colaboran:
    title: Colaboran
  blog:
    title: Blog Ciudadanía
    button: Revisa Nuestro Blog
    more: Leer artículo

  instagram:
    title: Síguenos en Instagram

newsletter:
  title: Suscríbete a Ciudadanía Inteligente y obtén las últimas notificaciones y noticias.
  email: nombre@dominio.com
  button: Quiero Suscribirme

footer:
  title: Ciudadanía Inteligente

about_fci:
  title:  ¡Somos una organización latinoamericana con 15 años de trayectoria!
  title_2: Desarrollamos herramientas digitales para fortalecer las democracias
  video: Ver video
  content:
    hacemos:
      title: ¿Qué hacemos?
      desarrollamos_title: Desarrollamos herramientas tecnológicas que promueven la participación ciudadana.
      desarrollamos_text:  Exploramos soluciones a  las amenazas que enfrenta la democracia en América Latina y creamos herramientas para devolver el poder a la ciudadanía.
      entregamos_title: Formamos a agentes de cambio de toda la región para generar reales cambios sociales.
      entregamos_text: Facilitamos herramientas metodológicas a personas líderes activistas de Latinoamérica que luchan por la justicia social y la transformación de las democracias.
      disenamos_title: Creamos campañas comunicacionales de alto impacto para incidir en políticas públicas.
      disenamos_text: Compartimos nuestras campañas en la prensa internacional y medios digitales para presionar a quienes gobiernan por sociedades más justas e inclusivas.
    independencia:
      title: Ciudadanía Inteligente
      text01: 'Somos una organización latinoamericana que lucha por la transformación de las democracias. Con 15 años de experiencia en 14 países de la región, investigamos sobre los obstáculos que impiden el empoderamiento ciudadano y aplicamos nuestros hallazgos creando herramientas tecnológicas que fiscalizan autoridades, acercan comunidades a sus gobiernos, luchan contra la corrupción, fortalecen el trabajo de activistas, y permiten la incidencia en políticas públicas.'
      text02: Somos un equipo interdisciplinario e internacional que conecta profesionales del diseño, la ingeniería, las ciencias políticas, el derecho, el periodismo, la sociología, la literatura, la biología, para crear proyectos que nos permitan romper los límites que actualmente tiene la ciudadanía y avanzar hacia procesos colectivos y participativos en la toma de decisiones. Además, hoy nos encontramos en dos oficinas, una en Santiago de Chile y otra en Río de Janeiro.
      subtitle: Documentación Ciudadanía Inteligente
    item_estrategia: Ver estrategia (PDF)
    item_resumen: Resumen ejecutivo (PDF)
    item_estatutos: Nuestros estatutos (PDF)
    item_certificacion: Certificado FECU Social (Comunidad de Organizaciones Solidarias)
    item_agenda: Agenda de gestión de intereses de FCI y Equipo
    item_codigo: Código de Conducta
    item_manual: Manual de Prevención de Conflictos de Interés
    item_protocolo: Protocolo para la Prevención y Abordaje del Acoso Sexual
    item_manifiesto: Manifiesto y Protocolo para Promover la Igualdad de Género
    item_memoria2014: Memoria Ciudadanía Inteligente 2014
    item_memoria2015: Memoria Ciudadanía Inteligente 2015
    item_memoria2016: Memoria Ciudadanía Inteligente 2016
    item_memoria2017: Memoria Ciudadanía Inteligente 2017
    item_memoria2018: Memoria Ciudadanía Inteligente 2018
    item_memoria2019: Memoria Ciudadanía Inteligente 2019
    item_memoria2020: Memoria Ciudadanía Inteligente 2020
    item_memoria2021: Memoria Ciudadanía Inteligente 2021
    item_memoria2022: Memoria Ciudadanía Inteligente 2022
    item_memoria2023: Memoria Ciudadanía Inteligente 2023

    ciudadania_numeros:
      title: Ciudadanía Inteligente en números
      organizaciones_title: Alianzas con organizaciones
      organizaciones_counter: '00'
      organizaciones_description: Creamos una red en toda la región
      personas_title: Usuarias/os en nuestros sitios web
      personas_counter: '000000'
      personas_description: Cifras hasta el 2020
      paises_title: Países de Latinoamérica
      paises_counter: '00'
      paises_description: Presencia Regional
      herramientas_title: Herramientas replicadas
      herramientas_counter: '00'
      herramientas_description: Creemos en la colaboración y el código abierto
      alcance_escuela_title:  Alcance Escuela Incidencia Virtual
      alcance_escuela_counter: '0000'
      alcance_escuela_description: Convocamos a jóvenes activistas
      activistas_formados_title: Formamos agentes de cambio
      activistas_formados_counter: '0000'
      activistas_formados_description: A través de nuestra metodología de LabCívico
      seguidores_rrss_title: Seguidores en nuestras redes sociales
      seguidores_rrss_counter: '000000'
      seguidores_rrss_description: En Facebook, Instagram y Twitter
    funding: Revisa acá cómo nos financiamos
    funding_view: Ver financiamiento

documentation:
  title: Misión y pilares estratégicos de Ciudadanía Inteligente
  button: Ver Estrategia

timeline:
  title: Historia de Ciudadanía Inteligente
  timeline: Línea de tiempo
  cronologia: Cronología

equipo:
  title: Conoce a nuestro Equipo

board:
  title: Conoce a nuestro Directorio

campaigns:
  title: Campañas
  more: Ver campaña

projects:
  title: Conoce nuestros proyectos
  map:
    title: ¿Sabes dónde trabajamos?
    subtitle: Ciudadanía Inteligente
  see_more: Ver proyecto
  projects: Proyectos
  project:
    year: Año
    category: Categoría
    site: Sitio
    no_available: Sitio inactivo

tools:
  tools: Herramientas
  description: En Ciudadanía Inteligente ofrecemos una gran variedad de herramientas para fortalecer a organizaciones, sociedad civil, gobiernos centrales y locales, y empresas, con el fin de hacer democracias más transparentes, participativas e inclusivas para todas y todos. ¡Revísalas!
  title: Crea mejores sociedades y democracias con nuestras herramientas
  button: Contáctanos
  all: Todas
  advocacy: Incidencia
  information: Información efectiva
  audit: Fiscalización
  citizen_participation: Participación Ciudadana
  call_to_action: Si crees que esta herramienta es para ti y deseas llevarla a cabo ¡contáctanos!
  contact_us: Escríbenos a

support:
  title: Colaboran

blog:
  title: Blog Ciudadanía Inteligente
  previous: Página anterior
  next: Página siguiente
  more: Leer artículo

contact:
  title: ¡Contáctate con nosotros!
  subtitle: ¿Tienes dudas, preguntas o sugerencias sobre nuestro trabajo?
  subtitle_2: Llena este formulario y nuestro equipo se contactará contigo lo antes posible.
  form:
    name: Nombre*"
    last: Apellido
    email: Email*
    asunto: Asunto
    mensaje: Mensaje*
    prensa: Soy prensa
    button: Enviar mensaje

join:
  title: únete a Ciudadanía Inteligente
  subtitle: ¿Quieres ser parte de nuestro equipo?
  text: Recibimos solicitudes de prácticas, pasantías y voluntariados todo el año para apoyarnos en investigación, diseño, desarrollo y comunicaciones. Para cargos full-time las postulaciones sólo se realizarán al momento de abrir nuevas convocatorias. Si te interesa ser parte de Ciudadanía Inteligente sólo debes llenar este formulario contándonos brevemente sobre tu experiencia y por qué te interesa nuestra organización.
  form: Postula acá

lealtad:
  documentation: Ver documento
  lealtad: Fundación Lealtad Chile
  text: Cumplimos los Estándares de Transparencia y Buenas Prácticas de Fundación Lealtad Chile.
  description: En Ciudadanía Inteligente luchamos por la justicia social, por la transformación de las democracias, promovemos la fiscalización ciudadana y exigimos prácticas transparentes  a las autoridades en América Latina. No solo exigimos a nuestros gobernantes, sino que también aplicamos altos estándares de transparencia y buenas prácticas a nuestra propia organización. Buscamos cumplir con los principios que exigimos, por eso nos sometimos voluntariamente a la evaluación de la Fundación Lealtad.
  description2: Fundación Lealtad identifica en todo el mundo a las ONGs que cumplen íntegramente con indicadores de transparencia y buenas prácticas, agrupados en nueve estándares con 29 indicadores. Como organización cumplimos íntegramente con 27 indicadores relacionados con las regulaciones de los órganos competentes y con las obligaciones legales del Estados donde efectuamos nuestro trabajo.Somos consistentes y claros con nuestro fin social, transparentes en nuestra comunicación y financiamiento, y promovemos la participación de practicantes y voluntarios.

funding:
  title: Cómo nos financiamos
  no_report: Para estos fondos no hay exigencia de reportes.
  RLTL: Montos entregados a Red Latinoamericana para la Transparencia Legislativa, a repartir entre organizaciones participantes.
  table_title: Fondos / Montos en USD

404:
  title: '404'
  page: Ups! La página no existe
  text: La página que buscas no puede ser encontrada. Revisa la URL o vuelve a la página de inicio.
  home: Volver al Inicio

chile_crisis:
  title_1: "Cronología de la crisis de"
  title_2: "la desigualdad en Chile"
  description: "Hace dos años la ciudadanía chilena se movilizó para exigir dignidad. Hoy comienza un nuevo camino para que la Convención Constitucional escriba una nueva Constitución que garantice derechos sociales. Revisa aquí la línea de tiempo del descontento ciudadano en Chile ocurridas en el 2019."
  precedents: Antecendentes
  precedent_1: '140 personas concentran el 20% de la riqueza de todo el país.'
  precedent_2: 'El 50,6% de los trabajadores de Chile ganan 534 dólares mensuales o menos.'
  precedent_3: 'Un 68% de la ciudadanía chilena siente injusticia respecto a la salud en el país, un 67% respecto a la educación, y un 66% respecto a que algunas personas sean tratadas con mucha más dignidad y respeto que otras.'
  precedent_4: 'Países como Croacia y Rumania, que tienen un PIB idéntico al de Chile, tienen un sueldo mínimo 47% y 59% superior, respectivamente.'
  precedent_5: 'Los últimos casos de evasión de impuestos, fraudes al fisco, colusiones y otras fuentes de desigualdad, suman 4,982 millones de dólares: Esto equivale a un 1.7% del PIB de Chile, 126.621 viviendas sociales, 28 hospitales o 33.117.619 pensiones básicas solidarias mensuales.'
  see_more: 'Ver más'

donations:
  title: Apóyanos
  subtitle: ¡Necesitamos tu ayuda! Para seguir trabajando por más justicia social y por mejores democracias en América Latina
  lead: Somos un equipo que trabaja día a día para mejorar las democracias en Latinoamérica. Pero contribuir a redistribuir el poder de los pocos hacia toda la ciudadanía NO es una tarea fácil y por eso tu apoyo es fundamental.
  video: Echa un vistazo a nuestro trabajo
  text_1: 'Tu donación nos permitirá seguir:'
  list_1: 'Desarrollando herramientas tecnológicas que promueven la participación ciudadana.'
  list_2: 'Incidir para romper la relación de dinero y política.'
  list_3: 'Apoyar a formar a más liderazgos emergentes y a gobiernos locales de la región que luchan por un real cambio social.'
  text_2: Ciudadanía Inteligente es una organización con 15 años de trayectoria, sin fines de lucro y que cuenta con el certificado de la Fundación Lealtad. Organización que nos premió con resultados de excelencia en los criterios de la evaluación sobre transparencia y buenas prácticas.
  text_3: 'Sé parte de nuestra gran comunidad que lucha por fortalecer nuestras democracias. Hoy vivimos tiempos de incertidumbre, pero en Ciudadanía Inteligente nos mueve una sola certeza: que juntas y juntos somos más fuertes.'
  text_4: Opciones para donar
  text_5: 'Si tienes algún inconveniente con los medios anteriores, necesitas más información o te quieres comunicar con nosotros, no dudes en escribirnos a <a href="mailto:info@ciudadaniai.org">info@ciudadaniai.org</a>'
  option_1:
    title: Aporte único en CLP
  option_2:
    title: Aporte mensual en CLP
  option_3:
    title: Aporte en USD
  disclaimer: "Porque nuestro compromiso con la probidad y la transparencia es absoluto, todas las donaciones hechas a Ciudadanía Inteligente son públicas a través de nuestra lista mensual de aportantes, donde se registra el nombre y apellido de cada donante. Así, podemos seguir contribuyendo a fortalecer las democracias en América Latina de forma independiente y sin riesgos de conflictos de interés."

our_campaign:
  title: Conoce nuestras campañas
  title_2: Revisa y descarga todas nuestras campañas
  description_our_campaign_1: En Ciudadanía Inteligente exploramos soluciones a las amenazas que enfrenta nuestra democracia. Para esto creamos campañas de incidencia, originales y de alto impacto para que la ciudadanía de América Latina recupere el poder que le corresponde y que hoy está en manos de unos pocos.
  description_our_campaign_2: "En tiempos de incertidumbre, nos mueve una sola certeza: que juntas y juntos somos más fuertes."

our_documents:
  title: Nuestras publicaciones
  title_2: Revisa y descarga todas nuestras publicaciones
  description_our_document_1: En Ciudadanía Inteligente exploramos soluciones a las amenazas que enfrenta nuestra democracia. En el marco de nuestros proyectos hemos generado publicaciones que pueden ser de interés.
  description_our_document_2: Te invitamos a leer y utilizar estos materiales.
