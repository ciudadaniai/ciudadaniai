---
layout: post
title: 'Las niñas pueden ser lo que sueñan'
intro: 'Invitamos a dos ingenieras a poner en palabras cómo cambiar los paradigmas y cerrar la brecha de género.'
day: 08
month: marzo
date: 2020-03-21 12:00:00 -0300)
categories: Invitación
highlighted: false
share_message: 'Las niñas pueder lo que sueñan'
img: 8m.png
caption:
---
*Invitamos a dos ingenieras Paulina Peña Romero, Ingeniera Civil Industrial y Javiera Ketterer Hoppe, Ingeniera Civil Eléctrica a  escribir en una nueva conmemoración del Día Internacional de la Mujer sobre las mujeres y niñas en el mundo STEM. Acá su columna.*


No hay duda alguna que existe un consenso mundial: necesitamos más mujeres en áreas de STEM. Si observamos la situación en Chile, en los últimos 15 años sólo un 19,7% de quienes se matricularon en carreras de tecnología, donde se encuentran las ingenierías, fueron mujeres. Es decir, en Chile la representatividad de género sigue siendo muy baja en estas áreas.

Podríamos enumerar múltiples razones de la importancia que tiene la inclusión de más mujeres en estas áreas, partiendo porque la igualdad de género es un derecho humano, y además un Objetivo de Desarrollo Sostenible de la ONU. También, podemos explicar que para afrontar la incertidumbre y complejidad del mundo actual se necesitan diversas miradas e interpretaciones, ya no basta sólo con la mirada del hombre blanco heterosexual, el mundo está habitado por una gran diversidad de personas, que distan mucho de esta única visión.

Incluso, desde el punto de vista económico, podemos destacar que la inserción de más mujeres en el ámbito profesional y en las áreas relacionadas con STEM aceleran el desarrollo de los países. A la fecha, contamos con diversos estudios que señalan que si más mujeres son parte del mercado laboral se incrementa el desarrollo económico, y en consecuencia aumenta el crecimiento del PIB en torno a un 5%. Por lo que impulsar las habilidades de niñas en estas áreas no solo mejora el desarrollo del talento de nuestro país, sino que nos sirve a todes, ya que todes estaríamos en un país con mejores condiciones.

Como es evidente, esta inclusión también beneficia a las mujeres, las profesiones relacionadas con el área STEM conllevan conseguir una mayor remuneración y optar a puestos de liderazgo dentro del mundo profesional, donde hoy está subrepresentada, sólo el 10% de los puestos directivos de las grandes empresas del país son ocupados por mujeres. Una mayor presencia de mujeres en este tipo de profesiones y puestos de liderazgo contribuirá a una mayor independencia económica, creando un círculo virtuoso, puesto que son las mujeres las que más invertimos en la educación de nuestras hijas e hijos.

La pregunta clave es ¿qué podemos hacer para motivar esta inclusión?

Primero que todo, romper con las falsas creencias. Las niñas sí son buenas para las ciencias y las matemáticas, existen 30 países donde las mujeres obtienen mejores resultados que los hombres en la prueba PISA de ciencias y matemáticas, como es el caso de Finlandia, Tailandia, Grecia, Turquía, Suecia, Indonesia e Islandia, o sea las niñas sí destacan en estas materias. Además, es importante considerar que investigaciones de diversos países, han deducido que las diferencias de resultados por género no están relacionadas con los factores biológicos, sino que son el resultado de la experiencia de las y los jóvenes a lo largo de la vida, y que éstos están conectados con los estereotipos de género intrínsecos e involuntarios presentes en la sociedad.

Chile es el cuarto país con la mayor brecha entre mujeres y hombres en la prueba PISA de ciencias y matemáticas, donde los hombres obteniendo mejores resultados que las mujeres, ampliándose incluso con los años de escolaridad, es decir esta situación parte desde la infancia. Es imperativo que avancemos desde la educación en todos sus niveles, hacia una educación no sexista, por ejemplo, haciendo una revisión de los elementos de apoyo en el aprendizaje, ¿qué dicen los textos escolares y los libros que tienen que leer?

Hay que derribar las falsas creencias de profesores sobre los rendimientos por género, que generan mayores expectativas sobre el desempeño de los niños en matemáticas, al igual que las expectativas de las madres, padres y apoderados sobre el futuro de sus hijas.

Las y los invitamos a fomentar la curiosidad innata de todos los seres humanos desde las edades mas tempranas y a lo largo de toda su vida, es fundamental una educación inclusiva que tenga el foco en la creatividad y el pensamiento crítico y reflexivo. Hay que promover la heterogeneidad y diversidad en todos los ámbitos, como los cuentos que les leemos a nuestros niñas y niños, los juguetes y la ropa que les compramos o regalamos, las películas que ven, quienes son sus héroes y heroínas y si los personajes que representan una profesión sean tanto femeninos como masculino. Es fundamental promover la aceptación de las diferencias y encontrar las fortalezas de los demás, muy importante cuidar el lenguaje con el que se describe a las y los otros, y enseñar con el ejemplo.

Igualmente, hay que fortalecer los modelos a seguir para las niñas, ya que esto contribuye a visibilizar la capacidad de las mujeres, en especial de aquellas relacionadas con las áreas STEM. Esto fortalece la confianza mostrando que, si una mujer puede, ellas también pueden, ya que les permite identificarse con un ejemplo real y concreto. Adicionalmente, se pueden crear mas programas de vinculación de las niñas con las áreas STEM, donde puedan contar con experiencias concretas, que transformen estas áreas en oportunidades más cercanas y cotidianas. Por otro lado, impulsarlas a pedir ayuda y encontrar mentoras, como nosotras, mujeres que motiven a las niñas a entrar en este maravilloso mundo y hacerles ver sus beneficios.

Es tiempo de actuar y cerrar la brecha de género, para contar con equidad entre hombres y mujeres. Debemos generar, desde los distintos espacios, pequeñas o grandes intervenciones que contribuyan a cambiar los estereotipos de género, desafiar las normas y erradicar las prácticas que causan discriminación. Visibilizar a las cientos de mujeres que han y están contribuyendo con descubrimientos y avances tecnológicos. Fortalezcamos el talento de nuestras futuras generaciones, alentemos a las niñas a buscar un futuro relacionado con las matemáticas, la tecnología, la ingeniería o la ciencia. Iniciemos un nuevo mundo donde las niñas puedan ser lo que sueñan, sin límites ni creencias falsas que las alejen de grandes oportunidades.




**Bibliografía**

Aguirre, D, Leila Hoteit, Christine Rupp, y Karim Sabbagh. «Empowering the third billion. Women and the world of work in 2012». Booz and Company, 2012.

Comunidad Mujer. «Mujer y trabajo: Brecha de género en STEM, la ausencia de mujeres en Ingeniería y Matemáticas». 42. Santiago, Chile, diciembre de 2017.

Consejo Nacional de Educación. «Índices Educación Superior». Índices, 2020. https://www.cned.cl/indices-educacion-superior.

Elborgh-Woytek, Katrin, Monique Newiak, Kalpana Kochhar, Stefania Fabrizio, Kangni Kpodar, Philippe Wingender, Benedict Clements, y Gerd Schwartz. «Las mujeres, el trabajo y la economía: Beneficios macroeconómicos de la equidad de género». Documento de análisis del personal técnico del FMI, 2013.

Fundación ChileMujeres, PwC Chile, y LT Pulso. «Directoras en Chile, nuevos desafíos», s. f. https://chilemujeres.cl/wp-content/uploads/2020/03/Estudio-Impulsa-N5-Febrero-2020.pdf.
Great Place to Work México. «¿En tu empresa hay equidad? Te decimos sus beneficios y cómo promoverla», septiembre de 2019. https://www.entrepreneur.com/article/340137.

Instituyo Nacional de Estadística. «Género y Empleo», mayo de 2017. https://historico-amu.ine.cl/genero/files/estadisticas/pdf/documentos/enfoque-estadistico-genero-y-empleo.pdf.
Mizala, Alejandra. «Desempeño en matemáticas, elección de carreras y brecha salarial», marzo de 2014. https://vox.lacea.org/?q=eleccion-de-carreras-y-brecha-salarial.

Mizala, Alejandra, Francisco Martínez, y Salomé Martínez. «Pre-service elementary school teachers’ expectations about student performance: How their beliefs are affected by their mathematics anxiety and student’s gender». Teaching and Teacher Education 50 (1 de agosto de 2015): 70-78. https://doi.org/10.1016/j.tate.2015.04.006.

OCDE. «¿Qué subyace bajo la desigualdad de género en educación?» PISA in Focus, marzo de 2015. https://www.oecd.org/pisa/pisaproducts/pisainfocus/PIF-49%20(esp).pdf.

Olguín Inostroza, Camila Fernanda. «Análisis de desempeño de estudiantes del programa de ingreso especial de equidad de género de la Facultad de Ciencias Físicas y Matemáticas, Universidad de Chile», 2020.

Pulso - La Tercera. «Mujeres ocupan el 10% de los puestos en directorios de grandes empresas», marzo de 2020. https://www.latercera.com/pulso/noticia/mujeres-ocupan-el-10-de-los-puestos-en-directorios-de-grandes-empresas/SWO5J2A34JGW7EVMT3LWBHGAVU/#:~:text=El%20quinto%20estudio%20Impulsa%2C%20realizado,del%20pa%C3%ADs%2C%2076%20son%20mujeres.

Schulz, Hans. «Del conocimiento a la realidad: igualando las condiciones para las mujeres», agosto de 2020. https://idbinvest.org/es/blog/genero/del-conocimiento-la-realidad-igualando-las-condiciones-para-las-mujeres.

UNESCO. «Descifrar el código: La educación de las niñas y las mujeres en ciencias, tecnología, ingeniería y matemáticas (STEM)», 2019.
