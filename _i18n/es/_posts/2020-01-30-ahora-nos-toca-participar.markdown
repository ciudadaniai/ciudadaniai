---
layout: post
title: 'En Chile este 2020'
intro: 'Nuestro compromiso por la participación ciudadana luego del estallido social de octubre del 2019.'
day: 30
month: Ene
date: 2020-01-27 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Que la ciudadanía se empodere en el Chile 2020.'
img: 30-1-20.png
caption:
---
Desde el primer momento del estallido social, en [Ciudadanía Inteligente](https://ciudadaniai.org/) hemos buscado hacer el mejor aporte posible desde nuestra experiencia y profunda vocación por mejores democracias. Partimos con una [cronología del descontento](https://ciudadaniai.org/chile), una [campaña anti militarización](https://ciudadaniai.org/crisis), la elaboración de una [metodología de participación ciudadana](https://ciudadaniai.org/actualidad/2020/01/20/haz-tu-cabildo-ciudadano.html) para cabildos y hoy queremos contarle al mundo sobre un gran proyecto: [Ahora Nos Toca Participar](https://ahoranostocaparticipar.cl/).

Al alero de la red de redes de organizaciones de la sociedad civil Nuevo Pacto Social, Ahora Nos Toca Participar es un proyecto interorganizacional de enorme alcance territorial que buscará formar a casi 1 millón de personas en la importancia de la Constitución y los detalles del proceso constituyente. Aquí buscaremos empoderar a cada ciudadano y ciudadana para que pase por nuestros talleres, recordándoles el rol crucial que juegan hoy en las decisiones transformadoras de nuestro país.

¡Hoy  nadie puede quedar fuera del plebiscito del 26 de abril! Nuestra meta es que la mayor cantidad posible de chilenos, chilenas y migrantes vayan a votar por cualquiera sea su opción de preferencia. Este proceso sólo tendrá legitimidad si todas y todos somos parte de él.

**¿Qué pasará después del plebiscito?**

Una vez que conozcamos los resultados del referéndum nos abocaremos por completo a promover la participación ciudadana en encuentros presenciales en todo Chile. También realizaremos encuentros digitales para que todas las personas tengan un espacio dónde intercambiar sus visiones y construir en conjunto propuestas para un el nuevo pacto social que estamos construyendo como país. Todo lo que se recopile de estas instancias de diálogo se sistematizará de manera transparente, para lograr un producto que sea útil y abarcable que represente los intereses de toda la ciudadanía.

Desde Ahora Nos Toca Participar trabajaremos sin descanso para  hacer llevar todas las propuestas ciudadanas al 100% de candidatos y candidatas inscritas para la elección de constituyentes (en caso de ganar el Apruebo) o de gobiernos locales y regionales (en caso de ganar el Rechazo). Nuestra convicción es que quienes aspiran a cargos de poder deben hacerlo con el respaldo de la ciudadanía. ¿Qué mejor manera para obtener este respaldo que abanderándose con sus ideas, levantadas en un proceso participativo?

*En Ciudadanía Inteligente trabajamos en el desarrollo del sitio web del proyecto. [Acá](https://ahoranostocaparticipar.cl/) puedes revisar la web*
