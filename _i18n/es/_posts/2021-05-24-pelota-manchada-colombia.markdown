---
layout: post
title: 'Pelota manchada'
intro: '¿Qué está sucediendo en Colombia? Revisa la reflexión de un ciudadano colombiano que prefirió firmar sin nombre.'
day: 24
month: mayo
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Pelota manchada by @ciudadaniai'
img: colombia.jpg
caption:
---
*Por: Un ciudadano colombiano*

En Popayán, una ciudad a casi 600 kilómetros de Bogotá y capital de uno de los departamentos más afectados por el conflicto armado en Colombia, se veían este jueves decenas de jóvenes en la calle. Era el día 21 de protestas ciudadanas en un país que cada día se hunde más en una crisis social sin precedente, pero esta marcha tenía algo particular: muchos de ellos venían vestidos con las camisetas de sus equipos de fútbol.

En un país donde la gente muere por el hambre, la pobreza, el abandono, la violencia, y también por el fútbol, era al menos curioso ver en un mismo lugar a hinchas de América, Millonarios, Nacional, Cali y Santa Fe caminando juntos. Armados de sombrillas y tambores, de banderas y trapos, cantaban al unísono “la copa no se juega”.

Se referían a la última obsesión del presidente Iván Duque: organizar junto a Argentina la Copa América de fútbol, un histórico evento del que Colombia ya fue sede en 2001, cuando el país intentaba lograr un acuerdo con la guerrilla de las Farc y las selecciones de Brasil y Argentina decidieron no venir, o mandar equipos juveniles, por miedo a su seguridad.

“Sin pan no hay circo”, se leía en redes sociales cuando la Conmebol le bajó el pulgar al país y le quitó la posibilidad de organizar el evento. Una anécdota frente a la realidad de un país que en tres semanas de protesta ha perdido más de 50 vidas, un país que noche a noche se acuesta en vilo ante la posibilidad de una nueva matanza, de nuevos abusos policiales, de nuevos ataques a mamás que defienden a sus hijos o a niñas que salen a la calle a protestar.

Colombia vive una crisis que significa varias cosas: la primera y más grave es la desconexión del gobierno con la ciudadanía. A la obsesión por organizar la Copa en medio del estallido social se suman la poca disposición al diálogo, la falta de condena a la violencia de decenas de miembros de la fuerza pública, las cartas irrespetuosas a las Naciones Unidas, la terquedad por mantener proyectos de ley que sacaron a la gente a la calle y la concentración de poder (esto antes de las marchas) en una democracia que hoy parece en peligro.

Iván Duque no escucha. Sigue profundizando una narrativa que le ha permitido a su mentor, Álvaro Uribe Vélez, mantenerse en el poder por casi 20 años: la del enemigo interno. Primero fueron las Farc, luego el castrochavismo y el miedo a que Colombia se volviera Venezuela. Ahora, durante las marchas, el enemigo son los vándalos, muchos de ellos venezolanos según el gobierno, que hacen parte de disidencias de las Farc o de la guerrilla del ELN.

Este argumento no solo profundiza la desconexión con el país sino que criminaliza a miles de personas que pacíficamente y con motivaciones válidas salen a protestar a la calle. En su mayoría jóvenes a los que este país no les ofrece nada, una nación que quitó tanto que les quitó el miedo. Y junto a ellos salen sus madres, sus tías y tíos, sus familias. Y la respuesta del Estado es la represión, es abordar una conflictividad social como un problema de orden público y acusar a quienes se manifiestan de bloquear el país, impedir el desarrollo y ser un obstáculo en la reactivación social.

Y cada bomba, cada herido y cada muerto es un combustible para la revuelta, para la indignación y la resistencia. También por primera vez en la historia de este país, ningún político se adjudica la autoría del paro, ni siquiera un comité nacional cuya representatividad está en duda. El paro es de la gente.

Esto hace del estallido un movimiento genuino, que se manifiesta en la calle pero también en las casas, en los restaurantes y bares. El paro es hoy la conversación del país. Las reformas arbitrarias, los ministros que renuncian, las consecuencias de la protesta ya no son un diálogo de jóvenes politizados o de políticos escondidos. El paro también se tomó la agenda nacional.

El gobierno puso en marcha espacios de diálogo con diferentes actores. Las y los jóvenes siguen sin sentirse escuchadxs, el Comité de Paro convoca a más movilizaciones y el gobierno insiste en que está haciendo lo que debe hacer. En la mitad, los muertos. Esta manera que tiene Colombia de arreglar los conflictos, de agarrar la vida a patadas, de patear cada tanto el tablero de los derechos humanos y de amenazarnos con conmociones y pequeñas dictaduras.

Y mientras tanto la Copa, que durante años fue el circo en el que nos escondimos como país, ya no se juega.
