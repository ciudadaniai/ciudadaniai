---
layout: post
title: 'Innovación crítica para gobiernos locales'
intro: 'Compartimos herramientas para incluir a la ciudadanía en la toma de decisiones a nivel municipal'
day: 27
month: Nov
date: 2019-11-27 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Innovación crítica para gobiernos locales'
img: 27-11-19.jpg
caption:
---

***Parte de nuestro equipo viajó a la [Smart City Expo](http://www.smartcityexpo.com/) a compartir herramientas para incluir a la ciudadanía en la toma de decisiones a nivel municipal.***

El taller fue organizado en el marco de la [Digital Future Society Summit](https://digitalfuturesociety.com/agenda/digital-future-society-summit-at-scewc-2019/), un evento organizado por el [think tank del mismo nombre](https://digitalfuturesociety.com/) del que somos parte, y que ocurrió en el marco de la [Smart City Expo](http://www.smartcityexpo.com/en/home), un evento enorme y masivo que reúne a miles de personas, organizaciones y empresas alrededor de las ciudades, la digitalización, el futuro y la sustentabilidad.

Nuestra sesión fue intensa: tuvimos asistentes de todo el mundo (entre ellos, Haití, Francia, Polonia, España, Chile, entre otros países) que hicieron un ejercicio práctico sobre cómo co-crear soluciones a problemas públicos entre gobiernos y ciudadanía. Para hacer esto, pusimos a su disposición algunas de nuestras herramientas como LabCívico, la metodología de Abre, y la mirada crítica sobre el uso de la tecnología que propone la Alianza A+.

En relación a este último punto, compartimos algunos datos para hacer conscientes a quienes trabajan en y con gobiernos locales de que la tecnología no es neutra, y si bien puede ayudar mucho a las democracias, si no se audita y transparenta correctamente, puede ser extremadamente dañina. Algunos ejemplos:

* Las mujeres tenemos un 47% más de probabilidades de quedar heridas seriamente en un accidente de tránsito y un 17% más de morir en alguno de ellos, porque los asientos de los autos no están pensados en personas de menor altura, que necesitan estar más cerca de los pedales. Además, las pruebas con crash test dummies femeninos se hacen la mayoría de las veces en el asiento del copiloto, ignorando que las mujeres sí manejan.

* Los sistemas de reconocimiento facial, que se usan para acciones tan sensibles como identificación de criminales, son mucho más precisos identificando a gente blanca, y tienen problemas identificando a personas de comunidades que ya son discriminadas y criminalizadas, como afrodescendientes.

Destacamos que la tecnología es un fenómeno político, y que no se puede hablar de innovación pública sin considerar la efectiva participación ciudadana en los procesos de creación. Con eso buscamos avanzar nuestra agenda institucional sobre la tecnología de interés público, dónde los ciudadanos son parte efectiva del proceso de innovación, desde su concepción hasta implementación, dónde los problemas sociales son priorizados en la creación de nuevas tecnologías, y el desarrollo tecnológico de las ciudades respecta a los derechos digitales de la ciudadanía.

En resumen, pusimos a disposición de líderes internacionales una visión general de nuestros diez años de experiencia promoviendo la participación ciudadana y el uso responsable y estratégico de la tecnología, principios que esperamos se expanden cada día más por todas las ciudades del mundo para construcción de un futuro más democrático y inclusivo.
