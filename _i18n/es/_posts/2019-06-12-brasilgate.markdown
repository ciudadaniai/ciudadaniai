---
layout: post
title: 'BRASILGATE'
intro: 'The Intercept publicó los diálogos entre Moro y los fiscales del juicio LavaJato. Una patada a la democracia.'
day: 12
month: Jun
date: 2019-06-12 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: "BRASILGATE: Sin libertad de prensa no hay democracia @ciudadaniai."
img: 12-06-19.jpg
single_image:
  -
    caption: Diario Río Negro
---
***Columna escrita por Ana Carolina Lourenço coordinadora de Incidencia Regional de Ciudadanía Inteligente. Publicada en el medio [Nodal](https://www.nodal.am/2019/06/brasilgate-sin-libertad-de-prensa-no-hay-democracia-por-ana-carolina-lourenco/)***

Intercept Brasil publicó tres reportajes sobre una serie de manipulaciones indebidas del entonces juez Sérgio Moro, ahora ministro de Justicia, en las investigaciones de la operación brasileña Lava Jato, que impidió la participación del líder del Partido de los Trabajadores, Luiz Inácio Lula da Silva en las elecciones presidenciales en 2018. El material: mensajes intercambiados de 2015 a 2018, vía Telegram, por Sérgio Moro, y el fiscal Deltan Dallagnol, y que fueron obtenidos a través de una fuente anónima. Las conversaciones ofrecen evidencia irrefutable de actuaciones irregulares hasta ilegales y politizadas, por la fiscalía a cargo. De acuerdo a los mensajes publicados, el hoy ministro Sérgio Moro sugirió testigos a la Fiscalía, opinó sobre el avance de las constataciones, anticipó una decisión de los acusadores y articuló movimientos que, en caso de ser comprobados, dañan infinitamente la imparcialidad de un juez.

Más allá de una indicación de acciones fuera de la competencia de un juez, las revelaciones son especialmente graves por los efectos de éstas en las elecciones brasileñas de 2018. A sólo cuatro días de la presentación de la denuncia del caso Tríplex contra Lula en 2017, el coordinador de la investigación de Lava Jato, Deltan Dallagnol, tuvo dudas sobre la solidez de la historia que contó al juez Moro. Hecha la denuncia de corrupción pasiva y lavado de dinero contra Lula, Dallagnol envió mensajes directamente a Moro, quien debería hacer el juicio de la denuncia, para explicar los términos de la denuncia, sus debilidades, y sus elecciones argumentativas.

Las revelaciones evidencian una politización de la justicia en distintas fases del proceso. Hoy se presentan ante la opinión pública hechos que evidencian un patrón de conducta a través del tiempo por parte de los fiscales de la Lava Jato para interferir en el resultado de las elecciones. Por ejemplo, en el intercambio de mensajes se prueba una maniobra política, no jurídica, utilizada para bloquear, con éxito, una entrevista de Lula a Folha de São Paulo antes de la primera vuelta de las elecciones presidenciales. ¿El motivo? El impacto de la entrevista presentaba riesgos y una posible victoria de Fernando Haddad, candidato del PT a la presidencia. Los supuestos argumentos técnicos e imparciales que se mostraron en la prensa nacional e internacional, incluso, en las redes sociales, hoy pierden validez con la publicación de Intercept.

Desde 2016, la politización de la operación Lava Jato estuvo en el centro del debate polarizando a la sociedad brasileña y latinoamericana. Un lado de la discusión defendía la imparcialidad de los operadores de justicia brasileños aplaudidos por su lucha contra la corrupción más allá del partido político, en este caso el PT, involucrado. Del otro lado, se señalaba con preocupación el notorio tono político de sus acciones. Ante las nuevas revelaciones es evidente e irrefutable que los fiscales de la operación Lava Jato no son personas apartidarias ni apolíticas, sino todo lo contrario. Son personas que actúan por convicciones ideológicas y que trabajaron para evitar el retorno del PT al poder, en conductas contrarias a un Estado de Derecho, debilitando la democracia y la confianza de la ciudadanía en sus instituciones.

Ahora, la pregunta más importante es: ¿qué sucede luego de estas revelaciones? ¿Se considerará legítima una elección en donde existió interferencia, ya no de actores externos, sino de actores internos, del propio Poder Judicial, que perjudicó activamente, de forma premeditada y continuada, a uno de los partidos políticos que aspiraban al poder? ¿Se despojó a los votantes brasileños de la posibilidad de votar por el proyecto político de su preferencia? ¿Debe Sérgio Moro permanecer en el gobierno, como ministro, especialmente si la Policía Federal – bajo su mando y control – es la entidad a la que corresponde investigar los contenidos de las conversaciones?

En este contexto, en Ciudadanía Inteligente reforzamos nuestra misión de apoyo a los denunciantes y todas las personas que trabajan día a día por buscar la verdad: las y los periodistas. Esta revelación mediática, lamentablemente, devela una captura maliciosa de la lucha legítima contra la corrupción, es un ataque contra la Constitución brasileña, una violación a la separación de los poderes de la República y es un insulto a la democracia misma.

Las revelaciones de hoy, y el trabajo que sigue refuerzan la importancia del periodismo de investigación y de los denunciantes (whistleblowers) como contrapesos a la arbitrariedad del poder y como piezas centrales en una lucha contra la corrupción que sirva al interés público y no a vendettas políticas.

Y es que la democracia fuerte sólo existe con libertad de prensa. No podría ser de otro modo y Brasil es el ejemplo para impedir que triunfe la impunidad.

Luchemos por democracias más fuertes.

A The Intercept y a la sociedad brasileña, toda nuestra solidaridad.

Continuamos.
