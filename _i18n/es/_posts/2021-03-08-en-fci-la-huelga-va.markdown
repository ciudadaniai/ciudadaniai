---
layout: post
title: '¡En FCI la Huelga Feminista VA!'
intro: 'Nuestra directora Eglé Flores escribe sobre las luchas del feminismo y cómo unidas haremos reales cambios.'
day: 08
month: marzo
date: 2020-03-21 12:00:00 -0300)
categories: Invitación
highlighted: false
share_message: 'En FCI la huelga va'
img: huelga.png
caption:
---
*Invitamos a nuestra directora Eglé Flores y escribir una columna para conmemorar el Día Internacional de la Mujer. Acá sus palabras.*

Seré breve, los tiempos cambian.

Hoy no nos dirigimos a quienes nos gobiernan, pero les agradecemos el lienzo para honrar a nuestras muertas. Ojalá esa proactividad y urgencia la tuvieran para hacer cumplir las leyes que nos protegen. Hoy no gastaremos nuestras palabras en ustedes.

Hoy nos dirigimos a nosotras, a las que acuerpamos la lucha feminista. Los tiempos lo exigen. Nos dirigimos a nosotras para recordarnos que esta lucha es más grande que nosotras mismas.

Compañera, si crees que las violencias contra las mujeres tienen su origen en el sistema patriarcal, necesitas ampliar el panorama. Nos enseñaron a comprender el mundo de forma compartimentalizada. Cada tema en su cajita y debidamente etiquetado: el racismo por acá, el nacionalismo por allá; la lucha de clases aquí, la xenofobia acullá. Como si las reivindicaciones históricas por la libertad de los cuerpos no estuvieran conectadas.

La realidad es más compleja que eso. Los sistemas que oprimen a las mujeres - y a muchos hombres - son múltiples y se sobreponen. Existen cuerpos de mujeres racializadas, atravesadas por el colonialismo y explotadas por el capitalismo neoliberal. Hay cuerpos despojados de sus territorios y territorios que existen más allá de los mapas. A algunas nos ha tomado más tiempo observar esto por falta de perspectiva y de referencias. Nos educaron para tener una memoria corta, forjada por libros que muestran una historia única y limitada; narrada desde quienes descubrieron mundos que no necesitaban ser descubiertos, mucho menos colonizados.  

Hoy sabemos que nuestras experiencias también son válidas, tenemos mujeres que narran realidades que se entretejen con nuestros territorios. Hoy atesoramos las palabras de: María Lugones, Ochy Curiel, Yuderkys Espinosa, Aura Cumes, Silvia Rivera Cusicanqui, Leydy Pech, Rita Segato, Yasnaya Elena Gil, Gladys Tzul Tzul, Marielle Franco y tantas otras.

La lucha feminista es el llamado a no soltar la resistencia de 500 años para muchas, y a despertar de la larga noche para otras. Es una lucha que no es una y no es única: es lucha de luchas, conecta las narrativas desde los feminismos negros, descoloniales, periféricos, populares, indígenas, transincluyentes. Los feminismos construidos desde las vivencias de las mujeres, los movimientos sociales y la academia en estos territorios, que hoy nos permiten situarnos en la complejidad global desde nuestras propias experiencias.

Hace poco escuché a [Angela Davis](https://twitter.com/BlackQueerTH/status/1275115258697433088?s=20) decir que: “Las personas asumen que cuando te comprometes con prácticas feministas te enfocas sólo en problemas de género, o te enfocas sólo en mujeres. Para mí, el feminismo es una metodología, una forma de pensar respecto del mundo; es un método de organización, nos llama a ser inclusivxs. A ser inclusivxs entendiendo las conexiones y las relaciones. El feminismo asegura que nadie se quede atrás”. Y sí, ser feminista es mucho más que romper el pacto y tirar al patriarcado. Ser feminista es declarse antirracista, anticolonialista; es actuar para romper el pacto de raza y clase. Ser feminista es luchar por nosotras aboliendo nuestros propios privilegios, porque el poder entre pocas, tampoco sirve.

Los feminismos representan al movimiento social más relevante de la historia contemporánea; también pueden ser la oportunidad de deconstruir los sistemas político-económicos que amenazan la vida; la oportunidad de forjar democracias que respondan a las demandas sociales y ambientales de nuestros tiempos. A partir del tejido fino entre los feminismos y sus luchas, podemos imaginar un sistema alternativo que garantice una vida digna para todes y reconozca la autodeterminación de las naciones sobre sus territorios. Ese sistema ya está ahí, dando vueltas en los imaginarios de muchas que saben que un mundo ch’ixi es posible*.

Seré breve, nada tiene que ser como es.

**Este es el momento histórico que nos tocó vivir; asumámoslo en colectivo y en plural.**


*“Las entidades ch’ixi, son poderosas porque son indeterminadas, porque no son blancas ni negras, son las dos cosas a la vez. La serpiente es de arriba y a la vez de abajo; es masculina y femenina; no pertenece ni al cielo ni a la tierra pero habita ambos espacios, como lluvia o como río subterráneo, como rayo o como veta de la mina.” Extraído de UN MUNDO CH´IXI ES POSIBLE ENSAYOS DESDE UN PRESENTE EN CRISIS de Silvia Rivera Cusicanqui, puedes [descargarlo gratis aquí](https://www.facebook.com/gislatino/posts/descarga-gratisun-mundo-chixi-es-posible-ensayos-desde-un-presente-en-crisis-sil/2503036926381758/).
