---
layout: post
title: '¡Tenemos nuevo Director Ejecutivo!'
intro: 'El abogado Octavio Del Favero llega a liderar Ciudadanía Inteligente.'
day: 08
month: ene
date: 2020-03-21 12:00:00 -0300)
categories: Invitación
highlighted: false
share_message: 'Tenemos nuevo Director Ejecutivo'
img: octavioblog.png
caption:
---
¡Tenemos muy buenas noticias! Se suma a nuestra organización un nuevo director ejecutivo. El abogado chileno, **Octavio Del Favero** viene a liderar nuestra organización con una mirada que funde derechos, participación ciudadana, innovación y trabajo arduo en el fortalecimiento de las democracias de América Latina.

Octavio Del Favero es abogado de la Universidad de Chile con máster en Política Comparada de la London School Economics y con vasta experiencia trabajando en la sociedad civil y sector público. Trabajó cuatro años en [Ciudadanía Inteligente](https://ciudadaniai.org/) donde lideró la incidencia legislativa en Chile, contribuyendo en la reforma de profundización democrática en temas de corrupción, transparencia y partidos políticos. Junto con eso, coordinó la incidencia regional a través del trabajo con activistas y liderando redes de organizaciones latinoamericanas.

Sabemos que nuestras democracias están debilitadas y cuestionadas, que la crisis sanitaria ha profundizado aún más las desigualdades sociales y económicas. Sin embargo, somos optimistas frente al enorme poder de cambio que han demostrado las movilizaciones y organización ciudadana en momentos clave para la región. Nuestra tarea será impulsar cambios profundos en nuestros sistemas democráticos que reivindiquen el poder de la ciudadanía en la toma de decisiones con el propósito de construir sociedades más justas basadas en derechos y la dignidad de todas las personas y grupos.

Hoy comienza un ciclo para FCI y estamos convencides de que este nuevo liderazgo se incorporará al gran trabajo que hemos realizado todo el equipo ejecutivo y que de manera mancomunada trabajaremos en los desafíos que actualmente enfrenta América Latina. En esta nueva etapa seguiremos trabajando como agentes activos en los procesos históricos con nuevas iniciativas y poniendo nuestros más de 10 años de experiencia al servicio de una mayor participación ciudadana, rendición de cuentas e inclusión. Por que nuestro objetivo es que la voz de todos y todas sea escuchada.

Le damos la más cálida bienvenida a nuestro nuevo director ejecutivo y seguimos para adelante que en la lucha por mejores democracias, juntas y juntos somos más fuertes.
