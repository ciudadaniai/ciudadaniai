---
layout: post
title: 'Abre Alcaldías: Gestión pública innovadora, abierta y participativa en Latinoamérica'
intro: 'Más de 60 equipos de Guatemala, México y Colombia se postularon para ser parte del proyecto Abre Alcaldías'
day: 20
month: jul
date: 2022-07-20 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Abre Alcaldías: Fortaleciendo las capacidades para una gestión pública innovadora abierta y participativa en Latinoamérica'
img: abre-post-20-07-22.jpg
caption:

---

Un grupo diverso de servidores públicos locales, representantes de la sociedad civil y de la academia se embarcaron el 19 de Julio en un proceso de fortalecimiento y formación impartido por [Fundación Ciudadanía Inteligente](http://ciudadaniai.org/) en conjunto con otras seis organizaciones de la sociedad civil: [Extituto de política abierta (Colombia)](https://www.extituto.com/), [Fundación Paz & Reconciliación (Pares)(Colombia)](https://www.pares.com.co/), [Red Ciudadana (Guatemala)](https://redciudadana.org/), [Acción Ciudadana (Guatemala)](https://accionciudadanagt.org/), [Ollín AC (México)](https://ollinac.org/), [Escuela Mexicana para la Participación Ciudadana (México)](http://eparticipa.org/).

Durante la formación, se transferirán capacidades y conocimientos para la integración de herramientas, diagnóstico y recomendaciones de apertura, participación ciudadana y transparencia, con la finalidad de enfrentar la creciente desconfianza y lejanía entre ciudadanía e institucionalidad pública.

Abre Alcaldías nace en el 2020

Tras evidenciar el aumento -en participación y frecuencia- de movilizaciones y manifestaciones ciudadanas en Latinoamérica. Este escenario de crisis social demostró que la ciudadanía necesitaba más de sus gobiernos: más incidencia, más políticas públicas que respondan a su descontento, más participación, más transparencia. A su vez, los gobiernos locales han demostrado ser un espacio estratégico para la promoción de instancias de conexión y participación, pudiendo responder más directamente a la necesidad de las diversas comunidades. Adicionalmente,  debido a la acelerada urbanización y focalización económica que representan los municipios en la región, se vuelve prioritario abordar las formas de gobernanza a nivel local para responder a dichos desafíos de forma efectiva y legítima.

Durante el año 2020 y 2021 los equipos tuvieron que adaptarse a los distintos y complejos escenarios que provocó la pandemia, priorizando la tecnología y la innovación pública como aspectos necesarios para una respuesta ágil y coordinada a la crisis. Sin embargo, la apertura paulatina en la cual se enmarca esta actual iteración del proyecto trae una nueva oportunidad, permitiéndonos abordar la lejanía y la desconfianza a través de estratégias integrales; online y offline de participación, conexión e incidencia ciudadana. De esta manera, buscamos abordar los tres desafíos principales del proyecto:

Fortalecimiento de la gestión pública: A través de transferencia y práctica de herramientas, instrumentos y capacidades que permiten conectar con la comunidad
Colaboración: Promover un ejercicio de ciudadanía que contemple la colaboración y co-construcción entre diversos actores.
Participación vinculante: Consolidación espacios participativos vinculantes en donde la comunidad pueda ser parte de la toma de decisiones

Creemos que nuestras ciudades cuentan con personas y servidores ya comprometidos con el fortalecimiento de nuestras democracias, las mejoras a la gobernanza y la promoción de acciones colectivas, por lo que nuestra contribución es potenciar este proceso a través de la transferencia de aprendizajes acumulados, la interconexión dentro y fuera de sus países, y la facilitación de espacios estratégicos para la incidencia. De esta forma, buscamos dar cumplimiento a nuestra misión de impulsar y fortalecer la participación y co-construcción en los procesos de toma de decisión en los territorios para fortalecer la democracia en el continente.

