---
layout: post
title: 'Contra la discriminación racial'
intro: 'El COVID-19 perpetuará las discriminaciones raciales ¡Exijamos políticas de protección!.'
day: 20
month: Mar
date: 2020-01-27 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Contra la discriminación racial.'
img: discriminacionraza.jpg
caption:
---
El 21 de marzo ya ha entrado en el calendario mundial como un momento de reflexión sobre los avances y retrocesos en la lucha contra la discriminación racial en el mundo. Si el surgimiento de una nueva extrema derecha nacionalista no ha facilitado los últimos años a los activistas contra el racismo, el momento actual parece apuntar a un período de aún más precariedad e incertidumbre para estas comunidades

En 2020 el día internacional de lucha contra la discriminación racial coincide con el punto álgido de una crisis sanitaria mortal que puede dar lugar a un intenso proceso de degradación social, económica y política: la pandemia COVID-19. Si el efecto de esta crisis en todas las clases y sectores de la sociedad es innegable, no podemos ser ingenuos sobre qué grupos serán los más afectados en este momento. En un contexto, como el de América Latina, donde la salud es una mercancía y el acceso a ella está definido por un proceso histórico de estratificación social basada en la raza, el COVID-19 puede representar décadas de regresión en nuestros indicadores de desigualdad racial.

A pesar de la diferencia en la recolección de datos demográficos racializados en los diferentes países de América Latina, el escenario es claro. Los afrodescendientes de América Latina tienen 2,5 veces más probabilidades de vivir en la pobreza crónica que los blancos o los mestizos (Banco Mundial).  La esperanza de vida de los pueblos indígenas puede ser de hasta 20 años menos que el promedio en la mayoría de los países de América Latina (ONU). Además, a pesar de que la población afrodescendiente e indígena es una mayoría demográfica en algunos países de la región - el Brasil, Bolivia, Guatemala y el Perú - siguen ocupando la minoría de los puestos de decisión política.

Este cuadro de desigualdad racial es un factor agravante de los efectos de COVID-19, y no faltan ejemplos concretos. En el Brasil, aunque la mayoría de los casos siguen estando relacionados con las clases altas, las primeras muertes se concentran en los afrodescendientes. En la mayoría de los países todavía no se ha avanzado en la información apropiada para los contextos indígenas y muchos parques de tierras indígenas siguen abiertos a las visitas. Las famosas brechas en el acceso a la salud, las condiciones de vivienda y los ingresos que marcan los cuerpos racializados en nuestros países siguen estando al margen de los debates sobre cómo enfrentar la crisis en nuestra región, pero créanme, son sin duda pistas importantes para el triste futuro que se avecina.

A pesar del nivel de alerta, ningún país latinoamericano ha presentado un paquete de medidas sociales y económicas acorde con la magnitud del desafío que enfrentará la región más desigual del mundo. En la Fundación Ciudadanía Inteligente hemos trabajado activamente en los últimos años para abordar la discriminación racial: cambiando nuestros protocolos de trabajo, apoyando proyectos que promuevan nuevas voces y cuerpos en la política como Mulheres Negras Decidem y Me Representa, y adoptando la perspectiva interseccional como punto de partida de nuestros nuevos proyectos. Como organización comprometida con la corrección de las desigualdades sociales y raciales, nos unimos a los esfuerzos mundiales para hacer frente a COVID-19 y exigimos medidas eficaces para detener la aceleración de las desigualdades en este nuevo contexto.
