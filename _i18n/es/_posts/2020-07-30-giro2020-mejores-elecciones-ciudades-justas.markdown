---
layout: post
title: 'GIRO2020: Un giro para construir mejores elecciones y ciudades más justas'
intro: '¡Ya está arriba GIRO2020! Postula a la formación Política de candidaturas municipales de Río de Janeiro.'
day: 30
month: Julio
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Revisa nuestro proyecto Giro2020'
img: giro2020.png
caption:
---
Río de Janeiro es una de las 10 metrópolis más desiguales del mundo. La Región Metropolitana, con 13 millones de habitantes distribuidos en 22 ciudades, tiene unos de los peores indicadores de desarrollo humano y social de Brasil, según el [Mapa de Desigualdad](https://casafluminense.org.br/mapa-da-desigualdade/) desarrollado por [Casa Fluminense](https://casafluminense.org.br/).

El contexto electoral municipal del 2020  ha traído consigo muchas preocupaciones y desafíos. Por ejemplo, de los 22 municipios de la Región Metropolitana, sólo siete cuentan con comisarías especializadas para mujeres y, además, el puntaje promedio del IDEB (Índice de Desarrollo de la Educación Básica) mostró que, con excepción de la capital, todos los municipios de la Región Metropolitana tienen puntajes inferiores al promedio nacional en la educación pública. En la región también se registran altas tasas de abandono escolar y la depredación o inexistencia de aparatos públicos de cultura accesibles. Esto presenta un gran reto, especialmente para los liderazgos sociales, la sociedad civil y los movimientos de activistas que han estado movilizando la política desde sus territorios, tratando de avanzar en la superación de estas desigualdades históricas.

Las tensiones políticas, ligadas a la polarización y extremismos, y la inseguridad de las elecciones de este año causada por la pandemia exigen respuestas y soluciones concretas para contener la debilidad de los procesos democráticos. En un momento en que la ciudadanía experimenta un bajo nivel de confianza en la gestión pública, donde el 79% de las personas de América Latina creen que sus países están gobernados "por unos cuantos grupos poderosos que lo hacen en su propio beneficio", según el Latinobarómetro del 2018, es necesario combatir con más fuerza que nunca  aquellas incertidumbres y dar paso a un mayor fortalecimiento de la democracia.

La crisis humanitaria mundial de salud pública del COVID-19 que hoy enfrentamos, ha intensificado este escenario de desigualdades y retrocesos de oportunidades, dejando en evidencia la urgencia de reajustar nuestras prioridades para el avance de agendas proactivas que pongan en primer lugar la justicia social y el derecho a la vida, superando las desigualdades estructurales y de emergencia en los municipios de la metrópolis.

En los últimos años, desde [Ciudadanía Inteligente](https://ciudadaniai.org/) y [Casa Fluminense](https://casafluminense.org.br/) hemos observado que diversas ciudades, barrios y territorios sufren el abandono y decadencia como resultado de las acciones de los representantes que hoy están en el poder. De hecho, cada vez más experimentamos la disminución, o la exclusión total, de la ciudadanía respecto de su participación y protagonismo en la construcción de las agendas de políticas públicas.

Las elecciones son un momento decisivo en la creación de nuestro futuro. La construcción de un nuevo imaginario de la sociedad también depende de quién protege y defiende nuestras agendas, programas y prioridades.

Para superar esta crisis sanitaria debemos actuar cuanto antes, pero  sin cometer los mismos errores que en el pasado. Es decir, para impulsar una real democracia es fundamental llevar al poder a quienes están comprometidos con los desafíos y necesidades de toda la ciudadanía. En este contexto de fragilidad institucional, polarización y desfavorabilidad política, y desgaste del tejido social, analizamos que se aproxima un desequilibrio en la disputa electoral. En los últimos años, los índices de violencia política, que culminaron con el asesinato de la ex concejala negra Marielle Franco, han aumentado drasticamente. Los grupos paramilitares y los poderes paralelos, como líderes religiosos conservadores, han aumentado su influencia electoral con prácticas injustas que se benefician de las desigualdades socioeconómicas para ganar cuerpo. Las estructuras partidarias de apoyo y financiación siguen privilegiando a los mismos grupos privilegiados.

Por todo estos escenarios y complejos desafíos, lanzamos la [Formación Política GIRO2020](https://giro2020.org/), una iniciativa que realizamos junto a [Casa Fluminense](https://casafluminense.org.br/) para entregar contenidos de políticas públicas, estrategias y herramientas de comunicación y financiación  para fortalecer y apoyar las candidaturas municipales de la Región Metropolitana de Río de Janeiro, de todos los colores políticos, pero enfocadas en la protección de los derechos humanos y la justicia social. Nuestro objetivo se centra principalmente en potenciar a la sociedad civil  para fortalecer la democracia  durante todo el proceso electoral.

Esta formación es totalmente gratuita y se llevará a cabo en un entorno de aprendizaje cerrado dentro de la plataforma [GIRO2020](https://giro2020.org/),junto a un espacio de debate virtual participativo.

El contenido del curso contiene dos pilares principales:

* Una detallada agenda propositiva basada en un diagnóstico centrado en datos y propuestas de políticas públicas para la Región Metropolitana de Río de Janeiro, a partir de la Agenda de Río y el Mapa de Desigualdad organizado por Casa Fluminense.

* Un conjunto de herramientas y estrategias sobre las tendencias del proceso electoral del año 2020, que abordan los desafíos que deberán enfrentar las candidaturas en la arena de la campaña.

Considerando los efectos de la pandemia de COVID-19 y el nuevo escenario de disputa electoral en nuestras ciudades, [GIRO2020](https://giro2020.org/) prioriza el fortalecimiento de las candidaturas desde una perspectiva sociopolítica alineada con nuestra misión y valores. Por ello, nuestra formación se centra en las pre candidaturas municipales de la región metropolitana de Río de Janeiro, preferentemente en aquellos grupos histórica y sistemáticamente vulnerados: mujeres, personas negras, jóvenes, lgbt+ en todas sus intersecciones y diversidades, y personas que participan activamente en el debate para reducir las desigualdades y aumentar el acceso a los derechos sociales.

Nuestro objetivo es concreto: impulsar mayor participación ciudadana, más transparencia, más respeto, mayor apreciación de las diversidades, y el compromiso público de reducir las desigualdades y aumentar las oportunidades.

Hoy más que nunca hemos centrado nuestros esfuerzos en la degradación democrática que se evidencia en Brasil: violencia política, abuso de poder, la restricciones a la libertad de expresión y el aumento de la desigualdad social contaminan el entorno político y debilitan el impacto de la ciudadanía en los procesos políticos. Ante esto, el año 2020 marca el inicio de un nuevo ciclo electoral municipal en el país y con él una nueva oportunidad para construir el futuro que queremos para nuestras ciudades.

**¡Construyamos nuevas agendas y nuevos protagonistas de la política en nuestras ciudades!**
