---
layout: post
title: 'Acuerdo amplio para habilitar nuevo proceso constituyente'
intro: 'Columna de Francisca Minassian en DF.cl'
day: 03
month: dic
date: 2022-12-03 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Alcanzar un acuerdo amplio para habilitar un nuevo proceso constituyente es una necesidad democrática urgente'
img: 03-12-22.jpg
caption:

---

Han transcurrido tres meses desde que se rechazó el texto elaborado por la Convención Constitucional y las negociaciones entre las fuerzas políticas en el Congreso no han logrado llegar a un acuerdo final para darle continuidad al proceso.

Alcanzar un acuerdo amplio y de forma oportuna para habilitar un nuevo proceso constituyente es una necesidad democrática urgente. Para ello, es hora de que los y las representantes políticas pasen del dicho al hecho, con la diligencia necesaria para avanzar hacia una nueva Constitución y demostrar la capacidad y voluntad de ofrecer al país un camino institucional y democrático.

Si bien se han alcanzado algunos acuerdos, la opacidad ha sido la tónica de la discusión y la política partidista ha monopolizado el debate en nombre de la urgencia a puertas cerradas de la ciudadanía. Al no existir canales de difusión oficiales y permanentes de las negociaciones que se han llevado a cabo en el Congreso, la ciudadanía carece de certezas acerca del itinerario constitucional y de los contenidos oficiales de los acuerdos alcanzados.

<b>Honrar la soberanía</b>

A la fecha, las distintas fuerzas que están sentadas en la mesa oficial de negociación en el Congreso se encuentran en un tira y afloja por definir las características de la institucionalidad pública que redactará un nuevo texto constitucional. Esta demora, además de la falta de transparencia en las negociaciones y la actitud complaciente de las dirigencias políticas han aletargado las expectativas e interés de la ciudadanía en el tema constitucional, ya que no han sido capaces de responder a esta urgencia ciudadana y democrática.

En los tiempos que corren es fundamental poder contar con representantes políticos que honren la soberanía del pueblo y demuestren un entendimiento acabado de las lecciones que ha dejado este proceso constituyente desde sus orígenes en la crisis social de octubre de 2019.

La legitimidad democrática de una nueva Constitución se juega justamente en que le pertenezca a la ciudadanía, incidiendo en su elaboración y validando su contenido en tanto responda sus grandes necesidades y anhelos.

Es vital que sectores políticos de convicciones claras y raíces democráticas no cedan los estándares mínimos democráticos ante la conveniencia electoral de sus partidos. Un órgano 100% electo por la ciudadanía, paridad, escaños reservados de pueblos originarios y mecanismos de participación ciudadana incidentes son los mínimos necesarios para poder dotarnos de una Constitución que represente plenamente a la ciudadanía en toda su diversidad y que atienda sus exigencias históricas.

<b>El mandato democrático</b>

Ante una crisis de confianza hacia las instituciones y la política parece atractivo querer recurrir a soluciones técnicas por temor a repetir los supuestos errores del proceso anterior.

Sin embargo, sería un grave error creer que un desafío político y democrático de estas dimensiones se resuelve a través de la designación de personas expertas, que además de subestimar a la ciudadanía, sirve como pretexto para aparentar neutralidad ideológica.

De esta manera, para responder a una necesidad de legitimidad democrática de esta profundidad debemos hacerlo con más democracia y participación, no con menos.

El mandato democrático de la ciudadanía exige a las y los representantes políticos que se comprometieron con un nuevo texto que aceleren el proceso constitucional con transparencia, participación de la sociedad civil y puertas abiertas. Gozamos como país de una trayectoria de instituciones sólidas y una democracia sana y estable, no claudiquemos en la tarea de continuar por esa ruta.