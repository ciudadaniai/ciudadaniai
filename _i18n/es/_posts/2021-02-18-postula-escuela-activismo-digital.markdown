---
layout: post
title: '¡Sé parte de la Escuela de Activismo Digital!'
intro: 'Herramientas tecnológicas completamente gratuitas para implementar tu proyecto en tiempos de crisis.'
day: 18
month: feb
date: 2020-03-21 12:00:00 -0300)
categories: Proyecto
highlighted: false
share_message: 'Escuela de Activismo Digital'
img: EAD.png
caption:
---
Los ritmos de expansión del COVID delimitaron no solo la reunión de personas en las calles y la acción colectiva, sino que también confinaron la ciudadanía al espacio digital. El activismo como lo conocíamos cambió radicalmente y frente a ese escenario es que decidimos lanzar la convocatoria: [Escuela de Activismo Digital](https://docs.google.com/forms/d/e/1FAIpQLSeGj_JRok1gg8IM1WsjmlRZ4P_lUvmQAM7UvB8lCmuRitsa1g/viewform). Una iniciativa dirigida a activistas e toda América Latina que trabajan por la justicia social y por apoyar la participación de quienes han sido histórica y sistemáticamente excluidos de los espacios de poder.

Nuestra iniciativa tiene el objetivo de entregar metodologías y herramientas digitales completamente gratuitas que hacen posible ejercer activismo más allá de las calles. Las postulaciones estarán abiertas hasta el **28 de febrero** y podrán participar activistas de toda América Latina que cuenten con acceso a un equipo computacional con internet, conocimiento básico de uso de redes sociales y muchas ganas de aprender a realizar activismo, utilizando medios y herramientas digitales.

Nuestra subdirectora, Colombina Schaeffer, explicó que “la pandemia y el distanciamiento social han afectado profundamente la posibilidad de realizar acciones de activismo. Muchas personas y organizaciones se organizan y presentan sus demandas ante las autoridades a través de la presencia y la interacción cara a cara. Es por esto que la pandemia pone de manifiesto otra gran desigualdad: sin herramientas para trabajar y activarse en el mundo digital, muchas comunidades y personas se verán nuevamente excluidas de las esferas de poder. Transitar a lo digital tiene complejidades, y por eso en Ciudadanía Inteligente ponemos esta herramienta al servicio de la ciudadanía para formarse en activismo digital”.

El Coronavirus no sólo hizo más evidente la fragilidad de la democracia, sino que también eliminó la posibilidad de que la ciudadanía y los movimientos sociales se manifestaran en el espacio físico. Desde Ciudadanía Inteligente sabemos que hoy las tecnologías han ganado terreno como la principal forma en que la ciudadanía puede ejercer la acción política. Sin embargo, existe un gran número de personas que no conoce los riesgos de las tecnologías y cómo utilizarlas para hacer incidencia. Por esto, hacemos un llamado a todos quienes quieran aprender a hacer incidencia segura y masiva a postular a la [Escuela de Activismo Digital](https://docs.google.com/forms/d/e/1FAIpQLSeGj_JRok1gg8IM1WsjmlRZ4P_lUvmQAM7UvB8lCmuRitsa1g/viewform).
