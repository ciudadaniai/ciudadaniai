---
layout: post
title: 'La lucha por la desmilitarización en Honduras'
intro: 'Exigimos justicia para activistas y la prohibición de la intervención antiética de EE.UU en Centroamérica.'
day: 08
month: Ago
date: 2019-08-08 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'La lucha por la desmilitarización en Honduras @ciudadaniai.'
img: 09-08-19.png
caption: 
---
***Texto escrito por Kimberly Duron, apoyo de internacional de Ciudadanía Inteligente***

Escándalos de corrupción, narcotráfico, pobreza extrema y altas tasas de femicidio, son resultados del contexto de gobernanza en Honduras.  Si damos un paso atrás, las causas fundamentales de este mal tienen una clara la relación con el brutal asesinato contra Berta Cáceres. Berta, una activista ambiental y de los derechos humanos de origen Lenca indígena, fue asesinada durante una batalla ardua, protegiendo tierra indígena de un embalse hidroeléctrico con claros intereses políticos y privados. 

La conexión entre el abuso de derechos humanos en centroamérica, como en el caso de Berta, suele omitir el contexto de responsabilidad por parte de los EE.UU. Sin embargo en un [artículo de The Guardian](https://www.theguardian.com/world/2017/feb/28/berta-caceres-honduras-military-intelligence-us-trained-special-forces) se reveló evidencia clave de un asesinato extrajudicial que involucra a tres partes: La primera es la DESA (la empresa detrás del embalse). El segundo es el gobierno hondureño, dado que estaba a cargo de la protección de activistas, como la de Berta. Y tercero son las fuerzas armadas de los EE.UU, puesto que fueron ellas las que  entrenaron por lo menos a dos de los ocho hombres arrestados en conexión al asesinato de Cáceres. 

En este contexto, siete hombres han sido [declarados culpables](https://www.telesurenglish.net/news/7-Sentenced-for-Environmental-Activist-Berta-Caceres-Murder-20181129-0036.html) por la corte suprema de Honduras y 16 oficiales del sector público y privado, incluyendo el presidente de DESA, fueron acusados este año por fraude y actividad ilícita respecto del asesinato de Cáceres. Además, el presidente de DESA, Roberto David Castillo Mejía, fue subteniente de Inteligencia Militar en la FF.AA. de Honduras, y se graduó de la academia militar notoria de los EE.UU., [West Point](https://criterio.hn/2018/03/04/quien-noveno-implicado-capturado-caso-berta-caceres/). Aunque los Estados Unidos busquen constantemente desligarse de su intervención extranjera antiética, no cabe duda su responsabilidad de encauzar millones de dólares tributarios de ciudadanos para financiar y facilitar violencia en centroamérica, y realizar operaciones encubiertas, usualmente en nombre de la democracia.

Tras años de asumir que los disturbios sociopolíticos, específicamente en Honduras, son producto de su propia gobernanza, un nuevo proyecto de ley en el congreso de los Estados Unidos está comenzando a cambiar la conversación y posiblemente la historia. Se trata del acta H.R.1945, o el Acta Berta Cáceres de Derechos Humanos en Honduras, que fue presentado por el congresista Hank Johnson en conjunto con 43 colegas, incluyendo a la representante Alexandria Ocasio-Cortez. 

De ser aprobado, el acta suspende la ayuda militar de los EE.UU., incluyendo entrenamiento y armas, a las fuerzas de seguridad hondureñas, y “desalentará las prestaciones de bancos de desarrollo multilaterales hasta que el gobierno hondureño investigue y persiga a aquellos en los cuerpos militares o policíacas que han violado los derechos humanos” y “hasta que los defensores de los derechos humanos sean protegidos”. A favor del acta, la [ONG CARECEN](http://www.carecen-la.org/) llevó a cabo una [presentación en el congreso](http://www.carecen-la.org/carecen_la_hosts_congressional_briefing_links_refugee_crisis_to_us_military_aid_in_central_america_exposes_inhumane_treatment_of_migrants_in_southern_border) este mes, en donde un “panel de expertos jurídicos y políticos, solicitantes de asilo y observadores de derechos humanos” plantearon el tema de las violaciones de los derechos en Honduras por el financiamiento y ayuda militar de los EE.UU. La redacción del acta deja con más responsabilidad al gobierno hondureño que al de Estados Unidos y sugiere un paro de financiamiento claramente provisional. Sin embargo es un hito importante en la relación de las dos regiones.

##La intervención dañina de los Estados Unidos en Centroamérica

Mientras que la H.R.1945 se enfoca en la ayuda militar en Honduras, el apoyo de los EE.UU. y las estrategias en la región casi siempre han sido vinculadas al fomento de acuerdos comerciales/incentivos fiscales y/o la detención de la presunta expansión del comunismo. Esto es una de las razones por las que Honduras, Guatemala y El Salvador han sido históricamente llamados la “República Bananera”. Para los años 30’, The United Fruit Company, (empresa estadounidense ahora llamada Chiquita Brands) era propietaria de 3,5 millones de hectáreas de tierra en todo centroamérica, explotandola para exportar la fruta favorita de EE.UU.: [bananos](https://www.statista.com/statistics/477475/us-most-consumed-fruit-and-fruit-products-by-type/).

Posteriormente, los motivos de involucramiento estadounidense, a lo largo de dos siglos, entrelazan los intereses privados y políticos. Los siguientes son algunos ejemplos; el escritor Mark Tseng-Putterman recopiló una [línea de tiempo(https://medium.com/s/story/timeline-us-intervention-central-america-a9bea9ebc148) extensa sobre la intervención en la historia del Triángulo del norte (El Salvador, Honduras y Guatemala), aunque ésta excluye el rol que jugó los EE.UU. en la [guerra civil de Nicaragua](https://www.globalresearch.ca/the-cias-dirty-war-in-nicaragua/5629008). Estos hitos han exacerbado los problemas de la región y son algunas de las causales de la crisis migratoria que hoy vivimos:

* 1950: La expansión de United Fruit Company (UFC) en Guatemala se vio amenazada por las reformas agrarias propuestas por el presidente (electo democráticamente) el izquierdista Jacobo Arbenz Guzmán, que catalizó una campaña agresiva por parte de los EE.UU. para perseguirlo como comunista (sin siquiera serlo)

* 1954: El presidente Eisenhower ordenó una operación encubierta de la CIA que [derrocó con éxito](https://www.prwatch.org/news/2010/12/9834/banana-republic-once-again) al presidente Arbenz.

* 1975: El presidente de UFC, Eli Black, se suicidó tras las noticias de que había sobornado al presidente hondureño con $1,2 millones de dólares a cambio del apoyo para la reducción de aranceles de exportación de los bananos.

* 1996: Bill Clinton aprobó [IIRIRA](https://www.vox.com/2016/4/28/11515132/iirira-clinton-immigration), una reforma migratoria sin precedentes que deportaba a miles de centroamericanos y que incluía a aquellos en maras estadounidenses como la MS-13, provocando más violencia entre las pandillas en la región, y que hoy son la causa de porqué los migrantes deben huir de sus propios países. 

* 2017: Desde el golpe militar en Honduras en el 2009, que fue poyado por el gobierno de EE.UU., los Estados Unidos ha invertido casi [$114 millones de dólares en ayuda de seguridad](https://www.theguardian.com/world/2017/dec/07/crisis-of-honduras-democracy-has-roots-in-us-tacit-support-for-2009-coup) para establecer unidades militares y policiacos de élites, con el objetivo, según dicen, de aumentar la seguridad fronteriza y llevar a cabo operaciones contra el narcotráfico. 


Hoy el presidente Trump sigue impulsando su promesa de construir un muro como una presunta solución a la crisis migratoria, lo que  costaría más de $20 mil millones de dólares, excluyendo el mantenimiento, y que ni siquiera abordaría las causales de la crisis, ni sus efectos, como el aumento de [terrorismo doméstico](https://www.bbc.com/news/world-us-canada-49226573) a base de retórica anti-migratoria. Una real solución para la crisis migratoria es la que se propone en el [Acta de Berta Cáceres](https://www.congress.gov/bill/116th-congress/house-bill/1945/text). En [Ciudadanía Inteligente](https://ciudadaniai.org/) apoyamos las acciones para comenzar a curar las heridas que fueron hechas por las políticas extranjeras, y sobre todo por las políticas intervencionistas de los EE.UU. y que ocurren desde hace décadas. En nuestra lucha por la justicia y el apoyo de todos aquellos que protegen los derechos humanos, creemos que estos pasos hacía la responsabilidad y la reducción de las FF.AA. estadounidenses en la región son obligatorias para prevenir el éxodo de más centroamericanos y evitar, para siempre, más asesinatos a activistas como el caso de Berta.

¡Luchemos por democracias más justas!