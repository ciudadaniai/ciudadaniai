---
layout: post
title: 'Brasil se enfrenta al mayor dilema democrático de los últimos 30 años'
intro: 'Este domingo, Jair Bolsonaro y Lula da Silva se enfrentan en el ballotage'
day: 28
month: oct
date: 2022-10-28 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Brasil se enfrenta al mayor dilema democrático de los últimos 30 años'
img: 28-10-22.jpg
caption:

---

Columna publicada en Infobae.com

Por María Luiza Freire

A dos días de la segunda vuelta de las elecciones presidenciales, Brasil se enfrenta a su mayor dilema democrático y probablemente a las elecciones más decisivas desde el retorno de la democracia en 1988.

El país formó parte de la ola de movilizaciones ciudadanas en América Latina fundadas en el descontento popular hacia los gobiernos, la desconfianza institucional por los escándalos de corrupción, el clamor por la renovación política y la recuperación económica.

Este momento también estuvo marcado por el deterioro democrático y económico que ocurrió en la región entre 2010 y 2020. Una evidencia de aquello es que solo 14% de la población en América Latina apoya y confía en la democracia como sistema de organización político, según datos de Latinobarómetro. Lo anterior apunta a un gran descontento social por las élites políticas y los partidos políticos tradicionales que han ocupado los cargos de representación política en los últimos años.

Son precisamente las consecuencias de este escenario regional con las maniobras políticas del impeachment de la presidenta Dilma Roussef, la persecución judicial politizada del expresidente Lula en la Operación Lava-Jato para impedir su candidatura en las elecciones de 2018, la elección del presidente Bolsonaro y la crisis promovida por el covid-19, cicatrices que marcan una profunda crisis sociopolítica e institucional en la que los candidatos se disputan estas elecciones.

El primer debate presidencial en la segunda vuelta marcó el tono de la campaña y surgieron fantasmas del pasado en un intercambio plagado de ataques personales, con foco en la agenda de valores y propagación de desinformación por parte de Bolsonaro, mientras Lula buscó renovar la esperanza de su gobierno pasado, promover su plataforma de propuestas y reaccionar a las provocaciones.

La evaluación general al día siguiente al debate fue que la disputa no promovió ganadores, sino la preocupación por el aumento del rechazo político de personas indecisas respecto a su voto y el incremento de las críticas a los candidatos.

Tras la primera vuelta, Bolsonaro es derrotado, pero el bolsonarismo se institucionaliza. El actual líder del poder ejecutivo sigue siendo el primer presidente en funciones que pierde la reelección en la primera vuelta y que, para una segunda vuelta, no figura en el primer lugar en las intenciones de voto.

Lula se encuentra en primer lugar en las encuestas y vale destacar que la única vez que el PT (Partido de los Trabajadores) tuvo un índice de aprobación tan alto en unas elecciones fue en el año 2006, cuando Lula se encontraba en la presidencia, con un índice de aprobación del 85%.

<b>Riesgos para la democracia brasileña</b>

Hay dos puntos que me parecen interesantes de analizar para entender tanto el crecimiento de Bolsonaro en las encuestas electorales de las últimas semanas, como lo que puede estar en juego en el futuro de la democracia brasileña.

El primero y más grave de esta disputa es el hecho de que el bolsonarismo (Bolsonaro y sus aliados), junto con el Congreso han acabado o modificado para su propio beneficio las reglas fiscales y electorales que se construyeron en los últimos 30 años precisamente para evitar el abuso de poder político y económico.

El presidente Bolsonaro ha movilizado la maquinaria pública para consolidar apoyos y votos por medio del pánico moral, la desinformación masiva, además de solicitar votos a través de las iglesias católicas y neopentecostales, aprobar un presupuesto secreto para comprar apoyos dentro del Congreso Nacional, cuestionar y deslegitimar las urnas y el proceso electoral, incitar al odio y sostener compromisos billonarios en las redes sociales y plataformas de streaming, entre otros.

Una de las consecuencias de esta cooptación política es el nuevo diseño del Congreso Nacional del país, que presentó una votación récord para las candidaturas bolsonaristas, estrechamente vinculadas a las agendas del actual gobierno. Algunos de ellos, como el ex ministro de Salud, Eduardo Pazuello, responsable de una de las peores gestiones de la pandemia en el mundo y el ex ministro de Medio Ambiente, Eduardo Salles, que permitió al gobierno la mayor deforestación de la Amazonía de los últimos gobiernos.

Esto demuestra que el bolsonarismo va ganando espacio como marca ideológica que no sólo elige candidatos, sino que se mantiene enquistado social y políticamente en el país, perpetuando el identitarismo de extrema derecha.

También se puede considerar que la nueva composición del Congreso cuenta con la tasa de renovación más baja desde la redemocratización del país. Aunque el PT (Partido de los Trabajadores) ha aumentado su base, el PL (Partido Liberal), el partido del actual presidente, tendrá la mayor bancada. En la Cámara, algunos analistas atribuyen este impulso al uso del presupuesto secreto, un reparto de fondos públicos a cambio de favores, que podría consolidarse como la mayor trama de corrupción por su absoluta falta de transparencia y el reparto de miles de millones de dólares sin supervisión ni control social. En el Senado también, contaremos con la presencia del ex vicepresidente Mourão y de Sérgio Moro, que vuelve a ser aliado del bolsonarismo.

Los retos para la gobernabilidad de quien sea elegido, es enfrentar un Congreso con mayor musculatura política y con división entre las bancadas lo que puede significar una gran necesidad de acuerdos y barreras en la implementación de políticas.

El segundo punto, se refiere a la responsabilidad institucional con el escenario actual. Son muchos los cargos a las distintas autoridades públicas que han tenido el tiempo, las referencias y los insumos necesarios para preparar una elección tan importante y decisiva que no se veía desde 2018, sin embargo, a pesar del gran esfuerzo la actualización de los procedimientos y protocolos por parte de las autoridades electorales y judiciales ha resultado insuficientes para enfrentar: la desinformación rampante, las disputas injustas dirigidas a los más vulnerables, los debates presidenciales sin verificación de hechos, las plataformas sociales no reguladas, el uso de la máquina pública para hacer campaña y varios otros delitos electorales cometidos sin ningún tipo de medida aplicable.

El 6% de los votantes dice estar abierto a cambiar su voto, es poco, pero también podría ser decisivo para los últimos días de la campaña. El refuerzo de las agendas enfocadas en las demandas de la población del sureste es estratégico, ya que es el sector geográfico más poblado y podría ser la gran baza en la carrera.

Otra medida para impulsar el voto es la decisión del Tribunal Superior Electoral que señala a los municipios que establezcan un transporte público gratuito para que la gente más pobre y periférica pueda votar. Otro público objetivo de las dos campañas.

<b>¿Cuáles son las consecuencias externas de la elección?</b>

Un gigante como Brasil podría tener consecuencias devastadoras para la región en los próximos años, dependiendo del resultado de las elecciones.

De resultar elegido Lula se uniría al campo de los nuevos líderes de izquierda elegidos con agendas más progresistas en latinoamérica, como Chile y Colombia. Su candidatura apunta a cumplir promesas de los mandatos anteriores en el sentido de que promueve el apoyo a las instituciones regionales multilaterales, la cooperación entre los países del Sur Global, en función de las elecciones norteamericanas, el fortalecimiento de las relaciones económicas, el diálogo con la Unión Europea y el retorno a un papel de liderazgo en los asuntos regionales.

Bolsonaro, en su gobierno no ha respetado ninguna medida de la OMS ni del Acuerdo de París, mucho menos el Acuerdo de Escazú. En la ONU, asume una postura conservadora que se asemeja a Pakistán, Rusia y Arabia Saudita. Ha promovido una política externa de rupturas contra Argentina, Chile, Colombia, Cuba, Venezuela, China, Estados Unidos y Francia, entre otros, en una retórica antiglobalista. En un segundo mandato, puede tensionar aún más estas relaciones y distanciarse.

Lula defendió en más de una ocasión que transformaría a Brasil en un país dispuesto y capaz de contribuir a enfrentar los desafíos globales, destacando su compromiso de combatir la deforestación de la Amazonía y la defensa de los pueblos indígenas con la creación de un Ministerio Indígena. Una promesa fundamental para Brasil, que dejó de recibir apoyo económico durante este gobierno a través del Fondo Amazónico por parte de países como Alemania y Noruega debido a la postura anti ambientalista de Bolsonaro. En el actual escenario mundial, la preservación del medio ambiente se valora en las relaciones internacionales y puede ser el principal motor del crecimiento económico e inclusión social.

<b>Consecuencias internas</b>

Entre las consecuencias del plano interno, el trabajo consiste en recomponer lo que queda del pacto social, devolver la legitimidad a las instituciones democráticas y científicas, atender las demandas sociales ante un escenario complejo, reducir las brechas socioeconómicas que presenta en la actualidad las peores tasas desde hace más de 30 años, impulsar la economía y proyectar y fortalecer internacionalmente la credibilidad de Brasil.

Todo esto se comprobará el 30 de octubre.

<i>La autora es coordinadora de Proyectos e Investigación de la Fundación Ciudadanía Inteligente, abogada de la PUC-Rio, feminista y decolonialista. Máster en políticas públicas y planeamiento urbano latinoamericano en la IPPUR / UFRJ.</i>

