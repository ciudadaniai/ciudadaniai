---
layout: post
title: 'La Constitución es Nuestra'
intro: 'Plataforma abierta, colaborativa y colectiva que acerca las normas de la nueva Constitución a la ciudadanía'
day: 10
month: sept
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'La Constitución es nuestra by @ciudadaniai'
img: lcen-blog.jpg
caption:
---
La oportunidad de llevar la voz de toda la ciudadanía a la Convención Constitucional es lo que ofrecemos con nuestro proyecto: [La Constitución es Nuestra](https://laconstitucionesnuestra.cl/). Una plataforma abierta, colectiva y colaborativa que impulsamos junto a [la Iniciativa Global por los Derechos Económicos](https://www.distritoglobal.org/), [Consti Tu+Yo](https://www.constituyo.cl/) y la [FES](https://chile.fes.de/), que busca visibilizar y articular propuestas ciudadanas para la nueva Constitución que serán conectadas con el trabajo diario de las y los convencionales.

Porque queremos colaborar en el proceso constituyente es que ofrecemos la posibilidad de que organizaciones, colectivos, cabildos y personas individuales puedan crear propuestas y subirlas al [sitio](https://laconstitucionesnuestra.cl/). Además, las y los convencionales podrán comprometerse de manera digital con aquellas que más le hagan sentido de manera pública y transparente. El proyecto ya cuenta con el apoyo de más de 30 organizaciones tales como: Amnistía Internacional, Greenpeace, Corporación Humanas, Iguales, Comunidad Mujer, Techo y Fundación Vivienda, El Colegio Médico, Servicio Jesuita Migrante, Observatorio de Género y Equidad, Educación2020, entre otras.

Nuestro director ejecutivo y coordinador del proyecto La Constitución es Nuestra, Octavio Del Favero, señaló que “el proceso constituyente que hoy vivimos es gracias al arrojo y a la valentía de una ciudadanía que exige justicia y dignidad. Frente a esto, es que quisimos contribuir con una plataforma que fortalece la participación ciudadana y permite que todas las voces sean escuchadas. Invitamos a la ciudadanía, cabildos, juntas vecinales y a todas las organizaciones a subir sus propuestas para hacerlas llegar a la Convención Constitucional.”

La Constitución es Nuestra promueve propuestas que estén dentro de las facultades de la Convención Constitucional e impulsen la construcción de una Constitución que garantice  los derechos económicos, sociales, culturales, ambientales así como aquellas que fortalezcan la democracia y sean de interés colectivo. Toda la información está disponible en nuestro sitio [https://laconstitucionesnuestra.cl/](https://laconstitucionesnuestra.cl/)


<iframe width="560" height="315" src="https://www.youtube.com/embed/38HD1vXub_A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
