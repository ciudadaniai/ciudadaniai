---
layout: post
title: 'Una política fiscal para la garantía de los derechos en la nueva Constitución'
intro: '¿Cómo avanzar hacia un marco constitucional que permita poner la política fiscal al servicio de los derechos?'
day: 01
month: jun
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Una política fiscal para la garantía de los derechos en la nueva Constitución'
img: 01-06-21.jpg
caption:
---
*Columna publicada en [La Tercera](https://www.latercera.com/opinion/noticia/una-politica-fiscal-para-la-garantia-de-los-derechos-en-la-nueva-constitucion/7YCPQ3R4YZGWDJHNMYDTQYSSRM/). Foto de [RFI](https://www.rfi.fr/es/)*


Una de las lecciones de la experiencia constitucional de América Latina, es que no basta con consagrar generosas declaraciones de derechos si no se establecen los mecanismos para que estas no se queden solo en el papel. Entre esos mecanismos, las políticas de impuestos y gasto público son fundamentales. La forma como un Estado consigue sus ingresos y los destina a distintos fines es la expresión más fiel de sus prioridades. Es allí donde se definen asuntos tan importantes como la cobertura de la salud, la calidad de la educación pública o el compromiso con la igualdad de género y el cuidado del medio ambiente.

Tan claro como que los derechos precisan de recursos para su realización, debería ser que las obligaciones nacionales e internacionales de los estados con los derechos sociales tendrían que guiar las decisiones sobre cómo se obtienen, distribuyen y utilizan los recursos públicos. Muchas voces hasta ahora han insistido exclusivamente en lo primero, a veces para justificar la inacción por parte del Estado frente a las demandas sociales con el argumento de la insuficiencia de recursos o de la afectación a la sostenibilidad fiscal. La redacción de una nueva Constitución es una oportunidad para diseñar un marco institucional para que decisiones se tomen en base a principios de derechos humanos, al tiempo que se toma en serio el buen manejo de las finanzas públicas.

Pensando en este desafío, un grupo de organizaciones de América Latina, junto a un comité de expertas y expertos de variadas disciplinas, han publicado los Principios de Derechos Humanos en la Política Fiscal. Éstos plantean que la política fiscal debería cumplir las siguientes funciones para alinearse con los derechos: 1) movilizar recursos suficientes, 2) redistribuir el ingreso y la riqueza (y cerrar las brechas de género y otras formas de desigualdad), 3) regular conductas a través de incentivos para proteger los derechos (como en el caso de los impuestos verdes o a las bebidas endulzadas), 4) reactivar o estabilizar la economía conforme al ciclo en que se encuentre, 5) representar de forma democrática y transparente las preferencias de la población en torno a las decisiones sobre los recursos públicos, y 6) contribuir a reparar el legado de exclusión social y daño ecológico.

¿Cómo avanzar hacia un marco constitucional que permita poner la política fiscal al servicio de los derechos, en vez de ponerle límites a su alcance como ha venido sucediendo hasta hoy? Ese es el debate que proponemos, y al cual llamamos a organizaciones de distintos sectores a sumarse para ampliar la mirada tradicional sobre cómo ha sido concebida y discutida la política fiscal en nuestro país y, así, ser capaces de responder de forma efectiva a las demandas ciudadanas.


**Autores**

**Vicente Silva**, Representante Latino América de la [Iniciativa Global de Derechos Económicos, Sociales y Culturales](https://www.gi-escr.org/)

**Sergio Chaparro**, Oficial de Programas del [Centro de Derechos Económicos y Sociales](https://www.cesr.org/)(CESR)

**Octavio Del Favero**, Director Ejecutivo de [Ciudadanía Inteligente](https://ciudadaniai.org/)
