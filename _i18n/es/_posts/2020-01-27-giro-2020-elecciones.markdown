---
layout: post
title: 'Un giro para construir mejores elecciones'
intro: 'Muy pronto Giro2020. Nuestra iniciativa que será parte de todo el proceso electoral de Brasil.'
day: 27
month: Ene
date: 2020-01-27 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Muy pronto Giro2020. Nuestra iniciativa que será parte de todo el proceso electoral de Brasil.'
img: giro_blog.png
caption:
---
En [Ciudadanía Inteligente](https://ciudadaniai.org/), llegamos a Brasil en 2017 con la misión de contribuir en una de las democracias más complejas y desafiantes de América Latina. Desde nuestra oficina en Río de Janeiro hemos denunciado y centrado nuestros esfuerzos en los acontecimentos de los últimos años que son un foco de preocupación sobre la degradación democrática en todo el mundo. La violencia política, el abuso de poder, la restricción de la libertad de expresión y el aumento de la desigualdad social contaminan el entorno político y debilitan el impacto de la ciudadanía en los procesos políticos. Ante esto, el año 2020 marca el inicio de un nuevo ciclo electoral municipal en Brasil y con él una nueva oportunidad para construir el futuro que queremos para nuestras ciudades.

Por eso creamos el proyecto **GIRO 2020**, una propuesta de [Ciudadanía Inteligente](https://ciudadaniai.org/) en conjunto con la [Casa Fluminense](https://casafluminense.org.br/) para hacer  elecciones más participativas y representativas, con una agenda de políticas públicas con trabajo en los territorios y alineada con las necesidades de la ciudadanía. Mucho más allá de la votación, nuestro giro es un viaje que comienza en este año electoral, atravesará toda la jornada electoral y luego avanzará después de los resultados de la votación, monitoreando la gestión de nuestras ciudades y autoridades.  

En febrero comenzaremos una serie de reuniones con la sociedad civil en Río de Janeiro donde hablaremos de los cambios recientes en nuestro país y de los desafíos que debemos enfrentar para fortalecer los espacios de participación y las redes de monitoreo en nuestras ciudades. Con el aprendizaje de este movimiento y en diálogo con más de 100 organizaciones y colectivos consolidaremos una agenda con propuestas concretas para los principales desafíos que enfrentan las ciudades de Río de Janeiro.

Comprometidos con el aumento del impacto de las demandas de la ciudadanía en el período electoral y postelectoral, llevaremos a cabo procesos de capacitación con más de 100 candidaturas, así como la construcción de un proceso colectivo de movilización territorial y seguimiento de propuestas enfocado en una red capacitada de 50 líderes territoriales.

Con **GIRO 2020** invitamos a la ciudadanía a permanecer unida y fuerte, a revertir los retrocesos de los derechos y a disputarse una vida más digna en nuestras ciudades. Por qué sí, no nos damos por vencidos.
