---
layout: post
title: '¡Quédate en la casa!'
intro: '¿Qué necesitas para hacer un buen trabajo remoto? Revisa nuestra experiencia.'
day: 17
month: Mar
date: 2020-01-27 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Quédate en la casa by @ciudadaniai.'
img: coronaviruscolor.png
caption:
---
Lo más importante para lograr un gran trabajo remoto es tomarlo con seriedad, pero sin volvernos locas ni locos. Hoy vivimos un momento difícil debido a la propagación del Coronavirus y por eso quizás nos cueste más concentrarnos y dejar de ver las noticias. En [Ciudadanía Inteligente](https://ciudadaniai.org/) hemos trabajado por casi 2 años con facilidades remotas. Tenemos oficinas físicas en Chile y Brasil, pero muchxs trabajan desde diferentes rincones del mundo. Por eso desde [Ciudadanía Inteligente](https://ciudadaniai.org) queremos entregar algunos consejos y recomendaciones para trabajar desde la casa y que para nosotrxs han sido sumamente útiles.

**AUTOCUIDADO**

**Comenzamos con lo primero: el autocuidado. Es normal no poder concentrarse, mirar el infinito, revisar noticias, redes sociales o ponernos a ordenar la cocina. No estamos acostumbradxs a estar tanto tiempo en casa y trabajar. Pero acá damos unos tips para no desesperarse.**

* Levantarse temprano y ducharse. Quedarse en pijama no es una buena idea.

* Dedicar un espacio de trabajo separado del resto de la casa (si es posible). Nunca desde la cama.

* Dedicar tiempo para tomar desayuno, almuerzo y snacks con calma.

* Agendar horarios de descanso.

* Para los tiempos de descanso recomendamos escuchar algún podcast o música. No es buena idea revisar las redes sociales. Eso sólo nos genera más ansiedad.

* No trabajar por medio de las redes sociales como WhatsApp. Hay muchas herramientas de trabajo que podemos usar (más adelante se las explicaremos) y que nos harán mejorar la concentración.

* Paciencia para quienes no tienen buena conexión a internet.

* Paciencia y respeto para quienes están en casa con hijxs o con más personas.

* No podemos exigirnos tener el mismo ritmo de la oficina en casa. Eso toma tiempo.

* No realizar reuniones durante la hora de almuerzo (el hambre distrae a todxs).

* Saludar nos llena de energía. Recomendamos iniciar el día con una llamada de equipo para saber cómo están todxs, responder preguntas y dar un empujón para el día que se viene.

* Hacer gimnasia de pausa para estirar músculos y relajarse. [Acá](https://www.youtube.com/watch?v=PBZA6Pg9VxI) un video que puede ser útil

* Accountability de nuestras tareas una vez a la semana para que nuestro jefx y/o equipo esté al tanto de nuestro trabajo.

* No estar conectado las 24/7 con las noticias. Eso sólo genera estrés.


**REUNIONES Y TRABAJO DIARIO**

**Hoy es un gran momento para aplicar “esta reunión pudo haber sido un mail”. Tratemos de mostrar nuestras mejores habilidades comunicativas en mails o incluso videos. Acá algunas recomendaciones para reuniones y trabajo diario remoto.**

* Utilizar herramientas o espacios virtuales para reuniones como [Hangouts meet de Google](https://gsuite.google.com/intl/es-419/products/meet/), [Zoom](https://zoom.us/) o incluso [Skype](https://www.skype.com/es/).

* Todas las reuniones deben comenzar con un objetivo claro junto a sus expectativas.

* Debe haber una persona que modere la conversación para que nadie se interrumpa. Todxs deben pedir la palabra y además es fundamental que haya otra persona tomando acta de la reunión.

* Recomendamos usar [Documentos Drive](https://www.google.com/intl/es-419_cl/docs/about/) (aunque sea google… lo sabemos) para que todxs puedan revisar y crear documentos en conjunto.

* Usar [Google Calendar](https://calendar.google.com/calendar/). Es una buena forma de ver en qué está el equipo y agendar reuniones sin topes.

* También recomendamos herramientas para organizar tareas como [Asana](https://asana.com/es), [Todoist](https://todoist.com/es/), [Trello](https://trello.com/es), [Miro](https://miro.com/?utm_source=google&utm_medium=cpc&utm_campaign={_utmcampaign}&utm_term=miro&utm_content=421787753483&xuid=EAIaIQobChMIj76B__-f6AIVgg6RCh26yQSXEAAYASAAEgKeefD_BwE&gclid=EAIaIQobChMIj76B__-f6AIVgg6RCh26yQSXEAAYASAAEgKeefD_BwE), entre otras que nos ayudan a revisar qué cosas tenemos pendientes, priorizar y delegar tareas a compañerxs.


**NO AL AISLAMIENTO**

Hay muchas salas virtuales y gratuitas donde todo el equipo puede estar conectado al mismo tiempo como [Slack](https://slack.com/intl/es-cl/lp/three?utm_medium=ppc&utm_source=google&utm_campaign=d_ppc_google_latin-america-carribbean_es_brand-hv&utm_term=slack&ds_rl=1249094&gclid=EAIaIQobChMIoKv2zoOg6AIVkIeRCh3InwXYEAAYASAAEgKas_D_BwE&gclsrc=aw.ds), [Riot](https://about.riot.im/), [Discord](https://discordapp.com/) e incluso la ya nombrada [Hangouts meet de Google](https://gsuite.google.com/intl/es-419/products/meet/). Es importante que todxs se sientan acompañadxs. Si estamos todxs conectados es más fácil seguir la rutina diaria de trabajo. Es importante evitar que los canales de contacto sean nuestras redes sociales, dado que generará desconcentración y muchas veces desordena el trabajo.

Desde [Ciudadanía Inteligente](https://ciudadaniai.org/) sabemos que no es fácil. Nos costó llegar al punto de sentirnos cómodxs con el teletrabajo, pero ahora lo amamos. Nos permite ser más flexibles, autónomxs y tener la oportunidad de trabajar desde cualquier lugar del mundo. A veces puede ser agotador, pero recuerda que no eres el único. Todo tu equipo (y el mundo) está trabajando desde casa. Y siempre puedes hacer pausas y pedir ayuda para que de a poco recuperemos el ritmo.

Para hacer mejores democracias nos necesitamos sanos y sanas #QuédateEnCasa.

Otras fuentes: [https://staythefuckhome.com/](https://staythefuckhome.com/)
