---
layout: post
title: 'El COVID no es pretexto para amenazar la libertad en Internet'
intro: 'Revisa acá nuestras 8 propuestas para reconfigurar Internet post-pandemia.'
day: 17
month: junio
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'El COVID no es pretexto para amenazar la libertad en Internet'
img: es-blog.png
caption:
---
América Latina atraviesa un periodo de incertidumbre marcado por un profundo descontento social y una acelerada y opaca marea de cambios sociales y políticos producto de la emergencia sanitaria. En el 2019, países como Chile, Colombia o México vivían momentos convulsos donde, a través de la protesta social, se exigían mejoras y expansión de derechos. Miles de personas unían su hartazgo intentando transformar el orden social y reclamando soluciones a problemas estructurales como la corrupción y la violencia.

Pero llegó 2020 y el accionar ciudadano al que estábamos acostumbrados simplemente dejó de ser posible. Si bien hemos visto que las protestas contra el racismo y el abuso policial no han impedido la concentración de personas en México, Brasil o fuera de la región, la emergencia sanitaria ha recortado los derechos ciudadanos e incrementado los poderes de los gobernantes para vigilar, controlar y, en muchos casos, legislar por decreto. Los gobiernos nacionales y locales están adoptando nuevas tecnologías y sistemas de gestión de datos para hacer frente a la emergencia de salud pública y el trauma económico general, aumentando la dependencia de los sistemas basados en datos para rastrear enfermedades y perfilar a la población. Los ritmos de expansión del COVID-19 delimitaron no solo la reunión de personas en las calles y la acción colectiva, sino que también confinaron la ciudadanía al espacio digital, un dominio mayoritariamente privado, vigilado y opaco.

Estos cambios no son [temporales](https://www.technologyreview.com/2020/03/17/905264/coronavirus-pandemic-social-distancing-18-months/) ni [intrascendentes](https://www.nesta.org.uk/blog/there-will-be-no-back-normal/), sino que cambian radicalmente la forma en que entendemos el ejercicio efectivo de los derechos humanos y políticos. Es tal vez nuestra cotidianidad la que ya empieza a vivir esta nueva normalidad. Hemos recurrido a la virtualidad para el trabajo, la escuela, los activismos y las relaciones cotidianas. Pero la digitalización de las interacciones sociales genera cuestionamientos por parte de la sociedad civil por el falso dilema al que nos enfrentan las acciones de muchos gobiernos: el de la salud o la privacidad.

Esto ocurre por la forma poco democrática en la que se han configurado las relaciones sociales en el ámbito digital. Por ello, controlar y enfrentar la crisis sanitaria a través de la tecnología ha acentuado las desigualdades y nos ha propuesto retos urgentes para reconfigurar un espacio digital que le sirva a la democracia.

Uno de ellos es reconocer Internet como espacio público que habitamos y que nos permite ejercer derechos y libertades: como libertad de expresión, reunión, privacidad. También cuestionar si Internet es actualmente un espacio democrático e identificar qué condiciones pone en peligro nuestra vida democrática en espacios digitales.

Si apuntamos a una nueva normalidad ¿qué cambios estructurales necesitamos extrapolar  al ámbito digital? ¿qué discursos, ideas y concepciones sobre la tecnología obstaculizan o amenazan una **Internet de derechos y libertades que permita la acción colectiva y la participación ciudadana?**

No hay momento más urgente para **reconfigurar las narrativas o discursos predominantes sobre Internet y democracia** y proponer alternativas que apunten hacia otra dirección. Sin embargo, la participación de la sociedad civil en los debates sobre los derechos digitales sigue siendo limitada a pocas organizaciones, por lo que es vital sumar voces diversas, desde el ambientalismo, las luchas de género, la defensa de los derechos humanos, y muchas otras.

Estas son algunos de los discursos que vemos reproducirse con la actual crisis sanitaria y ante los cuales proponemos otros caminos para un bien común.

* Hoy Internet es vista como “un mundo no real” (ejemplo: el mundo real y el mundo digital).

***Proponemos reconocer internet como un espacio público que compartimos y habitamos. La red también es un espacio real, con impactos tangibles en nuestras vidas.***

* El uso mayoritario de la Internet y las tecnologías digitales se ha volcado a los negocios y la propaganda. Ello ha abierto la puerta al incremento de la vigilancia gubernamental y la censura a voces disidentes. Actualmente Internet y el rastreo que permite la actividad digital son usadas por gobiernos autoritarios para ejercer control y represión.

***Defendamos Internet como bien común, como espacio para afirmar y defender derechos y la acción colectiva. Defendamos las tecnologías digitales como herramientas que apoyan procesos de cambio social, de organización, reunión y participación. ¡Defendamos una Internet que fortalezca la democracia!***

* Unos pocos grandes actores tienen el monopolio para definir cómo funcionan las redes y sistemas que utilizamos, cómo interactuamos, qué vemos, qué contenidos se privilegian.

***Pero todxs creamos contenido y relaciones socio digitales, identidades y nuevas formas de pensar-hacer a través de estos sistemas. No somos simples usuarios, una buena parte de nuestra vida democrática ocurre allí mismo. Es necesario entonces hacer más democrática y abierta la gestión de estos sistemas.***

* Las tecnologías digitales son vistas como una solución a todo tipo de problemas.

***Sin embargo, es clave someter a deliberación democrática la implantación de nuevas tecnologías. ¿Qué implican? ¿Cómo funcionan? ¿Cuáles son los riesgos relacionados? ¿A quiénes benefician y a quiénes perjudican? ¿Cómo se hacen más beneficiosas? ¿En realidad son necesarias?***

* La vigilancia pareciera estar justificada bajo el argumento de que “no tengo nada qué esconder”.

***Creemos que la democracia y la privacidad están íntimamente relacionadas. Una sana democracia depende de la interacción de individuos autónomos. La autonomía individual depende de contar con privacidad. Defender Internet como un espacio donde nos importa la privacidad beneficia a toda la sociedad.***

* Actualmente impera un discurso del “salvajismo de internet” donde las violencias son normalizadas y se minimiza su impacto.

***Es importante no normalizar la violencia digital como parte de las interacciones de Internet, reconociendo que tiene impactos reales en la vida de las personas. La violencia digital es una extensión de la violencia “análoga”, que se hace evidente y es realizada a través de la tecnología.***

* Lo digital es visto como un mundo ilimitado en el que todo se viraliza y reproduce al infinito.

***Debemos ser conscientes de que lo digital tiene un impacto físico y social. Las visitas, los likes, las reproducciones, las interacciones implican más consumo de energía, generación de basura digital y un mayor despliegue de infraestructuras como cables marinos y centros de datos. Por ejemplo, se ha [estimado](https://theshiftproject.org/article/climat-insoutenable-usage-video/) que en 2020 sólo el streaming de video emitiría tanto CO2 como España.***

* La tecnología en contexto emergencia sanitaria ha [planteado](https://www.accessnow.org/tecnologias-de-vigilancia-para-controlar-el-covid-19-en-america-latina/) una dicotomía entre salud y privacidad.

***La tecnología puede apoyar al control de expansión de la pandemia respetando derechos y libertades. Todas las iniciativas basadas en tecnología y manejo de bases de datos personales y de salud, deben ser transparentes, respetuosas con los derechos y cumplir con medidas que protejan nuestra información. La situación de alerta que enfrentamos frente al COVID-19 no es pretexto para recopilar datos innecesarios, no mejorar las prácticas de protección y desarrollo ético de tecnología para interés público.***

Acá puedes revisar el [video](https://www.youtube.com/watch?v=yi6vovsSj7g) 

**Autores**


*Ricardo Zapata - El Derecho a No Obedecer (Colombia)*

*Beatriz Quesadas - SocialTIC (México)*

*Haydeé Quijano - SocialTIC (México)*

*Daniela Jordán - Frena la Curva (Chile)*
