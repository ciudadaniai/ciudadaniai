---
layout: post
title: 'Los privilegios de unos pocos cimentan la desigualdad de la región'
intro: 'Lanzamos nuestro proyecto DataIgualdad ¡Revísalo!'
day: 02
month: Dic
date: 2019-12-02 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Cómo los privilegios de pocos cimentan la desigualdad de la región.'
img: dataIgualdad.png
caption:
---
***En América Latina las personas más ricas pagan sólo el 4,8% de su ingreso en impuestos, siendo que en promedios debieran pagar el 27%. El sitio [Dataigualdad.com](http://www.dataigualdad.com/), desarrollado por la Ciudadanía Inteligente junto a la organización internacional Oxfam, muestra con estas cifras y más qué tan profunda es la desigualdad en la región.***


América Latina es la región más desigual del planeta en la distribución de su ingreso. Ciudadanía Inteligente junto a Oxfam, lanzaron hoy la plataforma DataIgualdad.org que revela datos y gráficas sobre cómo los privilegios que se entregan a individuos y empresas fomentan la desigualdad en América Latina y el Caribe.

La plataforma entrega, entre otros, datos procesados sobre cómo los beneficios tributarios afectan la vida de las personas de un país y también otros indicadores de desigualdad en salud, educación y género. Por ejemplo: cuántos profesores y médicos podrían contratarse con todo el dinero que las empresas dejan de pagar por privilegios tributarios en todos los países de la región.  También como una mujer latinoamericana dedica 36 horas semanales a trabajo no remunerado, mientras que un hombre destina sólo 16 horas. Asimismo, la web señala que en la región el 75% de la población cree que el gobierno gobierna para sí mismo y no para las mayorías.

La coordinadora de Laboratorio de Democracia de Ciudadanía Inteligente, Auska Ovando, dijo que “en momentos de movilización social en toda América Latina, DataIgualdad ofrece datos para actuar que muestran cómo los privilegios de ciertos grupos están negando los derechos de otros. Visibilizar esta información es clave para hacer consciente entre nuestros representantes la gravedad del problema, y entregar herramientas a la ciudadanía que le permitan exigir cambios a quienes los gobiernan”.

El proyecto de Ciudadanía Inteligente y Oxfam fue creado sobre un análisis profundo de los últimos datos disponibles de la OCDE, el Banco Mundial y CEPAL, entre otras fuentes de información.  La iniciativa, además, destaca que para garantizar los derechos de la mayoría, deben reducirse los privilegios de unos pocos.  DataIgualdad invita a la ciudadanía a participar en los procesos de decisiones con propuestas y exigir a sus gobiernos la representación de sus intereses.
