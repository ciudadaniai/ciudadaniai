---
layout: post
title: '¿Qué tanta información entregan los gob de la región sobre COVID19?'
intro: 'Nuestro informe revela cuáles son los países que entregan de manera más transparentes y eficiente los datos.'
day: 24
month: Abril
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: '¿Qué tanta información entregan los gob de la región sobre COVID19?'
img: covid19.png
caption:
---
*Datos levantados entre el 22 y 23 de abril*

En Ciudadanía Inteligente hemos trabajado hace más de 15 años por la promoción de la transparencia, la rendición de cuentas y la disminución de las brechas de información entre la ciudadanía y gobernantes. Hoy vivimos tiempos difíciles. La pandemia del COVID19 se ha insertado en nuestras vidas, y los gobiernos decretaron el aislamiento social masivo como forma de prevenir y combatir la enfermedad. La guerra por la información se convirtió en parte fundamental en la defensa de la vida y en el diseño de políticas públicas para el escenario que se avecina. Sin embargo, ¿Por qué no todos podemos conocer la información?

Por eso en Ciudadanía Inteligente decidimos evaluar la calidad de apertura de datos oficiales publicados por los gobiernos de América Latina sobre el COVID-19. Revisamos las cifras en los 20 países de la región, considerando dos categorías de información: estadísticas generales y microdatos anonimizados, es decir, la desagregación de datos que permite identificar la información del paciente, pero al mismo tiempo ocultando su información sensible sin vulnerar la protección de sus datos.

Los países mejor evaluados en la publicación de estadísticas generales fueron: **México Colombia y Ecuador**, y aquellos con peor puntaje fueron **Guatemala y Nicaragua**. Solamente ocho países publican la información (de manera total o parcial) en formato de base de datos. Además, Chile obtuvo el quinto lugar en este ranking, aunque mejoraría significativamente su posición al publicar mayor detalle a nivel comunal.

Respecto de los microdatos anonimizados, sólo cuatro países de la región proporcionan esta información: **México**, **Colombia**, **Ecuador** y **Cuba**.

A raíz de la actual pandemia, se han creado innumerables iniciativas desde la sociedad civil y desde los principales usuarios de los datos relacionados al COVID-19. En este contexto, proveer datos más robustos y más abiertos a estos grupos significa una oportunidad para que los gobiernos puedan compartir sus decisiones a la sociedad civil con el potencial respaldo de la comunidad científica y de expertos en políticas públicas.

Revisa el estuido completo en [ciudadaniai.org/covid19](https://ciudadaniai.org/campaigns/covid19)
