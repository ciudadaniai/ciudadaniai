---
layout: post
title: 'Posturas claras para un debate transparente'
intro: 'Compara aquí las posturas de distintos actores políticos para Chile.'
day: 20
month: oct
date: 2020-03-21 12:00:00 -0300)
categories: Proyecto
highlighted: false
share_message: 'Compara aquí las posturas de distintos actores políticos para Chile @ciudadaniai'
img: chilemarcablog.png
caption:
---
Para alimentar el debate que se abre sobre el futuro del país a raíz del plebiscito, lanzamos junto a la [Fundación Friedrich Ebert (FES)](https://www.fes-chile.org/) el proyecto [Chilemarcapreferencia.cl](https://chilemarcapreferencia.cl/) Una plataforma para que la ciudadanía acceda a información comparable y transparente sobre las posiciones de distintos actores, como partidos políticos y organizaciones sociales, en distintas áreas temáticas clave para Chile.

Acá buscamos acercar a la ciudadanía discusiones complejas como son los distintos modelos de salud, pensiones o migración que podría adoptar el país. Para sistematizar las distintas visiones respecto a cada tema, entrevistamos a distintas instituciones y especialistas en cada tema, y luego encuestamos a más de 60 actores políticos sobre sus posturas.

El sitio, además, permite que las usuarias y usarios respondan el test de preguntas, obteniendo automáticamente su propio mapa político virtual y comparar con qué partidos políticos y organizaciones de la sociedad civil tiene más afinidad.

Nuestra Coordinadora General, Auska Ovando, explicó que “independientemente de los resultados del plebiscito, el 25 de octubre comienza una nueva etapa en que Chile empieza a definir en concreto cómo va a ser su modelo de desarrollo y de seguridad social. Porque estos debates le pertenecen a toda la ciudadanía y no sólo a las élites, es que creamos esta plataforma para aterrizar la discusión que se viene”.

Arlette Gay, Directora de Proyectos de la FES, señaló que “la participación ciudadana activa y vinculante es una demanda cada vez más fuerte en Chile, que debiera empezar a traducirse en cambios concretos a nuestra institucionalidad con el proceso constituyente. Esta plataforma es un ejercicio para comenzar a ensayar esa participación y para tomar partido sobre el diseño del futuro de nuestro país.”

**En [Chilemarcapreferencia.cl](https://chilemarcapreferencia.cl/) buscamos comparar y transparentar las posturas de los principales actores políticos del país y de ese modo poner en manos de la ciudadanía un insumo que informe la toma de decisiones.**
