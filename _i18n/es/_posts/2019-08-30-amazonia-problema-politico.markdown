---
layout: post
title: 'La deforestación de la Amazonía es un problema político'
intro: 'Los gobiernos deben tomar medidas en la COP25 para frenar la devastación de la Amazonía y la crisis climática.'
day: 30
month: Ago
date: 2019-08-30 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'La deforestación de la Amazonía es un problema político @ciudadaniai.'
img: 30-08-19.png
caption:
---
Nubes negras de la Amazonía se han extendido y se han apoderado de cientos de ciudades brasileñas y algunas sudamericanas en la última semana. En medio de la conmoción, las imágenes de incendios forestales en la Amazonía han recorrido el mundo y ampliado el debate sobre el futuro del planeta con implicaciones ambientales, políticas, económicas, geopolíticas y sociales.

La reacción global está lejos de ser alarmista, [en 2019 los incendios en todo Brasil aumentaron un 82%, en un total de 71.497 registros realizados por el INPE, de los cuales el 54% se produjeron en la Amazonia.](ttps://www.google.com/amp/s/exame.abril.com.br/brasil/e-a-amazonia-incendios-florestais-ganham-as-redes-sob-silencio-do-governo/amp/) Los incendios no son un hecho aislado. En el corto período del gobierno de Bolsonaro, la deforestación, la explotación ilegal de los recursos naturales, las invasiones de las tierras de los pueblos indígenas y la devastación del medio ambiente también han aumentado. Mientras que el gobierno justifica la flexibilidad de las políticas ambientales como necesaria para la mejora de la economía, especialmente del [sector agrícola y ganadero , que es responsable 65% ( IBGE 2017) de la deforestación amazónica.](https://www.google.com.br/amp/s/amp.dw.com/pt-br/a-m%25C3%25A1quina-que-move-o-desmatamento-da-amaz%25C3%25B4nia/a-40224333)

A pesar de la responsabilidad de Brasil en la mayor parte de la Amazonía, su preservación es, sin duda, un objetivo global. El Bioma Amazónico es compartido por ocho países - Bolivia, Brasil, Colombia, Ecuador, Guyana, Perú, Surinam y Venezuela - y un departamento francés de ultramar, Guayana Francesa, que representa el equivalente al 40% del territorio sudamericano. La Amazonia es un actor clave en la regulación del clima mundial y en la conservación de las reservas de agua. Su devastación aceleraría la desertificación, aniquilará uno de los ecosistemas más diversos del planeta e incitará a la mayor crisis de refugiados en la historia de América Latina, una región donde más de 180 millones de personas viven en extrema pobreza.

La necesidad de enfrentar una posible catástrofe en la Amazonía es grande e inmediata y las respuestas del gobierno brasileño han sido insuficientes.  Jair Bolsonaro ha desmantelado y desmoralizado las inspecciones ambientales, ha hecho numerosas declaraciones alentando las prácticas depredadoras en la Amazonía y ha criminalizado a quienes defienden su preservación. Más aún, ha hipotecado, por motivos personales, el apoyo internacional, que históricamente ha fortalecido las políticas y a la sociedad civil en materia de protección forestal, [negándose en primera instancia a aceptar la ayuda monetaria ofrecida por el G7 para sobrellevar la crisis que ha generado el siniestro en la Amazonía.](https://elpais.com/internacional/2019/08/27/actualidad/1566889091_205938.html)

Aunque hay pocas razones para el optimismo, la crisis también trae oportunidades. En pocos meses, jefes de estado y representantes de todas las naciones del mundo se reunirán en la COP 25 en Santiago de Chile para debatir y acordar acciones para enfrentar la crisis climática mundial. Es muy importante que el creciente interés en la Amazonía que se experimentó en las últimas semanas se convierta en medidas efectivas para reducir las tasas de deforestación, reanudar las políticas regionales y fortalecer la cooperación internacional.

Si el horizonte parece decididamente espinoso, el momento exige nuestro compromiso real. **El camino a seguir es sumarnos las voces y movimientos con capacidad para ampliar la agenda de preservación de la Amazonía. Debemos fortalecer los foros de reuniones a nivel internacional, orientar las investigaciones para la acción y basar nuestras decisiones políticas en pruebas.**
