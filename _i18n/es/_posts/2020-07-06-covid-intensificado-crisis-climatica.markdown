---
layout: post
title: 'El COVID ha intensificado la crisis climática'
intro: 'Proponemos 8 acciones para mitigar la crisis medioambiental, agudizada por la pandemia.'
day: 06
month: Julio
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'COVID ha intensificado la crisis climática'
img: crisis_climatica.png
caption:
---
Previo a la pandemia sanitaria generada por el COVID19 Latinoamérica y el mundo entero ya se encontraba enfrentando una crisis; la crisis climática. Esta crisis no sólo afecta la biodiversidad, las tierras, la flora y la fauna, sino que también a la ciudadanía. Hoy, con la crisis sanitaria, las desigualdades y problemas que por años activistas y defensores ambientales han tratado de hacer llegar a los gobiernos, se han intensificado y muchos de estos siguen sin respuestas.

Los efectos de la sobreexplotación de nuestros recursos y la sobrecarga de los ecosistemas en una de las regiones más desiguales del mundo también es desigual. Sólo en latinoamérica, cerca de [37 millones de personas no tienen acceso a agua potable y sólo el 37% de la región tiene acceso a un saneamiento seguro](https://www.elmostrador.cl/agenda-pais/2020/04/04/millones-en-america-latina-tienen-que-combatir-el-coronavirus-sin-agua-potable/).

Las enfermedades respiratorias asociadas a contaminantes ambientales y gases de efecto invernadero, que están más presentes en sectores marginalizados y con alta prevalencia de la pobreza, provocan [cerca de 7 millones de muertes anualmente en el mundo.](https://www.terram.cl/2020/04/el-impacto-del-desarrollo-sostenible-en-la-crisis-por-covid-19-y-viceversa/)

Los grandes desastres naturales, como las sequías, inundaciones, bajas de temperatura, incendios y huracanes, [afectan intensamente a aquellas comunidades que viven y generan ingresos directamente de la tierra](https://blog.oxfamintermon.org/afecta-el-cambio-climatico-a-la-desigualdad-social/), como comunidades indígenas y sectores rurales.

Todas estas cifras son aún peores en un contexto de crisis sanitaria, donde la ciudadanía además de sobrellevar la [inequidad existente](https://www.paho.org/salud-en-las-americas-2017/?post_type=post_t_es&p=312&lang=es) en la [cobertura y acceso a servicios de salud](https://news.un.org/es/story/2018/04/1430582), enfrenta de manera diferente los estragos que ha provocado nuestra relación con el medio ambiente y ha provocado que las muertes por COVID19 sean también más altas, donde las condiciones sociales y economías informales o a pequeña escala no permiten enfrentar la pandemia.

Pero la ciudadanía no es la única afectada por el COVID19; el aumento explosivo del uso de material plástico con razones sanitarias también es particularmente alarmante, sobretodo teniendo en cuenta que previo a la pandemia ya llegaban al océano cerca de 8 millones de toneladas de plástico al año, los que además, tardarán entre 450 y 1000 años en degradarse.

Una parte importante de esta explosión de plásticos está relacionada directamente al cuidado y contacto con pacientes contagiados, es decir, platos, vasos y utensilios plásticos. Sin embargo, los resultados de la investigación que realizaron el Instituto Nacional de Alergias y Enfermedades Infecciosas (NIAID) de Estados Unidos, los Centros de Control y Prevención de Enfermedades (CDC) de EE.UU., la Universidad de California en Los Ángeles y la Universidad de Princeton indican que el [virus puede llegar a sobrevivir](https://www.bbc.com/mundo/noticias-51955233) hasta tres días en superficies plásticas, es decir, estamos promoviendo su desplazamiento dentro de la sociedad, cuando,  Hospital Infection, señaló que es más efectivo destruir el virus al desinfectar las superficies con etanol al 62-71%, peróxido de hidrógeno (agua oxigenada) al 0.5% o hipoclorito de sodio (lejía doméstica) al 0.1%.

Las crisis nos presentan una oportunidad única para replantear la sociedad que queremos ser: una sociedad atenta no sólo a las necesidades de nuestros territorios y la naturaleza, sino que, por sobretodo, interiorice que esta relación sustentable y respetuosa con el medio, también promoverá mejores condiciones de vida a quienes hoy, más que nunca, están en riesgo; las comunidades más vulneradas.

Los gobiernos y la sociedad deben implementar cambios, levantamos nuestra voz para luchar por una región más justa y sostenible con nuestro medio ambiente.
Exigimos:

* Políticas públicas y acciones de contingencia a la pandemia que considere las necesidades de las comunidades vulneradas y, a largo plazo, comenzar a co-diseñar políticas públicas con la ciudadanía, buscando minimizar el crecimiento y la magnitud del impacto que tiene la desigualdad social.

* Un compromiso tangible, de parte de los Estados de América Latina, con los objetivos de desarrollo sostenible.

* Promover y legislar a favor de acciones sostenibles y alternativas amigables con el territorio, respetando las diferencias culturales, económicas y sociales, en los contextos urbanos y rurales.

* Establecer una fuerte legislación que aborde los desechos plásticos; reducir su producción, promover la reutilización o disposición eficiente y promover el uso de materiales alternativos.

* Abrir espacios a colectivos y comunidades que busquen informar y promover a la ciudadanía en torno a acciones y hábitos amigables con el medio ambiente.

* Políticas de protección a comunidades, pueblos y activistas que defienden el medioambiente y las tierras.

* Mejores políticas públicas de educación que permitan fomentar la resiliencia y la conciencia del cuidado del medio ambiente.

* Legislar y exigir, por parte de los gobiernos, corresponsabilidad a empresas y sectores industriales tanto con el ambiente en el que vive la ciudadanía, como las repercusiones que su producción tienen en el medio natural, flora y fauna.

Esperamos que estas acciones, sumadas a muchas otras posibles, nos permitan construir y brindar una nueva oportunidad. Que generaciones actuales y futuras tengan el derecho de vivir en una sociedad equitativa, saludable y amigable con el medio ambiente. Que nuestras 3R, en época de crisis, sean repensar, reaccionar y revivir.
