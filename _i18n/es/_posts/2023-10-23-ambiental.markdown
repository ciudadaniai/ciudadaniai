---
layout: post
title: "Environmental democracy: where environment and democracy meet"
intro: "Columna de Colombina Schaeffer(FCI) y Ezio Costa(FIMA)"
day: 03
month: oct
date: 2023-10-23 10:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: "Environmental democracy: where environment and democracy meet"
img: 30-10-23.png
caption:
---

Colombina Schaeffer (<cschaeffer@ciudadaniai.org>)
Deputy Director Ciudadanía Inteligente

Ezio Costa (<e.costa@fima.cl>)
Executive Director ONG FIMA

<b>Revisa la columna en la [revista Red Información](https://www.redinnovacion.org/revista/red-informaci%C3%B3n-edici%C3%B3n-n%C2%B0-38-agosto-2023)</b>(En español)

<b>There is no democracy without protecting the environment</b>

The idea of an environmental (or ecological) democracy has been around for decades in academic circles and the environmental movement. In spaces more oriented to practice and public policy, it is still new and not always clearly defined and has slowly started to be part of the conversation.

The relationship between democracy and the environment is also not entirely clear or free of controversy. As we will see, it is related to a political-legal concept that coexists with a sociopolitical one.

For organizations such as ours, working on environment and democracy, the relationship between the two concepts has become inescapable (Schaeffer, 2016). Democracy is not possible without a protected environment, nor is environmental protection without a healthy democracy. Both theory and practice attest to this, as does work in the territories and with the citizenry.

In the following article, we explore the concept of environmental and ecological democracy and the tensions that can arise between them. We delve into why, from our perspective, both concepts require each other and point out how, in Latin America, we have a unique opportunity to make environmental democracy a reality with the Escazú Agreement (CEPAL, 2022).

<b>The emergence of the environment

Since the 1960s, environmental protection has slowly emerged as a topic of widespread interest. It first achieved some prominence in the industrialized countries and gradually gained popularity worldwide as a new and distinct issue. The rapid degradation of the natural world and our habitats has been a significant driver in advancing this concern.

From the 1990s onwards, the environment became an issue of widespread concern, occupying an inescapable space in the public and private domain. The latter was related to a crisis that various authors have defined as a crisis of modernity and industrial societies, as well as to the emergence of new social movements (Dryzek, 2005; Dryzek & Schlosberg, 2005; Latour, 2004; Meyer, 2006).
Citizen movements, organizations, and political parties emerged making environmental protection their priority. At the same time major international agreements on environmental policy and legislation were reached during that decade. At the 1992 Earth Summit, while the Rio Declaration established guidelines for environmental protection, Agenda 21 did the same for sustainable development, and the Framework Conventions on Climate Change and Biological Diversity opened up governance over global common goods.

Political reflection on environment protection was both the cause and effect of the emergence of the environment as a topic of interest and its relationship with other issues, such as democracy. Are democratic societies more sustainable? What type of governance promotes a healthy relationship between the environment and society? What is the role of citizen participation in environmental decision-making? Are there types of democracy, such as deliberative democracy, more adequate to build greener societies?

Environmental democracy then took two paths. The first, of a juridical nature, would seek the concretization of environmental democracy through access rights, such as access to environmental information, participation, and justice. The second, of a sociopolitical nature, would consider the distribution of power and the incorporation of environmental variables in the construction of the state and modern societies.

<b>Democracy and the environment

It should be noted that the relationship between democracy and the environment is neither obvious nor evident. There is no convincing evidence to show that democratic societies are necessarily more ecological, nor vice versa. There is an important group of experts and academics who have tried to prove the ecological credentials of democracy, but also others who have strongly questioned that assertion, and even those who have theorized about environmental authoritarianism.

Democracy is perceived as too slow, compromising, cumbersome, and captured by interest groups and veto players to generate the transformative change needed for sustainability. Instead, ‘eco-authoritarian’ or ‘survivalist’ accounts argue that a hierarchical, technocratic and centralised response featuring a strong state or ‘green leviathan’ – and a corresponding global authority – is necessary to avert environmental catastrophe (Pickering et al. 2020, p. 3-4).

In this sense, there are significant challenges regarding the ecological crisis and linking it to the democratic discussion. We mention here some of them (based on Pickering et al. 2020):

- Citizen participation and populisms: not always the processes of citizen participation will involve ecological decisions based on accurate information or contemplating long-term interests. On the other hand, the growing disaffection with democracy at the global level has also led to the rise of authoritarian populisms that are often anti-environmental.
- The role of technology and experts: the historical tension between science, technology, and democracy, and the role of experts and citizens in general, as well as which voices should be heard with greater (or lesser) force.
- The nexus between democracy and the environment at different scales: environmental problems are often transnational and need to be addressed at that scale, while many of the solutions to today's challenges involve a return to the local level. Reconciling different scales and levels of action is a significant challenge.
- The problem of limits: when discussing environmental rights or the environment, the boundaries are no longer evident (do we want anthropo or ecocentric protection, for example). When thinking about ecological limits and what effects these should have on democracy, reflections arise as to whether these limits should limit democracy.

Addressing climate change is a paradigmatic example of a democratic challenge that arises from an environmental problem. International forums have been unable to deliver at the necessary speed and scale. We do not have a global system for making enforceable decisions beyond (and/or above) national states - which often look after their interests with a national and not global logic - and to address the consequences of actions that take place in one territory but that have an impact on another, distant and apparently unrelated one. The democratic principle par excellence of consulting all parties affected by a decision is often not fulfilled, and not all voices carry equal weight when geopolitical dynamics come into play. The challenge of including future generations or non-human entities in the discussion has also proven highly complex and challenging to face.

According to our diagnosis, the problems of democracies lie mainly in the design and practice of their institutions. Mere electoral democracy, although necessary and an aspect of the democratic process, reduces the opportunities for political action almost exclusively to the moment of electing representatives. These systems have uneven levels of development and are often reduced in terms of other elements necessary for the existence and protection of a full democracy, such as adequate checks and balances, permanent participation, competitiveness, and inclusion. The latter prevents the incorporation of the intersectional diversity of society in decisions, affecting the legitimacy and effectiveness of public policies because they lack social roots or strong coalitions to support them (Brinks, Levitsky & Murillo, 2020). This is why, to be effective, environmental (and ecological) democracy needs to take on these challenges to achieve both legitimacy and the desired environmental protection and sustainability.

<b>Environmental and ecological democracy: a sociopolitical concept

The tensions described above can be better understood if we focus on the proposal of Pickering et al. (2020), who carry out an exhaustive review of the concepts of environmental and ecological democracy.

The distinction between ecological and environmental democracy can help to categorise theories of the democracy-environment nexus. Ecological democracy is more critical of existing liberal democratic institutions – particularly those associated with capitalist markets, private property rights and the prevailing multilateral system – and more ecocentric. Eckersley (2004), a key proponent of ecological democracy, notes the crucial importance of ensuring that the interests of non-humans and future generations are represented in decision-making. Environmental democracy, by contrast, revolves around reforming (rather than transforming) existing institutions of liberal democracy. Environmental democracy thus resonates with ideas of green liberalism (Wissenburg, 1998) or liberal environmentalism (Bernstein, 2001) and is also more anthropocentric in its outlook (...) (p.4).

This typification is presented here because it shows useful characteristics to understand environmental (and ecological) democracy in its social-political sense. The authors unblock the discussion and propose to leave the excluding logic focused on how radical the approach should be and to find space for incrementality and collaboration between concepts, strategies, actions, and actors instead. Understanding that whatever concept is used (environmental or ecological), it will refer to the same issue that operates as a continuum of lesser or greater incorporation of environmental protection in the political configuration of a state.

In this process, the incorporation of environmental democracy from the juridical point of view, that is, centered on rights of access (to information, participation, and justice), is helpful but insufficient since the necessary deployment of forces that implies complete incorporation of environmental variables in the democratic scaffolding exceeds the possibilities of rights of access. Nevertheless, and as we shall see, these rights can help us create a path to oxygenate and deepen our democracies.

<b>A proposal from civil society in Latin America for the Global South

From our perspective, environmental democracy presents us with an opportunity to advance and deepen both the democratic processes and environmental protection of our societies. We also understand that it is impossible to separate the environmental and social dimensions and that social conflicts in the region are often socio-environmental (Escobar, 2008; Latour, 2004; Schaeffer, 2016).

At the judicial level, the relationship between both domains (environment and democracy) can be traced back to Principle 10 of the Rio Declaration on Environment and Development (1992), passing through the Aarhus Convention (1998), the Bali Guidelines (2010), and finally the Escazú Agreement (2018).

This last Agreement is undoubtedly the most relevant for us since it has a specific territorial scope in Latin America and the Caribbean and lays the foundations for generating environmental governance standards in the region. Thus, for example, Article 7, on "Public participation in environmental decision-making processes," establishes principles for environmental decision-making in the region, which include:

- Mechanisms for open and inclusive participation in the various decision-making processes with significant impacts on the environment and health.
- Early public participation, with reasonable deadlines, informed (understandable language and through relevant media), ensuring an opportunity to submit comments and inform the decisions adopted with their reasons and rationale.
- Cross-cultural approach and establish enabling conditions for participation to be adapted to social, economic, cultural, geographic, and gender characteristics.
- The competent bodies should try to identify and reach out to the public directly affected by projects and activities that have or may have a significant impact.

The Escazú Agreement was co-constructed by governments and civil society in the region. A recognized example of this is that the inclusion of the protection of environmental defenders in the Agreement arose at the request of civil society organizations. We could then argue that Escazú is a first step toward incorporating environmental preservation in our democracies.

Although the Escazú Agreement is very similar to the Aarhus Convention, its territorial scope of application, how it has been created, and the moment it is installed are particular and crucial. The climate and ecological crisis affecting the planet has a specific expression in Latin America and the Caribbean, especially when we think about the role that geopolitical and global dynamics want to assign to our countries in the ecological and economic transition.

In this sense, Escazú allows us to understand the access rights as an opportunity to effectively incorporate in our democracies a thinking that sees the interdependence of societies and natures and that, acting accordingly, makes decisions that enhance the welfare of all parties involved. The Escazú Agreement establishes standards at the regional level that, if incorporated into local and national decision-making, would bring us closer to the ideal of environmental democracy both as a political-legal concept and a political-social concept.

For this to happen, the work of individuals and organizations is crucial. First, achieving an effective implementation of the Escazú Agreement, and then conducting processes that impact our states' policies, deepening democratic and environmental logics.

<b>Bibliography

Brinks, D.M., Levitsky, S. & Murillo, M. (2020). The Politics of Institutional Weakness in Latin America. Cambridge University Press.

CEPAL. (2022). Acuerdo Regional sobre el Acceso a la Información, la Participación Pública y el Acceso a la Justicia en Asuntos Ambientales en América Latina y el Caribe. Disponible en:
<https://repositorio.cepal.org/bitstream/handle/11362/43595/S2200798_es.pdf>

Dryzek, J. (2005). The Politics of the Earth: Environmental Discourses. Oxford: Oxford University Press.

Dryzek, J. & Schlosberg, D. (Eds.). (2005). Debating the Earth: The Environmental Politics Reader. Oxford: Oxford University Press.

Escobar, A. (2008). Territories of Difference: Place, Movements, Life, Redes. Estados Unidos: Duke University Press.

Latour, B. (2004a). Politics of Nature. How to Bring the Sciences into Democracy. Cambridge, Londres: Harvard University Press.

Mason, M. (1999). Environmental democracy. London: Earthscan.

Meyer, J. M. (2006). Political Theory and the Environment. En: J. Dryzek, B. Honig & A. Philipps (Eds.), The Oxford Handbook of Political Theory. Estados Unidos: Oxford Handbooks Online.

Morrison, R. (1995). Ecological democracy. Boston, MA: South End Press.

Pickering, J., Bäckstrand, K. & Schlosberg, D. (2020). Between environmental and ecological democracy: theory and practice at the democracy-environment nexus, Journal of Environmental Policy & Planning, 22:1, 1-15, DOI: 10.1080/1523908X.2020.1703276

Schaeffer, C. (2016). Democratizing the Flows of Democracy: Patagonia Sin Represas in the Awakening of Chile’s Civil Society. En: S. Donoso y M. von Bülow (Eds.) (2016), Social movements in Chile: Organization, trajectories, and political impacts. Nueva York: Palgrave McMillan.
