---
layout: post
title: '¿Cómo puedo ser parte del proceso constituyente?'
intro: 'Revisa qué mecanismos tiene la ciudadanía para que su voz sea escuchada en la Convención Constitucional.'
day: 26
month: nov
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: '¿Cómo puedo ser parte del proceso constituyente by @ciudadaniai'
img: 26-11-21.png
caption:
---
Hace poco más de un mes la Convención Constitucional aprobó los cuatro reglamentos necesarios para su funcionamiento: General, Ética, Participación y Consulta Indígena, y finalmente, [Participación Popular](https://www.chileconvencion.cl/wp-content/uploads/2021/10/Reglamento-definitivo-Participacio%CC%81n-Popular-final-modificado-2.pdf). En este último, se definieron los mecanismos que tendrá la ciudadanía para realmente ser parte de todo el proceso constituyente. En Ciudadanía Inteligente junto a nuestro proyecto [La Constitución Es Nuestra](https://laconstitucionesnuestra.cl/), buscamos promover la participación informada, articulada e incidente de la ciudadanía movilizada, sociedad civil y grupos históricamente excluidos de la toma de decisiones durante el Proceso Constituyente, para producir cambios sustantivos que garanticen derechos sociales y nuevas formas de participación democrática en la Nueva Constitución de Chile.

¿Cómo la ciudadanía puede ser parte de todo el proceso constituyente? Acá te lo explicamos.


1. **Iniciativa popular de norma**, juntando 15.000 firmas se podrá hacer una  propuesta de norma para que la convención considere un determinado tema en la comisión a la que este corresponda. Acá podrán firmar personas mayores de 16 años.

2. **Plebiscito intermedio dirimente**, la Convención Constitucional podrá, solo una vez, llamar a la ciudadanía a pronunciarse a favor o en contra de algunas normas. ¿Cuáles? Aquellas normas en las que, habiendo votado dos veces, las y los convencionales hubiesen llegado a un acuerdo superior a ⅗ pero no suficiente para alcanzar los ⅔ establecidos para la aprobación directa en el pleno de la Convención. Para esto será necesaria una reforma desde el congreso.

3. **Cabildos distritales y locales**, una vez que la Convención Constituyente informe el orden cronológico de sus discusiones en cada comisión, los cabildos autoconvocados podrán organizarse para debatir diferentes temas de su interés. Todos serán sistematizados y entregados a la Convención.

4. **Foros deliberativos**, son mecanismos de levantamiento de propuestas para ser debatidas por la Convención.

5. **Audiencias públicas**, es el mecanismo funcional que permite a la Convención mantener un diálogo abierto y continuo con la ciudadanía y sus organizaciones, con el objetivo de informarse sobre diferentes asuntos que sean relevantes para su trabajo en comisiones y en el pleno.

6. **Jornadas nacionales de deliberación**, siguiendo el calendario de la Convención, se abrirán jornadas simultáneas en todo el territorio para que la ciudadanía pueda debatir y reflexionar junto a las y los constituyentes sobre los temas que está tratando en ese momento la Convención.

7. **Cuenta popular constituyente**, las y los constituyentes deberán rendir cuentas a sus distritos y territorios, ya sea de forma presencial o virtual, en donde informen a la ciudadanía de su trabajo en la Convención.

8. **Plataforma digital**, servirá para recepcionar, generar un registro público y sistematizar las propuestas e insumos producidos por los diferentes mecanismos de participación popular. Sube tu iniciativa de norma [acá](https://plataforma.chileconvencion.cl/m/iniciativa_popular/)

Nos encontramos en un momento histórico, ya que por primera vez la ciudadanía podrá ser parte del proceso de diseño constitucional, contribuyendo con ideas, inquietudes, debates, reflexiones y propuestas en instancias pensadas exclusivamente para incidir en la construcción de la nueva Constitución, diferenciándose de otros procesos constitucionales, donde todo es prerrogativa exclusiva de las y los convencionales.

En Fundación Ciudadanía Inteligente defendemos la Convención Constitucional y celebramos los mecanismos de participación aprobados. Hoy somos un ejemplo regional (incluso mundial) de ingeniería constitucional moderna, paritaria, inclusiva y democrática.

***¡Impulsemos una Constitución escrita por la ciudadanía!***
