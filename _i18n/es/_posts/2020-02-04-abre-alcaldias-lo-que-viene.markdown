---
layout: post
title: '¡Postula ya a ABRE ALCALDÍAS!'
intro: 'Luchamos por incorporar la voz ciudadana en la toma de decisiones.'
day: 04
month: Feb
date: 2020-01-27 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Así vamos con ABRE ALCALDÍAS.'
img: Frame 5.jpg
caption:
---
¡Ya está abierta la convocatoria para postular a AbreAlcaldías! Aplica [aquí](https://abrealcaldias.org/call/). Esta iniciativa es una gran oportunidad para nuestra región (e incluso para el mundo), con una escuela inédita que brindará recursos modernos y sencillos para la apertura de los gobiernos y acercamiento de las gestiones públicas hacia la ciudadanía.

El 2020 arrancamos con gran fuerza para impulsar nuestro proyecto ABRE ALCALDÍAS que busca capacitar ciudades latinoamericanas para innovar junto a la ciudadanía. Todo comenzó con un llamado a la sociedad civil para apoyar nuestra iniciativa. Hoy ya hemos logrado un gran avance: contamos con la firma de alianzas de cuatro organizaciones comprometidas con la innovación y democracia en cada uno de los países en los que trabajaremos durante dos años. Estas son [Méxiro](http://www.mexiro.org/), en México; [No-ficción](https://www.no-ficcion.com/), en Guatemala; ACCIÓN en El Salvador, y el [Observatorio de Políticas Públicas de Guayaquil](https://oppguayaquil.org/) en Ecuador. Para el equipo de [Ciudadanía Inteligente](https://ciudadaniai.org/) es un gran honor poder trabajar en conjunto con equipos muy capacitados y comprometidos con el cambio social en sus países y en la región.

Nuestro próximo paso será la selección de las alcaldías que participarán del proyecto. ¿Cómo lo haremos?
En cada país contaremos con 10 ciudades que se comprometerán a hacer parte de nuestra escuela de innovación pública y a partir de ahí implementarán proyectos pilotos de co-creación de políticas públicas, dónde los gobiernos locales incorporen efectivamente la ciudadanía en su proceso de diseño e implementación. Para esta selección tenemos definidos los siguientes pasos:

**Paso uno**: mapeo de ciudades en la región comprometidas con prácticas democráticas de gobierno abierto, innovación pública, participación ciudadana y tecnologia de interés público.

**Paso dos**: lanzamiento de convocatoria abierta para que los municipios postulen presentando propuestas de equipos de trabajo y proyectos de políticas públicas que pretenden desarrollar de forma participativa.

**Paso tres**: Invitación a alcaldes y alcaldesas de ciudades estratégicas de la región para que postulen al programa por medio de un equipo municipal capacitado.

**Paso cuatro**: selección de las mejores candidaturas, llegando hasta 10 ciudades seleccionadas en cada país, comunicando públicamente los resultados.

**Hoy estamos en el paso dos ¡No te quedes fuera de la [postulación](https://abrealcaldias.org/call/)!**

Con ABRE ALCALDÍAS esperamos recuperar la confianza de los habitantes de Latinoamérica en sus gobiernos locales y construir un futuro más democrático, más justo, inclusivo y sostenible.
