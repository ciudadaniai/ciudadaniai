---
layout: post
title: 'Nota de apoyo para el pueblo Waiãpi'
intro: 'Lamentamos y exigimos justicia por el asesinato en contra de Emyra Waiãpi, líder indígena en el estado de Amapá.'
day: 30
month: Jul
date: 2019-07-30 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: "Nota de apoio ao povo Waiãpi @ciudadaniai."
img: 30-07-19.jpg
caption: '"Roda de Conversa em Macapá" by CulturaGovBr is licensed under CC BY 2.0'
---
Recibimos con gran preocupación y lamentamos la noticia del asesinato violento perpetrado en contra de  Emyra Waiãpi, líder indígena en el estado de Amapá - Brasil. Los discursos de odio y agresión por parte del presidente de Brasil, Jair Bolsonaro, y otros representantes de su gobierno sirven como combustible y estimulan la invasión y las acciones violentas contra los pueblos indígenas en la Amazonía de Brasil.

Presentamos nuestras condolencias y solidaridad al pueblo Waiãpi y exigimos que  las autoridades públicas brasileñas tomen medidas urgentes, y políticamente extensas para identificar y castigar a los responsables del ataque a los Wajãpi de conformidad con la ley, y permitiendo la supervisión y apoyo de organismos internacionales en el proceso. También demandamos que se preste seguridad y apoyo psicosocial a la comunidad afectada por las amenazas directas en su contra. Asimismo, esperamos que el gobierno de Bolsonaro adopte importantes y amplias medidas para garantizar las vidas y combatir la invasión de las tierras indígenas en Brasil.

¡Exigimos al presidente Bolsonaro respeto a los pueblos indígenas!

30 de julio de 2019
Fundación Ciudadanía Inteligente

**Adherentes**

*Fundación Ciudadanía Inteligente (Chile)
*Acceso y desarrollo (Guatemala)
*Litigio Estratégico en Derechos Sexuales y Reproductivos, Ledeser A.C.. (México)
*Planeta Conciencias (Chile)
*Federación de Estudiantes Universitarios del Ecuador (Ecuador)
*Red de Memorias en Resistencia. (Chile)
*Muitas (Brasil)
*Comisión Departamental por la Transparencia y Probidad de Chiquimula (Guatemala)
*Asociación de Mujeres Gente Nueva -AMUGEN-. (Guatemala)
*Ciudadanía Joven. (Perú)
*Red de jovenes ixiles guatemala. (Guatemala)
*Concertación Médica (Perú)
*Colectivo sembremos (Ecuador)
*Educación Intermedia (México).
*Organización Diálogo Diverso (Ecuador.)
*Rede Nacional de Jovens Vivendo e Convivendo com HIV/AIDS (Brasil)
*Coletivo Mente ABertha (Brasil)
*Alianza Política Sector de Mujeres (Guatemala)
*Rede Feminista de Juristas - deFEMde - (Brasil)
*Daniela Jordán (Chile)
*José Alonso Cornejo Sologuren (Ecuador)
*Gabriela Quemé (Guatemala)
*Marieliv Flores Villalobos (Perú)
*Pablo Alonzo (Guatemala)
