---
layout: post
title: 'Protejamos las libertades civiles durante la pandemia'
intro: 'Cinco principios para cuidar nuestros datos en tiempos de crisis sanitaria.'
day: 24
month: Mar
date: 2020-01-27 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Cinco principios para cuidar nuestros datos en tiempos de crisis sanitaria.'
img: datacorona.jpg
caption:
---
*Información replicada del [Manifiesto](https://www.eff.org/deeplinks/2020/03/protecting-civil-liberties-during-public-health-crisis) de [Eletronic Frontier Foundation](https://www.eff.org/) escrito por Matthew Guariglia y Adam Schwartz.*


En todo el mundo, las autoridades de salud pública trabajan para contener la propagación del COVID-19. Con este objetivo, urgente y necesario, muchas agencias gubernamentales están recopilando y analizando información personal de un gran número de personas identificables, como datos sobre su salud, sus desplazamientos y sus relaciones personales. Al mismo tiempo que luchamos como podemos para minimizar la propagación de la enfermedad, debemos vigilar de cerca el impacto que tienen en nuestras libertades digitales las herramientas que almacenan big data.

Los esfuerzos extraordinarios que hacen las agencias de salud pública para contener la propagación del COVID-19 están justificados. Tanto en el mundo digital como en el físico, las políticas públicas deben reflejar un equilibrio entre el bien común y las libertades civiles a fin de proteger nuestra salud y seguridad frente a los brotes de enfermedades contagiosas. Pero es fundamental que las medidas extraordinarias implementadas para gestionar una crisis concreta no se perpetúen y se conviertan en una nueva intrusión del gobierno en nuestras vidas cotidianas. Hay precedentes históricos de programas de este tipo —con la intromisión en las libertades digitales que suponen— que se han mantenido una vez superado el período de emergencia.

Por este motivo, toda recopilación de datos y monitorización digital de posibles portadores del COVID-19 debe tener en cuenta y comprometerse con estos principios:

* **Las intrusiones en la privacidad deben ser necesarias y proporcionadas.** Todo programa que recoja masivamente información identificable sobre personas debe estar científicamente justificado y contar con la aprobación de expertos en salud pública a efectos de contención. Así mismo, el procesamiento de datos debe ser proporcional a la necesidad. No sería proporcional a la necesidad de contener el COVID-19, por ejemplo, mantener durante diez años el historial de desplazamientos que ha hecho la gente, cuando se trata de una enfermedad con un período de incubación de dos semanas.

* **La recopilación de datos debe basarse en la ciencia, no en prejuicios.** Dado el alcance mundial de las enfermedades contagiosas, conocemos precedentes históricos de gobiernos que han aplicado medidas de contención deshonestas basadas en prejuicios de nacionalidad, etnia, raza y religión, en lugar de en hechos que permitan calcular la probabilidad real de que una persona en particular contraiga el virus, como su historial de desplazamientos o su contacto con personas potencialmente infectadas. Tenemos que asegurarnos de que los sistemas de datos automatizados que se están utilizando para contener el COVID-19 no identifiquen erróneamente a miembros de grupos demográficos específicos como particularmente propensos a contraer la infección.

* **Caducidad.** Como en otras grandes emergencias del pasado, existe el peligro de que la infraestructura de seguimiento de datos que se construye para contener el COVID-19 dure más tiempo que la crisis que pretendía abordar. El gobierno y las corporaciones que participan en estos programas invasivos creados en nombre de la salud pública deben deshacer el camino una vez la crisis esté bajo control.

* **Transparencia.** El gobierno debe explicar públicamente con celeridad y claridad todo uso de big data que se haga con el fin de rastrear la propagación del virus. Debe proporcionar información detallada sobre los datos que se recopilan, el período de almacenamiento de estos datos, las herramientas que se utilizan para procesarlos, cómo influye la forma de procesarlos en las decisiones que se toman en materia de salud pública y si estas herramientas han dado buen resultado o arrojan consecuencias negativas.

* **Garantías legales.** Si el gobierno trata de limitar los derechos de una persona basándose en esta rastreo de big data (por ejemplo, para confinarla basándose en las conclusiones del sistema sobre sus contactos o desplazamientos), esta persona debe tener la oportunidad de impugnar estas conclusiones y limitaciones a tiempo y con garantías suficientes.
