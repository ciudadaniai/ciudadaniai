---
layout: post
title: 'Cleptocracia: La otra crisis de América Latina'
intro: 'La emergencia sanitaria del COVID-19 no sólo ha dejado miles de muertes, sino que también ha mostrado la peor cara de la corrupción.'
day: 13
month: Julio
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Cleptocracia la otra crisis de América Latina'
img: cleptocracia.png
caption:
---
Durante el mes de mayo, el Ministerio Público del Perú identificó que en la región de la amazonía, una de las más afectadas por la pandemia, habrían existido compras irregulares donde el costo de balones de oxígeno vacíos -sin oxígeno-, con kit completos habrían sido casi un 20% más caros que los adquiridos en una primera instancia, los cuales además de tener el kit completo, [contaban con balones llenos de oxígeno.](https://comprasestatales.org/ministerio-publico-investiga-presuntas-compras-sobrevaloradas-de-tanques-de-oxigeno-en-la-diresa-de-iquitos/)  

Casos como estos se han hecho presentes prácticamente en toda Latinoamérica desde que el COVID19 llegó a la región. Esta pandemia, además de permitirnos realizar un duro diagnóstico de las condiciones de desarrollo en la que se encontraban los países a nivel mundial y lo crudo e indudable de las desigualdades estructurales existentes en la ciudadanía latinoamericana, nos ha abierto una puerta para evidenciar una segunda crisis que nos golpeaba desde mucho antes: la naturalización de la corrupción dentro del ejercicio del poder público.

Actualmente nos encontramos en un escenario de cuarentena (una de las estrategias más eficientes de contención y más adoptadas por gobiernos en el mundo) pero que limita fuertemente la realización de actividades económicas, lo que tiene como consecuencias el incremento de la pobreza y la imposibilidad de atender necesidades básicas, especialmente en la población más vulnerada.

Por tal motivo, países latinoamericanos como Perú, Chile, Ecuador, México, Guatemala y Colombia han promovido acciones que movilizan sumas importantes de dinero, esperando poder sobrellevar la crisis, pero no siempre es la ciudadanía más vulnerada la que es beneficiada con estas acciones.

**Ecuador**

En Ecuador, por ejemplo, se registraron irregularidades respecto de la identificación de poblaciones vulnerables, pues se registró la adjudicación de “carnés de discapacidad” para asambleístas, jueces, deportistas de alto rendimiento, e incluso el presidente de Consejo de Participación Ciudadana y Control Social. Tal es así que el ministro de Salud, Juan Carlos Zevallos, señaló que se habían detectado 3.000 carnés otorgados de forma “ilegal”, emitidos durante la emergencia por COVID19, [de los cuales 2.281 están en proceso de anulación.](https://www.elcomercio.com/actualidad/destitucion-beneficiarios-carnes-discapacidad-vehiculos.html)

En el mismo país, el Instituto de Seguridad Social habría comprado mascarillas con un 150% de sobreprecio, y en mayo de 2020 se hizo público que el Hospital de “Los Ceibos” de Guayaquil realizó la compra de 4.000 fundas para cadáveres por un precio de 148 dólares, cifra 12 veces más alta que el precio del mercado, bordeando [un monto total de 592.000 dólares.](https://www.elcomercio.com/datos/coronavirus-fundas-cadaveres-hospitales-compras.html)

Finalmente, la corrupción impactó en la construcción del hospital en Pedernales, provincia de Manabí, ciudad que fue golpeada por el terremoto del 2013,  dado que la licitación fue adjudicada a empresas ligadas al asambleísta Daniel Mendoza, quien es acusado de liderar una red de crimen organizado.

**Perú**

En el caso del Perú, dentro de las estrategias para aminorar el impacto económico de la pandemia, se promovió la entrega de bonos para sobrellevar el aislamiento social obligatorio. Estos fueron inicialmente dirigidos a poblaciones de extrema pobreza, pero progresivamente se convirtieron en bonos universales. A pesar de lo atractivo de la medida, su ejecución fue ineficiente; [los destinatarios no fueron necesariamente las y los más pobres y ha estado acompañado de varias irregularidades.](https://www.defensoria.gob.pe/defensoria-del-pueblo-interviene-por-cobro-irregular-de-bonos-en-municipalidades-de-ancash-huancavelica-la-libertad-piura-y-tumbes/)

Por otro lado, en el mismo país, se les asignó a los gobiernos locales presupuesto y competencias funcionales para la entrega de [canastas básicas a fin de asegurar la reclusión de la ciudadanía.](https://ojo-publico.com/1748/covid-19-en-regiones-investigan-malos-manejos-en-entrega-de-canastas) En múltiples casos no se transparentó el gasto y se han denunciado, incluso, condiciones insalubres de los alimentos otorgados en determinados distritos de la capital, lo que deja en evidenciala existencia de compras y licitaciones irregulares.

**Chile**

Situación similar se registró en Chile, donde la estrategia de otorgamiento de canastas básicas no ha sido eficiente, entre otras cosas, porque se han registrado irregularidades en la definición de personas en situación de vulnerabilidad. Es decir, [se han entregado cajas a personas sin las necesidades económicas](https://www.elmostrador.cl/dia/2020/06/12/es-un-escandalo-diputada-natalia-castillo-recibe-caja-de-alimentos-para-chile-y-anuncia-que-recurrira-a-contraloria/) y dejado sin cajas a [sectores vulnerables.](https://www.biobiochile.cl/noticias/nacional/chile/2020/06/21/alimentos-chile-gobierno-se-enfoca-la-entrega-pendiente-cajas-regiones.shtml). Además, se han encontrado diferencias en el contenido de las cajas entre un sector y otro, y se presume que el espacio fue aprovechado para la negociación de [licitaciones con grandes empresas privadas.](https://www.eldesconcierto.cl/2020/06/14/escandalo-en-arica-piden-la-destitucion-del-intendente-y-administrador-regional-por-irregularidades-en-la-adquisicion-de-cajas-de-alimentos/)


**México**

Al igual que en los países ya mencionados, la emergencia sanitaria ha permitido mayor flexibilización en el manejo del presupuesto público, lo que ha sido aprovechado también, para flexibilizar en la transparencia del manejo de estos recursos. En México, el presidente ordenó un operativo especial ante un posible desvío de recursos e insumos, así como la [intervención de la Unidad de Inteligencia Financiera para el monitoreo del dinero que se estará haciendo llegar a los Estados por este mismo motivo.](https://www.animalpolitico.com/2020/04/amlo-operativo-desvio-insumos-covid-19/)

La [investigación realizada por la organización “Mexicanos contra la Corrupción y la Impunidad” (MCCI)](https://www.forbes.com.mx/corrupcion-y-volatilidad-en-tiempos-de-covid-19-en-mexico-mcci/) detectó que en la delegación regional del Instituto Mexicano del Seguro Social (IMSS) en el Estado de Hidalgo, se compró durante mayo del presente año, 20 ventiladores a una empresa que es propiedad de León Manuel Bartlett Álvarez, hijo del director de la Comisión Federal de Electricidad (CFE), Manuel Bartlett Díaz. Esta empresa (Cyber Robotic Solutions) vendió a un sobreprecio de hasta un 50% adicional .

**Guatemala**

En Guatemala el gobierno destituyó a dos viceministros de Salud y presentó denuncias contra 8 funcionarios unidos en red para cometer fraude. Un país conocido por la alianza entre el Organismo Ejecutivo, Legislativo y Judicial que permite mantener un sistema de corrupción e impunidad, garantizando privilegios a las mafias enquistadas en estos tres organismos de Estado. Por esta razón, se han realizado todo tipo de maniobras para coaptar la elección de magistrados que manipulen la actuación de la justicia hacia personajes que incurren en actos de corrupción, quienes incluso ponen en riesgo a la sociedad de instaurar una narcocleptocracia.

Casos como estos, donde se ha flexibilizado o postergado la transparencia y la rendición de cuentas de parte del sector público, ha permitido la compra sobrevalorada de insumos y contrataciones irregulares de recursos tan necesarios en época de crisis como el oxígeno medicinal o los ventiladores mecánicos. Pero, además de esto, nos encontramos con otro factor común: la complicidad de los actores del Estado, que incurren en actos de corrupción en conjunto con actores del sector privado que detentan el poder económico.

Así como en Guatemala, la conexión entre el poder político, el poder económico e incluso, el poder judicial, hace que no podamos entender estos actos como simple corrupción. Son estrategias para operar en el mercado (contrarias al conducto regular) modificando y abusando de las reglas del juego: una Cleptocracia; una forma de corrupción institucionalizada.

La **cleptocracia** como forma de gestionar el poder político no puede permanecer vigente. Su impacto no es solo es macroeconómico, sino que transgrede los derechos fundamentales de las personas, siendo aún más evidente ante esta coyuntura de la pandemia por el COVID19. Por ese motivo, resulta vital el establecimiento de políticas que promuevan la transparencia, que incluyan de manera integral a las poblaciones vulnerabilizadas, con un enfoque transversal e interseccional, promoviendo además un respaldo y protección a las y los ciudadanos que deciden denunciar las prácticas contrarias al desarrollo de un Estado democrático.

A pesar de esto, la movilización ciudadana y sus intenciones de fiscalizar el poder político se han mantenido, como en el caso de Colombia, donde las organizaciones de la Sociedad Civil y la Academia, han emprendido el estudio de los contratos realizados en el marco de la pandemia, emitiendo las banderas rojas y las denuncias necesarias de aquellos casos que presentan sobrecosto o sospechas en la ausencia de objetividad y transparencia en la contratación.  Son, sin lugar a dudas, un buen punto de inicio, pero necesitamos mucho más.

**¡Exigimos una lucha frontal contra la cleptocracia enquistada en todos los Estados de América Latina!:**

* Protección a denunciantes (ciudadanía) para evitar las represalias laborales, físicas y/o económicas. ¡Que termine el miedo por denunciar y alertar los abusos!

* Respaldo a medios de comunicación y sus periodistas que cumplen con labores de investigación y transparentan la verdad.

* Promoción de campañas informativas a la ciudadanía respecto de las contrataciones del Estado, su importancia y cómo la corrupción daña a todxs.

* Creación de protocolos a nivel nacional y supranacionales para que determinadas comunidades cuenten con el apoyo de atención a sus necesidades especiales, bloqueando el espacio habilitante para la malversación de fondos públicos.
