---
layout: post
title: 'Lanzamos nueva plataforma: Ahora Nos Toca Participar'
intro: 'A 90 días del plebiscito del 25 de octubre, nuestro proyecto busca interactuar y conocer nuevos contenidos sobre la Constitución y participación.'
day: 28
month: Julio
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Revisa Ahora Nos Toca Participar'
img: antp (1).png
caption:
---
*A 90 días del plebiscito del 25 de octubre, nuestra iniciativa [Ahora Nos Toca Participar](https://ahoranostocaparticipar.cl/) dispone de una nueva [plataforma web](https://ahoranostocaparticipar.cl/) para interactuar y conocer nuevos contenidos sobre Constitución y participación, en distintos formatos, con el objetivo de contribuir al acceso a la información de manera anticipada y educativa.*

Promover el derecho a la formación y participación  ciudadana como contribución para  fortalecer la democracia y disponer de un Plebiscito seguro y participativo es el llamado que hace **“Ahora Nos Toca Participar”**, nuestra iniciativa ciudadana que tiene como objetivo contribuir al fortalecimiento de la democracia y de la cohesión social en Chile a través de la generación de contenidos, materiales de formación ciudadana y de encuentros de participación para fortalecer el protagonismo de las ciudadanías en los procesos eleccionarios previstos desde este año incluyendo el plebiscito del 25 de octubre y el eventual proceso constituyente.

El proyecto que venimos trabajando desde el estallido social, hoy da a conocer una nueva plataforma web  poniendo a disposición nuevos contenidos para la formación cívica y ciudadana en diferentes lenguas (mapuzungún, aymará, rapanui y prontamente en creole)  y accesos inclusivos. En esta plataforma iremos sumando nuevos materiales temáticos provenientes de los territorios y grupos de especial protección.
En la misma línea, nuestra iniciativa da cuenta del trabajo que se ha levantado desde las distintas regiones del país gracias a la participación de distintas organizaciones locales que conforman los “Consejos territoriales  de [“Ahora Nos Toca Participar”](https://ahoranostocaparticipar.cl/), hoy ya contamos con 12 Consejos en regiones y esperamos avanzar hacia los 16. A través de nuestra plataforma, se podrá conocer el detalle de lo que se ha venido trabajando, así como también la invitación de participar en los espacios de  “Voces territoriales”, encuentros de diálogo y de formación donde se abordan temas de interés local y nacional.

**Talleres de formación ciudadana y próximas actividades**

La formación ciudadana anticipada, hoy más que nunca, por la pandemia, es clave para aumentar la participación electoral. Con este proyecto buscamos generar contenidos desde los estándares de la participación ciudadana con enfoque de derechos humanos y ponerlos al servicio de la ciudadanía a través de distintos dispositivos y herramientas como: encuentros digitales, radios comunitarias, redes sociales, entre otros.

Así, buscamos que la ciudadanía se forme en Participación y Constitución para luego propiciar talleres de formación autoconvocados, que permitan replicar este conocimiento en un gran número de comunidades y grupos, promoviendo el trabajo colaborativo, la  autoformación y a la ciudadanía como protagonista.

Acá puedes revisar la próximas actividades de formación:
* Talleres “Voces Territoriales” partiendo por la región de Biobío, el próximo miércoles 29 de julio a las 19:00 hrs
* Talleres de formación ciudadana abiertos sobre “Participación Ciudadana y Constitución"
* Campaña radial de formación enfocada a comunidades aisladas y/o rurales.
* Formación gráfica e inclusiva por las redes sociales del proyecto.
* Encuentros digitales formativos sobre experiencias internacionales en materia de  Constitución y  derechos económicos, sociales y culturales.

**Plebiscito seguro y participativo**

Dentro de nuestro proyecto junto a [Espacio Público](https://www.espaciopublico.cl/)y otras organizaciones, estamos impulsando recomendaciones para que se garanticen medidas sanitarias durante el Plebiscito, pero también se resguarden campañas informativas y formativas oportunas, con más tiempo y utilizando la mayor diversidad de recursos comunicacionales. Es por eso que la semana pasada hicimos llegar una [carta](https://docs.google.com/document/d/1QAeTPHD1i9ixs9b9kO8rOiszuL8_To5Zvb1teZR1o0M/edit) a SERVEL, SEGPRES a la presidencia de ambas cámaras del Congreso y a los presidentes de las comisiones de Constitución con el fin de manifestar la voluntad de integrar una mesa ampliada para poder abordar estas recomendaciones desde la sociedad civil.

Finalmente, sobre la campaña y la “cuenta regresiva” hacia el Plebiscito, Marcela Guillibrand, Coordinadora General de Ahora Nos Toca Participar señaló: “Desde la recuperación de la democracia en Chile, la ciudadanía vive el mayor momento de consciencia y activación política. Esta ciudadanía, está participando y manifestándose para fortalecer la democracia y avanzar hacia un país digno y justo con todas y todos. Desde ahí, hacemos un llamado a involucrarse en espacios de formación que nos permitan decidir libre e informádamente, y a participar para incidir en los procesos eleccionarios, desde el Plebiscito de octubre y durante todo el posible proceso constitucional”.

Para revisar el material e información sobre las futuras actividades, entra a [www.ahoranostocaparticipar.cl](https://ahoranostocaparticipar.cl/)
