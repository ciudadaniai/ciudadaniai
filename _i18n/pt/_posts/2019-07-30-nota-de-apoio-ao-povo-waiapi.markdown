---
layout: post
title: 'Nota de apoio ao povo Waiãpi'
intro: 'Lamentamos e exigimos justiça pelo assassinato de Emyra Waiãpi, líder indígena no estado do Amapá.'
day: 30
month: Jul
date: 2019-07-30 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: "Nota de apoio ao povo Waiãpi @ciudadaniai."
img: 30-07-19.jpg
single_image:
  -
caption: '"Roda de Conversa em Macapá" by CulturaGovBr is licensed under CC BY 2.0'
---

Recebemos com grande preocupação e lamentamos a notícia do violento assassinato cometido contra Emyra Waiãpi, líder indígena en el estado de Amapá - Brasil. Os discursos de ódio e agressão por parte do presidente do Brasil il, Jair Bolsonaro e outros representantes do seus governo servem como combustível e estimulam a invasão e as ações violentas contra os povos indígenas na Amazônia brasileira.

Apresentamos nossas condolências e solidariedade ao povo Waiãpi e exigimos que as autoridades públicas brasileiras tomem medidas urgentes, e politicamente extensas para identificar e punir os responsáveis pelo ataque aos Wajãpi em conformidade com a lei, e permitindo a supervisão e apoio dos organismos internacionais no processo. Também demandamos que se preste segurança e apoio psicossocial a comunidade afetada pelas ameaças diretas. Assim como, esperamos que o governo de Bolsonaro adote importantes e amplas medidas para garantir as vidas e combate a invasão das terras indígenas no Brasil.

Exigimos ao presidente Bolsonaro respeito aos povos indígenas!

30 de julio de 2019
Fundação Cidadania Inteligente

**Aderiram**    

Fundación Ciudadanía Inteligente (Chile)
Acceso y desarrollo (Guatemala)
Litigio Estratégico en Derechos Sexuales y Reproductivos, Ledeser A.C.. (México)
Planeta Conciencias (Chile)
Federación de Estudiantes Universitarios del Ecuador (Ecuador)
Red de Memorias en Resistencia. (Chile)
Muitas (Brasil)
Comisión Departamental por la Transparencia y Probidad de Chiquimula (Guatemala)
Asociación de Mujeres Gente Nueva -AMUGEN-. (Guatemala)
Ciudadanía Joven. (Perú)
Red de jovenes ixiles guatemala. (Guatemala)
Concertación Médica (Perú)
Colectivo sembremos (Ecuador)
Educación Intermedia (México).
Organización Diálogo Diverso (Ecuador.)
Rede Nacional de Jovens Vivendo e Convivendo com HIV/AIDS (Brasil)
Coletivo Mente ABertha (Brasil)
Alianza Política Sector de Mujeres (Guatemala)
Rede Feminista de Juristas - deFEMde - (Brasil)
Daniela Jordán (Chile)
José Alonso Cornejo Sologuren (Ecuador)
Gabriela Quemé (Guatemala)
Marieliv Flores Villalobos (Perú)
Pablo Alonzo (Guatemala)
