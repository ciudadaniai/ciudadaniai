---
layout: post
title: 'Comunicação com foco na cidadania'
intro: 'Convidamos Larissa Santiago de Blogueiras Negras para falar sobre comunicação pública e desinformação.'
day: 01
month: dic
date: 2020-03-21 12:00:00 -0300)
categories: Invitación
highlighted: false
share_message: 'Comunicação com foco na cidadania'
img: larissa.jpg
caption:
---
**Convidamos Larissa Santiago do Blogueiras Negras para escrever em nosso blog. Aqui está sua coluna sobre comunicação pública, desinformação e novas tecnologias de informação.**

Esse ano, em setembro, as [Blogueiras Negras](https://www.instagram.com/blogueirasnegras/?hl=es-la) conversaram com integrantes do projeto [Abre Alcaldías](https://abrealcaldias.org/) da [Fundação Cidadania Inteligente](https://ciudadaniai.org/pt/) sobre processos, estratégias e desafios da comunicação para a incidência política. Dentre os participantes, funcionários públicos, comunicadores e ativistas que, com base na chamada “pergunte o que quiser”, interagiram e trocaram impressões sobre os contextos das diferentes cidades da América Latina.

Neste exato momento, a região passa por intensos acontecimentos nos principais países, que refletem a conjuntura política num momento de efervescência política. O Chile acaba de viver um processo de plebiscito para a construção popular de uma nova constituição; a Bolívia em novo pleito, elege Luis Arce, candidato do partido Movimento ao Socialismo (MAS); além de no Brasil estarmos vivendo o ano das eleições municipais, onde serão escolhidos na próxima semana os prefeitos das mais de 5.000 cidades do país.

É nesse cenário em que conversamos sobre os desafios, a partir da perspectiva das mulheres negras, já que as **Blogueiras Negras** são uma organização que atua diretamente com o direito desta parte significativa da população brasileira. Na nossa conversa, abordamos sobretudo quais são os desafios de engajar as cidadãs e cidadãos latinoamericanos quando vivemos tempos onde a democracia passar por momentos críticos, sobretudo quando olhamos para o agravamento das desigualdades sociais, a crescente militarização dos governos e a criminalização de movimentos sociais.

Na nossa troca, ressaltamos a importância de fortalecer a comunicação entre as instâncias de poder institucional e os movimentos sociais, incentivando não somente que estes últimos aprendam e participem dos mecanismos de controle social - em geral burocráticos e entediantes - mas também criando novas alternativas de advocacy, como é o caso das ferramentas que temos chamado de tecnologia cívica.  Assim como é importante aprender como atuar nos mecanismos tradicionais, locais, nacionais e internacionais, acreditamos ser possível inventar, elaborar e disseminar outras formas de incidência que não necessariamente as convencionais, afinal as mulheres negras - assim como outros grupos minorizados - são sistematicamente afastados desses espaços de decisão.

Conversamos também sobre quais são essas alternativas e o que as mulheres negras tem feito no Brasil, e ao falarmos da comunicação como um dos vetores da ação política, reiteramos a necessidade de valorizar a construção de narrativas nos diferentes meios, da importância de apoiar financeiramente os projetos das mulheres negras e de criar pontes entre as novas gerações para que seja possível um novo jeito de incidência na política.

Considerando nossos cenários hoje, onde a crise pandêmica nos impõe a desafios políticos, a nossa troca no [Abre Alcaldías](https://abrealcaldias.org/) se tornou um espaço criativo em que as experiências das diferentes pessoas ao redor da América Latina fornece um arcabouço de alternativas para pensar novas democracias possíveis e a exemplo do que temos vivido, validamos naquele encontro que a nossa região tem oferecido a governos, organizações, cidadãs e cidadãos possibilidades transformadoras e novos modus de construir projetos políticos plurais, mais equânimes e que tenham os direitos humanos como valores inegociáveis.
