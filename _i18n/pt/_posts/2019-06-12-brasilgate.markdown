---
layout: post
title: 'Brasilgate'
intro: 'Intercept Brasil publicou três reportagens sobre uma série de manipulações indevidas do então juiz Sérgio Moro'
day: 12
month: Jun
date: 2019-06-12
categories: News
highlighted: false
share_message: "Brasilgate: sem liberdade de imprensa não há democracia"
img: 12-06-19.jpg
single_image:
  -
    caption: Diario Río Negro
---
***Columna escrita por Ana Carolina Lourenço coordinadora de Incidencia Regional de Ciudadanía Inteligente. Publicada en el medio [Nodal](https://www.nodal.am/2019/06/brasilgate-sin-libertad-de-prensa-no-hay-democracia-por-ana-carolina-lourenco/)***

Intercept Brasil publicou três reportagens sobre uma série de manipulações indevidas do então juiz Sérgio Moro, agora ministro da Justiça, nas investigações da operação brasileira Lava Jato, que impediu a participação do líder do Partido dos Trabalhadores, Luiz Inácio Lula da Silva nas eleições presidenciais em 2018. O material: mensagens trocadas entre de 2015 a 2018, via Telegram, por Sérgio Moro, e o procurador Deltan Dallagnol, e que foram obtidas através de uma fonte anônima. As conversas oferecem evidências irrefutáveis de atuações irregulares, até ilegais, e politizadas, pelos acusadores responsáveis. De acordo com as mensagens publicadas, o hoje Ministro Sérgio Moro sugeriu testemunhas ao procurador, opinou sobre o avance das constatações, antecipou uma decisão aos acusadores e articulou movimentos que, em caso de serem comprovados, danam infinitamente com a imparcialidade de um juiz.

Mais além de uma indicação de ações fora da competência de um juiz, as revelações são especialmente graves pelos efeitos destas nas eleições brasileiras de 2018. Faltando apenas quatro dias para apresentação da denúncia do caso Tríplex contra Lula em 2017, o coordenador da investigação da Lava Jato, Deltan Dallagnol, teve dúvidas sobre a solidez da história que contou ao juiz Moro. Feita a denúncia de corrupção passiva e lavagem de dinheiro contra Lula, Dallagnol enviou mensagens diretamente a Moro, quem deveria fazer o julgamento da denúncia, para explicar os termos da denúncia, suas debilidades e escolhas argumentativas.

As revelações evidenciam uma politização da justiça, em distintas fases do processo. Hoje se apresentam diante da opinião pública feitos que evidenciam um padrão de conducta através do tempo, pelos procuradores da Lava Jato, para interferir no resultado das eleições. Por exemplo, na troca de mensagens prova-se uma manobra política, não jurídica, utilizada para bloquear, com êxito, uma entrevista de Lula a Folha de São Paulo antes do primeiro turno das eleições presidenciais. O motivo? O impacto da entrevista poderia gerar uma possível vitória de Fernando Haddad, candidato do PT à presidência. Os supostos argumentos técnicos e imparciais que foram apresentados na imprensa nacional e internacional, inclusive, nas redes sociais, hoje perdem validade com a publicação do The Intercept.

Desde de 2016, a politização da operação Lava Jato esteve no centro do debate, polarizando a sociedade brasileira e latino-americana. Um lado da discussão defendia a imparcialidade dos operadores da justiça brasileira, aplaudidos por sua luta contra a corrupção para além do partido político, neste caso PT. Do outro lado, se sinalizava com preocupação o notário tom político de suas ações. Diante das novas revelações é evidente e irrefutável que os envolvidos na operação Lava Jato não são pessoas apartidárias nem a política, muito pelo contrário. São pessoas que atuam por convicções ideológicas e que trabalham para evitar o retorno do PT ao poder, em condutas contrárias ao Estado de Direito, debilitando a democracia e a confiança da cidadania nas suas instituições.

Agora, a pergunta mais importante é: o que sucederá com estas revelações? Se considerará legítima uma eleição onde existiu interferência, já não de atores externos, senão de atores internos, do próprio poder judicia, que prejudicou ativamente, de forma premeditada e continuada, a um dos partidos políticos que aspiravam o poder? Se retirou dos votantes brasileiro da possibilidade de votar pelo projeto político de sua preferência? Deve Sérgio Moro permanecer no governo, como ministro, especialmente se a Polícia Federal – sob o seu controle – é a entidade com a responsabilidade de de investigar o conteúdos das conversas?

Neste contexto, em Cidadania Inteligente reforçamos nossa missão de apoio aos denunciante e todas as pessoas que trabalham no dia a dia por buscar a verdade: as e os jornalistas. Esta revelação midiática, lamentavelmente, revela uma captura maliciosa da luta legítima contra a corrupção, é um ataque contra a Constituição brasileira, uma violação da separação dos poderes da República e um insulto à democracia em si mesma.

As revelações hoje, e o trabalho que segue reforçam a importância do jornalismo investigativo e dos denunciantes ( whistleblowers) como contrapesos a arbitrariedade do poder e como peças centrais em uma luta contra a corrupção que sirva ao interesse público e não a vinganças políticas.

A democracia fote só existe com liberdade de imprensa. No poderia ser de outro modo e o Brasil é um exemplo para impedir que triunfe a impunidade.

Lutemos por democracia mais fortes.

Ao The Intercept e a sociedade brasileira, toda nossa solidariedade.

Continuamos.
