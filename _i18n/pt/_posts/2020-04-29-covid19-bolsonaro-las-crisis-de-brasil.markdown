---
layout: post
title: 'Covid 19 e Bolsonaro: As ameaças do Brasil'
intro: 'Hoje o Brasil está passando por uma de suas piores crises sanitárias, econômicas e políticas.'
day: 29
month: Abril
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Covid 19 e Bolsonaro: as ameaças do Brasil'
img: 29-04-20.png
caption:
---
Com casos de contágio e mortes subindo exponencialmente, o Brasil enfrenta hoje dois grandes inimigos: o COVID-19 e o presidente Jair Bolsonaro. Desde o início da pandemia, o atual presidente minimiza os riscos da doença, menospreza medidas de contenção e vem praticando ações, ainda com suspeita de estar contaminado, que colocam em risco a saúde da população e o futuro econômico do país.

Em 10 de março, classificou a doença como uma “fantasia criada pela mídia”. Um mês depois, o país somava mais de 38 mil contaminados e mais de 2.400 mortos. Defensor da volta imediata das atividades, ao ser indagado sobre qual seria o número de mortes aceitáveis para obter tal fim, respondeu: "eu não sou coveiro, tá?".

Desde o início da pandemia, bolsonaro vem coletando acirramentos de tensões com líderes das outras esferas de poder, tendo sido criticado pelos presidentes da Câmara dos Deputados e do Senado nas tomadas de decisão sobre a contenção da doença, o que causou que sua articulação no Congresso ficasse prejudicada. Igualmente, continua disputando com governadores a prioridade de dar ordens sobre a suspensão das quarentenas, alegando que não está disposto a negociar.  

Após demitir o então ministro da Saúde, participou e apoiou um ato de ataque ao Congresso e ao STF, pedindo a volta da ditadura. Pelo feito, recebeu inúmeras notas de repúdio dos líderes do poder legislativo e judiciário, dos governadores, dos ex-presidentes Lula e Fernando Henrique Cardoso e diversas OSCs. A própria ala militar, grande base de apoio do governo, se movimentou para frear a radicalização dos discurso, gerando um pronunciamento do presidente em que alegou que não precisaria conspirar, pois já estava no poder e completou: “Eu sou, realmente, a Constituição.”.  

Preocupado com suas pretensões eleitorais em 2022, Bolsonaro vêm buscando apoio do chamado centrão em um dos momentos mais críticos de seu governo, oferecendo cargos de segundo escalão em troca de apoio de partidos tradicionalmente associados ao fisiologismo. O centrão representa hoje 260 dos 513 deputados da câmara e no leque de opções do presidente, estão desde políticos condenados no mensalão até envolvidos em outros escândalos de corrupção da política nacional.

Tendo sido determinado que um dos maiores desafetos políticos atuais de Bolsonaro, o presidente da Câmara dos Deputados , Rodrigo Maia, se manifeste sobre os pedidos de impeachment em aberto, hoje enfrentamos nova crise governamental. Sérgio Moro, até então Ministro da Justiça e Segurança Pública, pede demissão do governo com duras críticas às trocas de indulgências de Bolsonaro para se proteger do possível futuro de investigações. Em 16 meses de governo, 7 ministros já saíram ou foram retirados dos cargos.

O presidente rompe com antigos acordos com Moro, responsável por grande parte da sua base eleitoral de apoio, exonerando o comandante da Polícia Federal para indicar um aliado em um momento de investigações que envolvem, primordialmente, seus filhos. O cargo também é responsável pela investigação do chefe do executivo no caso de um processo de impeachment. A demissão acarretou exposições de possíveis crimes que a família bolsonaro possa ter cometido cumprindo mandatos, gerando incidência em investigações sobre corrupção, enriquecimento ilícito e caixa 2. Cabe destacar que a família mantém-se como principal suspeita pela morte da vereadora e ativista Marielle Franco.

Além do isolamento no campo sanitário, as atuações do presidente colocam em risco também a recuperação econômica.  Até então, sua possibilidade da reeleição estava conectada com o prometido crescimento econômico. Contudo, em 2019 tivemos um PIB pequeno sem projeção de melhora. O cenário criou desconfiança de avanço dentre empresários e políticos centrados no mercado que se beneficiariam através de apoio à agenda ultraliberal e antiestadista de Paulo Guedes. A equipe econômica não conseguia conferir respostas mesmo antes da pandemia e agora está promovendo uma das maiores intervenções realizadas pelo Estado na economia brasileira.

Os posicionamentos de Bolsonaro durante a pandemia têm sido motivo de perda de influência nas redes mais usadas pela estratégia bolsonarista desde 2018, como o twitter. A queda do engajamento começou a ser registrada no segundo trimestre de 2019, mas a situação piorou com o início da pandemia.  Ainda assim, mesmo diante do cenário de baixa oscilação da aprovação do governo nas pesquisas oficiais, os que consideram a condução da crise ruim ou péssima foram de 33% em março para 38% em 17 de abril.

No cenário catastrófico em que vivemos, o presidente deveria apostar em uma liderança de unificação dos interesses do país, como forma de diminuir tensões da polarização ao invés de focar no seu núcleo duro de eleitores para se manter relevante no cargo executivo. Seu descolamento das diretrizes da OMS e sua afronta às regras democráticas constitucionais o posicionam em um campo cada vez mais restrito, radical e autoritário.

Suas movimentações recentes dizem respeito ao seu desespero de reeleição.  Para isso, está disposto a desonrar compromissos políticos, fragilizar o país sócio-economicamente e acirrar as tensões de ataque às instituições democráticas. Se pautando pelos esquemas da velha política, falha em observar que o atual momento pede renovação de modelos de governança, defesa das instituições democráticas e proteção das classes sociais de base. Qualquer palpite sobre um impeachment ainda é estudado, contudo Bolsonaro continua a cavar seu declínio político enquanto o Brasil sofre em meio à uma de suas maiores crises.

No período que antecede as eleições municipais, o clima é cada vez mais violento, devido às disputas entre diferentes esferas de poder. Da [Ciudadanía Inteligente](https://ciudadaniai.org/) entendemos que alguns compromissos devem ser assumidos e não vamos ficar parados. Nosso projeto #GIRO2020 propõe precisamente uma retomada da defesa da igualdade de acesso aos serviços essenciais, do desenvolvimento sustentável e do fortalecimento democrático. Nosso objetivo é claro: fortalecer candidaturas comprometidas com debates aprofundados sobre desigualdades estruturais e direitos humanos, bem como a defesa do Estado Democrático de Direito.
