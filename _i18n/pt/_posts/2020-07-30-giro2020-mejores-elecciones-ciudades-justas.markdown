---
layout: post
title: 'GIRO2020: Um giro para construir eleições melhores e cidades mais justas.'
intro: '¡GIRO2020 está pronto! Candidatar-se à formação política de candidatos municipais do Rio de Janeiro.'
day: 30
month: Jul
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Veja nosso projeto Giro 2020'
img: giro2020.png
caption:
---
O Rio de Janeiro é uma das 10 metrópoles mais desiguais do mundo. A Região Metropolitana (RMRJ), que conta com 13 milhões de habitantes em 22 cidades, possui alguns dos piores indicadores de desenvolvimento humano e social do Brasil, segundo o [Mapa da Desigualdade](https://casafluminense.org.br/mapa-da-desigualdade/) lançado ineditamente pela [Casa Fluminense](https://casafluminense.org.br/).

O contexto eleitoral de 2020 traz muitas inquietações e desafios, principalmente para as lideranças sociais, sociedade civil e movimentos ativistas que vêm mobilizando política desde seus territórios, tentando avançar na superação dessas desigualdades históricas.

As tensões políticas, ligadas à polarização e extremismos, e a insegurança das eleições por conta do contexto pandêmico demandam respostas e soluções para conter a debilidade dos processos democráticos. Em um momento que a cidadania está experimentando baixo nível de confiança na gestão pública é preciso prosperar frente às incertezas.

A crise de saúde pública humanitária global da COVID-19 que estamos enfrentando intensifica esse cenário, demonstrando a urgência de reajustar nossas prioridades para o avanço de agendas propositivas que priorizem a justiça social e o direito à vida, superando as desigualdades estruturais e emergenciais dos municípios.

Nos últimos anos, observamos as cidades, bairros e territórios sofrerem da negligência e decadência pela atuação dos representantes eleitos. Cada vez mais, também experimentamos a diminuição, ou total exclusão, da cidadania na participação e protagonismo da construção das agendas de políticas públicas. As crises que vivemos atualmente, política e sanitária, são consequência também de abandonos históricos das cidades, seus cidadãos e suas prioridades coletivas.

As eleições são um momento decisivo na criação do nosso futuro. A construção de um novo imaginário de sociedade depende também de com quem nos alinhamos na defesa das nossas agendas, pautas e prioridades.

Para superar esse momento, sem cometer os mesmos erros do passado, precisamos levar ao poder quem se compromete com quem vive e atua no cotidiano dos desafios urbanos. Nesse contexto de fragilidade institucional, polarização e desprestígio políticos e desgaste do tecido social, observamos que um desequilíbrio na disputa eleitoral se aproxima.

Nos últimos anos, os índices de violência política, que culminou no homicídio da ex-vereadora negra Marielle Franco, vêm crescendo. Grupos paramilitares e poderes paralelos conservadores aumentam sua influência eleitoreira com práticas desleais que se beneficiam das desigualdades sócio-econômicas para ganhar corpo. As estruturas partidárias de apoio e financiamento continuam privilegiando os mesmos corpos.

Por isso, lançamos de forma inédita a [Formação GIRO2020](https://giro2020.org/), entregando conteúdo, estratégias e ferramentas para fortalecer as candidaturas. O [GIRO2020](https://giro2020.org/) é uma iniciativa da sociedade civil para fortalecer a democracia, antes, durante e depois das eleições na Região Metropolitana do Rio de Janeiro.

Fruto da parceria entre a [Fundação Cidadania Inteligente](https://ciudadaniai.org/) e a [Casa Fluminense](https://casafluminense.org.br/), é um ciclo formativo gratuito que entrega conteúdo de políticas públicas, tendências sobre os desafios eleitorais e imaginário político, ferramentas de comunicação e financiamento voltado para campanhas eleitorais.
O material do curso conta com dois pilares de conteúdo principais:

* Uma agenda propositiva detalhada a partir de um diagnóstico centrado em dados e propostas de políticas públicas para a Região Metropolitana do Rio de Janeiro, baseada na Agenda Rio e no Mapa da Desigualdade organizados pela Casa Fluminense.

* Um conjunto de ferramentas e estratégias sobre as tendências do processo eleitoral em 2020, que abordam os desafios a serem enfrentados pelas candidaturas na arena de disputa de campanhas.

Considerando os efeitos da pandemia do COVID-19 e a nova arena de disputa eleitoral das nossas cidades, o [GIRO2020](https://giro2020.org/) prioriza o fortalecimento de candidaturas a partir de um recorte sócio-político alinhado com nossa missão e valores. Por isso, a nossa formação é voltada com especial enfoque em pré-candidaturas municipais da Região Metropolitana do Rio de Janeiro, especialmente mulheres, negros, jovens, lgbt+ em todas as suas interseções e diversidades, e pessoas com atuação no debate da redução das desigualdades e ampliação do acesso à direitos sociais.
Nosso novo imaginário político da sociedade brasileira, compactua com as propostas de avanço baseadas na participação social, transparência, respeito e valorização das diversidades e compromisso público com a redução das desigualdades e aumento das oportunidades.

**Precisamos girar as pautas e os protagonistas na política das nossas cidades!**
