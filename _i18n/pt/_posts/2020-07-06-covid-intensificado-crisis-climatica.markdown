---
layout: post
title: 'A COVID-19 intensificou a crise climática'
intro: 'Propomos 8 ações para mitigar a crise ambiental, exacerbada pela pandemia.'
day: 06
month: Jul
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'A COVID-19 intensificou a crise climática'
img: crisis_climatica.png
caption:
---
Antes da pandemia sanitária gerada pela COVID19, a América Latina e o mundo inteiro já enfrentavam uma crise; a crise climática. Esta crise não só afeta a biodiversidade, a terra, a flora e a fauna, mas também os cidadãos. Hoje, com a crise de saúde, as desigualdades e problemas que durante anos ativistas e defensores do meio ambiente tentaram trazer aos governos se intensificaram e muitos deles permanecem sem resposta.

Os efeitos da exploração excessiva de nossos recursos e a sobrecarga dos ecossistemas em uma das regiões mais desiguais do mundo também são desiguais. Só na América Latina, [quase 37 milhões de pessoas não têm acesso a água limpa e apenas 37% da região tem acesso a saneamento seguro](https://www.elmostrador.cl/agenda-pais/2020/04/04/millones-en-america-latina-tienen-que-combatir-el-coronavirus-sin-agua-potable/).

As doenças respiratórias associadas a poluentes ambientais e gases de efeito estufa, que estão mais presentes em setores marginalizados e com uma alta prevalência de pobreza, causam [quase 7 milhões de mortes anuais no mundo.](https://www.terram.cl/2020/04/el-impacto-del-desarrollo-sostenible-en-la-crisis-por-covid-19-y-viceversa/)

Grandes desastres naturais, como secas, inundações, quedas de temperatura, incêndios e furacões, [afetam fortemente as comunidades que vivem e geram renda diretamente da terra](https://blog.oxfamintermon.org/afecta-el-cambio-climatico-a-la-desigualdad-social/), como as comunidades indígenas e os setores rurais.

Todos estes números são ainda piores em um contexto de crise de saúde, onde os cidadãos não só têm que lidar com a [desigualdade existente](https://www.paho.org/salud-en-las-americas-2017/?post_type=post_t_es&p=312&lang=es) em [cobertura e acesso aos serviços de saúde](https://news.un.org/es/story/2018/04/1430582), mas também enfrentam de maneira diferente o caos causado por nossa relação com o meio ambiente e causaram mortes por COVID19, onde as condições sociais e as economias informais ou de pequena escala não permitem enfrentar a pandemia.

Mas o público não é o único afetado pela COVID19 ; o aumento explosivo no uso de material plástico por razões de saúde também é particularmente alarmante, especialmente considerando que antes da pandemia quase 8 milhões de toneladas de plástico já estavam chegando ao oceano a cada ano, o que também levará entre 450 e 1000 anos para se degradar.

Uma parte importante desta explosão plástica está diretamente relacionada ao cuidado e contato com pacientes infectados, ou seja, placas, copos e utensílios de plástico. Entretanto, os resultados das pesquisas conduzidas pelo Instituto Nacional de Alergias e Doenças Infecciosas dos EUA (NIAID), os Centros de Controle e Prevenção de Doenças dos EUA (CDC) a Universidade da Califórnia em Los Angeles e a Universidade de Princeton indicam [que o vírus pode sobreviver](https://www.bbc.com/mundo/noticias-51955233) até três dias em superfícies plásticas, ou seja, estamos promovendo seu deslocamento dentro da sociedade, quando, Infecção Hospitalar, indicou que é mais eficaz destruir o vírus desinfetando superfícies com 62-71% de etanol, 0,5% de peróxido de hidrogênio (peróxido de hidrogênio) ou 0,1% de hipoclorito de sódio (lixívia doméstica).

As crises nos apresentam uma oportunidade única de repensar a sociedade que queremos ser: uma sociedade não apenas atenta às necessidades de nossos territórios e da natureza, mas, sobretudo, que interioriza que esta relação sustentável e respeitosa com o meio ambiente também promoverá melhores condições de vida para aqueles que hoje, mais do que nunca, estão em risco; as comunidades mais vulneráveis.

Os governos e a sociedade devem implementar mudanças; levantamos nossa voz para lutar por uma região que seja mais justa e sustentável com nosso meio ambiente. Nós exigimos:
Políticas públicas e ações de contingência para a pandemia que consideram as necessidades das comunidades vulneráveis e, a longo prazo, começam a co-desenhar políticas públicas com os cidadãos, buscando minimizar o crescimento e a magnitude do impacto da desigualdade social.

* Um compromisso tangível, por parte dos Estados latino-americanos, com os objetivos do desenvolvimento sustentável.

* Promover e legislar em favor de ações sustentáveis e alternativas favoráveis ao território, respeitando as diferenças culturais, econômicas e sociais, nos contextos urbanos e rurais.

* Estabelecer uma legislação forte que trate dos resíduos plásticos; reduzir sua produção, promover a reutilização ou o descarte eficiente e promover o uso de materiais alternativos.

* Abrir espaços para grupos e comunidades que buscam informar e promover cidadãos em torno de ações e hábitos amigáveis ao meio ambiente.

* Políticas para proteger comunidades, povos e ativistas que defendem o meio ambiente e a terra.
Melhores políticas públicas de educação que permitam fomentar a resiliência e a consciência do cuidado com o meio ambiente.

* Legislar e exigir, por parte dos governos, a co-responsabilidade das empresas e setores industriais tanto com o meio ambiente em que os cidadãos vivem, quanto com as repercussões que sua produção tem sobre o meio ambiente natural, a flora e a fauna

Esperamos que estas ações, somadas a muitas outras possíveis, nos permitam construir e oferecer uma nova oportunidade. Que as gerações atuais e futuras tenham o direito de viver em uma sociedade equitativa, saudável e amiga do meio ambiente. Que nossos 3Rs, em tempos de crise, devem repensar, reagir e reanimar.
