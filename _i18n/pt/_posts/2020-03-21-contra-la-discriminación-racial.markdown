---
layout: post
title: 'Contra a discriminação racial'
intro: 'COVID-19 perpetuará a discriminação racial Vamos exigir políticas de proteção!.'
day: 20
month: Mar
date: 2020-01-27 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Contra a discriminação racial.'
img: discriminacionraza.jpg
caption:
---
Dia 21 de março já entrou no calendário mundial como um momento obrigatório de reflexão sobre os avanços e retrocessos na luta contra a discriminação racial no mundo. Se a ascensão de uma nova extrema direita nacionalista, sem dúvidas, não fizeram dos últimos anos fáceis para os ativistas antirracismo o momento atual parece apontar  para um período de ainda mais precariedade e incerteza para essas comunidades

Em 2020  o  dia internacional do combate à discriminação racial coincide com o ponto alto de uma crise mortal na área da saúde, mas que tem o potencial de resultar em  um intenso processo de degradação social, econômica e política: a pandemia do COVID-19. Se é inegável o efeito dessa crise em todas as camadas e setores da sociedade  não podemos ser ingênuos quanto a quais grupos serão mais afetadas neste momento. Em um contexto como o latino americano onde saúde é mercadoria e que o acesso a ela é definido por um histórico processo de estratificação social com base racial o COVID-19 pode representar décadas de retrocessos nos nossos indicadores de desigualdade racial.

Em que pese a diferança na coleta de dados  demográficos racializados  nos diferentes países latino americanos, o cenário é claro. Os afrodescendentes na América Latina tem 2,5 mais probabilidade de viver na pobreza crônica que os brancos ou mestiços (Banco Mundial).  A expectativa de vida dos povos indígenas  pode chegar a  20 anos menos do que a média na maior parte dos países da América Latina (ONU). Somado a isso, apesar de a população afrodescente e indígena serem maioria demográficas em alguns países da região: Brasil, Bolívia, Guatemala e Peru ainda ocupam a minoria dos postos de decisões políticas.

Esse cenário de desigualdade racial é um agravador do impacto do COVID-19, exemplos concretos não faltam. No Brasil apesar da maior parte dos casos ainda estar relacionados as classes mais altas primeiras mortes estão concentrados em afrodescendentes prestadores de serviços, em especial dométiscos. As informações adequadas aos contextos indigenas ainda não avançaram na maior parte dos países e muitos parques de terra indigenas ainda estão abertos para visitação. As famosas lacunas de acesso à saúde, condições de moradia e renda que marcam os corpos racializados em nossos países ainda estão na margem dos debates de como enfrentar a crise em nossa região, mas, acredite, sem dúvidas são pistas importantes do triste futuro que está por vir.

Apesar do nível de alerta,  nenhum país latino americano apresentou um pacote de medidas sociais e econômicas consistente ao tamanho do desafio que a região mais desigual do mundo irá enfrentar. Nós da Fundação Cidadania Inteligente temos trabalhado nos últimos anos de forma ativa no enfrentamento a discriminação racial: mudando os nossos protocolos de trabalho, apoiando projetos que promovem novas vozes e corpos na política como Mulheres Negras Decidem e Me Representa e adotando a perspectiva interseccional como ponto de partida para a elaboração dos nossos novos projetos. Como uma organização comprometida com a correção das desigualdades sociais e raciais nos somamos aos esforços globais de enfrentamento ao COVID-19 e cobramos medidas eficazes para frear o aceleramento das desigualdades nesse novo contexto.
