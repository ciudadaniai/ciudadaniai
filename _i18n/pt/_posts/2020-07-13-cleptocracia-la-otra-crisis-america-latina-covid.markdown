---
layout: post
title: 'Cleptocracia: a outra crise na América Latina'
intro: 'A emergência sanitária do COVID-19 não tem só deixado milhões de mortos, como também tem mostrado a pior da corrupção'
day: 13
month: Jul
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Cleptocracia la otra crisis de América Latina'
img: cleptocracia.png
caption:
---
Durante o mês de maio, o Ministério Público do Peru identificou que na região amazônica, uma das mais afetadas pela pandemia, teria havido compras irregulares onde o custo dos balões de oxigênio vazios - sem oxigênio -, com kits completos, teria sido quase 20% mais caro do que aqueles comprados em primeira instância, que além de terem o kit completo, [tinham balões de oxigênio cheios.](https://comprasestatales.org/ministerio-publico-investiga-presuntas-compras-sobrevaloradas-de-tanques-de-oxigeno-en-la-diresa-de-iquitos/)  

Casos como estes estão presentes praticamente em toda a América Latina desde que a COVID19 chegou à região. Esta pandemia, além de nos permitir fazer um diagnóstico rigoroso das condições de desenvolvimento nas quais os países se encontravam em nível global e das desigualdades estruturais rudes e inquestionáveis existentes na cidadania latino-americana, nos abriu uma porta para evidenciarmos uma segunda crise que já nos havia atingido muito antes: a naturalização da corrupção dentro do exercício do poder público.

Estamos atualmente em um cenário de quarentena (uma das estratégias mais eficientes de contenção e mais adotadas pelos governos do mundo), mas que limita fortemente a realização de atividades econômicas, o que tem como conseqüência o aumento da pobreza e a impossibilidade de atender às necessidades básicas, especialmente na população mais vulnerável.

Por esta razão, países latino-americanos como Peru, Chile, Equador, México, Guatemala e Colômbia promoveram ações que mobilizam quantidades significativas de dinheiro, esperando superar a crise, mas nem sempre são os cidadãos mais vulneráveis que se beneficiam destas ações.

Equador

No Equador, por exemplo, foram registradas irregularidades em relação à identificação de populações vulneráveis, como a concessão de "cartões de deficiência" a membros da assembléia, juízes, atletas de alto rendimento e até mesmo ao presidente do Conselho de Participação Cidadã e Controle Social. Tanto que o Ministro da Saúde, Juan Carlos Zevallos, [disse que 3.000 cartões emitidos "ilegalmente" durante a emergência pela COVID19 foram detectados, dos quais 2.281 estão em vias de serem cancelados.](https://www.elcomercio.com/actualidad/destitucion-beneficiarios-carnes-discapacidad-vehiculos.html)

No mesmo país, diz-se que o Instituto de Previdência Social comprou máscaras com uma sobretaxa de 150%, e em maio de 2020 foi tornado público que o "Los Ceibos" de Guayaquil comprou [4.000 sacos para cadáveres por $148, 12 vezes o preço de mercado, por um total de $592.000.](https://www.elcomercio.com/datos/coronavirus-fundas-cadaveres-hospitales-compras.html)

Finalmente, a corrupção afetou a construção do hospital em Pedernales, província de Manabí, uma cidade que foi atingida pelo terremoto de 2013, já que a licitação foi adjudicada a empresas ligadas ao deputado Daniel Mendoza, acusado de liderar uma rede de crime organizado.

**Peru**

No caso do Peru, como parte das estratégias para reduzir o impacto econômico da pandemia, foram promovidos laços para superar o isolamento social obrigatório. Inicialmente, estes eram destinados a populações extremamente pobres, mas gradualmente se tornaram vínculos universais. Apesar da atratividade da medida, sua implementação foi ineficiente; [os destinatários não eram necessariamente os mais pobres e ela foi acompanhada de várias irregularidades](https://www.defensoria.gob.pe/defensoria-del-pueblo-interviene-por-cobro-irregular-de-bonos-en-municipalidades-de-ancash-huancavelica-la-libertad-piura-y-tumbes/).

Por outro lado, no mesmo país, foi atribuído aos governos locais um orçamento e competências funcionais para a [entrega de cestas básicas a fim de garantir a reclusão dos cidadãos.](https://ojo-publico.com/1748/covid-19-en-regiones-investigan-malos-manejos-en-entrega-de-canastas) Em muitos casos, não houve transparência nos gastos e houve até mesmo reclamações de condições insalubres em certos distritos da capital, deixando provas de compras e licitações irregulares.

**Chile**

Uma situação semelhante foi registrada no Chile, onde a estratégia de concessão de cestas básicas não foi eficiente, entre outras coisas, porque foram registradas irregularidades na definição de pessoas em situação de vulnerabilidade. Em outras palavras, as [caixas foram entregues a pessoas sem necessidades econômicas](https://www.elmostrador.cl/dia/2020/06/12/es-un-escandalo-diputada-natalia-castillo-recibe-caja-de-alimentos-para-chile-y-anuncia-que-recurrira-a-contraloria/) e deixadas sem caixas a [setores vulneráveis](https://www.biobiochile.cl/noticias/nacional/chile/2020/06/21/alimentos-chile-gobierno-se-enfoca-la-entrega-pendiente-cajas-regiones.shtml). Além disso, foram encontradas diferenças no conteúdo das caixas entre um setor e outro, e presume-se que o espaço foi utilizado para negociar [licitações com grandes empresas privadas.](https://www.eldesconcierto.cl/2020/06/14/escandalo-en-arica-piden-la-destitucion-del-intendente-y-administrador-regional-por-irregularidades-en-la-adquisicion-de-cajas-de-alimentos/)

**México**

Como nos países já mencionados, a emergência sanitária permitiu uma maior flexibilidade na gestão do orçamento público, que também foi utilizada para tornar a gestão desses recursos mais transparente. No México, o Presidente ordenou uma operação especial no caso de um possível desvio de recursos e insumos, bem como a intervenção da [Unidade de Inteligência Financeira para monitorar o dinheiro que será enviado aos Estados por este mesmo motivo.](https://www.animalpolitico.com/2020/04/amlo-operativo-desvio-insumos-covid-19/)

[A investigação realizada pela organização "Mexicanos contra a Corrupção e Impunidade" (MCCI)](https://www.forbes.com.mx/corrupcion-y-volatilidad-en-tiempos-de-covid-19-en-mexico-mcci/) detectou que na delegação regional do Instituto Mexicano de Seguridade Social (IMSS) no Estado de Hidalgo, 20 ventiladores foram adquiridos durante o mês de maio deste ano de uma empresa de Leon Manuel Bartlett Alvarez, filho do diretor da Comissão Federal de Eletricidade (CFE), Manuel Bartlett Diaz. Esta empresa (Cyber Robotic Solutions) vendeu a um prêmio de até 50% a mais.

**Guatemala**

Na Guatemala, o governo demitiu dois vice-ministros da saúde e apresentou queixas contra oito funcionários que haviam unido forças para cometer fraude. Um país conhecido pela aliança entre os Poderes Executivo, Legislativo e Judiciário que permite a manutenção de um sistema de corrupção e impunidade, garantindo privilégios às máfias entrincheiradas nestes três órgãos do Estado. Por esta razão, todos os tipos de manobras foram realizadas a fim de coagir a eleição de magistrados que manipulam o funcionamento da justiça em relação a personagens que incorrem em atos de corrupção, que inclusive colocam a sociedade em risco de estabelecer uma narco-cleptocracia.

Casos como estes, onde a transparência e a responsabilidade por parte do setor público foram relaxadas ou adiadas, permitiram a compra supervalorizada de insumos e a contratação irregular de recursos tão necessários em tempos de crise, tais como oxigênio medicinal ou ventiladores mecânicos. Mas, além disso, encontramos outro fator comum: a cumplicidade de atores estatais, que incorrem em atos de corrupção juntamente com atores do setor privado que detêm o poder econômico.

Assim como na Guatemala, a conexão entre o poder político, o poder econômico e mesmo o judiciário significa que não podemos entender estes atos como simples corrupção. São estratégias para operar no mercado (ao contrário do canal normal) modificando e abusando das regras do jogo: uma cleptocracia; uma forma de corrupção institucionalizada.

A **cleptocracia** como forma de administrar o poder político não pode permanecer no lugar. Seu impacto não é apenas macroeconômico, mas também transgride os direitos fundamentais das pessoas, sendo ainda mais evidente nesta situação de pandemia pela COVID19. Por esta razão, é vital estabelecer políticas que promovam a transparência, que incluam de forma abrangente as populações vulneráveis, com uma abordagem transversal e intersetorial, ao mesmo tempo em que promovam apoio e proteção aos cidadãos que decidam denunciar práticas contrárias ao desenvolvimento de um Estado democrático.

Apesar disso, a mobilização cidadã e suas intenções de controlar o poder político foram mantidas, como no caso da Colômbia, onde as organizações da sociedade civil e a Academia empreenderam o estudo dos contratos feitos no âmbito da pandemia, emitindo as bandeiras vermelhas necessárias e denunciando os casos que apresentam custos excessivos ou suspeitas na ausência de objetividade e transparência na contratação.  Estes são, sem dúvida, um bom ponto de partida, mas precisamos de muito mais.

**Exigimos uma luta direta contra a cleptocracia entrincheirada em todos os estados da América Latina!**

* Proteção aos denunciantes (cidadãos) para evitar represálias trabalhistas, físicas e/ou econômicas. Acabe com o medo de denunciar e alertar sobre abusos!

* Apoio à mídia e seus jornalistas que realizam trabalhos de investigação e tornam a verdade transparente.

* Promoção de campanhas de informação para os cidadãos sobre contratos do Estado, sua importância e como a corrupção prejudica a todos.

* Criação de protocolos em nível nacional e supranacional para que certas comunidades possam ter o apoio de atenção às suas necessidades especiais, bloqueando o espaço para a apropriação indevida de fundos públicos.
