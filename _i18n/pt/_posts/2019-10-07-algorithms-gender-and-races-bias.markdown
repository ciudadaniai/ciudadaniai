---
layout: post
title: 'algoritmos replicam as desigualdades de gênero e raça'
intro: 'Nosso projeto Aliança A+ busca combater as barreiras impostas às mulheres na tecnologia.'
day: 07
month: Oct
date: 2019-10-07 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Nosso projeto Aliança A+ busca combater as barreiras impostas às mulheres na tecnologia.'
img: 07-10-19-pt.png
caption:
---
Hoje, a relação entre gênero e tecnologia está reproduzindo e até ampliando o modelo do patriarcado. Por exemplo, de todos os profissionais que trabalham em inteligência artificial (IA), apenas 22% são mulheres. Google e Facebook, enquanto isso, relatam que mulheres que trabalham em IA representam 10% e 15%, respectivamente. Na América Latina, apenas 38% dos usuários de internet são mulheres e, no Reino Unido, as mulheres que trabalham em segurança cibernética ganham 16% menos em comparação aos homens.

Se considerarmos raça, o cenário é ainda pior. Em 2018, o [Google reportou](https://diversity.google/annual-report/) que 25,7% das posições técnicas eram ocupadas por mulheres, mas esse número cai para 0,8% se considerarmos mulheres negras. A mesma tendência existe em outras grandes empresas de tecnologia, como [Facebook, Apple e Microsoft](https://www.vox.com/2017/11/9/16628286/apple-2017-diversity-report-black-asian-white-latino-women-minority), evidenciando a crise da diversidade no ecossistema tecnológico e de TI.

E por que esse tópico é tão importante hoje? Atualmente, há um rápido aumento de sistemas e aplicativos que automatizam processos, usando inteligência artificial (IA). Cada vez mais países criam [estratégias nacionais de IA](https://medium.com/politics-ai/an-overview-of-national-ai-strategies-2a70ec6edfd) para promover seu uso. Isso porque eles permitem resolver problemas de alta complexidade, facilitam a automação e conseguem personalizar processos com o uso eficiente de recursos.

## As máquinas não apenas obedecem, elas também aprendem (e podem aprender muito mal)

O campo de IA ​​é gigantesco e, dentro dessa combinação de algoritmos, está o Machine Learning (ML), ou aprendizagem por máquinas ou automático. Um sistema que é amplamente usado hoje e que basicamente consiste em que os computadores aprendam. Em outras palavras, o desempenho da máquina melhora com a experiência. Por exemplo, a [Netflix usa ML para recomendar de forma personalizada](https://www.wired.co.uk/article/how-do-netflixs-algorithms-work-machine-learning-helps-to-predict-what-viewers-will-like) conteúdo para os usuários, e essas recomendações são ajustadas com base nos dados fornecidos. Esses dados podem ser explícitos, como dar um like em um filme ou série, ou, na maioria das vezes, implícitos, como pausar uma certa série após 10 minutos ou assistir a um filme em dois dias. Todas essas informações que fornecemos à Netflix, juntamente com as de todos os outros usuários, formam o dataset, ou conjunto de dados, que o seu sistema de recomendação usa para melhorar constantemente.

Nesses processos, existem vários casos específicos que mostram como o uso da IA ​​para automatizar processos replica preconceitos de gênero. Por exemplo, em 2014, a Amazon começou a desenvolver um programa que, usando [Machine Learning](https://www.reuters.com/article/us-amazon-com-jobs-automation-insight-idUSKCN1MK08G), permitia revisar e avaliar automaticamente centenas de currículos. Esse sistema aprendeu com os currículos recebidos nos últimos 10 anos e com o desempenho das pessoas contratadas nesse período. Em 2015, a empresa percebeu que esse novo sistema desfavorecia as mulheres em cargos de desenvolvimento de software ou outras tarefas técnicas. Um reflexo do domínio histórico masculino na indústria de tecnologia. Apesar dos esforços, a Amazon não conseguiu reverter esse aprendizado e rejeitou o programa.

Por outro lado, Joy Buolamwini e Timnit Gebru, pesquisadores do MIT, [avaliaram o software de reconhecimento facial da IBM, Microsoft e Face ++](http://news.mit.edu/2018/study-finds-gender-skin-type-bias-artificial-intelligence-systems-0212), descobrindo que todas as empresas tiveram melhor desempenho no reconhecimento de rostos de homens do que mulheres e rostos de pessoas de pele clara versus pessoas de pele escura. Ao analisar os subgrupos, o reconhecimento facial das mulheres negras foi o que apresentou a maior taxa de erro nas três empresas: erro médio de 31%. Este resultado é contrastado com o erro de menos de 1% para homens brancos.

Essas taxas de erro são críticas. [Essa tecnologia é usada hoje pelos governos para detectar tendências criminais](https://www.nytimes.com/2017/05/01/us/politics/sent-to-prison-by-a-software-programs-secret-algorithms.html). E sabemos bem que isso pode ser usado para gerar um relatório de probabilidades de reincidência ou influenciar as sentenças das pessoas.

Exemplos como esses explicam o papel do uso da Inteligência Artificial na criação de sistemas automatizados. O fato de tais sistemas serem treinados com um conjunto de dados gerados por pessoas e sua experiência implica que os sistemas aprenderão e manterão a subjetividade que pode vir com eles. O que significa que os preconceitos existentes no mundo offline são transferidos para o online. Nesse sentido, é necessário promover estratégias que garantam que a adoção de novas tecnologias em grande escala pare a criação de mais desigualdades de gênero, raça, entre muitas outras.

Essa realidade assusta e se torna um cenário crítico quando pensamos que essa será e está sendo a tecnologia promovida pelos Estados e que influenciará diretamente as liberdades e os direitos civis do povo. Provavelmente, determinará bolsas de estudo, subsídios de moradia, permissões de migração e terá um papel fundamental na segurança do cidadão.

Na Cidadania Inteligente, trabalhamos constantemente para encontrar maneiras pelas quais os atuais vieses da sociedade não se replicam nesses sistemas e que possam ser usados ​​inclusive para revertê-los. Em aliança com [Women at the table](https://www.womenatthetable.net/) (W@tt), uma organização da sociedade civil que promove a igualdade de gênero, nós nos esforçamos para tornar visíveis os preconceitos existentes, chamando a atenção de diferentes atores para defender a transparência e comandar ações afirmativas nos algoritmos. Aderimos à [declaração](https://www.womenatthetable.net/blog/triple-a-affirmative-action-for-algorithms) de W@tt que promete:

* Defender e adotar diretrizes que estabeleçam transparência na tomada de decisões algorítmicas.

* Incluir uma variedade interseccional e número igual de mulheres no código de criação, design e tomada de decisão com base em algoritmos.

* A existência de cooperação internacional e uma abordagem baseada em direitos humanos no desenvolvimento desses sistemas.

Estamos em um ponto de inflexão na história. O número de sistemas automatizados de tomada de decisões é sem precedentes e esses estão sendo implementados em alta velocidade. Podemos deixar a tecnologia continuar colocando barreiras para as mulheres e grupos sub-representados, ou podemos usá-la a nosso favor. Na Cidadania Inteligente, continuaremos trabalhando para desenvolver ações afirmativas, a fim de corrigir os vieses que impedem a plena participação das pessoas nas diferentes esferas da sociedade.
