---
layout: post
title: 'Giro2020: As reuniões e debates com os candidatas'
intro: 'Analisamos quais propostas da candidata Benedita da Silva estão alinhadas com a publicação.'
day: 13
month: oct
date: 2020-03-21 12:00:00 -0300)
categories: Proyecto
highlighted: false
share_message: 'Revisa Giro2020'
img: Foto_blog.png
caption:
---
A iniciativa [Giro 2020](https://giro2020.org/) iniciou nesta quinta-feira (08/10) a série Encontros com Prefeitáveis que tem como objetivo criar um canal de diálogo entre candidatos da cidade do Rio de Janeiro e a sociedade civil.  O evento é produzido pela Casa Fluminense e a Fundação Cidadania Inteligente, mas conta também com a parceria de mais 20 organizações sociais cariocas. A deputada federal e ex-governadora do Rio, Benedita da Silva (PT), foi a primeira convidada.

Dividido em 3 blocos, o Encontro pautou os temas de saúde, educação, habitação, transporte, saneamento, segurança pública, mudanças climáticas, assistência social e juventude. A Casa Fluminense monitorou as respostas da candidata, analisando quais estavam alinhadas com as demandas trazidas na Agenda Rio 2030. A publicação é um conjunto de propostas de políticas públicas produzida pela Casa junto com a sua rede de parceiros, fruto de um trabalho de análise de dados e escuta ativa com pesquisadores, ativistas e organizações sociais.

Confira abaixo os principais pontos de convergência entre as temáticas da Agenda e a fala da candidata:

1 **Combate ao racismo estrutural**

*“A transversalidade do tema do racismo está garantido no nosso plano de orçamento. Essa questão tem que ser pautada na habitação, saúde, educação em todos as políticas públicas.”*

2 **Ampliar a cobertura da atenção básica nos municípios**

*“Vamos fazer a reabertura das clínicas da família e UPAs, recontratar os profissionais de saúde que foram demitidos em plena pandemia e buscar mais agentes especializados. Esses recursos vão vir do Fundo Municipal da Saúde.”*

3 **Realizar campanhas de sensibilização para a anemia falciforme**

*“O governo federal acabou com a superintendência que tratava da questão da anemia falciforme, e os municípios seguiram essa medida. Acabamos sem nenhuma política voltada para a doença, mas nós pretendemos retomar com essa superintendência dentro da Secretaria de Saúde.”*

4 **Financiar a segurança alimentar e nutricional nas escolas**

*“É importante que a prefeitura estimule que a agricultura familiar, responsável por 70% da nossa alimentação, seja fornecedora dos alimentos das escolas públicas.”*

5 **Manter a CEDAE como empresa pública responsável pela água e esgoto**

*“Sou contra a privatização, mas a Cedae precisa subir o morro e a gente vai fazer ela subir. É trabalho da prefeitura fiscalizar isso.”*

6 **Expandir serviços de coleta seletiva pelos municípios**

*“Quero tornar a Comlurb em parte uma empresa ambiental. Ela tem que trabalhar junto com as cooperativas dos catadores de reciclagem. Precisamos tornar esse trabalho rentável dentro das nossas comunidades.”*

7 **Estruturar os contratos das linhas de ônibus municipais**

*“O primeiro passo para uma mudança no transporte é rever esses contratos. O nosso compromisso é com o usuário não com os donos das empresas. Precisamos também repor as linhas que sumiram.”*

8 **Reduzir as tarifas do transporte público, controlando as margens de lucro**

*“A gente hoje não tem condição de tarifa zero, mas na medida em que se for melhorando as condições dos transportes, nós poderemos ir reduzindo gradativamente a tarifa.”*

9 **Estruturar política de habitação de interesse social nas áreas centrais dos municípios**

*“É preciso revitalizar de verdade a zona portuária com a criação também de residências, usando a infraestrutura sanitária que já existe e criando espaços culturais.”*

10 **Mapear estoque de imóveis e prédios públicos vagos**

*“Nós temos edificações – algumas inacabadas – que são do município. Vamos fazer todo esse levantamento do patrimônio, não só para criar albergues para as pessoas situação de rua como também para tornar essas construções residenciais.”*

O debate completo já está disponível no facebook da Casa Fluminense e pelo youtube. Na próxima quarta-feira, dia 14 de outubro, acontece o segundo encontro com prefeitáveis com a deputada estadual Renata Souza (PSOL).

Quem convoca os encontros?
Casa Fluminense, Fundação Cidadania Inteligente, Ação da Cidadania, Agência de Redes para a Juventude, Arteiros, Centro de Estudos de Segurança e Cidadania (CESeC), Casa Dona Amélia, Centro de Criação de Imagem Popular (CECIP), Circo Crescer e Viver, Coletivo Papo Reto, DataLabe, Centro Cultural Phabrika de Artes, Instituto Alziras, Instituto de Arquitetos do Brasil (IAB), Instituto de Estudos da Religião (ISER), Instituto de Políticas de Transporte e Desenvolvimento (ITDP), Instituto Mariele Franco, Mulheres Negras Decidem, Observatório de Favelas, Projeto Manivela, Redes da Maré, Ser Cidadão e Santa Cruz 2030.

Fotos: Mayara Donaria

Texto: Luiza Sampaio
