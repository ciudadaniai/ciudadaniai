---
layout: post
title: 'O COVID não é uma desculpa para ameaçar a liberdade na Internet'
intro: '8 propostas para reconfigurar a Internet pós-pandêmica.'
day: 17
month: junio
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'O COVID não é uma desculpa para ameaçar a liberdade na Internet'
img: pt-blog.png
caption:
---
A América Latina atravessa um período de incerteza marcado por um profundo descontentamento social e uma maré acelerada e opaca de mudanças sociais e políticas resultantes da emergência sanitária. Em 2019, países como Chile, Colômbia e México viviam momentos convulsivos onde, através do protesto social, exigiam melhorias e ampliação de direitos. Milhares de pessoas estavam se juntando a suas fileiras tentando transformar a ordem social e exigindo soluções para problemas estruturais, como corrupção e violência.

Mas 2020 chegou e a ação cidadã a que estávamos acostumados simplesmente deixou de ser possível. Embora tenhamos visto que os protestos contra o racismo e os abusos policiais não impediram que as pessoas se reunissem no México, no Brasil ou fora da região, a emergência sanitária cerceou os direitos dos cidadãos e aumentou os poderes dos que estão no poder para monitorar, controlar e, em muitos casos, legislar por decreto. Os governos nacionais e locais estão adotando novas tecnologias e sistemas de gestão de dados para enfrentar a emergência de saúde pública e o trauma econômico geral, aumentando a dependência de sistemas baseados em dados para rastrear doenças e traçar o perfil das populações. Os ritmos de expansão da COVID-19 definiram não só o encontro das pessoas nas ruas e a ação coletiva, mas também confinaram a cidadania ao espaço digital, um domínio em sua maioria privado, vigiado e opaco.

Essas mudanças não são [temporárias](https://www.technologyreview.com/2020/03/17/905264/coronavirus-pandemic-social-distancing-18-months/) nem [inconseqüentes](https://www.nesta.org.uk/blog/there-will-be-no-back-normal/), mas mudam radicalmente a forma como entendemos o exercício efetivo dos direitos humanos e políticos. Talvez seja o nosso cotidiano que já esteja começando a experimentar essa nova normalidade. Temos recorrido à virtualidade para o trabalho, escola, ativismo e relacionamentos cotidianos. Mas a digitalização das interações sociais gera questionamentos da sociedade civil sobre o falso dilema com o qual muitos governos nos confrontam: o da saúde ou da privacidade.

Isso se deve à forma antidemocrática com que as relações sociais têm sido moldadas no ambiente digital. Portanto, controlar e enfrentar a crise da saúde através da tecnologia tem acentuado as desigualdades e nos apresentado desafios urgentes para reconfigurar um espaço digital a serviço da democracia.

Uma delas é reconhecer a Internet como um espaço público que habitamos e que nos permite exercer direitos e liberdades: como a liberdade de expressão, de reunião, de privacidade. Questionar também se a Internet é atualmente um espaço democrático e identificar quais condições colocam em risco nossa vida democrática nos espaços digitais.

Se visamos uma nova normalidade, que mudanças estruturais precisamos extrapolar para o âmbito digital? Que discursos, idéias e concepções sobre tecnologia impedem ou ameaçam uma **Internet de direitos e liberdades que permita a ação coletiva e a participação cidadã?**

Não há momento mais urgente para **reconfigurar as narrativas ou discursos predominantes na Internet e na democracia** e propor alternativas que apontem em outra direção. Entretanto, a participação da sociedade civil nos debates sobre direitos digitais ainda é limitada a algumas poucas organizações, por isso é vital reunir diversas vozes, desde ambientalismo, lutas de gênero, defesa dos direitos humanos, entre muitas outras.


* Hoje a Internet é vista como "um mundo não real" (ou seja, o mundo real e o mundo digital).

***Propomo-nos a reconhecer a internet como um espaço público que compartilhamos e habitamos. A rede é também um espaço real, com impactos tangíveis em nossas vidas.***

* A maior parte do uso da Internet e das tecnologias digitais tem se voltado para os negócios e a publicidade. Isso abriu a porta para o aumento da vigilância governamental e da censura de vozes dissidentes. Hoje a Internet e o rastreamento que permite a atividade digital são utilizados por governos autoritários para exercer controle e repressão.

***Defendamos a Internet como um bem comum, como espaço de afirmação e defesa dos direitos e ações coletivas. Defendamos as tecnologias digitais como ferramentas de apoio aos processos de mudança social, organização, reunião e participação. Defendamos uma Internet que fortaleça a democracia!***

* Alguns grandes players têm o monopólio de definir como funcionam as redes e sistemas que utilizamos, como interagimos, o que vemos, que conteúdo é privilegiado.

***Mas ainda criamos conteúdos e relações sócio-digitais, identidades e novas formas de pensar e fazer através desses sistemas. Não somos apenas usuários, uma boa parte da nossa vida democrática acontece ali mesmo. É necessário, portanto, tornar a gestão desses sistemas mais democrática e aberta.***

* As tecnologias digitais são vistas como uma solução para todos os tipos de problemas.

***No entanto, é fundamental submeter a implementação de novas tecnologias à deliberação democrática. O que elas envolvem? Como funcionam? Quais são os riscos relacionados? Quem elas beneficiam e quem elas prejudicam? Como se tornam mais benéficas? Elas são realmente necessárias?***

* A vigilância parece ser justificada com o argumento de que "eu não tenho nada a esconder".

***Acreditamos que a democracia e a privacidade estão intimamente relacionadas. Uma democracia saudável depende da interação de indivíduos autônomos. A autonomia individual depende de ter privacidade. Defender a Internet como um espaço onde nos preocupamos com a privacidade beneficia a sociedade como um todo.***

* Atualmente, prevalece um discurso de "selvageria na internet" onde a violência é normalizada e seu impacto é minimizado.

***É importante não normalizar a violência digital como parte das interações na Internet, reconhecendo que ela tem impactos reais na vida das pessoas. A violência digital é uma extensão da violência "análoga", que é evidente e realizada através da tecnologia.***

* O digital é visto como um mundo ilimitado em que tudo se torna viral e se reproduz até o infinito

***Devemos estar conscientes de que o digital tem um impacto físico e social. As visitas, os gostos, as peças, as interações envolvem mais consumo de energia, geração de lixo digital e uma maior implantação de infra-estrutura, como cabos marinhos e data centers. Por exemplo, [estima-se](https://theshiftproject.org/article/climat-insoutenable-usage-video/) que até 2020 apenas a transmissão de vídeo emitiria tanto CO2 quanto a Espanha.***

*  A tecnologia no contexto das emergências de saúde tem [colocado](https://www.accessnow.org/tecnologias-de-vigilancia-para-controlar-el-covid-19-en-america-latina/)uma dicotomia entre saúde e privacidade.

***A tecnologia pode apoiar o controle da propagação da pandemia, respeitando direitos e liberdades. Todas as iniciativas baseadas em tecnologia e gestão de bancos de dados pessoais e de saúde devem ser transparentes, respeitosas aos direitos e obedecer a medidas que protejam nossas informações. A situação de alerta que enfrentamos com a COVID-19 não é um pretexto para a coleta de dados desnecessários, não melhorando as práticas de proteção e desenvolvimento ético da tecnologia no interesse público.***

Aqui você pode conferir o [vídeo](https://youtu.be/xvF_trReL5E)

**Autores**


*Ricardo Zapata - El Derecho a No Obedecer (Colombia)*

*Beatriz Quesadas - SocialTIC (México)*

*Haydeé Quijano - SocialTIC (México)*

*Daniela Jordán - Frena la Curva (Chile)*
