---
layout: post
title: 'A violência política como um ataque permanente à democracia'
intro: 'Exigimos medidas de proteção a todos os Estados da América Latina.'
day: 23
month: junio
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'A violência política como um ataque permanente à democracia'
img: violence.png
caption:
---
A América Latina guarda no seu processo de constituição um histórico de expropriação e violência contra os povos originários, africanos e afro descentes que aqui foram escravizados e vitimados pelo abandono estatal no centro de sua constituição colonial. A colonização não foi um evento histórico que encontra limites entre o seu início, meio e fim. Por meio da compreensão de seus efeitos sobre as dinâmicas de organização social, a essas dinâmicas intrínsecas ao tecido social dos países latino americanos chama-se colonialismo.

Dessa forma, os processos sociais, educacionais, formativos e políticos instalados nesses países acumulam as tecnologias do colonialismo, que mantém os desequilíbrios e assimetrias no acesso ao poder, as instituições e aos direitos sociais como o padrão institucional.

Em muitos países da América Latina, por meio das lutas incansáveis dos movimentos sociais de bases e da projeção de algumas lideranças, foi possível acompanhar a experiência de um reposicionamento dos grupos socialmente minorizados por meio do acesso a renda, a cargos de liderança e universidades. Ainda assim, a brevidade da ascensão dos grupos minorizados socialmente não teve capacidade de penetrar em estruturas tão bem assentadas de controle e poder estabelecidas pelo colonialismo, sendo acompanhado, por outro lado, do enfrentamento declarado a esses setores, pelo desequilíbrio promovido pela luta por igualdade. As elites econômicas e políticas da América Latina e Caribe tomaram a frente em um processo de moralismo e manipulação das pautas para a garantia de direitos sociais e ampliação de direitos, defendendo um estado mínimo no que se refere aos direitos e máximo no que se volta à punição e morte.

O acompanhamos hoje é a materialização da necropolítica, da violência política e da violência política contra mulheres no centro de um avanço conservador e neocolonial.

Com o contexto da pandemia, no Brasil, foi possível acompanhar a apresentação de toda a agenda nefasta de ataque à população vulnerabilizada de uma só vez. De março a Junho a violência contra mulheres em todo Brasil aumentou cerca de 22%, segundo dados do Fórum Brasileiro de Segurança Pública. Já em relação aos moradores de favelas e periferias e população negra, esses além de figurarem como as maiores vítimas fatais da COVID-19, ainda durante a quarentena,  dobrou o número de vitimados pela violência policial em estados como Rio de Janeiro e São Paulo, ambos epicentros de contágio do novo coronavírus no Brasil. Mas situação semelhante ocorreu, em menor medida, em outros países da américa latina, como Chile, Equador, Argentina e México, através da carência na atenção social, suporte econômico e acesso à saúde pública, sobretudo diante dos grupos historicamente vulnerabilizados.  Diante de uma política de segurança pública , em casos como o do Brasil, a letalidade das ações do estado invadiu as casas de pobres e afrodescendentes, quando a recomendação era ficar em casa, bem como a  ausência de ações de segurança financeira ou alimentar conduziam à morte, de igual maneira.

De modo que as práticas compreendidas como violência política, que se materializam nos campos físicos, econômicos, simbólicos, psicológicos e virtuais tem encontrado terreno fértil para difusão, sendo hoje o direito do inimigo a prática em vigor, cada vez que direitos coletivos, difusos e que buscam corrigir desigualdades se apresentam. Os padrões estabelecido nas relações sociais, políticas e virtuais deste tempo revelam hoje como a violência política implica em prejuízo à todos, não apenas aos diretamente vitimados.

A violência política contra as mulheres no Brasil, em relação às prefeitas brasileiras, é a segunda principal barreira para acesso e permanência dessas na política, segundo dados do Instituto Alziras. E, mesmo em países como México e Bolívia, que contam com uma legislação avançada sobre o tema, é recorrente ações que busquem impedir ou cessar a participação política das mulheres, chegando em muitos casos à violência física e assassinatos. Em relação aos jovens, moradores de favelas e periferias, lideranças comunitárias, ativistas políticos, acompanhamos uma crescente criminalização e aguda precarização das condições de trabalho e vida, uma curva ascendentes de ataques e descendente de posição em espaços de tomadas de poder e decisão.

Por isso, é central que sejam promovidas ações que possibilitem que hoje, no tempo atual, as condições que afetam a participação política desses grupos sejam modificadas. É necessário atenção às condições materiais, de acesso à renda, mas também de suporte político, como infraestrutura para a participação dos processos e segurança. A desnaturalização da violência política e seu enfrentamento é parte de condição central para a defesa da democracia e fortalecimento das instituições públicas e grupos minorizados, para que mais do que acessem mais espaços de poder e tomada de decisão, possam ali permanecer em segurança.

Exigimos medidas de proteção a todos os Estados da América Latina:

* Campanhas públicas que evidenciem as desigualdades estruturais de gênero, raça, classe, sexualidade e território.

* Sejam elaborados projetos de lei nacionais e sejam difundidos marcos supranacionais sobre violência política.

* Que sejam apoiadas iniciativas e organizações que atuem diante da proteção de grupos minorizados e de lideranças políticas populares.

* Promover e garantir, da melhor forma possível, espaços para a representação de grupos historicamente vulnerabilizados na construção, desenho e implementação de políticas públicas.

* A reconfiguração da força estatal, desde a sua formação, com foco especial na diminuição da reação excessiva.

Aqui você pode conferir o [vídeo](https://www.youtube.com/watch?v=ZBTR9riz-iQ)
