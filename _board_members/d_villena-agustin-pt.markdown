---
ref: agustinvillena
lang: pt
name: Agustin Villena
function: Secretário
bio: Engenheiro de software e professor na Universidade do Chile. Praticante e mentor das metodologias de trabalho colaborativo Lean+Agile com o objetivo de melhorar as culturas trabalhistas no mundo de trabalho incerto e dinâmico de hoje. Pioneiro da Agile no Chile desde 2001, e entre os pioneiros do Lean Kanban na América Latina desde 2007
image: agustin.jpg
email: avillena@ciudadaniai.org
network_twitter: https://twitter.com/agustinvillena
network_linkedin: http://cl.linkedin.com/in/agustinvillena
network_github:
active: true
---
