---
ref: paularojo
lang: es
name: Paula Rojo
function: Presidenta
bio: Diseñadora Industrial. Cuenta con años de experiencia en comunicaciones, movilización ciudadana, desarrollo de habilidades emprendedoras, gestión, diseño y desarrollo de proyectos. Ha estado a cargo de organizaciones de la sociedad civil que han sido pioneras en la implementación y uso de las TICs, la alfabetización digital, el voluntariado y movilización social. Junto con ello es cofundadora de Mi Voz, empresa de comunicaciones que cuenta con una agencia y con 14 medios ciudadanos, siendo la primera red de periodismo ciudadano de habla hispana, con cientos de corresponsales
image: paula.jpg
email: projo@ciudadaniai.org
network_twitter: https://twitter.com/paularojo
network_linkedin: https://cl.linkedin.com/in/paula-rojo-almarza-4b995455
network_github:
active: true
---
