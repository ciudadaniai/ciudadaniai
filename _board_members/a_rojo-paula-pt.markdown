---
ref: paularojo
lang: pt
name: Paula Rojo
function: Presidenta
bio: Designer industrial, com anos de experiência em comunicação, mobilização cidadã, desenvolvimento de habilidades empreendedoras, gestão, design e desenvolvimento de projetos.  Esteve à frente de organizações da sociedade civil  pioneiras na implementação e uso das TICs, alfabetização digital, voluntariado e mobilização social. É co-fundadora da Mi Voz, uma empresa de comunicação que tem uma agência e 14 meios de comunicação cidadãos, sendo a primeira rede de jornalismo cidadão de língua espanhola, com centenas de correspondente
image: paula.jpg
email: projo@ciudadaniai.org
network_twitter: https://twitter.com/paularojo
network_linkedin: https://cl.linkedin.com/in/paula-rojo-almarza-4b995455
network_github:
active: true
---
