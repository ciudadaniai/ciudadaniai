---
ref: agustinvillena
lang: es
name: Agustin Villena
function: Secretario
bio: Ingeniero de software y profesor de la Universidad de Chile. Practicante y mentor en metodologías de trabajo colaborativo Lean+Agile con el propósito de mejorar las culturas laborales en el incierto y dinámico mundo del trabajo actual. Pionero de Agile en Chile desde 2001, y entre los pioneros de Lean Kanban en Hispanoamérica de desde 2007
image: agustin.jpg
email: avillena@ciudadaniai.org
network_twitter: https://twitter.com/agustinvillena
network_linkedin: http://cl.linkedin.com/in/agustinvillena
network_github:
active: true
---
