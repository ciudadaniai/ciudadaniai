---
ref: alvaroramirez
lang: es
name: Álvaro Ramirez
function: Director
bio: Co-fundador del GIGAPP, profesor en la Universidad de Chile y director académico del Diploma en Gobierno Abierto e Innovación en el Sector Público. Ha sido director de diversas organizaciones de la sociedad civil en temas de transparencia y participación ciudadana. Consultor internacional en modernización del Estado, gobierno abierto e innovación, y ponente de la Carta Iberoamericana de Gobierno Abierto del CLAD en 2016
image: alvaro.png
email: 
network_twitter: 
network_linkedin: 
network_github:
active: true
---
