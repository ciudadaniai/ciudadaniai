---
ref: luciadammert
lang: es
name: Lucía Dammert
function: Directora
bio: Socióloga con maestría en políticas públicas y doctorado en ciencias políticas, ha escrito libros y artículos en revistas nacionales y extranjeras, así como ha sido docente y conferencista en temas vinculados a seguridad estratégica, políticas públicas y justicia criminal en múltiples países. Especialista en políticas públicas con amplia experiencia en seguridad ciudadana y reforma del Estado en América Latina
image: lucia.png
email: 
network_twitter: 
network_linkedin: 
network_github:
active: true
---
