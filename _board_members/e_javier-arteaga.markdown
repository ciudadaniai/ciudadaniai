---
ref: javierarteaga
lang: es
name: Javier Arteaga
function: Director
bio: Publicista, con especialización en Opinión Pública y Marketing Político. Creador y director de Feeling, metodología para innovar centrada en los sentimientos de las personas. Experto en Gobierno Abierto e Innovación Social. Docente Universitario. Miembro de la Red de innovación Política de Latinoamérica
image: javier.jpg
email: bienpensado@gmail.com
network_twitter: 
network_linkedin: https://co.linkedin.com/in/bienpensado
network_github:
active: true
---
