---
ref: mariajaraquemada
lang: es
name: María Jaraquemada
function: Directora
bio: Abogada de la Universidad Católica con Magíster en Derechos Fundamentales de la Universidad Carlos III de Madrid. Con experiencia de más de 15 años en materias de integridad, transparencia y fortalecimiento democrático, tanto en organizaciones públicas como de la sociedad civil. Fue parte del equipo de Ciudadanía Inteligente como Directora de Investigación e Incidencia del 2014 al 2015. Desde enero de 2024 integra el Consejo Directivo del Consejo para la Transparencia por un periodo de 6 años
image: maria.png
email: 
network_twitter: 
network_linkedin: 
network_github:
active: true
---
