---
ref: paularojo
lang: en
name: Paula Rojo
function: President
bio: Industrial designer. She has years of experience in communications, citizen mobilization, development of entrepreneurial skills, management, design and project development. She has been in charge of civil society organizations that have been pioneers in the implementation and use of ICTs, digital literacy, volunteering and social mobilization. She is also the co-founder of Mi Voz, a communications company that has an agency and 14 citizen media, being the first Spanish-speaking citizen journalism network, with hundreds of citizen correspondents
image: paula.jpg
email: projo@ciudadaniai.org
network_twitter: https://twitter.com/paularojo
network_linkedin: https://cl.linkedin.com/in/paula-rojo-almarza-4b995455
network_github:
active: true
---
