---
ref: julietasuarez
lang: en
name: Julieta Suárez Cao
function: Vice President
bio: Associate Professor in the area of ​​Comparative Politics at the Catholic University of Chile. She is coordinator of the Network of Women Political Scientists. She holds a doctorate (Ph.D.) in Political Science from Northwestern University, United States. Her areas of expertise are political and government institutions, federalism and subnational politics, political parties, women's representation, and electoral systems. She participated in the design of the parity gender electoral system that was implemented for the elections to the Constitutional Convention in Chile in 2021
image: julieta.jpg
email: jujuchi@gmail.com
# network_twitter: https://twitter.com/paularojo
# network_linkedin: https://www.linkedin.com/profile/view?id=196439757
# network_github:
active: true
---
