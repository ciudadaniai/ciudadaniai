---
ref: javierarteaga
lang: pt
name: Javier Arteaga
function: Diretor
bio: Publicista, especializado em Opinião Pública e Marketing Político. Criador e diretor da Feeling, uma metodologia para inovação focada nos sentimentos das pessoas. Especialista em Governo Aberto e Inovação Social. Docente universitário. Membro da Rede Latino-Americana de Inovação Política.
image: javier.jpg
email: bienpensado@gmail.com
network_twitter: 
network_linkedin: https://co.linkedin.com/in/bienpensado
network_github:
active: true
---
