---
ref: julietasuarez
lang: pt
name: Julieta Suárez Cao
function: Vicepresidenta
bio: Professora associada na área de Política Comparada na Universidade Católica do Chile. Coordenadora da Red de Politólogas. Doutora (Ph.D.) em Ciência Política pela Northwestern University, Estados Unidos. Suas áreas de especialidade são instituições políticas e governança, federalismo e política subnacional, partidos políticos, representação de mulheres e sistemas eleitorais. Ela participou do desenho do sistema eleitoral de paridade de gênero que foi implementado para as eleições da Convenção Constitucional no Chile em 2021
image: julieta.jpg
email: jujuchi@gmail.com
# network_twitter: https://twitter.com/paularojo
# network_linkedin: https://www.linkedin.com/profile/view?id=196439757
# network_github:
active: true
---
