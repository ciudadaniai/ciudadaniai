---
ref: javierarteaga
lang: en
name: Javier Arteaga
function: Director
bio: Publicist, specialized in Public Opinion and Political Marketing. Creator and director of Feeling, a methodology to innovate focused on people's feelings. Expert in Open Government and Social Innovation. University teacher. Member of the Political Innovation Network of Latin America
image: javier.jpg
email: bienpensado@gmail.com
network_twitter: 
network_linkedin: https://co.linkedin.com/in/bienpensado
network_github:
active: true
---