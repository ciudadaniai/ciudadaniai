---
ref: agustinvillena
lang: en
name: Agustin Villena
function: Secretary
bio: Software engineer and professor at the University of Chile. Practitioner and mentor in Lean+Agile collaborative work methodologies with the purpose of improving work cultures in today's uncertain and dynamic world. He pioneered Agile methodologies in Chile since 2001, and is among the pioneers of Lean Kanban in Latin America since 2007
image: agustin.jpg
email: avillena@ciudadaniai.org
network_twitter: https://twitter.com/agustinvillena
network_linkedin: http://cl.linkedin.com/in/agustinvillena
network_github:
active: true
---
