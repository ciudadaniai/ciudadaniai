---
ref: julietasuarez
lang: es
name: Julieta Suárez Cao
function: Vicepresidenta
bio: Profesora Asociada en el área de Política Comparada de la Universidad Católica de Chile. Coordinadora de la Red de Politólogas. Doctora (Ph.D.) en Ciencia Política por la Universidad de Northwestern, Estados Unidos. Sus áreas de especialización son las instituciones políticas y de gobierno, el federalismo y la política subnacional, los partidos políticos, la representación de mujeres y los sistemas electorales. Participó del diseño del sistema electoral paritario de género que se implementó para las elecciones a la Convención Constitucional en 2021
image: julieta.jpg
email: jujuchi@gmail.com
# network_twitter: https://twitter.com/paularojo
# network_linkedin: https://www.linkedin.com/profile/view?id=196439757
# network_github:
active: true
---
