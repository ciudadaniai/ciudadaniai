---
ref: sistemadhondt
lang: pt
name: ¿Qué es el sistema D’Hondt y cómo funciona?
boton_descarga: Descarga la campaña acá y ayúdanos a difundir
descarga: sistema-dhondt.zip
modalname: sistemadhondt
slide_id: sistemadhondt-slide
image: sistema-dhondt/sistema-1.png
image2: sistema-dhondt/sistema-2.png
image3: sistema-dhondt/sistema-3.png
image4: sistema-dhondt/sistema-4.png
image5: sistema-dhondt/sistema-5.png
image6: sistema-dhondt/sistema-6.png
image7: sistema-dhondt/sistema-7.png
image8: sistema-dhondt/sistema-8.png
image9: sistema-dhondt/sistema-9.png
image10:
video1:
video2:
video3:
video4:
video5:
video6:
video7:
video8:
video9:
video10:
active: true
---