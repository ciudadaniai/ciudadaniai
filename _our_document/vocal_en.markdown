---
ref: vocal
lang: en
name: ¿Saliste vocal de mesa?
boton_descarga: Descarga la campaña acá y ayúdanos a correr la voz
descarga: vocaldemesa.zip
modalname: vocal
slide_id: vocal-slide
image: vocal/vocal-1.png
image2: vocal/vocal-2.png
image3: vocal/vocal-3.png
image4: vocal/vocal-4.png
image5: vocal/vocal-5.png
image6:
image7:
image8:
image9:
image10:
video1:
video2:
video3:
video4:
video5:
video6:
video7:
video8:
video9:
video10:
active: true
---