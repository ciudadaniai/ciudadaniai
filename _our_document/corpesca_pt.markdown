---
ref: corpesca
lang: pt
name: En Chile intentaron comprar la democracia
boton_descarga: Descarga la campaña acá y hagamos ruido para castigar la corrupción
descarga: corpesca.zip
modalname: corpesca
slide_id: corpesca-slide
image: corpesca/corpesca-1.png
image2: corpesca/corpesca-2.png
image3:
image4:
image5: corpesca/corpesca-5.png
image6: corpesca/corpesca-6.png
image7: corpesca/corpesca-7.png
image8: corpesca/corpesca-8.png
image9: corpesca/corpesca-9.png
image10: corpesca/corpesca-10.png
video1:
video2:
video3: corpesca/corpesca-3.mp4
video4: corpesca/corpesca-4-mp4
video5:
video6:
video7:
video8:
video9:
video10:
active: true
---