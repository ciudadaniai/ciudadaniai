---
ref: lcen-tema
lang: pt
name: Temas La Constitución es Nuestra
boton_descarga: Descarga la campaña acá
descarga: lcen-tema.zip
modalname: lcen-tema
slide_id: lcen-tema-slide
image: lcen-tema/lcen-tema-cultura.png
image2: lcen-tema/lcen-tema-democracia.png
image3: lcen-tema/lcen-tema-derechoscivilesypoliticos.png
image4: lcen-tema/lcen-tema-digital.png
image5: lcen-tema/lcen-tema-educacion.png
image6: lcen-tema/lcen-tema-genero.png
image7: lcen-tema/lcen-tema-infancia.png
image8: lcen-tema/lcen-tema-medioambiente.png
image9: lcen-tema/lcen-tema-politicafiscalyeconomia.png
image10: lcen-tema/lcen-tema-pueblosoriginarios.png
image11: lcen-tema/lcen-tema-salud.png
image12: lcen-tema/lcen-tema-trabajo.png
image13: lcen-tema/lcen-tema-vivienda.png
video1:
video2:
video3:
video4:
video5:
video6:
video7:
video8:
video9:
video10:
video11:
video12:
video13:
active: true
---