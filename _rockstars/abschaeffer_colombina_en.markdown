---
ref: colombina
lang: en
sede: chile
name: Colombina Schaeffer
function: Executive Director
bio: Sociologist from the P. Catholic University of Chile, PhD in Government and International Relations (University of Sydney).
image: colombina.jpg
email: cschaeffer@ciudadaniai.org
network_twitter: https://twitter.com/c_schaeffer
network_linkedin: https://www.linkedin.com/in/colombina-schaeffer-a39220160/
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
