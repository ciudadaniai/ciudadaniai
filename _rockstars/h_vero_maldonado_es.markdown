---
ref: vero
lang: es
sede: chile
name: Verónica Maldonado
function: Coordinadora de Administración y Finanzas
bio: Administradora Pública con mención en Gestión Pública de la U. de Chile. Su trayectoria se ha centrado en el área de administración y finanzas de distintas empresas y organizaciones del sector cultural.
image: vero.jpg
email: vmaldonado@ciudadaniai.org
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
