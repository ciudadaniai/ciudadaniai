---
ref: colombina
lang: es
sede: chile
name: Colombina Schaeffer
function: Directora Ejecutiva Subrogante
bio: Socióloga de la Universidad Católica de Chile y doctora en Gobierno y Relaciones Internacionales de la University of Sydney, Australia.
image: colombina.jpg
email: cschaeffer@ciudadaniai.org
network_twitter: https://twitter.com/c_schaeffer
network_linkedin: https://www.linkedin.com/in/colombina-schaeffer-a39220160/
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
