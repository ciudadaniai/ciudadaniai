---
ref: fran
lang: es
sede: chile
name: Francisca Valenzuela
function: Coordinadora de Comunicaciones
bio: Periodista de la Universidad de Chile con mención en comunicación estratégica. Cuenta con cuatro años de experiencia trabajando en comunicaciones externas en campañas de comunicación y organizaciones de la sociedad civil.
image: fran.png
email: fvalenzuela@ciudadaniai.org
network_linkedin:
active: true
---

