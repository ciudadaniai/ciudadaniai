---
ref: gabriela
lang: pt
sede: chile
name: Gabriela Luna Barraza
function: Coordinadora de proyectos e Investigación
bio: Licenciada en Ciencias Sociales. Major en Derecho, Minor en Políticas Públicas. Egresada de Derecho, Pontificia Universidad Católica de Chile.
image: gabriela.jpg
email: gluna@ciudadaniai.org
active: true
---
