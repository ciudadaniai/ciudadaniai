---
ref: felipe
lang: es
sede: chile
name: Felipe Perry
function: Desarrollador
bio: Periodista derivado en desarrollador. Linuxero de vocación y trabaja convencido de que la tecnología puede ayudarnos a vivir mejor (y/o peor).
image: felipe.png
email: fperry@ciudadaniai.org
network_twitter: https://twitter.com/feliperry
network_linkedin:
network_github: https://github.com/pedregalux
network_googleplus:
network_facebook:
network_instagram:
active: true
---
