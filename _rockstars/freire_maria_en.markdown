---
ref: malu
lang: en
name: Maria Luiza Freire
function: Project and Research Coordinator
bio: Lawyer from the PUC -Rio, feminist and decolonialist. Master student in public policies and urban planning in Latin America at IPPUR / UFRJ. Innovating in political participation.
image: malu.jpg
email: mfreire@cidadaniai.org
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
