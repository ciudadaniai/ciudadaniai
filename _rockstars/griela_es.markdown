---
ref: gabriela
lang: es
sede: chile
name: Gabriela Luna Barraza
function: Coordinadora de Proyectos e Investigación
bio: Licenciada en Ciencias Sociales. Major en Derecho, Minor en Políticas Públicas. Egresada de Derecho, Pontificia Universidad Católica de Chile.
image: gabriela.jpg
email: gluna@ciudadaniai.org
active: true
---
