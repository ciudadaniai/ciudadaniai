---
ref: sebastian
lang: en
sede: chile
name: Sebastián Benfeld
function: Coordinador de proyectos
bio: Periodista con minor en sostenibilidad por la Pontificia Universidad Católica de Valparaíso y diplomado en periodismo de investigación por CIPER y la Universidad Diego Portales.
image: sebastian.png
email: sbenfeld@ciudadaniai.org
active: true
---
