---
ref: vanessa
lang: es
sede: chile
name: Vanessa González
function: Coordinadora de Proyectos e Investigación
bio: Cientista Política de la U. Católica de Chile. Licenciada en Ciencias Sociales en la misma universidad con concentración mayor en Relaciones Internacionales, y dos concentraciones menores en Políticas Públicas y Promoción y cuidado de la salud.
image: vane.jpg
email: vanessa@ciudadaniai.org
network_linkedin: https://www.linkedin.com/in/vanessa-gonz%C3%A1lez-ratsch-4824a7143/
active: true
---
