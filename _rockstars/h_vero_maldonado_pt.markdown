---
ref: vero
lang: pt
sede: chile
name: Verónica Maldonado
function: Coordenadora de Administração e Finanças
bio: Administradora Pública com menção em Gestão Pública pela Universidade do Chile. Tem se dedicado à área de administração e finanças de diversas empresas e organizações do setor cultural.
image: vero.jpg
email: vmaldonado@ciudadaniai.org
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
