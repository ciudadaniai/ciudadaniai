---
ref: abel
lang: es
sede: chile
name: Abel Escobar
function: Coordinador Laboratorio de Diseño y Experiencia
bio: Diseñador gráfico de Duoc UC, con especialización en diseño de experiencia y desarrollo front-end. Actualmente, se encuentra cursando un Magíster en Diseño Avanzado con mención en Diseño de Servicios en la Universidad Católica de Chile (MADA) y además ejerce la docencia en diseño. Su dedicación se refleja tanto en su experiencia profesional como en su compromiso con la enseñanza.
image: abel.jpg
email: aescobar@ciudadaniai.org
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
