---
ref: mariapaz
lang: en
sede: chile
name: María Paz Aedo
function: Consultora en investigación y diseños metodológicos
bio: Socióloga de la Universidad de Chile. Postítulo en Estudios de Género y Sociedad de la Universidad Academia de Humanismo Cristiano. Máster en Humanidades Ecológicas, Sustentabilidad y Transición Ecosocial de la Universidad Politécnica de Valencia y Autónoma de Madrid. Doctora en Educación y postdoctorada en la Universidad de Santiago y en la Universidad de Barcelona. Amplia experiencia en procesos de formación e investigación en temas de género, ecología política y transiciones socioecológicas justas.
image: mpaz.jpg
email: mpaedo@ciudadaniai.org
active: true
---
