---
ref: colombina
lang: pt
sede: chile
name: Colombina Schaeffer
function: Coordinadora General
bio: Socióloga pela Universidade Católica do Chile e doutora em Governo e Relações Internacionais pela University of Sydney, Austrália.
image: colombina.jpg
email: cschaeffer@ciudadaniai.org
network_twitter: https://twitter.com/c_schaeffer
network_linkedin: https://www.linkedin.com/in/colombina-schaeffer-a39220160/
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
