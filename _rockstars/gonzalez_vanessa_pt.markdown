---
ref: vanessa
lang: pt
sede: chile
name: Vanessa González
function: Coordenadora de Projetos e Pesquisas
bio: Cientista Política da U. Católica do Chile. Licenciada em Ciências Sociais pela mesma universidade com concentração em Relações Internacionais e ênfases em Políticas Pública e promoção de cuidado da saúde.
image: vane.jpg
email: vanessa@ciudadaniai.org
network_linkedin: https://www.linkedin.com/in/vanessa-gonz%C3%A1lez-ratsch-4824a7143/
active: true
---
