---
ref: octavio
lang: pt
sede: chile
name: Octavio Del Favero
function: Coordinador General
bio: Advogado formado pela Universidade do Chile, mestre em Política Comparada pela London School Economics.
image: octavio.jpg
email: odfavero@ciudadaniai.org
network_twitter: https://twitter.com/octaviodfb
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
sede: chile
---
