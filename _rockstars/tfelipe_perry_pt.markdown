---
ref: felipe
lang: pt
sede: chile
name: Felipe Perry
function: Desenvolvedor
bio: Jornalista transformado em desenvolvedor. Linuxero de vocação e convencido de que a tecnologia pode nos ajudar a viver melhor.
image: felipe.png
email: fperry@ciudadaniai.org
network_twitter: https://twitter.com/feliperry 
network_linkedin:
network_github: https://github.com/pedregalux
network_googleplus:
network_facebook:
network_instagram:
active: true
---
