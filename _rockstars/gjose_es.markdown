---
ref: jose
lang: es
sede: chile
name: Josefina Correa Pérez
function: Coordinadora de Incidencia
bio: Abogada de la Universidad de Chile. Magíster en Derecho mención en Derecho Público. Amplia experiencia en el desarrollo de estrategias de incidencia política, litigación estratégica, diseño de herramientas y abordaje comunicacional político de materias de campañas vinculadas a agendas sociales, medio ambiente y democracia.
image: jose.jpg
email: jcorrea@ciudadaniai.org
network_linkedin: 
active: true
---
