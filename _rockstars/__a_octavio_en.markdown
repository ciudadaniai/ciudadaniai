---
ref: octavio
lang: en
sede: chile
name: Octavio Del Favero
function: Executive Director
bio: Lawyer from the University of Chile, MSc in Comparative Politics from University College London.
image: octavio.jpg
email: odfavero@ciudadaniai.org
network_twitter: https://twitter.com/octaviodfb
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
