let data = {
type: 'FeatureCollection',
features: [ {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-71.54296, -35.67514]
   },
   properties: {
     title_es: "Vota Inteligente",
     title_en: "Vote Smart",
     title_pt: "Vota Inteligente",
     description_es: "Herramienta digital para informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política. En Chile se realizó 6 veces en los años 2009, 2012, 2013, 2014, 2016, 2017",
     description_en: "Digital tool to inform citizens on presidential candidates, putting technology at the service of politics. In Chile, it was implemented 6 times in the years 2009, 2012, 2013, 2014, 2016, 2017",
     description_pt: "Ferramenta digital para informar a cidadania sobre as candidaturas presidenciais, colocando a tecnologia à disposição da política. No Chile, foi implementado 6 vezes nos anos de 2009, 2012, 2013, 2014, 2016, 2017",
     pais: "Chile",
     color: "#FFB906",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-58.44383, -23.4425]
   },
   properties: {
     title_es: "Vota Inteligente",
     title_en: "Vote Smart",
     title_pt: "Vota Inteligente",
     description_es: "Herramienta digital para informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política. ",
     description_en: "Digital tool to inform citizens on presidential candidates, putting technology at the service of politics.",
     description_pt: "Ferramenta digital para informar a cidadania sobre as candidaturas presidenciais, colocando a tecnologia à disposição da política. ",
     pais: "Paraguay",
     color: "#FFB906",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-63.61667, -38.41609]
   },
   properties: {
     title_es: "Vota Inteligente",
     title_en: "Vote Smart",
     title_pt: "Vota Inteligente",
     description_es: "Herramienta digital para informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política. En Argentina se realizó 2 veces en los años 2013 y 2015",
     description_en: "Digital tool to inform citizens on presidential candidates, putting technology at the service of politics. In Argentina, it was implemented 2 times in the years 2013 and 2015",
     description_pt: "Ferramenta digital para informar a cidadania sobre as candidaturas presidenciais, colocando a tecnologia à disposição da política. Na Argentina, foi implementado 2 vezes nos anos de 2013 e 2015",
     pais: "Argentina",
     color: "#FFB906",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-102.55278, 23.6345]
   },
   properties: {
     title_es: "Vota Inteligente",
     title_en: "Vote Smart",
     title_pt: "Vota Inteligente",
     description_es: "Herramienta digital para informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política. ",
     description_en: "Digital tool to inform citizens on presidential candidates, putting technology at the service of politics.",
     description_pt: "Ferramenta digital para informar a cidadania sobre as candidaturas presidenciais, colocando a tecnologia à disposição da política. ",
     pais: "México",
     color: "#FFB906",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-90.23075, 15.78347]
   },
   properties: {
     title_es: "Vota Inteligente",
     title_en: "Vote Smart",
     title_pt: "Vota Inteligente",
     description_es: "Herramienta digital para informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política. ",
     description_en: "Digital tool to inform citizens on presidential candidates, putting technology at the service of politics.",
     description_pt: "Ferramenta digital para informar a cidadania sobre as candidaturas presidenciais, colocando a tecnologia à disposição da política. ",
     pais: "Guatemala",
     color: "#FFB906",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-78.1834, -1.83123]
   },
   properties: {
     title_es: "Vota Inteligente",
     title_en: "Vote Smart",
     title_pt: "Vota Inteligente",
     description_es: "Herramienta digital para informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política. ",
     description_en: "Digital tool to inform citizens on presidential candidates, putting technology at the service of politics.",
     description_pt: "Ferramenta digital para informar a cidadania sobre as candidaturas presidenciais, colocando a tecnologia à disposição da política. ",
     pais: "Ecuador",
     color: "#FFB906",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-60.10584, -37.60293]
   },
   properties: {
     title_es: "Vota Inteligente",
     title_en: "Vote Smart",
     title_pt: "Vota Inteligente",
     description_es: "Herramienta digital para informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política. ",
     description_en: "Digital tool to inform citizens on presidential candidates, putting technology at the service of politics.",
     description_pt: "Ferramenta digital para informar a cidadania sobre as candidaturas presidenciais, colocando a tecnologia à disposição da política. ",
     pais: "Argentina",
     color: "#FFB906",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-66.58973, 6.42375]
   },
   properties: {
     title_es: "Vota Inteligente",
     title_en: "Vote Smart",
     title_pt: "Vota Inteligente",
     description_es: "Herramienta digital para informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política. ",
     description_en: "Digital tool to inform citizens on presidential candidates, putting technology at the service of politics.",
     description_pt: "Ferramenta digital para informar a cidadania sobre as candidaturas presidenciais, colocando a tecnologia à disposição da política. ",
     pais: "Venezuela",
     color: "#FFB906",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-7.09262, 31.7917]
   },
   properties: {
     title_es: "Vota Inteligente",
     title_en: "Vote Smart",
     title_pt: "Vota Inteligente",
     description_es: "Herramienta digital para informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política. ",
     description_en: "Digital tool to inform citizens on presidential candidates, putting technology at the service of politics.",
     description_pt: "Ferramenta digital para informar a cidadania sobre as candidaturas presidenciais, colocando a tecnologia à disposição da política. ",
     pais: "Marruecos",
     color: "#FFB906",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-63.58865, -16.29015]
   },
   properties: {
     title_es: "Vota Inteligente",
     title_en: "Vote Smart",
     title_pt: "Vota Inteligente",
     description_es: "Herramienta digital para informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política. ",
     description_en: "Digital tool to inform citizens on presidential candidates, putting technology at the service of politics.",
     description_pt: "Ferramenta digital para informar a cidadania sobre as candidaturas presidenciais, colocando a tecnologia à disposição da política. ",
     pais: "Bolivia",
     color: "#FFB906",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-55.76583, -32.52277]
   },
   properties: {
     title_es: "Del Dicho Al Hecho",
     title_en: "From Saying to the Fact",
     title_pt: "Do Dito ao Feito",
     description_es: "Estudio que mide el cumplimiento de las promesas legislativas establecidas por el gobierno en su programa presidencial y discurso de rendición de cuentas. En Uruguay se realizó 5 veces durante los años 2015, 2016, 2017, 2018, 2019.",
     description_en: "A study that measures the fulfilling of legislative promises made by governments during their campaigns and public accounts speeches. In Uruguay, it was carried out 5 times during the years 2015, 2016, 2017, 2018, 2019.",
     description_pt: "Um estudo que mede o cumprimento das promessas legislativas estabelecidas pelo governo em seu programa presidencial e o discurso de responsabilização. No Uruguai, foi realizado 5 vezes durante os anos de 2015, 2016, 2017, 2018, 2019.",
     pais: "Uruguay",
     color: "#FF483C",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-70.83834, -33.28453]
   },
   properties: {
     title_es: "Del Dicho Al Hecho",
     title_en: "From Saying to the Fact",
     title_pt: "Do Dito ao Feito",
     description_es: "Estudio que mide el cumplimiento de las promesas legislativas establecidas por el gobierno en su programa presidencial y discurso de rendición de cuentas. En Chile se realizó 16 veces durante los años 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2019",
     description_en: "A study that measures the fulfilling of legislative promises made by governments during their campaigns and public accounts speeches. In Chile, it was carried out 16 times during the years 2011. 2012, 2013, 2014, 2015, 2016, 2017, 2019.",
     description_pt: "Um estudo que mede o cumprimento das promessas legislativas estabelecidas pelo governo em seu programa presidencial e o discurso de responsabilização. No Chile, foi realizado 16 vezes durante os anos de 2011. 2012, 2013, 2014, 2015, 2016, 2017, 2019.",
     pais: "Chile",
     color: "#FF483C",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-65.30259, -15.99392]
   },
   properties: {
     title_es: "Escuela de Incidencia Bolivia ",
     title_en: "Advocacy School Bolivia",
     title_pt: "Escuela de Incidencia Bolivia",
     description_es: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en América Latina.",
     description_en: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America",
     description_pt: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na América Latina",
     pais: "Bolivia",
     color: "#0FCABC",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-74.29733, 4.57086]
   },
   properties: {
     title_es: "Escuela de Incidencia Colombia",
     title_en: "Advocacy School Colombia",
     title_pt: "Escuela de Incidencia Colombia",
     description_es: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en América Latina.",
     description_en: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America",
     description_pt: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na América Latina",
     pais: "Colombia",
     color: "#0FCABC",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-78.99639, -1.1063]
   },
   properties: {
     title_es: "Escuela de Incidencia Ecuador ",
     title_en: "Advocacy School Ecuador",
     title_pt: "Escuela de Incidencia Ecuador ",
     description_es: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en América Latina.",
     description_en: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America",
     description_pt: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na América Latina",
     pais: "Ecuador",
     color: "#0FCABC",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-75.01515, -9.18996]
   },
   properties: {
     title_es: "Escuela de Incidencia Perú",
     title_en: "Advocacy School Peru",
     title_pt: "Escuela de Incidencia Perú",
     description_es: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en América Latina.",
     description_en: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America",
     description_pt: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na América Latina",
     pais: "Perú",
     color: "#0FCABC",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-51.92527, -14.235]
   },
   properties: {
     title_es: "Escuela de Incidencia Brasil",
     title_en: "Advocacy School Brazil",
     title_pt: "Escuela de Incidencia Brasil",
     description_es: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en América Latina.",
     description_en: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America",
     description_pt: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na América Latina",
     pais: "Brasil",
     color: "#0FCABC",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-72.16363, -37.48477]
   },
   properties: {
     title_es: "Escuela de Incidencia Chile",
     title_en: "Advocacy School Chile",
     title_pt: "Escuela de Incidencia Chile",
     description_es: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en América Latina.",
     description_en: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America",
     description_pt: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na América Latina",
     pais: "Chile",
     color: "#0FCABC",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-90.20865, 14.83027]
   },
   properties: {
     title_es: "Escuela de Incidencia Guatemala",
     title_en: "Advocacy School Guatemala",
     title_pt: "Escuela de Incidencia Guatemala",
     description_es: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en América Latina.",
     description_en: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America",
     description_pt: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na América Latina",
     pais: "Guatemala",
     color: "#0FCABC",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-100.79357, 22.38899]
   },
   properties: {
     title_es: "Escuela de Incidencia México",
     title_en: "Advocacy School Mexico",
     title_pt: "Escuela de Incidencia México",
     description_es: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en América Latina.",
     description_en: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America",
     description_pt: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na América Latina",
     pais: "México",
     color: "#0FCABC",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-66.59014, 18.22083]
   },
   properties: {
     title_es: "Escuela de Incidencia Puerto Rico",
     title_en: "Advocacy School Puerto Rico",
     title_pt: "Escuela de Incidencia Puerto Rico",
     description_es: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en América Latina.",
     description_en: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America",
     description_pt: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na América Latina",
     pais: "Puerto Rico",
     color: "#0FCABC",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-101.80315, 19.94065]
   },
   properties: {
     title_es: "Colectiva",
     title_en: "Colectiva",
     title_pt: "Colectiva",
     description_es: "Encuentro regional realizado junto con Amnistía Internacional que reunirá a más de 100 activistas de todas las regiones de América Latina y más de 10 speakers para discutir 4 temas: Violencia Política, Cleptocracia, Tecnología y Crisis Climática.",
     description_en: "Regional summit organized in alliance with Amnesty International that will bring together more than 100 activists from Latin America and 10 speakers to discuss four themes: Political Violence, Kleptocracy, Technology and Climate Crisis",
     description_pt: "Reunião regional realizada em conjunto com a Anistia Internacional que reunirá mais de 100 ativistas de todas as regiões da América Latina e mais de 10 palestrantes para discutir 4 tópicos: Violência Política, Cleptocracia, Tecnologia e Crise Climática.",
     pais: "México",
     color: "#ED2518",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-72.52652, -39.41628]
   },
   properties: {
     title_es: "Chileleaks",
     title_en: "Chileleaks",
     title_pt: "Chileleaks",
     description_es: "Una plataforma segura para denunciar de forma anónima casos de cohecho y soborno en Chile.",
     description_en: "A secure platform to report anonymously and safely bribery cases in Chile. ",
     description_pt: "Una plataforma segura para denunciar de forma anônima casos de corrupção e suborno no Chile. ",
     pais: "Chile",
     color: "#FFDE00",
   }
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-48.93694, -16.02892]
   },
   properties: {
     title_es: "Rio por Inteiro",
     title_en: "Rio por Inteiro Brazil",
     title_pt: "Rio por Inteiro",
     description_es: "Una plataforma que permitió visualizar a las candidaturas comprometidas con propuestas ciudadanas.",
     description_en: "A platform that allowed to visualize candidacies committed with citizen proposals. ",
     description_pt: "Uma plataforma que permitiu visualizar as candidaturas comprometidas com propostas cidadãs.",
     pais: "Brasil",
     color: "#51FA00",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-48.7577, -12.2533]
   },
   properties: {
     title_es: "Tretaqui",
     title_en: "Tretaqui",
     title_pt: "Tretaqui",
     description_es: "Plataforma para denunciar de manera anónima discursos de odio durante procesos electorales",
     description_en: "Platform to report anonymously and safely hate speech during electoral processes.",
     description_pt: "Plataforma para denunciar anonimamente o discurso de ódio durante os processos eleitorais ",
     pais: "Brasil",
     color: "#A927F9",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-45.24559, -14.235]
   },
   properties: {
     title_es: "Me Representa",
     title_en: "Me Representa Brazil",
     title_pt: "Me Representa",
     description_es: "Una plataforma que conectó a las electoras y electores con candidaturas comprometidas con los derechos humanos.",
     description_en: "A platform that connected citizens and candidacies committed with human rights.",
     description_pt: "Uma plataforma que conectava os eleitores e eleitores às candidaturas aos direitos humanos. ",
     pais: "Brasil",
     color: "#FE3A93",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-70.9616, -32.20125]
   },
   properties: {
     title_es: "no más cohecho",
     title_en: "No more bribes Chile",
     title_pt: "no más cohecho",
     description_es: "Plataforma digital para la generación de propuestas ciudadanas contra el cohecho y de entrega de información de las querellas presentadas por Ciudadanía Inteligente contra personajes políticos chilenos involucrados en casos de cohecho y financiamiento ilegal de la política.",
     description_en: "Digital platform for the elaboration of citizen proposals against bribery and to inform on the lawsuits presented by Ciudadanía Inteligente against Chilean politicians involved in cases of bribery and illegal funding of politics",
     description_pt: "Plataforma digital para a geração de propostas cidadãs contra o suborno e a entrega de informações sobre as denúncias apresentadas pela Intelligent Citizenship contra figuras políticas chilenas envolvidas em casos de suborno e financiamento ilegal da política",
     pais: "Chile",
     color: "#04B4BC",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-70.83009, -30.88568]
   },
   properties: {
     title_es: "Levanta La Mano",
     title_en: "Raise Your Hand",
     title_pt: "Levanta la Mano",
     description_es: "Plataforma digital para fomentar participación y conciencia cívica de niños, niñas y adolescentes en la vida política de su país.",
     description_en: "Digital platform to promote the participation and civic conscioiusness of children and teenargesrs",
     description_pt: "Plataforma digital para incentivar a participação e conscientização cívica de crianças e adolescentes na vida política de seu país. ",
     pais: "Chile",
     color: "#E898F2",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-70.92727, -34.7029]
   },
   properties: {
     title_es: "Abre",
     title_en: "Open",
     title_pt: "Abre",
     description_es: "Una mezcla de herramientas digitales y metodológicas que acercan el trabajo de municipios a vecinas y vecinos, potenciando la construcción colectiva de barrios y comunidades.",
     description_en: "Digital and methodological tools to bring the work of municipalities closer to neighbours, fostering the collective construction of neighbourhoods and communities. ",
     description_pt: "Uma mistura de ferramentas digitais e metodológicas que aproximam o trabalho dos municípios aos vizinhos e vizinhos, promovendo a construção coletiva de bairros e comunidades.",
     pais: "Chile",
     color: "#9437FF",
   },
 },
 {
   type: 'Feature',
   geometry: {
     type: 'Point',
     coordinates: [-71.82891, -36.54705]
   },
   properties: {
     title_es: "Partidos Públicos",
     title_en: "Public Parties",
     title_pt: "Partidos Públicos",
     description_es: "Una plataforma que busca facilitar el acceso y la fiscalización ciudadana a la información que publican los partidos políticos chilenos en transparencia activa, a través de visualizaciones, filtros y comparativas amigables.",
     description_en: "A platform that looks to provide access to and citizen supervision on information published by Chilean political parties as active transparency, through visualizations, filters and comparisons.",
     description_pt: "Uma plataforma que busca facilitar o acesso e a supervisão dos cidadãos às informações publicadas pelos partidos políticos chilenos, em transparência ativa, através de visualizações, filtros e comparações amigáveis. ",
     pais: "Chile",
     color: "#00FA92",
   },
 }
]};

var markers = []; // makers array

var modalMarker;



function loadMarkers(map, lang){
  data.features.forEach(function(marker) {

    // create a HTML element for each feature
    var el = document.createElement('div');
    el.className = 'marker';

    let contentString = "";
    if (lang == 'es') {
      contentString = '<h6>' + marker.properties.title_es + '</h6>' + marker.properties.description_es + '';
    } else if (lang == 'pt') {
      contentString = '<h6>' + marker.properties.title_pt + '</h6>' + marker.properties.description_pt + '';
    } else {
      contentString = '<h6>' + marker.properties.title_en + '</h6>' + marker.properties.description_en + '';
    }

    var popup = new mapboxgl.Popup({ offset: [0, -15] })
      .setHTML(contentString);

    // make a marker for each feature and add to the map
    new mapboxgl.Marker({color: marker.properties.color})
      .setLngLat(marker.geometry.coordinates)
      .addTo(map)
      .setPopup(popup);

  });
}



function initMap() {
  mapboxgl.accessToken = 'pk.eyJ1IjoiY2l1ZGFkYW5pYWkiLCJhIjoiY2s3b3Vocm5qMDBpcTNsbzBjZ2Mxc2NudSJ9.aFSdmJC0G6-QaCLsYAzl7A';
  var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/dark-v10', // stylesheet location
    center: [-61.326854, -14.599413], // starting position [lng, lat]
    zoom: 1.8, // starting zoom
    hash: true,
    transformRequest: (url, resourceType)=> {
      if(resourceType === 'Source' && url.startsWith('http://myHost')) {
        return {
         url: url.replace('http', 'https'),
         headers: { 'my-custom-header': true},
         credentials: 'include'  // Include cookies for cross-origin requests
       }
      }
    }
  });


  let language = document.getElementById('language').getAttribute("data-lang");
  loadMarkers(map, language);
}


function resetMarkers(arr) {
  if (arr && arr.length != 0) {
      $.each(arr, function(index, value) { value.setMap(null); });
  }
}
