let sentences =
[
 {
   sentence_es: "Página perdida en el espacio",
   sentence_en: "This Page is Lost in Space",
   sentence_pt: "This Page is Lost in Space",
   img: 'astronaut.png',
   link: 'https://ciudadaniai.org',
   text_link: 'Vuelve al home',
 },
 {
   sentence_es: "Un perro se comió esta página",
   sentence_en: "A Dog Ate this Page",
   sentence_pt: "A Dog Ate this Page",
   img: 'dog.png',
   link: 'https://ciudadaniai.org',
   text_link: 'Vuelve al home',
 },
 {
   sentence_es: "Esta página es un fantasma",
   sentence_en: "This Page is a Ghost",
   sentence_pt: "This Page is a Ghost",
   img: 'ghost.png',
   link: 'https://ciudadaniai.org',
   text_link: 'Vuelve al home',
 },
 {
   sentence_es: "Esta página está rota",
   sentence_en: "This Page is Broken",
   sentence_pt: "This Page is Broken",
   img: 'broken-mug.png',
   link: 'https://ciudadaniai.org',
   text_link: 'Vuelve al home',
 },
]

$(document).ready(function() {
 var js404 = document.getElementById("js-404");
 if(js404){
   var sentence = sentences[Math.floor(Math.random()*sentences.length)];
   var img = document.getElementById("js-img");
   var lang = js404.getAttribute("data-language");
   var path_image = img.src + "/assets/images/404/" + sentence.img;
   img.src= path_image;
   if (lang == 'es') {
     js404.innerHTML = '<p>'+ sentence.sentence_es +'</p>';
   } else if (lang == 'pt') {
     js404.innerHTML = '<p>'+ sentence.sentence_pt +'</p>';
   } else {
     js404.innerHTML = '<p>'+ sentence.sentence_en +'</p>';
   }
 }
});
