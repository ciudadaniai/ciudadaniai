---
layout: campaigns/semaforoautoritario
version: Informe semáforo autoritario
title: '¿Está en riesgo la democracia en Chile?'
call: 'Revisa el informe'
summary: En tiempos donde cada vez más democracias están siendo amenazadas por el surgimiento de liderazgos autoritarios, es fundamental que la ciudadanía cuente con elementos claros para evaluar el riesgo que determinadas candidaturas pueden implicar en caso de acceder al poder.
img: slides/BoricKast.jpg
anchor: informe
update: Datos levantos entre el 7 y 13 de julio
---
