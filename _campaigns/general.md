---
layout: campaign
title: 'General'
summary: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
img: blog.png

---

# General

Hoy el mundo entero, pero sobre todo América Latina, vive momentos críticos. La propagación del **Coronavirus** está dejando profundas repercusiones en nuestra sociedad. Las vulneraciones y las desigualdades se han acrecentado a pasos gigantescos.  Se ha dejado de lado la protección y garantías de ciertos derechos y nuestros sistemas de salud están cada vez más colapsados. Con todo este contexto, la pandemia que hoy vivimos tiene un principal indicador: la letalidad, es decir, es el número de fallecidos en relación a aquellos diagnosticados con la enfermedad. Y durante los primeros días del mes de abril el mundo dio a conocer que uno de los países latinoamericanos con mayor índice de letalidad, es Ecuador.

La comunidad Ecuatoriana está pasando por un duro momento, la crisis está sobrepasando la capacidad institucional y es aún más crudo en la ciudad de Guayaquil. El gobierno nacional ha indicado múltiples medidas para afrontar la situación, desde limitar las reuniones públicas a un máximo de 30 personas y [prohibir las visitas a centros geriátricos](https://www.voanoticias.com/a/ecuador-medidas-coronavirus-covid19/5329117.html) hasta la [suspensión de la jornada laboral](https://www.eluniverso.com/noticias/2020/04/02/nota/7802859/presidente-lenin-moreno-extiende-suspension-jornada-laboral-hasta) y medidas para promover el [entierro digno](https://twitter.com/Lenin/status/1244705223815368705?s=20), pero incluso con estas acciones, vemos que diversas comunidades organizadas, como la Confederación de Nacionalidades Indígenas de Ecuador, o iniciativas como [“Frena la Curva Ecuador”](https://ecuador.frenalacurva.net/) están solicitando fuertemente la coordinación y la colaboración para [co-crear acciones de respuesta](https://www.elcomercio.com/actualidad/conaie-micc-distribucion-alimentos-covid19.html), donde se haga parte a la ciudadanía de la construcción del plan de contención.

Actualmente trabajamos con organizaciones aliadas en Ecuador y una de ellas es el Observatório de Políticas Públicas de Guayaquil. Su director ejecutivo, Manuel Macías, explica que la situación en el país es crítica “los servicios de salud públicos y privados están desbordados. Al momento, la comunidad guayaquileña está haciendo lo que puede para conseguir alimentos y medicinas. Ya se está convirtiendo en una situación de supervivencia; y en una ciudad tan desigual, la crisis sanitaria está golpeando de manera más dura a los sectores ya empobrecidos”.

Hace más de 15 años que en Ciudadanía Inteligente trabajamos por impulsar la transparencia y la participación ciudadana con el objetivo de fortalecer las instituciones democráticas. Asimismo, habilitamos espacios que permitan escuchar la voz de activistas que luchan por la justicia social la realización de  cambios sociales. Con toda esta experiencia, hoy nuestra labor se ha centrado en torno a la colaboración y la apertura gubernamental. Estamos más convencidos que nunca de que hoy es momento de que los gobiernos, sobre todo los locales, se abran a la colaboración y a las mesas interseccionales para hacerle frente a esta contingencia sin precedentes en los último 100 años.

Estamos en un momento clave, una oportunidad única que nos puede permitir acercar, por fin, las instituciones a la comunidad. A través de este mecanismo, podremos aumentar la legitimidad de la acción pública, empoderar a la ciudadanía, promover políticas más específicas y mejores -enfocadas a la diversidad de las necesidades-, asegurar el acceso equitativo a servicios y derechos, reducir barreras y aumentar las oportunidades para crear sociedades más justas. Esto, en su conjunto, permitirá que las medidas de contención sean más efectivas y que la “resistencias” a seguir estas políticas sean menores.

Hoy lo más importante es que la ciudadanía, en su totalidad, esté en el centro de las políticas públicas. Es decir, debe participar plenamente y el gobierno tiene la responsabilidad de abrir sus espacios progresivamente, a través de las siguientes acciones:

* **Informar**: Mantener a la ciudadanía informada de sus derechos y responsabilidades, así como de las decisiones que han sido tomadas. Principalmente busca crear conciencia pública.
¿Cómo? A través de sitios web, panfletos, posters y redes sociales. Específicamente, esto puede consistir en la digitalización de la información pública; publicación de documentos oficiales, proyectos, políticas, guías, manuales, reportes, normas, entre otros; presencia de datos, proyectos en marcha, entre otros.

* **Consultar**: Implica que además de informar, se obtiene retroalimentación de la ciudadanía en el análisis, definición de alternativas o decisiones públicas.
¿Cómo? A través de encuestas, foros de discusión, buzones electrónicos, medios electrónicos de comunicación con las autoridades, herramientas de retroalimentación en el sitio web. Cuando pase la crisis sanitaria también se pueden incluir acciones como; reuniones vecinales audiencias públicas.

* **Involucrar**: Trabajar con la ciudadanía para entender sus preocupaciones y aspiraciones.
¿Cómo? A través de juntas, comités asesores o de planificación, talleres. A nivel digital, esto se puede ver materializado en foros de discusión y de chats, entradas virtuales para la recepción de iniciativas de ciudadanos, grupos u organizaciones, con respuesta de los proyectos presentados.

* **Colaborar**: El poder es compartido entre el gobierno y la ciudadanía como aliados. La planificación y toma de decisión se realiza en conjunto para tener en cuenta tanto las opiniones ciudadanas como del gobierno. ¿Cómo? A través de juntas, comités de asesoramiento ciudadanos, o plataformas online de participación.

En tiempos de crisis la ciudadanía debe estar más activa y presente que nunca. El que no podamos reunirnos físicamente no significa que el trabajo en conjunto, las manifestaciones y el exigir nuestros derechos deban paralizarse. La sociedad civil debe estar atenta y ser crítica con las medidas que toman los representantes. Hoy debemos trabajar para que  esta crisis permita generar políticas de protección para todas nuestras comunidades y sobre todo para aquellas personas más vulnerables.  Hoy tenemos las herramientas para utilizar las plataformas virtuales de la mejor manera posible y cuidando nuestros datos. En tiempos de incertidumbre nos mueve una sola certeza: que juntos y juntas somos más fuertes. Empoderémonos, incidamos y cuidémonos.
