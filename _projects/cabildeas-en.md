---
layout: project
lang: en
name: "Cabildeas Chile"

description: "Project to foster citizen participation in the constitutional process for Chile’s new Constitution."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2016

versions:
  - name: "Cabildeas Chile"
    year: 2016
    country: "Chile"
    description: "Project to foster citizen participation in the constitutional process for Chile’s new Constitution."


---
