---
layout: project
lang: pt
name: "Lei do Lobby"

description: "Antes de o Chile regulamentar a Lei do Lobby, criamos uma plataforma para tornar transparentes as atividades de lobby que são exercidas sobre autoridades e autoridades."

visible_timeline: true
visible_home: false

image: ""
site: "http://leydelobby.cl/preguntas-frecuentes.html"
year: 2014

versions:
  - name: "Lei do Lobby"
    year: 2014
    country: "Brasil"
    description: "Antes de o Chile regulamentar a Lei do Lobby, criamos uma plataforma para tornar transparentes as atividades de lobby que são exercidas sobre autoridades e autoridades."

---
