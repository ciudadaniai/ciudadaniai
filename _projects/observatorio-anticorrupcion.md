---
layout: project
lang: es
name: "Observatorio Anticorrupción Chile"

description: "Proyecto de incidencia legislativa para el análisis de avance de propuestas emanadas del Consejo Asesor Presidencial sobre conflictos de Interés, tráfico de Influencias y corrupción."

visible_timeline: true
visible_home: false

image: ""
site: "https://observatorioanticorrupcion.cl/"
year: 2015

versions:
  - name: "Observatorio Anticorrupción Chile"
    year: 2015
    country: "Chile"
    description: "Proyecto de incidencia legislativa para el análisis de avance de propuestas emanadas del Consejo Asesor Presidencial sobre conflictos de Interés, tráfico de Influencias y corrupción."
  - name: "Observatorio Anticorrupción Chile"
    year: 2016
    country: "Chile"
    description: "Proyecto de incidencia legislativa para el análisis de avance de propuestas emanadas del Consejo Asesor Presidencial sobre conflictos de Interés, tráfico de Influencias y corrupción."


---
