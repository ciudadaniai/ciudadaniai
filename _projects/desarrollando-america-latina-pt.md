---
layout: project
lang: pt
name: "Programando a América Latina"

description: "Projeto para a promoção da inovação e empreendedorismo social através do poder da tecnologia, dados abertos e colaboração entre os atores sociais."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2011

versions:
  - name: "Programando a América Latina"
    year: 2011
    country: "Chile"
    description: "Projeto para a promoção da inovação e empreendedorismo social através do poder da tecnologia, dados abertos e colaboração entre os atores sociais."
  - name: "Programando a América Latina"
    year: 2012
    country: "Chile"
    description: "Projeto para a promoção da inovação e empreendedorismo social através do poder da tecnologia, dados abertos e colaboração entre os atores sociais."
  - name: "Programando a América Latina"
    year: 2014   
    country: "Chile"
    description: "Projeto para a promoção da inovação e empreendedorismo social através do poder da tecnologia, dados abertos e colaboração entre os atores sociais."
  - name: "Programando a América Latina"
    year: 2015   
    country: "Chile"
    description: "Projeto para a promoção da inovação e empreendedorismo social através do poder da tecnologia, dados abertos e colaboração entre os atores sociais."
---
