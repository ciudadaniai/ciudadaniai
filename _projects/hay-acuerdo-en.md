---
layout: project
lang: en
name: "There is an Agreement"

description: "Digital platform that allows for stances’ comparison between civil society and the government in relation to issues of public interest."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2011

versions:
  - name: "There is an Agreement"
    year: 2011
    country: "Chile"
    description: "Digital platform that allows for stances’ comparison between civil society and the government in relation to issues of public interest."
    year: 2014
    country: "Chile"
    description: "Digital platform that allows for stances’ comparison between civil society and the government in relation to issues of public interest."

---
