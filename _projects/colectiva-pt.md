---
layout: project
lang: pt
name: "Colectiva"
link_to_project: true
ref: colectiva
description: "Confira informações de nosso encontro latino-americano que reuniu mais de 100 ativistas para refletir sobre Violência Política, Cleptocracia, Tecnologia e Crise Climática."

image: "/assets/images/proyectos/colectiva.png"
hero-image: "/assets/images/hero_projects/hero-colectiva.jpg"

visible_timeline: true
visible_home: true

site: "https://colectiva.ciudadaniai.org/"
category: Encontro latino-americano
year: 2019

versions:
  - name: "Colectiva"
    year: 2019
    country: "México"
    description: "Reunião regional realizada em conjunto com a Anistia Internacional que reunirá mais de 100 ativistas de todas as regiões da América Latina e mais de 10 palestrantes para discutir 4 tópicos: Violência Política, Cleptocracia, Tecnologia e Crise Climática."

---
