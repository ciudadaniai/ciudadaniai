---
layout: project
lang: es
name: "La Constitución es Nuestra"
link_to_project: true
ref: alcen

description: "Te invitamos a conocer esta plataforma abierta, colaborativa y colectiva
que acerca las normas de la nueva Constitución a la ciudadanía."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/lcen.png"
hero-image:

site: "https://laconstitucionesnuestra.cl/"
category: Participación Ciudadana
year: 2021

versions:
  - name: "La Constitución es Nuestra"
    year: 2021
    country: "Chile"
    description: "Una plataforma abierta, colectiva y colaborativa impulsada por Ciudadanía Inteligente, la Iniciativa Global por los Derechos Económicos, Consti Tu+Yo y la FES, que busca visibilizar y articular propuestas ciudadanas para la nueva Constitución que serán conectadas con el trabajo diario de los convencionales."

---
