---
layout: project
lang: es
name: "Escuela de Incidencia"
link_to_project: true

description: "Únete a nuestra escuela de liderazgos emergentes. Acá podrás fortalecer tus habilidades de incidencia en políticas públicas y generar reales cambios sociales."

image: "/assets/images/proyectos/escuela-incidencia.png"
hero-image: "/assets/images/hero_projects/hero-escuela.jpg"

site: "https://escueladeincidencia.org"
category: Incidencia regional
year: 2018

visible_timeline: true
visible_home: true

versions:
  - name: "Escuela de Incidencia Colombia"
    year: 2017
    country: "Colombia"
    description: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en LATAM."
  - name: "Escuela de Incidencia Bolivia"
    year: 2017
    country: "Bolivia"
    description: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en LATAM."
  - name: "Escuela de Incidencia Perú"
    year: 2017      
    country: "Perú"
    description: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en LATAM."
  - name: "Escuela de Incidencia Ecuador"
    year: 2017      
    country: "Ecuador"
    description: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en LATAM."

  - name: "Escuela de Incidencia Brasil"
    year: 2018
    country: "Brasil"
    description: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en LATAM."

  - name: "Escuela de Incidencia Chile"
    year: 2018
    country: "Chile"
    description: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en LATAM."

  - name: "Escuela de Incidencia Guatemala"
    year: 2018      
    country: "Guatemala"
    description: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en LATAM."

  - name: "Escuela de Incidencia México"
    year: 2018      
    country: "México"
    description: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en LATAM."

  - name: "Escuela de Incidencia Puerto Rico"
    year: 2018
    country: "Puerto Rico"
    description: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en LATAM."

  - name: "Escuela de Incidencia para funcionarios públicos Ecuador"
    year: 2018
    country: "Ecuador"
    description: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en LATAM."

  - name: "Escuela de Incidencia Colombia"
    year: 2018
    country: "Colombia"
    description: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en LATAM."

  - name: "Escuela de Incidencia Perú"
    year: 2019     
    country: "Perú"
    description: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en LATAM."

  - name: "Escuela de Incidencia Ecuador"
    year: 2019   
    country: "Ecuador"
    description: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en LATAM."

  - name: "Escuela de Incidencia Guatemala"
    year: 2019   
    country: "Guatemala"
    description: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de líderes emergentes en LATAM."

---
