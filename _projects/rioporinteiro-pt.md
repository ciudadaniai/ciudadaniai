---
layout: project
lang: pt
name: "Rio por Inteiro"

description: "Confira quais são as candidaturas que estão comprometidas com as propostas cidadãs no Brasil."

visible_timeline: true
visible_home: false

image: "/assets/images/proyectos/rioporinteiro.png"
hero-image: "/assets/images/hero_projects/hero-rioporinteiro.jpg"

category: Informação efetiva
year: 2018

versions:
  - name: "Rio por Inteiro Brasil"
    year: 2018
    country: "Brasil"
    description: "Uma plataforma que permitiu visualizar as candidaturas comprometidas com propostas cidadãs."

---
