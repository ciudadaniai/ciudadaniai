---
layout: project
lang: en
name: "No More Bribery"
link_to_project: true

description: "Help us stop corruption, fight for more transparency, and secure that our democracy serves the general interest, and not privileged groups."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/nomascohecho.png"
hero-image: "/assets/images/hero_projects/hero-nomascohecho.jpg"

site: "https://nomascohecho.cl/"
category: Citizen participation
year: 2018

versions:
  - name: "No more bribes Chile"
    year: 2015
    country: "Chile"
    description: "Digital platform for the elaboration of citizen proposals against bribery and to inform on the lawsuits presented by Ciudadanía Inteligente against Chilean politicians involved in cases of bribery and illegal funding of politics."


---
