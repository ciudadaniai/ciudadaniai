---
layout: project
lang: es
name: "Índice Latinoamericano Transparencia Legislativa"

description: "Herramienta que permite a la ciudadanía comparar los niveles de transparencia y acceso a la información de los Congresos o Parlamentos de Latinoamérica."

visible_timeline: true
visible_home: false

image: ""
site: "https://www.transparencialegislativa.org/publicacion/indice-latinoamericano-de-transparencia-legislativa/"
year: 2011

versions:
  - name: "Índice Latinoamericano Transparencia Legislativa"
    year: 2011
    country: "Chile"
    description: "Herramienta que permite a la ciudadanía comparar los niveles de transparencia y acceso a la información de los Congresos o Parlamentos de Latinoamérica."
  - name: "Índice Latinoamericano Transparencia Legislativa"
    year: 2014
    country: "Chile"
    description: "Herramienta que permite a la ciudadanía comparar los niveles de transparencia y acceso a la información de los Congresos o Parlamentos de Latinoamérica."
  - name: "Índice Latinoamericano Transparencia Legislativa LATAM"
    year: 2016
    country: "Chile"
    description: "Herramienta que permite a la ciudadanía comparar los niveles de transparencia y acceso a la información de los Congresos o Parlamentos de Latinoamérica."

---
