---
layout: project
lang: es
name: "Abre"

description: "Construye gobiernos locales más abiertos, participativos e innovadores, que pongan las necesidades de la ciudadanía en el centro de la toma de decisiones públicas."

description-timeline: "Una mezcla de herramientas digitales y metodológicas que acercan el trabajo de municipios a vecinas y vecinos, potenciando la construcción colectiva de barrios y comunidades."

image: "/assets/images/proyectos/abre.png"
hero-image: "/assets/images/hero_projects/hero-abre.jpeg"

site: "http://abre.tumunicipio.org/"
category: Participación Ciudadana
year: 2017

visible_timeline: true
visible_home: false

versions:
  - name: "Abre Peñalolén"
    year: 2017
    country: "Chile"
    description: "Una mezcla de herramientas digitales y metodológicas que acercan el trabajo de municipios a vecinas y vecinos, potenciando la construcción colectiva de barrios y comunidades."


---
