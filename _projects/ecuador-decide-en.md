---
layout: project
lang: en
name: "Ecuador Decides"

description: "Vote Smart for municipal elections in Ecuador."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2019

versions:
  - name: "Ecuador Decides"
    year: 2019
    country: "Ecuador"
    description: "Vote Smart for municipal elections in Ecuador."

---
