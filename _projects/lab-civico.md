---
layout: project
lang: es
name: "LabCívico Chile"

description: "Metodología creada por Ciudadanía Inteligente para desarrollar estrategias de incidencia colaborativas que permiten a organizaciones y actores de la sociedad civil  encontrar soluciones a un problema común."

visible_timeline: true
visible_home: false

image: ""
site: "https://labcivico.org/"
year: 2015

versions:
  - name: "LabCívico Chile"
    year: 2015
    country: "Chile"
    description: "Metodología creada por Ciudadanía Inteligente para desarrollar estrategias de incidencia colaborativas que permiten a organizaciones y actores de la sociedad civil encontrar soluciones a un problema común."
  - name: "LabCívico Chile"
    year: 2016
    country: "Chile"
    description: "Metodología creada por Ciudadanía Inteligente para desarrollar estrategias de incidencia colaborativas que permiten a organizaciones y actores de la sociedad civil encontrar soluciones a un problema común."
  - name: "LabCívico Costa Rica"
    year: 2016
    country: "Costa Rica"
    description: "Metodología creada por Ciudadanía Inteligente para desarrollar estrategias de incidencia colaborativas que permiten a organizaciones y actores de la sociedad civil encontrar soluciones a un problema común."
  - name: "LabCívico Guatemala"
    year: 2016
    country: "Guatemala"
    description: "Metodología creada por Ciudadanía Inteligente para desarrollar estrategias de incidencia colaborativas que permiten a organizaciones y actores de la sociedad civil encontrar soluciones a un problema común."
  - name: "LabCívico Honduras"
    year: 2016
    country: "Honduras"
    description: "Metodología creada por Ciudadanía Inteligente para desarrollar estrategias de incidencia colaborativas que permiten a organizaciones y actores de la sociedad civil encontrar soluciones a un problema común."
  - name: "LabCívico sobre salud pública, Río de Janeiro, Brasil"
    year: 2017
    country: "Brasil"
    description: "Metodología creada por Ciudadanía Inteligente para desarrollar estrategias de incidencia colaborativas que permiten a organizaciones y actores de la sociedad civil encontrar soluciones a un problema común."
  - name: "LabCívico Belén, Brasil"
    year: 2017
    country: "Brasil"
    description: "Metodología creada por Ciudadanía Inteligente para desarrollar estrategias de incidencia colaborativas que permiten a organizaciones y actores de la sociedad civil encontrar soluciones a un problema común."
  - name: "LabCívico México"
    year: 2017
    country: "México"
    description: "Metodología creada por Ciudadanía Inteligente para desarrollar estrategias de incidencia colaborativas que permiten a organizaciones y actores de la sociedad civil encontrar soluciones a un problema común."
  - name: "LabCívico Panamá"
    year: 2017
    country: "Panamá"
    description: "Metodología creada por Ciudadanía Inteligente para desarrollar estrategias de incidencia colaborativas que permiten a organizaciones y actores de la sociedad civil encontrar soluciones a un problema común."

  - name: "Labcívico Asignación del gasto público en Puerto Rico"
    year: 2018
    country: "Puerto Rico"
    description: "Metodología creada por Ciudadanía Inteligente para desarrollar estrategias de incidencia colaborativas que permiten a organizaciones y actores de la sociedad civil encontrar soluciones a un problema común."

  - name: "LabCívico Igualdad de Género en el Mar México"
    year: 2018
    country: "México"
    description: "Metodología creada por Ciudadanía Inteligente para desarrollar estrategias de incidencia colaborativas que permiten a organizaciones y actores de la sociedad civil encontrar soluciones a un problema común."

  - name: "LabCívico inversión en el sector social en Puerto Rico"
    year: 2018
    country: "Puerto Rico"
    description: "Metodología creada por Ciudadanía Inteligente para desarrollar estrategias de incidencia colaborativas que permiten a organizaciones y actores de la sociedad civil encontrar soluciones a un problema común."

  - name: "Labcívico movimiento feminista Chile"
    year: 2018
    country: "Chile"
    description: "Metodología creada por Ciudadanía Inteligente para desarrollar estrategias de incidencia colaborativas que permiten a organizaciones y actores de la sociedad civil encontrar soluciones a un problema común."

---
