---
layout: project
lang: en
name: "Childhood Watchers Chile"
link_to_project: true

description: "Check the chilean Government's level of compliance on the commitments made in the National Agreement for Children."

visible_timeline: true
visible_home: false

image: "/assets/images/proyectos/vigilantes.png"
hero-image: "/assets/images/hero_projects/hero-vigilantes.png"
logo: "/assets/images/hero_projects/hero-vigilantes.png"

site: "https://vigilantesporlainfancia.cl/"
category: Oversees
year: 2018

versions:
  - name: "Childhood Watchers Chile"
    year: 2018
    country: "Chile"
    description: "Check the chilean Government's level of compliance on the commitments made in the National Agreement for Children."

---
