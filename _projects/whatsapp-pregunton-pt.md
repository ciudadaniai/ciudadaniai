---
layout: project
lang: pt
name: "Whatsapp Preguntón Chile"

description: "Plataforma digital para entrega de informação aos cidadãos sobre as eleições presidenciais e representantes ao Congresso Nacional."

visible_timeline: true
visible_home: false

site: ""
category:
year: 2017

versions:
  - name: "Whatsapp Preguntón Chile"
    year: 2017
    country: "Chile"
    description: "Plataforma digital para entrega de informação aos cidadãos sobre as eleições presidenciais e representantes ao Congresso Nacional."
---
