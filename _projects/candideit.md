---
layout: project
lang: es
name: "Candideit.org Chile"

description: "Primer producto como servicio desde FCI."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2012

versions:
  - name: "Candideit.org Chile"
    year: 2012
    country: "Chile"
    description: "Primer producto como servicio desde FCI."
---
