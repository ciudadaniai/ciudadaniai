---
layout: project
lang: es
name: "Rio por Inteiro"

description: "Revisa cuáles son las candidaturas que están comprometidas con propuestas ciudadanas en Brasil."

visible_timeline: true
visible_home: false

image: "/assets/images/proyectos/rioporinteiro.png"
hero-image: "/assets/images/hero_projects/hero-rioporinteiro.jpg"
category: Compara
year: 2018

versions:
  - name: "Rio por Inteiro Brasil"
    year: 2018
    country: "Brasil"
    description: "Una plataforma que permitió visualizar a las candidaturas comprometidas con propuestas ciudadanas."

---
