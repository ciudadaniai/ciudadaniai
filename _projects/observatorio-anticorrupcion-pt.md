---
layout: project
lang: pt
name: "Observatório Anticorrupção do Chile"

description: "Projeto de incidência legislativa para analisar o progresso das propostas emitidas pelo Conselho Consultivo Presidencial sobre Conflitos de Interesse, Influência do Tráfico e Corrupção."

visible_timeline: true
visible_home: false

image: ""
site: "https://observatorioanticorrupcion.cl/"
year: 2015

versions:
  - name: "Observatório Anticorrupção do Chile"
    year: 2015
    country: "Chile"
    description: "Projeto de incidência legislativa para analisar o progresso das propostas emitidas pelo Conselho Consultivo Presidencial sobre Conflitos de Interesse, Influência do Tráfico e Corrupção."
  - name: "Observatório Anticorrupção do Chile"
    year: 2016
    country: "Chile"
    description: "Projeto de incidência legislativa para analisar o progresso das propostas emitidas pelo Conselho Consultivo Presidencial sobre Conflitos de Interesse, Influência do Tráfico e Corrupção."


---
