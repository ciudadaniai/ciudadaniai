---
layout: project
lang: es
name: "Colectiva"
link_to_project: true
ref: colectiva
description: "Revisa información de nuestro encuentro Latinoamericano que reunió a más 100 activistas para reflexionar sobre Violencia Política, Cleptocracia, Tecnología y Crisis Climática."

image: "/assets/images/proyectos/colectiva.png"
hero-image: "/assets/images/hero_projects/hero-colectiva.jpg"

visible_timeline: true
visible_home: true

site: "https://colectiva.ciudadaniai.org/"
category: Encuentro Regional
year: 2019

versions:
  - name: "Colectiva"
    year: 2019
    country: "México"
    description: "Encuentro regional realizado  junto con Amnistía Internacional que reunirá a más de 100 activistas de todas las regiones de América Latina y más de 10 speakers para discutir 4 temas: Violencia Política, Cleptocracia, Tecnología y Crisis Climática."

---
