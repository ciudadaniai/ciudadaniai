---
layout: project
lang: en
name: "The Accusing Whatsapp Chile"

description: "Digital platform to denounce illegal electoral propaganda."

visible_timeline: true
visible_home: false

site: ""
category:
year: 2017

versions:
  - name: "The Accusing Whatsapp Chile"
    year: 2017
    country: "Chile"
    description: "Digital platform to denounce illegal electoral propaganda."


---
