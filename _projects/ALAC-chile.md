---
layout: project
lang: es
name: "ALAC Chile"

description: "Asesoría Legal anticorrupción que brinda asistencia legal gratuita a víctimas, testigos y/o denunciantes de corrupción de hechos que involucren a autoridades y/o funcionarios públicos."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2018

versions:
  - name: "ALAC Chile"
    year: 2018
    country: "Chile"
    description: "Asesoría Legal anticorrupción que brinda asistencia legal gratuita a víctimas, testigos y/o denunciantes de corrupción de hechos que involucren a autoridades y/o funcionarios públicos."

---
