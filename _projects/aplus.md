---
layout: project
lang: es
name: "&lt;A+&gt; Alianza por Algoritmos Inclusivos"
link_to_project: true
ref: aplus
description: "Participa de la alianza global por Algoritmos Inclusivos para combatir los sesgos que reproduce la Inteligencia Artificial."

description-timeline: "Alianza global por Algoritmos Inclusivos para combatir los sesgos que reproduce la Inteligencia Artificial."

image: "/assets/images/proyectos/aplus.png"
hero-image: "/assets/images/hero_projects/hero-aplus.jpg"

site: "http://aplusalliance.org/"
category: Género y tecnología
year: 2019

visible_timeline: true
visible_home: true

versions:
  - name: "<A+> Alianza por Algoritmos Inclusivos"
    year: 2019
    country: "Chile"
    description: "Alianza global por Algoritmos Inclusivos para combatir los sesgos que reproduce la Inteligencia Artificial."
  - name: "<A+> Alianza por Algoritmos Inclusivos"
    year: 2020
    country: "Chile"
    description: "Alianza global por Algoritmos Inclusivos para combatir los sesgos que reproduce la Inteligencia Artificial."


---
