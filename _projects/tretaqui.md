---
layout: project
lang: es
name: "Tretaqui"
link_to_project: false

description: "Denuncia de manera anónima manifestaciones de violencia y discurso de odio durante los procesos electorales en Brasil para que sean redirigidas al Ministerio Público Federal."

visible_timeline: true
visible_home: false

image: "/assets/images/proyectos/tretaqui.png"
hero-image: "/assets/images/hero_projects/hero-tretaqui.jpg"
logo: "/assets/images/hero_projects/tretaqui-logo.png"

site: "http://tretaqui.org/"
category: Denuncia Cohecho
year: 2018

versions:
  - name: "Tretaqui"
    year: 2018
    country: "Brasil"
    description: "Denuncia de manera anónima manifestaciones de violencia y discurso de odio durante los procesos electorales en Brasil y que serán redirigidas al Ministerio Público Federal."

---
