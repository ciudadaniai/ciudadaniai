---
layout: project
lang: en
name: "Colectiva"
link_to_project: true
ref: colectiva
description: "Review information from our Latin American summit that gathered more than 100 activists to reflect on Political Violence, Kleptocracy, Technology and Climatic Crisis."

image: "/assets/images/proyectos/colectiva.png"
hero-image: "/assets/images/hero_projects/hero-colectiva.jpg"

visible_timeline: true
visible_home: true

site: "https://colectiva.ciudadaniai.org/"
category: Regional summit
year: 2019

versions:
  - name: "Colectiva"
    year: 2019
    country: "México"
    description: "Regional summit organized in alliance with Amnesty International that will bring together more than 100 activists from Latin America and 10 speakers to discuss four themes: Political Violence, Kleptocracy, Technology and Climate Crisis."

---
