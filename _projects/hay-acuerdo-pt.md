---
layout: project
lang: pt
name: "Tem Acordo Chile"

description: "Plataforma digital para comparação de posições entre a sociedade civil e o governo em relação a um problema conjuntural."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2011

versions:
  - name: "Tem Acordo Chile"
    year: 2011
    country: "Chile"
    description: "Plataforma digital para comparação de posições entre a sociedade civil e o governo em relação a um problema conjuntural."
  - name: "Hay Acuerdo"
    year: 2014
    country: "Chile"
    description: "Plataforma digital para comparação de posições entre a sociedade civil e o governo em relação a um problema conjuntural."

---
