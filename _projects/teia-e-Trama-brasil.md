---
layout: project
lang: es
name: "Teia e Trama Brasil"

description: "Un proyecto que busca exponer el papel de las empresas brasileras en la crisis política institucional, en el marco de la operación Lava Jato."

visible_timeline: true
visible_home: false

image: ""
site: "https://ateiaeatrama.org/"
year: 2017

versions:
  - name: "Teia e Trama Brasil"
    year: 2017
    country: "Brasil"
    description: "Un proyecto que busca exponer el papel de las empresas brasileras en la crisis política institucional, en el marco de la operación Lava Jato."

---
