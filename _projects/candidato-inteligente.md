---
layout: project
lang: es
name: "Candidato Inteligente Chile"

description: "Plataforma digital para generar conversación con inteligencias artificiales en representación de los candidatos presidenciales presentes en la segunda vuelta."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2017

versions:
  - name: "Candidato Inteligente Chile"
    year: 2017
    country: "Chile"
    description: "Plataforma digital para generar conversación con inteligencias artificiales en representación de los candidatos presidenciales presentes en la segunda vuelta."
---
