---
layout: project
lang: en
name: "GIRO2020"
link_to_project: true
ref: giro2020

description: "Check how we promote citizen participation and social mobilization in the municipal elections of the state of Rio de Janeiro."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/giro.png"

site: "https://giro2020.org/"
category: Participación Ciudadana
year: 2020

versions:
  - name: "GIRO2020"
    year: 2020
    country: "Brazil"
    description: "An initiative that seeks to provide, throughout the municipal elections process in the State of Rio de Janeiro, greater citizen participation and training for candidates."

---
