---
layout: project
lang: en
name: "Anticorruption Observatory Chile"

description: "Legislative advocacy project for tracking and analysing the progress of proposals issued by the Presidential Advisory Council on interest conflicts, influence trafficking and corruption."

visible_timeline: true
visible_home: false

image: ""
site: "https://observatorioanticorrupcion.cl/"
year: 2015

versions:
  - name: "Anticorruption Observatory Chile"
    year: 2015
    country: "Chile"
    description: "Legislative advocacy project for tracking and analysing the progress of proposals issued by the Presidential Advisory Council on interest conflicts, influence trafficking and corruption."
  - name: "Anticorruption Observatory Chile"
    year: 2016
    country: "Chile"
    description: "Legislative advocacy project for tracking and analysing the progress of proposals issued by the Presidential Advisory Council on interest conflicts, influence trafficking and corruption."


---
