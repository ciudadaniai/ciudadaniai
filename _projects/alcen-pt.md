---
layout: project
lang: pt
name: "A constituição é nossa"
link_to_project: true
ref: alcen

description: "Vamos promover uma Constituição escrita pela cidadania, junte-se a nós na sua proposta e a enviaremos à Convenção!"

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/lcen.png"
hero-image:

site: "https://laconstitucionesnuestra.cl/"
category: Participación Ciudadana
year: 2021

versions:
  - name: "A constituição é nossa"
    year: 2021
    country: "Chile"
    description: "Uma plataforma aberta, coletiva e colaborativa promovida pela Cidadania Inteligente, a Global Initiative for Economic Rights, Consti Tu + Yo e a FES, que visa tornar visíveis e articular as propostas dos cidadãos para a nova Constituição que estarão conectadas com o trabalho diário da convencional."

---
