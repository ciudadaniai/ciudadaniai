---
layout: project
lang: en
name: "Developing Latin America"

description: "Project to foster innovation and social entrepreneurship through the power of technology, open data and collaboration among social actors."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2011

versions:
  - name: "Developing Latin America"
    year: 2011
    country: "Chile"
    description: "Project to foster innovation and social entrepreneurship through the power of technology, open data and collaboration among social actors."
  - name: "Developing Latin America"
    year: 2012
    country: "Chile"
    description: "Project to foster innovation and social entrepreneurship through the power of technology, open data and collaboration among social actors."
  - name: "Developing Latin America"
    year: 2014   
    country: "Chile"
    description: "Project to foster innovation and social entrepreneurship through the power of technology, open data and collaboration among social actors."
  - name: "Developing Latin America"
    year: 2015   
    country: "Chile"
    description: "Project to foster innovation and social entrepreneurship through the power of technology, open data and collaboration among social actors."
---
