---
layout: project
lang: pt
name: "Quem te financia?"

description: "Website com informações sobre a legislação sobre financiamento da política no Chile. "

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2014

versions:
  - name: "Quem te financia?"
    year: 2014
    country: "Chile"
    description: "Website com informações sobre a legislação sobre financiamento da política no Chile."
---
