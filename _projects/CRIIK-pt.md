---
layout: project
lang: pt
name: "CRIIK Chile"

description: "Plataforma digital de dados abertos para permitir que os cidadãos acessem e compartilhem informações públicas de seus governos online."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2011

versions:
  - name: "CRIIK Chile"
    year: 2011
    country: "Chile"
    description: "Plataforma digital de dados abertos para permitir que os cidadãos acessem e compartilhem informações públicas de seus governos online."
  - name: "CRIIK Chile"
    year: 2016
    country: "Chile"
    description: "Plataforma digital de dados abertos para permitir que os cidadãos acessem e compartilhem informações públicas de seus governos online."

---
