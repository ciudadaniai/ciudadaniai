---
layout: project
lang: es
name: "Acceso Inteligente Chile"

description: "La primera plataforma en Chile que centraliza el sistema de solicitudes de información pública y permite que ocurra en línea."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2011

versions:
  - name: "Acceso Inteligente"
    year: 2011
    country: "Chile"
    description: "La primera plataforma en Chile que centraliza el sistema de solicitudes de información pública y permite que ocurra en línea."


---
