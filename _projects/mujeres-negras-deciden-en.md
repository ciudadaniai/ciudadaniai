---
layout: project
lang: en
name: "Black Women Decide"
link_to_project: true

description: "Check the information on the Black Women Decide campaign and the data and the underrepresentation of black women in Brazilian politics."

visible_timeline: true
visible_home: false

image: "/assets/images/proyectos/mujeresnegrasdeciden.png"
hero-image: "/assets/images/hero_projects/hero-mujeresnegras.png"
logo: "/assets/images/hero_projects/hero-mujeresnegras.png"

site: "https://mulheresnegrasdecidem.org/"
category:
year: 2018
versions:
  - name: "Black Women Decide"
    year: 2018
    country: "Brasil"
    description: "A mobilization platform centered on the use of data to increase the presence of black women in institutional politics in Brazil."
---
