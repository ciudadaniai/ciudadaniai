---
layout: project
lang: en
name: "ALAC Chile"

description: "Anticorruption legal advice that offers free assistance to victims, witnesses and/or whistleblowers exposed to corruption where authorities and/or public servants are involved."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2018

versions:
  - name: "ALAC Chile"
    year: 2018
    country: "Chile"
    description: "Anticorruption legal advice that offers free assistance to victims, witnesses and/or whistleblowers exposed to corruption where authorities and/or public servants are involved."

---
