---
layout: project
lang: pt
name: "Transparência Legislativa do Índice Latino-Americano"

description: "Ferramenta que permite aos cidadãos comparar os níveis de transparência e acesso à informação dos Congressos ou Parlamentos da América Latina."

visible_timeline: true
visible_home: false

image: ""
site: "https://www.transparencialegislativa.org/publicacion/indice-latinoamericano-de-transparencia-legislativa/"
year: 2011

versions:
  - name: "Transparência Legislativa do Índice Latino-Americano"
    year: 2011
    country: "Chile"
    description: "Ferramenta que permite aos cidadãos comparar os níveis de transparência e acesso à informação dos Congressos ou Parlamentos da América Latina."
  - name: "Transparência Legislativa do Índice Latino-Americano"
    year: 2014
    country: "Chile"
    description: "Ferramenta que permite aos cidadãos comparar os níveis de transparência e acesso à informação dos Congressos ou Parlamentos da América Latina."
  - name: "Transparência Legislativa do Índice Latino-Americano"
    year: 2016
    country: "Chile"
    description: "Ferramenta que permite aos cidadãos comparar os níveis de transparência e acesso à informação dos Congressos ou Parlamentos da América Latina."

---
