---
layout: project
lang: es
name: "Nuestra Elección Guatemala"

description: "Vota Inteligente elecciones presidenciales Guatemala."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2019

versions:
  - name: "Nuestra Elección Guatemala"
    year: 2019
    country: "Guatemala"
    description: "Vota Inteligente elecciones presidenciales Guatemala."

---
