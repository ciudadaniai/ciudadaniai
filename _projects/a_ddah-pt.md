---
layout: project
lang: pt
name: "Do dito ao feito"
link_to_project: true

description: "Descubra em nosso estudo o quanto os governos estão cumprindo suas promessas legislativas em seus programas governamentais e discursos de prestação de contas."

image: "/assets/images/proyectos/ddah.png"
hero-image: "/assets/images/hero_projects/hero-DDAH.jpg"

visible_timeline: true
visible_home: true

site: "https://deldichoalhecho.cl/"
category: Fiscaliza
year: 2010-2019

versions:
  - name: "Do dito ao feito"
    year: 2011
    country: "Chile"
    description: "Um estudo que mede o cumprimento das promessas legislativas estabelecidas pelo governo em seu programa presidencial e o discurso de transparência e prestação de contas."
  - name: "Do dito ao feito"
    year: 2012
    country: "Chile"
    description: "Um estudo que mede o cumprimento das promessas legislativas estabelecidas pelo governo em seu programa presidencial e o discurso de transparência e prestação de contas.."
  - name: "Do dito ao feito"
    year: 2013
    country: "Chile"
    description: "Um estudo que mede o cumprimento das promessas legislativas estabelecidas pelo governo em seu programa presidencial e o discurso de transparência e prestação de contas."
  - name: "Do dito ao feito"
    year: 2014
    country: "Chile"
    description: "Um estudo que mede o cumprimento das promessas legislativas estabelecidas pelo governo em seu programa presidencial e o discurso de transparência e prestação de contas.."
  - name: "Do dito ao feito"
    year: 2015
    country: "Chile"
    description: "Um estudo que mede o cumprimento das promessas legislativas estabelecidas pelo governo em seu programa presidencial e o discurso de transparência e prestação de contas."
  - name: "Do Dito Ao Feito conta pública presidencial Uruguai"
    year: 2016
    country: "Uruguay"
    description: "Um estudo que mede o cumprimento das promessas legislativas estabelecidas pelo governo em seu programa presidencial e o discurso de transparência e prestação de contas."
  - name: "Do Dito Ao Feito  à conta pública presidencial feita Chile"
    year: 2016
    country: "Chile"
    description: "Um estudo que mede o cumprimento das promessas legislativas estabelecidas pelo governo em seu programa presidencial e o discurso de transparência e prestação de contas."
  - name: "Do Dito ao Feito ao programa governamental feito Chile"
    year: 2016
    country: "Chile"
    description: "Um estudo que mede o cumprimento das promessas legislativas estabelecidas pelo governo em seu programa presidencial e o discurso de transparência e prestação de contas."
  - name: "Do Dito Ao Feito conta pública presidencial Uruguai"
    year: 2017
    country: "Uruguay"
    description: "Um estudo que mede o cumprimento das promessas legislativas estabelecidas pelo governo em seu programa presidencial e o discurso de transparência e prestação de contas."
  - name: "Do Dito Ao Feito  à conta pública presidencial feita Chile"
    year: 2017
    country: "Chile"
    description: "Um estudo que mede o cumprimento das promessas legislativas estabelecidas pelo governo em seu programa presidencial e o discurso de transparência e prestação de contas."
  - name: "Do Dito ao Feito ao programa governamental feito Chile"
    year: 2017
    country: "Chile"
    description: "Um estudo que mede o cumprimento das promessas legislativas estabelecidas pelo governo em seu programa presidencial e o discurso de transparência e prestação de contas."
  - name: "Dito e Feito Brasil"
    year: 2017
    country: "Brasil"
    description: "Del Dicho Al Hecho cuenta pública alcalde Río de Janeiro"
  - name: "Do Dito ao Feito conta pública presidencial Uruguai"
    year: 2018
    country: "Uruguay"
    description: "Um estudo que mede o cumprimento das promessas legislativas estabelecidas pelo governo em seu programa presidencial e o discurso de transparência e prestação de contas."
  - name: "Do Dito Ao Feito Conta Pública Chile"
    year: 2019
    country: "Chile"
    description: "Um estudo que mede o cumprimento das promessas legislativas estabelecidas pelo governo em seu programa presidencial e o discurso de transparência e prestação de contas.."
  - name: "Do Dito Ao Feito Programa de Governo Chile"
    year: 2019
    country: "Chile"
    description: "Um estudo que mede o cumprimento das promessas legislativas estabelecidas pelo governo em seu programa presidencial e o discurso de transparência e prestação de contas."
  - name: "Do Dito Ao Feito Programa de Governo Chile"
    year: 2020
    country: "Chile"
    description: "Um estudo que mede o cumprimento das promessas legislativas estabelecidas pelo governo em seu programa presidencial e o discurso de transparência e prestação de contas."
---
