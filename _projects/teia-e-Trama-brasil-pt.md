---
layout: project
lang: pt
name: "Teia e Trama Brasil"

description: "Um projeto que busca expor o papel das empresas brasileiras na crise política institucional, no marco da operação Lava Jato."

visible_timeline: true
visible_home: false

image: ""
site: "https://ateiaeatrama.org/"
year: 2017

versions:
  - name: "Teia e Trama Brasil"
    year: 2017
    country: "Brasil"
    description: "Um projeto que busca expor o papel das empresas brasileiras na crise política institucional, no marco da operação Lava Jato."

---
