---
layout: project
lang: en
name: "Advocacy School"
link_to_project: true

description: "Join our emerging leaderships’ school. Here you can strengthen your advocacy skills and generate real social change."

image: "/assets/images/proyectos/escuela-incidencia.png"
hero-image: "/assets/images/hero_projects/hero-escuela.jpg"

site: "https://escueladeincidencia.org"
category: Advocacy
year: 2018

visible_timeline: true
visible_home: true

versions:
  - name: "Advocacy School Colombia"
    year: 2017
    country: "Colombia"
    description: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America."
  - name: "Advocacy School Bolivia"
    year: 2017
    country: "Bolivia"
    description: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America."
  - name: "Advocacy School Perú"
    year: 2017      
    country: "Perú"
    description: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America. "
  - name: "Advocacy School Ecuador"
    year: 2017      
    country: "Ecuador"
    description: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America."

  - name: "Advocacy School Brasil"
    year: 2018
    country: "Brasil"
    description: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America."

  - name: "Advocacy School Chile"
    year: 2018
    country: "Chile"
    description: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America."

  - name: "Advocacy School Guatemala"
    year: 2018      
    country: "Guatemala"
    description: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America."

  - name: "Advocacy School Mexico"
    year: 2018      
    country: "México"
    description: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America."

  - name: "Advocacy School Puerto Rico"
    year: 2018
    country: "Puerto Rico"
    description: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America."

  - name: "Advocacy School for public servants Ecuador"
    year: 2018
    country: "Ecuador"
    description: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America."

  - name: "Advocacy School Colombia"
    year: 2018
    country: "Colombia"
    description: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America."

  - name: "Advocacy School Peru"
    year: 2019     
    country: "Peru"
    description: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America."

  - name: "Advocacy School Ecuador"
    year: 2019   
    country: "Ecuador"
    description: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America."

  - name: "Advocacy School Guatemala"
    year: 2019   
    country: "Guatemala"
    description: "Project that transfers facilitation tools and methodologies to activists and forms emerging leaders in Latin America."

---
