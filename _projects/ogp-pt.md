---
layout: project
lang: pt
name: "OGP Chile A Aliança para o Governo Aberto"

description: "Uma iniciativa multilateral que visa assegurar compromissos concretos de governos nacionais e subnacionais para promover o governo aberto, capacitar os cidadãos, combater a corrupção e usar novas tecnologias para fortalecer a governança."

visible_timeline: true
visible_home: false

image: ""
site: "https://ogp.gob.cl/es/principios/"
year: 2016

versions:
  - name: "OGP Chile A Aliança para o Governo Aberto"
    year: 2016
    country: "Chile"
    description: "Uma iniciativa multilateral que visa assegurar compromissos concretos de governos nacionais e subnacionais para promover o governo aberto, capacitar os cidadãos, combater a corrupção e usar novas tecnologias para fortalecer a governança."
    year: 2017
    country: "Chile"
    description: "Uma iniciativa multilateral que visa assegurar compromissos concretos de governos nacionais e subnacionais para promover o governo aberto, capacitar os cidadãos, combater a corrupção e usar novas tecnologias para fortalecer a governança."

---
