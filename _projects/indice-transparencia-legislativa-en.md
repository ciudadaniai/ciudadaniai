---
layout: project
lang: en
name: "Latin American Legislative Transparency Index"

description: "Assessment that allows citizens to compare the levels of congresses and parliaments’ transparency and access to information in Latin America."

visible_timeline: true
visible_home: false

image: ""
site: "https://www.transparencialegislativa.org/publicacion/indice-latinoamericano-de-transparencia-legislativa/"
year: 2011

versions:
  - name: "Latin American Legislative Transparency Index"
    year: 2011
    country: "Chile"
    description: "Assessment that allows citizens to compare the levels of congresses and parliaments’ transparency and access to information in Latin America."
  - name: "Latin American Legislative Transparency Index"
    year: 2014
    country: "Chile"
    description: "Assessment that allows citizens to compare the levels of congresses and parliaments’ transparency and access to information in Latin America."
  - name: "Latin American Legislative Transparency Index"
    year: 2016
    country: "Chile"
    description: "Assessment that allows citizens to compare the levels of congresses and parliaments’ transparency and access to information in Latin America."

---
