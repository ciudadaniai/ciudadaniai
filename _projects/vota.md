---
layout: project
lang: es
name: "Vota Inteligente"
link_to_project: true

description: "Crea y apoya propuestas, durante procesos electorales, que serán enviadas a las candidaturas para que las incorporen en sus programas de gobierno."

description-timeline: "Primer Proyecto de Ciudadanía inteligente. Herramienta digital para  informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/vota.png"
hero-image: "/assets/images/hero_projects/hero-vota.jpg"

site: "https://votainteligente.cl/"
category: Denuncia Cohecho
year: 2009
status: Activo

versions:
  - name: "Vota Inteligente Chile"
    year: 2009
    country: "Chile"
    description: "Primer Proyecto de Ciudadanía inteligente. Herramienta digital para  informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política."
  - name: "Vota Inteligente Chile"
    year: 2011
    country: "Chile"
    description: "Herramienta digital para  informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política."
  - name: "Vota Inteligente Chile"
    year: 2012
    country: "Chile"
    description: "Herramienta digital para  informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política."
  - name: "Vota Inteligente Chile"
    year: 2013
    country: "Chile"
    description: "Herramienta digital para  informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política."
  - name: "Vota Inteligente Chile"
    year: 2014
    country: "Chile"
    description: "Herramienta digital para  informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política."
  - name: "Vota Inteligente México"
    year: 2016
    country: "México"
    description: "Herramienta digital para  informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política."
  - name: "Vota Inteligente Municipal Chile"
    year: 2016
    country: "Chile"
    description: "Herramienta digital para  informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política."

---
