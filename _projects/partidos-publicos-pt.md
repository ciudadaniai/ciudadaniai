---
layout: project
lang: pt
name: "Partidos Públicos"

description: "Acessar e monitorar de forma interativa e amigável a informação pública dos partidos políticos ativos no Portal da Transparência do Chile."

visible_timeline: true
visible_home: false

image: "/assets/images/proyectos/partidos-publicos.png"
hero-image: "/assets/images/hero_projects/hero-partidospublicos.jpg"

site: "https://partidospublicos.cl/"
category: Fiscalização
year: 2016

versions:
  - name: "Partidos Públicos"
    year: 2016
    country: "Chile"
    description: "Uma plataforma que busca facilitar o acesso e a supervisão dos cidadãos às informações publicadas pelos partidos políticos chilenos, em transparência ativa, através de visualizações, filtros e comparações amigáveis. "

---
