---
layout: project
lang: en
name: "Rio por Inteiro"

description: "Check which candidates are committed with citizen proposals in Brazil."

visible_timeline: true
visible_home: false

image: "/assets/images/proyectos/rioporinteiro.png"
hero-image: "/assets/images/hero_projects/hero-rioporinteiro.jpg"

category: Effective Information
year: 2018

versions:
  - name: "Rio por Inteiro Brazil"
    year: 2018
    country: "Brazil"
    description: "A platform that allowed to visualize candidacies committed with citizen proposals."

---
