---
layout: project
lang: pt
name: "GIRO2020"
link_to_project: true
ref: giro2020

description: "Veja como promovemos a participação cidadã e a mobilização social nas eleições municipais do estado do Rio de Janeiro."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/giro.png"

site: "https://giro2020.org/"
category: Participación Ciudadana
year: 2020

versions:
  - name: "GIRO2020"
    year: 2020
    country: "Brasil"
    description: "Iniciativa que busca proporcionar, durante todo o processo eleitoral municipal no Estado do Rio de Janeiro, maior participação cidadã e maior treinamento dos candidatos."

---
