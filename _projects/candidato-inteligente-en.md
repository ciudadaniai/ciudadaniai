---
layout: project
lang: en
name: "Intelligent Candidate Chile"

description: "Project developed with artificial intelligence to bring politics closer to citizens."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2017

versions:
  - name: "Intelligent Candidate Chile"
    year: 2017
    country: "Chile"
    description: "Project developed with artificial intelligence to bring politics closer to citizens."
---
