---
layout: project
lang: es
name: "Colectiva 2024"
link_to_project: true
ref: colectiva2024

description: "Conectar con una democracia más participativa, transparente y sostenible en América Latina y el Caribe. Encuentro realizado en Santiago con invitad@s de toda Latam."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/colectiva-img.png"
hero-image: "/assets/images/hero_projects/hero aa.png"

site: "https://colectiva.ciudadaniai.org"
category: Participación Ciudadana
year: 2024
---