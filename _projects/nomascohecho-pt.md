---
layout: project
lang: pt
name: "Não há mais suborno"
link_to_project: true

description: "Ajude-nos a acabar com a corrupção, a lutar por uma maior transparência e a assegurar que a nossa democracia sirva ao interesse geral, e não à dos grupos privilegiados."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/nomascohecho.png"
hero-image: "/assets/images/hero_projects/hero-nomascohecho.jpg"

site: "https://nomascohecho.cl/"
category: Participação cidadã
year: 2018

versions:
  - name: "Não há mais suborno"
    year: 2015
    country: "Chile"
    description: "Plataforma digital para a geração de propostas cidadãs contra o suborno e a entrega de informações sobre as denúncias apresentadas pela Intelligent Citizenship contra figuras políticas chilenas envolvidas em casos de suborno e financiamento ilegal da política"

---
