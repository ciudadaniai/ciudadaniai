---
layout: project
lang: es
name: "Hacking Cívico Brasil"

description: "Un curso de generación y formación de proyectos sobre los desafíos de la democracias y generar soluciones en Brasil."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2017

versions:
  - name: "Hacking Cívico Brasil"
    year: 2017
    country: "Brasil"
    description: "Un curso de generación y formación de proyectos sobre los desafíos de la democracias y generar soluciones en Brasil."

---
