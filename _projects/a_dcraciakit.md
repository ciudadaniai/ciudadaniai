---
layout: project
lang: es
name: "Democracia Kit"
link_to_project: true
ref: democraciakit

description: "Toolkit para aplicar la metodología de Abre a equipos de trabajo. Herramienta online interactiva."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/img-democraciakit.png"
hero-image: "/assets/images/hero_projects/hero aa.png"

site: "https://democraciakit.abrealcaldias.org"
category: Participación Ciudadana
year: 2024
---