---
layout: project
lang: pt
name: "CILA 2021"
link_to_project: true
ref: amcila

description: "Cidades latino-americanas em ação. Faça parte da construção de cidades mais abertas, democráticas e inclusivas."
visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/cila-2021.png"
hero-image:

site: "https://www.cilaglobal.org/"
category: Participación Ciudadana
year: 2021

versions:
  - name: "CILA 2021"
    year: 2021
    country: "Latin America "
    description: "Um encontro virtual de 3 dias, com palestrantes de todo o mundo para discutir como construir governos mais abertos e participativos, com uma perspectiva de gênero, diversificada, inclusiva e sustentável."

---
