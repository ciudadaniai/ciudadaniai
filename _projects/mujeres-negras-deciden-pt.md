---
layout: project
lang: pt
name: "Mulheres Negras Decidem"
link_to_project: true

description: "Confira a informação sobre a campanha Mulheres Negras Decidem e os dados sobre a sub-representação das mulheres negras na política brasileira."

visible_timeline: true
visible_home: false

image: "/assets/images/proyectos/mujeresnegrasdeciden.png"
hero-image: "/assets/images/hero_projects/hero-mujeresnegras.png"
logo: "/assets/images/hero_projects/hero-mujeresnegras.png"

site: "https://mulheresnegrasdecidem.org/"
category:
year: 2018

versions:
  - name: "Mulheres Negras Decidem"
    year: 2018
    country: "Brasil"
    description: "Uma plataforma de mobilização que centrou-se em dados para aumentar a presença de mulheres negras na política institucional no Brasil."
---
