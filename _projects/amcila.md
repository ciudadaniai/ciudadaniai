---
layout: project
lang: es
name: "CILA 2021"
link_to_project: true
ref: amcila

description: "Ciudades Latinoamericanas en Acción. Sé parte de la construcción de ciudades más abiertas, democráticas e inclusivas."
visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/cila-2021.png"
hero-image:

site: "https://www.cilaglobal.org/"
category: Participación Ciudadana
year: 2021

versions:
  - name: "CILA 2021"
    year: 2021
    country: "América Latina"
    description: "Un encuentro virtual de 3 días, con speakers de todo el mundo para dialogar sobre cómo construir gobiernos más abiertos, participativos, con enfoque de género, diversos, inclusivos y sustentables."

---
