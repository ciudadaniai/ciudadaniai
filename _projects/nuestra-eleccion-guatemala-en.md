---
layout: project
lang: en
name: "Our Election Guatemala"

description: "Vote Smart for presidential elections in Guatemala."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2019

versions:
  - name: "Our Election Guatemala"
    year: 2019
    country: "Guatemala"
    description: "Vote Smart for presidential elections in Guatemala."

---
