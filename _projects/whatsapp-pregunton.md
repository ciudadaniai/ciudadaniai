---
layout: project
lang: es
name: "Whatsapp Preguntón Chile"

description: "Plataforma digital para la entrega de información a la ciudadanía sobre las elecciones presidenciales y de representantes al Congreso Nacional."

visible_timeline: true
visible_home: false

site: ""
category:
year: 2017

versions:
  - name: "Whatsapp Preguntón Chile"
    year: 2017
    country: "Chile"
    description: "Plataforma digital para la entrega de información a la ciudadanía sobre las elecciones presidenciales y de representantes al Congreso Nacional."
---
