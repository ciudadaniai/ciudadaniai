---
layout: project
lang: en
name: "Inspector of Interests"

description: "Digital platform where you can find detailed information about the conflicts interests of parliamentarians."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2011

versions:
  - name: "Inspector of Interests"
    year: 2011
    country: "Chile"
    description: "Digital platform where you can find detailed information about the conflicts interests of parliamentarians."

---
