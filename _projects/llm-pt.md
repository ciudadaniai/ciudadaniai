---
layout: project
lang: pt
name: "Levante a mão"
link_to_project: true

description: "Incentiva a participação e conscientização cívica de crianças e adolescentes para que possam criar propostas para problemas percebidos em sua escola ou contexto local."

description-timeline: "Plataforma digital para incentivar a participação e conscientização cívica de crianças e adolescentes na vida política de seu país."

image: "/assets/images/proyectos/LLM.png"
hero-image: "/assets/images/hero_projects/hero-LLM.jpg"

visible_timeline: true
visible_home: false

site: "http://levantalamano.org/"
category: Participacão Infantojuvenil
year: 2018

versions:
  - name: "Levante a mão Chile"
    year: 2017
    country: "Chile"
    description: "Plataforma digital para incentivar a participação e conscientização cívica de crianças e adolescentes na vida política de seu país."

  - name: "Levante a mão pelo futuro Hijuelas Chile"
    year: 2019
    country: "Chile"
    description: "Plataforma digital para incentivar a participação e conscientização cívica de crianças e adolescentes na vida política de seu país."

---
