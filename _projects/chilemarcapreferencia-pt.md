---
layout: project
lang: pt
name: "Chile Marca Preferencia"
link_to_project: true
ref: chilemarcapreferencia

description: "Revise e compare as posições dos principais atores políticos em mais de 20 questões-chave para o Chile."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/chilemarca-thumb.png"
hero-image: "/assets/images/hero_projects/hero aa.png"

site: "https://chilemarcapreferencia.cl/"
category: Participação cidadã
year: 2020

versions:
  - name: "Chile Marca Preferencia "
    year: 2020
    country: "Chile"
    description: "Um site que disponibiliza aos cidadãos informações comparáveis ​​e transparentes sobre as posições de diferentes atores, como partidos políticos e organizações sociais, em diferentes áreas temáticas chave para o Chile."

---
