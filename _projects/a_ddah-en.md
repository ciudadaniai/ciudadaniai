---
layout: project
lang: en
name: "From Saying to the Fact"
link_to_project: true

description: "Find information in our study on how much our governments fulfill the legislative promises made in their programs and public account speeches."

image: "/assets/images/proyectos/ddah.png"
hero-image: "/assets/images/hero_projects/hero-DDAH.jpg"

visible_timeline: true
visible_home: true

site: "https://deldichoalhecho.cl/"
category: Citizen oversight
year: 2010-2019

versions:
  - name: "From Saying to the Fact"
    year: 2011
    country: "Chile"
    description: "A study that measures the fulfilling of legislative promises made by governments during their campaigns and public accounts speeches."
  - name: "From Saying to the Fact"
    year: 2012
    country: "Chile"
    description: "A study that measures the fulfilling of legislative promises made by governments during their campaigns and public accounts speeches."
  - name: "From Saying to the Fact"
    year: 2013
    country: "Chile"
    description: "A study that measures the fulfilling of legislative promises made by governments during their campaigns and public accounts speeches."
  - name: "From Saying to the Fact"
    year: 2014
    country: "Chile"
    description: "A study that measures the fulfilling of legislative promises made by governments during their campaigns and public accounts speeches."
  - name: "From Saying to the Fact"
    year: 2015
    country: "Chile"
    description: "A study that measures the fulfilling of legislative promises made by governments during their campaigns and public accounts speeches."
  - name: "From Saying to the Fact Presidential Public Account Uruguay"
    year: 2016
    country: "Uruguay"
    description: "A study that measures the fulfilling of legislative promises made by governments during their campaigns and public accounts speeches."
  - name: "From Saying to the Fact Presidential Public Account Chile"
    year: 2016
    country: "Chile"
    description: "A study that measures the fulfilling of legislative promises made by governments during their campaigns and public accounts speeches."
  - name: "From Saying to the Fact Government Program Chile "
    year: 2016
    country: "Chile"
    description: "A study that measures the fulfilling of legislative promises made by governments during their campaigns and public accounts speeches."
  - name: "From Saying to the Fact Presidential Public Account Uruguay"
    year: 2017
    country: "Uruguay"
    description: "A study that measures the fulfilling of legislative promises made by governments during their campaigns and public accounts speeches."
  - name: "From Saying to the Fact  Presidential Public Account Chile"
    year: 2017
    country: "Chile"
    description: "A study that measures the fulfilling of legislative promises made by governments during their campaigns and public accounts speeches."
  - name: "From Saying to the Fact Government Program Chile "
    year: 2017
    country: "Chile"
    description: "A study that measures the fulfilling of legislative promises made by governments during their campaigns and public accounts speeches."
  - name: "From Saying to the Fact Brazil"
    year: 2017
    country: "Brasil"
    description: "Study on the public account of the mayor of Rio de Janeiro."
  - name: "From Saying to the Fact Presidential Public Account Uruguay"
    year: 2018
    country: "Uruguay"
    description: "A study that measures the fulfilling of legislative promises made by governments during their campaigns and public accounts speeches."
  - name: "From Saying to the Fact  Presidential Public Account Chile"
    year: 2019
    country: "Chile"
    description: "A study that measures the fulfilling of legislative promises made by governments during their campaigns and public accounts speeches."
  - name: "From Saying to the Fact Government Program Chile "
    year: 2019
    country: "Chile"
    description: "A study that measures the fulfilling of legislative promises made by governments during their campaigns and public accounts speeches."
  - name: "From Saying to the Fact Government Program Chile "
    year: 2020
    country: "Chile"
    description: "A study that measures the fulfilling of legislative promises made by governments during their campaigns and public accounts speeches."
---
