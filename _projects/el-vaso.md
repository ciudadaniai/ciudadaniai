---
layout: project
lang: es
name: "El Vaso Chile"

visible_timeline: true
visible_home: false

description: "El primer blog de Ciudadanía Inteligente donde se publicó contenido regularmente sobre nuestro trabajo."

image: ""
site: ""
year: 2011

versions:
  - name: "El Vaso Chile"
    year: 2011
    country: "Chile"
    description: "El primer blog de Ciudadanía Inteligente donde se publicó contenido regularmente sobre nuestro trabajo."
---
