---
layout: project
lang: en
name: "Chile Marca Preferencia"
link_to_project: true
ref: chilemarcapreferencia

description: "Check and compare the positions of the main political actors on more than 20 important issues for Chile."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/chilemarca-thumb.png"
hero-image: "/assets/images/hero_projects/hero aa.png"

site: "https://chilemarcapreferencia.cl/"
category: Citizen participation
year: 2020

versions:
  - name: "Chile Marca Preferencia "
    year: 2020
    country: "Chile"
    description: "A website for citizens to have comparable and transparent information on the positions of different Chilean actors, such as political parties and social organizations, in more than 20 thematic areas."

---
