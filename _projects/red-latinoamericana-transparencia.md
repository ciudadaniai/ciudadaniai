---
layout: project
lang: es
name: "Red Latinoamericana de Transparencia Legislativa"

description: "Red que promueve activamente la transparencia, el acceso a la información y la responsabilidad en los congresos de la región latinoamericana. Ciudadanía Inteligente coordinó la red."

visible_timeline: true
visible_home: false

image: ""
site: "https://www.transparencialegislativa.org/"
year: 2016

versions:
  - name: "Red Latinoamericana de Transparencia Legislativa"
    year: 2016
    country: "Chile"
    description: "Red que promueve activamente la transparencia, el acceso a la información y la responsabilidad en los congresos de la región latinoamericana. Ciudadanía Inteligente coordinó la red."
  - name: "Red Latinoamericana de Transparencia Legislativa"
    year: 2017
    country: "Chile"
    description: "Red que promueve activamente la transparencia, el acceso a la información y la responsabilidad en los congresos de la región latinoamericana. Ciudadanía Inteligente coordinó la red."
---
