---
layout: project
lang: es
name: "Me Representa"
link_to_project: true

description: "Conoce aquí cuáles son las candidaturas comprometidas con los derechos humanos y quienes representan mejor tus posturas en períodos de elección."

description-timeline: "Plataforma digital para fomentar participación y conciencia cívica de niños, niñas y adolescentes en la vida política de su país."

image: "/assets/images/proyectos/merepresenta.png"
hero-image: "/assets/images/hero_projects/hero-merepresenta.jpg"

site: "http://merepresenta.org.br/"
year: 2018

visible_timeline: true
visible_home: false

versions:
  - name: "Me Representa Brasil"
    year: 2018
    country: "Brasil"
    description: "Una plataforma que conectó a las electoras y electores con candidaturas comprometidas con los derechos humanos."
---
