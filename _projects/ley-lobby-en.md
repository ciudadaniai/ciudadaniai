---
layout: project
lang: en
name: "Lobby Law"

description: "Before Chile regulated lobby through a Lobby Law, we created a platform to transparent lobby activities exerted on authorities and public servants."

visible_timeline: true
visible_home: false

image: ""
site: "http://leydelobby.cl/preguntas-frecuentes.html"
year: 2014

versions:
  - name: "Lobby Law"
    year: 2014
    country: "Brasil"
    description: "Before Chile regulated lobby through a Lobby Law, we created a platform to transparent lobby activities exerted on authorities and public servants."

---
