---
layout: project
lang: es
name: "NEBE Bolivia"

description: "Proyecto que estudiaba los impactos de la nacionalización sobre los conflictos y la cooperación alrededor de las actividades extractivas en Bolivia, Ecuador y Perú."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2015

versions:
  - name: "NEBE Bolivia"
    year: 2015
    country: "Bolivia"
    description: "Proyecto que estudiaba los impactos de la nacionalización sobre los conflictos y la cooperación alrededor de las actividades extractivas en Bolivia, Ecuador y Perú."

---
