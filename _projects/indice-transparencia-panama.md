---
layout: project
lang: es
name: "Índice transparencia Panamá"

description: "Guía de políticos de Panamá."

visible_timeline: true
visible_home: false

image: ""
site: "https://espaciocivico.org"
year: 2018

versions:
  - name: "Índice transparencia Panamá"
    year: 2018
    country: "Panamá"
    description: "Guía de políticos de Panamá."

---
