---
layout: project
lang: es
name: "Estratega Chile"

description: "Innovación Social Aysén Chile."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2015

versions:
  - name: "Estratega Chile"
    year: 2015
    country: "Chile"
    description: "Innovación Social Aysén Chile."
  - name: "Estratega México"
    year: 2016
    country: "México"
    description: "Innovación Social México."
  - name: "Estratega Puerto Rico"
    year: 2016
    country: "Puerto Rico"
    description: "Innovación Social Puerto Rico."
  - name: "Estratega Puerto Rico"
    year: 2017
    country: "Puerto Rico"
    description: "Innovación Social Puerto Rico."
  - name: "Estratega Nicaragua"
    year: 2017
    country: "Nicaragua"
    description: "Innovación Social Nicaragua."
  - name: "Estratega Indonesia"
    year: 2017
    country: "Indonesia"
    description: "Innovación Social Indonesia."
---
