---
layout: project
lang: pt
name: "Abrelatam"

description: "Projeto para a construção e promoção de dados abertos, políticas e seus usos para melhorar a situação na América Latina."

visible_timeline: true
visible_home: false

image: ""
site: "https://2019.abrelatam.org/"
year: 2015

versions:
  - name: "Abrelatam"
    year: 2015
    country: "Chile"
    description: "Projeto para a construção e promoção de dados abertos, políticas e seus usos para melhorar a situação na América Latina."

---
