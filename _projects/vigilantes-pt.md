---
layout: project
lang: pt
name: "Observadores de crianças Chile"
link_to_project: true

description: "Verifique o nível de cumprimento por parte do Governo do Chile dos compromissos assumidos no âmbito do Acordo Nacional para a Infância."

visible_timeline: true
visible_home: false

image: "/assets/images/proyectos/vigilantes.png"
hero-image: "/assets/images/hero_projects/hero-vigilantes.png"
logo: "/assets/images/hero_projects/hero-vigilantes.png"

site: "https://vigilantesporlainfancia.cl/"
category: Supervisiona
year: 2018

versions:
  - name: "Observadores de crianças Chile"
    year: 2018
    country: "Chile"
    description: "Verifique o nível de cumprimento por parte do Governo do Chile dos compromissos assumidos no âmbito do Acordo Nacional para a Infância."

---
