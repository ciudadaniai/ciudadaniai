---
layout: project
lang: es
name: "Inspector de Intereses"

description: "Plataforma digital con resultados de investigación a las sociedades parlamentarias no declaradas por las autoridades legislativas en sus conflicto de interés"

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2011

versions:
  - name: "Inspector de Intereses"
    year: 2011
    country: "Chile"
    description: "Plataforma digital con resultados de investigación a las sociedades parlamentarias no declaradas por las autoridades legislativas en sus conflicto de interés"

---
