---
layout: project
lang: en
name: "The Glass Chile"

visible_timeline: true
visible_home: false

description: "Ciudadanía Inteligente’s first blog, where content on our work was regularly published."

image: ""
site: ""
year: 2011

versions:
  - name: "The Glass Chile"
    year: 2011
    country: "Chile"
    description: "Ciudadanía Inteligente’s first blog, where content on our work was regularly published."
---
