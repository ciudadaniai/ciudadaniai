---
layout: project
lang: en
name: "CILA 2021"
link_to_project: true
ref: amcila

description: "Latin American Cities in Action. Be part of the construction of more open, democratic and inclusive cities."
visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/cila-2021.png"
hero-image:

site: "https://www.cilaglobal.org/"
category: Participación Ciudadana
year: 2021

versions:
  - name: "CILA 2021"
    year: 2021
    country: "Latin America "
    description: "A 3-day virtual meeting, with speakers from all over the world to discuss how to build more open, participatory governments, with a gender perspective, diverse, inclusive and sustainable."

---
