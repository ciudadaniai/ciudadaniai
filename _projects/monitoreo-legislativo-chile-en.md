---
layout: project
lang: en
name: "Legislative tracking Chile"

description: "Digital platform to access information on elected legislators’ votes and on presented bills."

image: ""

visible_timeline: true
visible_home: false

site: ""
category:
year: 2010

versions:
  - name: "Legislative tracking Chile"
    year: 2010
    country: "Chile"
    description: "Digital platform to access information on elected legislators’ votes and on presented bills."

---
