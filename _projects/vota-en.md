---
layout: project
lang: en
name: "Vote Smart"
link_to_project: true

description: "Create and support proposals, during electoral processes, that will be sent to candidates so that they can incorporate them in their government programs."

description-timeline: "Ciudadanía Inteligente’s first project. Digital tool to inform citizens on presidential candidates, putting technology at the service of politics."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/vota.png"
hero-image: "/assets/images/hero_projects/hero-vota.jpg"

site: "https://votainteligente.cl/"
category: Citizen oversight
year: 2009
status: Activo

versions:
  - name: "Vote Smart Chile"
    year: 2009
    country: "Chile"
    description: "Ciudadanía Inteligente’s first project. Digital tool to inform citizens on presidential candidates, putting technology at the service of politics."
  - name: "Vote Smart Chile"
    year: 2011
    country: "Chile"
    description: "Digital tool to inform citizens on presidential candidates, putting technology at the service of politics."
  - name: "Vote Smart Chile"
    year: 2012
    country: "Chile"
    description: "Digital tool to inform citizens on presidential candidates, putting technology at the service of politics."
  - name: "Vote Smart Chile"
    year: 2013
    country: "Chile"
    description: "Digital tool to inform citizens on presidential candidates, putting technology at the service of politics."
  - name: "Vote Smart Chile"
    year: 2014
    country: "Chile"
    description: "Digital tool to inform citizens on presidential candidates, putting technology at the service of politics."
  - name: "Vote Smart Mexico"
    year: 2016
    country: "México"
    description: "Digital tool to inform citizens on presidential candidates, putting technology at the service of politics."
  - name: "Vote Smart Municipalities Chile"
    year: 2016
    country: "Chile"
    description: "Digital tool to inform citizens on presidential candidates, putting technology at the service of politics."

---
