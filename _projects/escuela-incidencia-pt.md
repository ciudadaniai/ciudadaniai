---
layout: project
lang: pt
name: "Escuela de Incidencia"
link_to_project: true

description: "Junte-se à nossa escola de líderes emergentes. Aqui você pode fortalecer suas habilidades de advocacy em políticas públicas e gerar mudanças sociais reais."

image: "/assets/images/proyectos/escuela-incidencia.png"
hero-image: "/assets/images/hero_projects/hero-escuela.jpg"

site: "https://escueladeincidencia.org"
category: Incidência
year: 2018

visible_timeline: true
visible_home: true

versions:
  - name: "Escuela de Incidencia Colômbia"
    year: 2017
    country: "Colômbia"
    description: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na LATAM."
  - name: "Escuela de Incidencia Bolívia"
    year: 2017
    country: "Bolivia"
    description: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na LATAM."
  - name: "Escuela de Incidencia Perú"
    year: 2017      
    country: "Perú"
    description: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na LATAM."
  - name: "Escuela de Incidencia Equador"
    year: 2017      
    country: "Equador"
    description: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na LATAM."

  - name: "Escuela de Incidencia Brasil"
    year: 2018
    country: "Brasil"
    description: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na LATAM."

  - name: "Escuela de Incidencia Chile"
    year: 2018
    country: "Chile"
    description: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na LATAM."

  - name: "Escuela de Incidencia Guatemala"
    year: 2018      
    country: "Guatemala"
    description: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na LATAM."

  - name: "Escuela de Incidencia México"
    year: 2018      
    country: "México"
    description: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na LATAM."

  - name: "Escuela de Incidencia Porto Rico"
    year: 2018
    country: "Porto Rico"
    description: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na LATAM."

  - name: "Escuela de Incidencia para funcionários públicos Equador"
    year: 2018
    country: "Equador"
    description: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na LATAM."

  - name: "Escuela de Incidencia Colômbia"
    year: 2018
    country: "Colômbia"
    description: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na LATAM."

  - name: "Escuela de Incidencia Perú"
    year: 2019     
    country: "Perú"
    description: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na LATAM."

  - name: "Escuela de Incidencia Equador"
    year: 2019   
    country: "Equador"
    description: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na LATAM."

  - name: "Escuela de Incidencia Guatemala"
    year: 2019   
    country: "Guatemala"
    description: "Projeto para transferir ferramentas de facilitação para ativistas e treinamento de líderes emergentes na LATAM."

---
