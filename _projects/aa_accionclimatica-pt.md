---
layout: project
lang: pt
name: "Acción Climática"
link_to_project: true
ref: accionclimatica

description: "Estudio que mide el nivel de cumplimiento de los compromisos adquiridos por el Estado de Chile para hacer frente al cambio climático a través de la Ley Marco de Cambio Climático."

visible_timeline: false
visible_home: false

image: "/assets/images/proyectos/accionclimatica_proyectos.png"
hero-image: "/assets/images/hero_projects/hero aa.png"

site: "https://accionclimaticachile.cl"
category: Participación Ciudadana
year: 2024
---