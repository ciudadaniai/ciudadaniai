---
layout: project
lang: es
name: "Dataigualdad"
link_to_project: true

description: "Revisa con datos y gráficas simples cómo los privilegios de unos pocos fomentan la desigualdad en América Latina y el Caribe."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/dataigualdad.png"
hero-image: "/assets/images/hero_projects/dataigualdad.png"
logo: "/assets/images/hero_projects/dataigualdad.png"

site: "http://dataigualdad.com/"
category: Fiscaliza
year: 2019

versions:
  - name: "Dataigualdad"
    year: 2019
    country: "Latinoamérica"
    description: "Revisa con datos y gráficas simples sobre cómo los privilegios de unos pocos fomentan la desigualdad en América Latina y el Caribe"
    versions:
  - name: "Dataigualdad"
    year: 2020
    country: "Latinoamérica"
    description: "Revisa con datos y gráficas simples sobre cómo los privilegios de unos pocos fomentan la desigualdad en América Latina y el Caribe"

---
