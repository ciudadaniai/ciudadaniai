---
layout: project
lang: en
name: "Dataigualdad"
link_to_project: true

description: "Check out these figures and graphics showing how the privileges of a few foster inequality in Latin America and the Caribbean."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/dataigualdad.png"
hero-image: "/assets/images/hero_projects/dataigualdad.png"
logo: "/assets/images/hero_projects/dataigualdad.png"

site: "http://dataigualdad.com/"
category: Fiscaliza
year: 2019

versions:
  - name: "Dataigualdad"
    year: 2019
    country: "Latinoamérica"
    description: "Check out these figures and graphics showing how the privileges of a few foster inequality in Latin America and the Caribbean."
  - name: "Dataigualdad"
    year: 2020
    country: "Latinoamérica"
    description: "Check out these figures and graphics showing how the privileges of a few foster inequality in Latin America and the Caribbean."
---
