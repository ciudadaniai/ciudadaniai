---
layout: project
lang: pt
name: "Globo Cidadã Chile"

description: "Projeto para registrar a 20 metros de altura as massivas mobilizações estudantis que ocorreram no país."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2011

versions:
  - nname: "Globo Cidadã Chile"
    year: 2011
    country: "Chile"
    description: "Projeto para registrar a 20 metros de altura as massivas mobilizações estudantis que ocorreram no país."

---
