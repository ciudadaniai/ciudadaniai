---
layout: project
lang: pt
name: "WIZARD Internacional"

description: "Uma estratégia de incidência para o UNICEF."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2015

versions:
  - name: "WIZARD Internacional"
    year: 2015
    country: "Chile"
    description: "Uma estratégia de incidência para o UNICEF."

---
