---
layout: project
lang: pt
name: "Pesquisa do Orçamento Aberto 2017 Chile"

description: "Uma pesquisa que avalia a transparência orçamentária com base no valor e na pontualidade das informações orçamentárias que os governos disponibilizam para o público."

visible_timeline: true
visible_home: false

image: ""
site: "https://www.internationalbudget.org/open-budget-survey/"
year: 2017

versions:
  - name: "Pesquisa do Orçamento Aberto 2017 Chile"
    year: 2017
    country: "Chile"
    description: "Uma pesquisa que avalia a transparência orçamentária com base no valor e na pontualidade das informações orçamentárias que os governos disponibilizam para o público."

---
