---
layout: project
lang: pt
name: "NEBE Bolívia"

description: "Projeto que estudou os impactos da nacionalização em conflitos e cooperação em torno de atividades extrativistas na Bolívia, Equador e Peru."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2015

versions:
  - name: "NEBE Bolívia"
    year: 2015
    country: "Bolivia"
    description: "Projeto que estudou os impactos da nacionalização em conflitos e cooperação em torno de atividades extrativistas na Bolívia, Equador e Peru."

---
