---
layout: project
lang: en
name: "Abrelatam"

description: "Project to promote and build open data, policies and open data uses to make Latin America in this area better."

visible_timeline: true
visible_home: false

image: ""
site: "https://2019.abrelatam.org/"
year: 2015

versions:
  - name: "Abrelatam"
    year: 2015
    country: "Chile"
    description: "Project to promote and build open data, policies and open data uses to make Latin America in this area better."

---
