---
layout: project
lang: en
name: "Electoral Traffic Light"
link_to_project: true

description: "Check what can be done during electoral propaganda and how toreport when the law is not followed."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/semaforo1.png"
hero-image: "/assets/images/hero_projects/semaforoelectoral.png"

site: ""
year: 2016

versions:
  - name: "Electoral Traffic Light"
    year: 2016
    country: "Chile"
    description: "A platform that clarifies what can and cannot be done during different periods of electoral campaigning."
  - name: "Electoral Traffic Light"
    year: 2017
    country: "Chile"
    description: "A platform that clarifies what can and cannot be done during different periods of electoral campaigning."
  - name: "Electoral Traffic Light"
    year: 2019
    country: "Chile"
    description: "A platform that clarifies what can and cannot be done during different periods of electoral campaigning."
---
