---
layout: project
lang: es
name: "Chileleaks"
link_to_project: true
ref: chileleaks
description: "¿Tienes información sobre casos de cohecho y soborno en Chile? Denuncia de forma segura y anónima a través de nuestra plataforma."

description-timeline: "Una plataforma segura para denunciar de forma anónima casos de cohecho y soborno en Chile."

image: "/assets/images/proyectos/chileleaks.png"
hero-image: "/assets/images/hero_projects/hero-chileleaks.jpg"

visible_timeline: true
visible_home: true

site: "https://chileleaks.org/index.html"
category: Denuncia Cohecho en Chile
year: 2018

versions:
  - name: "Chileleaks"
    year: 2018
    country: "Chile"
    description: "Una plataforma segura para denunciar de forma anónima casos de cohecho y soborno en Chile."


---
