---
layout: project
lang: es
name: "Por qué en mi jardín"

description: "Plataforma en alianza con serie documental de canal de televisión TVN que  cuenta la historia de una comunidad que se enfrenta a la inminente instalación de una industria productiva en sus comunidades."

visible_timeline: true
visible_home: false

image: ""
site: "https://www.cntv.cl/por-que-en-mi-jardin/cntv/2016-07-05/123008.html"
year: 2011

versions:
  - name: "Por qué en mi jardín"
    year: 2011
    country: "Chile"
    description: "Plataforma en alianza con serie documental de canal de televisión TVN que  cuenta la historia de una comunidad que se enfrenta a la inminente instalación de una industria productiva en sus comunidades."
---
