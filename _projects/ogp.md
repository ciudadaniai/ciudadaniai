---
layout: project
lang: es
name: "OGP Chile La Alianza para el Gobierno Abierto"

description: "Una iniciativa multilateral que intenta asegurar compromisos concretos de gobiernos nacionales y subnacionales para promover el gobierno abierto, dar más poder a los ciudadanos, luchar contra la corrupción y utilizar las nuevas tecnologías para fortalecer la gobernanza."

visible_timeline: true
visible_home: false

image: ""
site: "https://ogp.gob.cl/es/principios/"
year: 2016

versions:
  - name: "OGP Chile La Alianza para el Gobierno Abierto"
    year: 2016
    country: "Chile"
    description: "Una iniciativa multilateral que intenta asegurar compromisos concretos de gobiernos nacionales y subnacionales para promover el gobierno abierto, dar más poder a los ciudadanos, luchar contra la corrupción y utilizar las nuevas tecnologías para fortalecer la gobernanza."
  - name: "OGP"
    year: 2017
    country: "Chile"
    description: "Una iniciativa multilateral que intenta asegurar compromisos concretos de gobiernos nacionales y subnacionales para promover el gobierno abierto, dar más poder a los ciudadanos, luchar contra la corrupción y utilizar las nuevas tecnologías para fortalecer la gobernanza."

---
