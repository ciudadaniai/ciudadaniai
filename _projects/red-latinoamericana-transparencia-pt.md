---
layout: project
lang: pt
name: "Rede Latino-Americana de Transparência Legislativa"

description: "Rede que promove ativamente transparência, acesso à informação e responsabilidade nos congressos da região latino-americana. Cidadania Inteligente coordenou a rede."

visible_timeline: true
visible_home: false

image: ""
site: "https://www.transparencialegislativa.org/"
year: 2016

versions:
  - name: "Rede Latino-Americana de Transparência Legislativa"
    year: 2016
    country: "Chile"
    description: "Rede que promove ativamente transparência, acesso à informação e responsabilidade nos congressos da região latino-americana. Cidadania Inteligente coordenou a rede."
  - name: "Rede Latino-Americana de Transparência Legislativa"
    year: 2017
    country: "Chile"
    description: "Rede que promove ativamente transparência, acesso à informação e responsabilidade nos congressos da região latino-americana. Cidadania Inteligente coordenou a rede."
---
