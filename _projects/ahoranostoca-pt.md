---
layout: project
lang: pt
name: "Agora é a Nossa Vez de Participar"
link_to_project: true
ref: ahoranostoca

description: "Veja como promovemos a participação do cidadão em nível nacional durante os processos políticos no Chile após o surto social de outubro de 201."

visible_timeline: true
visible_home: false

image: "/assets/images/proyectos/antp.png"
hero-image: "/assets/images/hero_projects/hero-ahoranostoca.png"

site: "https://ahoranostocaparticipar.cl/"
year: 2020

versions:
  - name: "Agora é a Nossa Vez de Participar"
    year: 2020
    country: "Chile"
    description: "Veja como promovemos a participação do cidadão em nível nacional durante os processos políticos no Chile após o surto social de outubro de 201."
