---
layout: project
lang: pt
name: "Acceso Inteligente Chile"

description: "A primeira plataforma no Chile que centraliza o sistema de solicitações de informações públicas e permite que elas ocorram on-line."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2011

versions:
  - name: "Acceso Inteligente Chile"
    year: 2011
    country: "Chile"
    description: "A primeira plataforma no Chile que centraliza o sistema de solicitações de informações públicas e permite que elas ocorram on-line."

---
