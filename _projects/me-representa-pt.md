---
layout: project
lang: pt
name: "Me Representa"
link_to_project: true

description: "Saiba aqui quais são as candidaturas comprometidas com os direitos humanos e quem melhor representa suas posições durante os períodos eleitorais."

description-timeline: "Uma plataforma que conectava os eleitores e eleitores às candidaturas aos direitos humanos."

image: "/assets/images/proyectos/merepresenta.png"
hero-image: "/assets/images/hero_projects/hero-merepresenta.jpg"

visible_timeline: true
visible_home: false

category: Informação efetiva
year: 2018

versions:
  - name: "Me Representa Brasil"
    year: 2018
    country: "Brasil"
    description: "Uma plataforma que conectava os eleitores e eleitores às candidaturas aos direitos humanos."
---
