---
layout: project
lang: en
name: "CRIIK Chile"

description: "Open data digital platform that allows citizens to access and share public information on their governments online."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2011

versions:
  - name: "CRIIK Chile"
    year: 2011
    country: "Chile"
    description: "Open data digital platform that allows citizens to access and share public information on their governments online."
  - name: "CRIIK Chile"
    year: 2016
    country: "Chile"
    description: "Open data digital platform that allows citizens to access and share public information on their governments online."

---
