---
ref: calendario-electoral
lang: pt
name: El nuevo calendario electoral
boton_descarga: Descarga la campaña acá y anota las fechas en tu calendario
descarga: elnuevocalendarioelectoral.zip
modalname: calendario-electoral
slide_id: calendario-electoral-slide
image: calendario-electoral/calendario-1.png
image2: calendario-electoral/calendario-2.png
image3: calendario-electoral/calendario-3.png
image4: calendario-electoral/calendario-4.png
image5:
image6:
image7:
image8:
image9:
image10:
video1:
video2:
video3:
video4:
video5:
video6:
video7:
video8:
video9:
video10:
active: true
---