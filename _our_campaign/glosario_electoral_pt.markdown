---
ref: glosario-electoral
lang: pt
name: Revisa nuestro Glosario Electoral
boton_descarga: Descarga la campaña acá y compártela
descarga: glosario-electoral.zip
modalname: glosario-electoral
slide_id: glosario-electoral-slide
image: glosario-electoral/glosario-1.png
image2: glosario-electoral/glosario-2.png
image3: glosario-electoral/glosario-3.png
image4: glosario-electoral/glosario-4.png
image5: glosario-electoral/glosario-5.png
image6: glosario-electoral/glosario-6.png
image7:
image8:
image9:
image10:
video1:
video2:
video3:
video4:
video5:
video6:
video7:
video8:
video9:
video10:
active: true
---