---
ref: escanos-reservados
lang: es
name: Escaños reservados para Pueblos Originarios
boton_descarga: Descarga la campaña acá y ayúdanos a difundir
descarga: escanos-reservados.zip
modalname: escanos-reservados
slide_id: escanos-reservados-slide
image: escanos-reservados/escanos-1.png
image2: escanos-reservados/escanos-2.png
image3: escanos-reservados/escanos-3.png
image4: escanos-reservados/escanos-4.png
image5: escanos-reservados/escanos-5.png
image6: escanos-reservados/escanos-6.png
image7: escanos-reservados/escanos-7.png
image8:
image9:
image10:
video1:
video2:
video3:
video4:
video5:
video6:
video7:
video8:
video9:
video10:
active: true
---