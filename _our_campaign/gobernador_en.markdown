---
ref: gobernador
lang: en
name: "Nuevo cargo: Gobernador/a Regional ¿De qué se trata?"
boton_descarga: Descarga la campaña acá y difunde en tus redes sociales
descarga: gobernador.zip
modalname: gobernador
slide_id: gobernador-slide
image: gobernador/gobernador-1.jpg
image2: gobernador/gobernador-2.jpg
image3: gobernador/gobernador-3.jpg
image4: gobernador/gobernador-4.jpg
image5: gobernador/gobernador-5.jpg
image6: gobernador/gobernador-6.jpg
image7:
image8:
image9:
image10:
video1:
video2:
video3:
video4:
video5:
video6:
video7:
video8:
video9:
video10:
active: true
---