---
ref: tribunal
lang: pt
name: ¿Qué rayos hace el TC?
boton_descarga: Descarga la campaña acá y compártela en tus redes
descarga: tribunal-constitucional.zip
modalname: tribunal
slide_id: tribunal-slide
image: tribunal/tribunal-1.png
image2: tribunal/tribunal-2.png
image3: tribunal/tribunal-3.png
image4: tribunal/tribunal-4.png
image5: tribunal/tribunal-5.png
image6: tribunal/tribunal-6.png
image7: tribunal/tribunal-7.png
image8:
image9:
image10:
video1:
video2:
video3:
video4:
video5:
video6:
video7:
video8:
video9:
video10:
active: true
---