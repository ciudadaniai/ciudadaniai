---
ref: paridad
lang: pt
name: ¿Cómo funcionará la paridad en el Proceso Constituyente?
boton_descarga: Descarga la campaña acá y compártela
descarga: paridad.zip
modalname: paridad
slide_id: paridad-slide
image: paridad/paridad-1.png
image2: paridad/paridad-2.png
image3: paridad/paridad-3.png
image4: paridad/paridad-4.png
image5: paridad/paridad-5.png
image6: paridad/paridad-6.png
image7:
image8:
image9:
image10:
video1:
video2:
video3:
video4:
video5:
video6:
video7:
video8:
video9:
video10:
active: true
---