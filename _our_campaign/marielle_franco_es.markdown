---
ref: mariellefranco
lang: es
name: ¿Quién mandó a matar a Marielle Franco?
boton_descarga: Descarga la campaña acá y ayúdanos a exigir justicia
descarga: marielle-franco.zip
modalname: mariellefranco
slide_id: mariellefranco-slide
image: mf/mf-1.png
image2: mf/mf-2.png
image3: mf/mf-3.png
image4: mf/mf-4.png
image5:
image6:
image7:
image8:
image9:
image10:
video1:
video2:
video3:
video4:
video5:
video6:
video7:
video8:
video9:
video10:
active: true
---