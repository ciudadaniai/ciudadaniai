---
ref: informacion
lang: en
title: Transparent profiles
image: tools-07.gif
layout: tool
intro: A tool built for electoral processes that allows citizens to know different candidates through a short questionnaire.
---
