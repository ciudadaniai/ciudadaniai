---
ref: participacion
lang: en
title: Local governments close to their communities
image: tools-05.gif
intro: Digital and methodological tools to bring closer the work of municipalities to citizens, boosting the collective construction of neighborhoods.
layout: tool

---
