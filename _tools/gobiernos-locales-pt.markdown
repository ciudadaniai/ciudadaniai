---
ref: participacion
lang: pt
title: Governos locais próximos da comunidade
image: tools-05.gif
intro: Ferramentas digitais e metodólogicas para aproximar o trabalho de municípios a vizinhas e vizinhos, pontecializando a construção coletiva de bairros e comunidades.
layout: tool

---
