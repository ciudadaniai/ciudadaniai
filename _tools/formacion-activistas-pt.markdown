---
ref: incidencia
lang: pt
title: Formação de Ativistas
image: tools-04.gif
intro: Formamos e capacitamos lideranças jovens que transformarão suas comunidades e o mundo com metodologias de incidência política.
layout: tool

---
