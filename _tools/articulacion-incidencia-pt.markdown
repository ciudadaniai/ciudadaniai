---
ref: incidencia
lang: pt
layout: tool
title: Articulação para Incidência
image: tools-01.gif
intro: Entregamos ferramentas para planificação e acompanhamento. Nossa metodologia ajuda a definir objetivos de incidência desde a pluralidade, como a criação de redes de colaboração interdisciplinar para alcançar os fins planejados colaborativamente.
---
