---
ref: participacion
lang: pt
title: Eleições Participativas
image: tools-03.gif
intro: Plataforma que permite a cidadania, em momentos eleitorais, criar propostas e pressionar as candidaturas a comprometer-se com as demandas cidadãs.
layout: tool
---
