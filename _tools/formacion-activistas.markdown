---
ref: incidencia
lang: es
title: Formación de Activistas
image: tools-04.gif
layout: tool
description: Formamos y capacitamos liderazgos jóvenes que cambiarán sus comunidades y el mundo con metodologías de incidencia pública.
intro: Formación de Activista es una instancia que busca incentivar el liderazgo colaborativo y participativo de las nuevas generaciones para lograr cambios sociales a través de la incidencia en políticas públicas.
---


**Formación de Activistas** es una instancia de formación que busca fomentar el liderazgo colaborativo y participativo de nuevas generaciones para lograr cambios sociales a través de la incidencia en políticas públicas.

Los contenidos que se abordan dentro del proyecto corresponden a incidencia social y política, estrategias de facilitación de procesos y metodologías de trabajo colaborativo.

### Etapas

Las etapas de este proyecto son las siguientes:

1. Se abre un amplio proceso de convocatoria para jóvenes líderes en el territorio escogido.
2. Se evalúa exhaustivamente perfiles recibidos, de acuerdo a criterios de diversidad, paridad, trayectoria y potencial para seleccionar, a las y los finalistas que participan en la Formación.
3. Taller presencial de tres jornadas (6 hrs cada una) - facilitado por FCI - en que un grupo de líderes emergentes recibe capacitación en herramientas de facilitación y tecnológicas, estrategias de incidencia, e implementación de la metodología LabCívico (que permite identificar problemas públicos concretos y mapear agentes clave para generar un plan de trabajo colaborativo que permita avanzar hacia su solución).
4. Se preparan y ejecutan los Laboratorios Cívicos que realiza cada participante de la Escuela en su espacio de acción.
5. Quienes participan de la Escuela reportan avances y comparten aprendizajes para mejorar futuros procesos de incidencia.


### Experiencias destacadas

La primera versión, implementada con el apoyo del National Endowment for Democracy (NED), se desarrolló durante el año 2017 en Bolivia, Colombia, Ecuador y Perú.

La versión 2018, implementada con el apoyo del National Endowment for Democracy NED y el International Development Research Center IDRC, se realiza en Brasil, Chile, Colombia, Ecuador, Guatemala y México.

Si deseas ser y formar agentes de cambios, esta herramienta te ayudará a lograrlo, ¡contáctanos!



### Contáctanos

Si crees que esta herramienta es para ti y deseas llevarla a cabo ¡contáctanos!

Escríbenos a **herramientas@ciudadaniai.org**
