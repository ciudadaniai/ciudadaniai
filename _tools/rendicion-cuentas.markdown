---
ref: fiscalizacion
lang: es
title: Rendición de cuentas
image: tools-08.gif
layout: tool
description: Metodología y plataforma web que mide y visualiza el cumplimiento de promesas de autoridades.
intro: ¿Te gustaría saber cuánto cumplen las autoridades? Con nuestra herramienta podrás crear una metodología y una plataforma que analizará las promesas de gobierno y autoridades ¡Cuentas claras conservan la democracia!

---

La metodología de **Rendición de Cuentas** permite identificar aquellas promesas susceptibles de ser medidas, las áreas temáticas a las que corresponden y el nivel de avance con que cuentan. Esta plataforma facilita la visualización de manera amigable y cercana los avances en el cumplimiento de las promesas. Además, puede ser utilizada para medir el cumplimiento o avance de distintos tipos de iniciativas en diversas áreas temáticas, para distinto tipo de autoridades.

¿Para qué sirve? Al tener estos conocimientos podemos construir una democracia plena. Hoy más que nunca la ciudadanía requiere de herramientas para fiscalizar la labor de gobernantes y autoridades, así como el cumplimiento de sus compromisos.

### Etapas

Acá te dejamos las etapas de este proceso:

1. Identificación de compromisos o iniciativas a medir: Se identifican compromisos o iniciativas específicas susceptibles de ser medidas.
2. Clasificación de compromisos por área: Los compromisos son organizados en áreas temáticas.
3. Búsqueda de compromisos específicos: Se revisa la existencia o no de iniciativas que den cumplimiento a los compromisos.
4. Análisis de compromisos: Se establece un estado de avance de cada compromiso, que representa su progreso dentro del trámite de toma de decisiones que le corresponda.
5. Determinación total del avance: Se ponderan valores de cada compromiso para representar el cumplimiento total.

## Experiencias destacadas

Han utilizado esta herramienta

- UYCheck Uruguay: deldichoalhecho.com.uy
- Fiscalización del cumplimiento de los primeros 100 días de gobierno de la prefectura de Río de Janeiro, Brasil: ditoefeito.cidadaniainteligente.org
- Seguimiento del cumplimiento de los programas y discursos de los dos últimos gobiernos de Chile deldichoalhecho.cl

### Beneficios

- Entrega información relevante de forma sencilla y clara.
- Fomenta el voto informado basado en programas.
- Promueve el ejercicio responsable de campañas.
- Facilita el control de ciudadanía sobre autoridades


### Contáctanos

Si tu interés está en transparentar la información y fiscalizar el cumplimiento de promesas, ¡contáctanos!

Escríbenos a **herramientas@ciudadaniai.org**
