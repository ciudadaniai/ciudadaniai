---
ref: informacion
lang: es
title: Perfiles Transparentes
image: tools-07.gif
layout: tool
intro: ¿No sabes por quién votar? ¿No sabes cuál candidatura es más afín a tus intereses? Con esta herramienta podrás conocer todas las opciones que se presentan para un proceso eleccionario y revisar con cuáles haces match según tus intereses e ideas.
description: Herramienta utilizada en procesos electorales que permite a la ciudadanía conocer las diferentes candidaturas, a través de una batería de preguntas y respuestas determinadas.
---

En período de elecciones, las candidaturas quieren la atención de la ciudadanía. Pero, ¿cómo podemos conocer las diferencias que nos ofrece cada una de ellas? ¿Cómo decidir cuando muchos agentes buscan persuadir a la ciudadanía?. **Perfiles Transparentes** es una herramienta que permite a la ciudadanía conocer las diferentes candidaturas, a través de preguntas y respuestas determinadas.

¿De qué manera? Buscando que cada persona pueda conocer, considerar y comprender cuáles son las candidatas y candidatos más afines a sus ideas o intereses. Esta tecnología, además, permite a las candidaturas presentar información sobre su sitio web, campaña, propuesta de programa, antecedentes, cargos públicos y diferentes links para acceder de manera rápida a sus redes sociales.

Asimismo, esta herramienta cuenta con un juego de la “media naranja” que entrega el nivel de compatibilidad entre usuarios/as y personas, candidaturas o entidades, haciendo dinámica y atractiva la participación ciudadana y la toma de decisiones.

### Etapas

Duración aproximada: 3 meses (según cantidad de información a recolectar y publicar)

1. Definiciones estéticas del sitio.
2. Definición de las preguntas a realizar.
3. Recolección de posiciones políticas de cada candidatura.
4. Posicionamiento de la plataforma en diferentes medios de comunicación.

###  Experiencias destacadas

Han utilizado esta herramienta:
- YoQuieroSaber, Argentina (2017)
- Red Ciudadana, Guatemala (2015)
- Votainteligente.cl, Chile (2017)
- Elección Gubernamental Xalapa, México (2016)

### Beneficios

- Transparencia de posiciones políticas.
- Posicionamiento mediático de tu organización.
- Incentivo a la participación ciudadana a través de la facilitación de información de forma atractiva y amigable.


### Contáctanos

Si crees que esta herramienta es para ti y deseas llevarla a cabo ¡contáctanos!

Escríbenos a **herramientas@ciudadaniai.org**
