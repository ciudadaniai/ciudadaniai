---
ref: participacion
lang: es
title: Participación Infanto Juvenil
image: tools-06.gif
layout: tool
description: Una herramienta acompañada de una metodología especialmente diseñada para que niños, niñas y adolescente levanten sus propuestas manera ordenada y sistematizada.
intro: ¿Cómo podemos incluir a las niñas, niños y adolescentes (NNA) para que tengan experiencias participativas amigables para su formación como ciudadanas y ciudadanos? Acá te entregamos las herramientas para hacerlo
---

**Participación Infanto Juvenil** es una herramienta que te permitirá generar instancias de trabajo colaborativo y promover de manera interactiva el diálogo, para que las niñas, niños y adolescentes (NNA) identifiquen diversas problemáticas de su interés e ideen propuestas para resolverlas.

¿De qué manera? A través de una plataforma digital, estas propuestas podrán reunir apoyos ciudadanos y ser acogidas e implementadas por la unidad pertinente (centro de estudiantes, dirección del establecimiento educacional, gobierno local, presidencia, etc.).

### Experiencia destacada

En Chile 88 colegios utilizaron el proyecto durante las elecciones Presidenciales y Parlamentarias (2017). Participaron 836 NNA que crearon 211 propuestas, logrando 34 compromisos de candidatas y candidatos de los comicios electorales del país.

### Beneficios

1. Descubrimiento y la valorización de participación por NNA.
2. Desarrollo de competencias como pensamiento crítico, diálogo y trabajo colaborativo por NNA.
3. Contribuir a la valorización de la participación de NNA en comunidades escolares y espacios de toma de decisiones.



### Contáctanos

Si crees que esta herramienta es para ti y deseas llevarla a cabo ¡contáctanos!

Escríbenos a **herramientas@ciudadaniai.org**
