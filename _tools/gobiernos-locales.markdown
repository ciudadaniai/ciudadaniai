---
ref: participacion
lang: es
title: Gobiernos Locales Cercanos
image: tools-05.gif
layout: tool
description: Herramientas digitales y metodológicas para acercar el trabajo de municipios a vecinas y vecinos, potenciando la construcción de comunidades.
intro: Existen cerca de 17 mil gobiernos locales en América Latina, pero ¿cuántos de ellos crean políticas públicas con su comunidad? Con nuestra herramienta podrás hacer gobiernos más abiertos, efectivos y participativos.
---

¿Quieres generar cambios sociales potenciando a líderes de tu comunidad? ¿Crees que líderes latinoamericanos necesitan más herramientas para apoyar a sus comunidades? ¿Sientes que trabajar con otras personas se logran resultados más inclusivos y representativos?

Eso es lo que buscamos implementar Con **Gobiernos Locales Cercanos** creamos herramientas digitales y metodológicas que acercan el trabajo de municipios a vecinas y vecinos, potenciando la construcción colectiva de barrios y comunidades.

Esta plataforma, además, ofrece a los gobiernos locales de América Latina herramientas específicas en 4 etapas (levantamiento de propuestas, diseño, implementación y evaluación) con el objetivo de que las comunidades se sumen al centro de los procesos de toma de decisiones, promoviendo la participación vinculante de manera controlada y efectiva.

### Etapas
Las etapas son las siguientes:
Duración aproximada: 3 meses (según complejidad del desafío).

1. Propuestas: Las vecinas y vecinos crean y apoyan propuestas que responden a un desafío planteado por el municipio.
2. Talleres de diseño: La comunidad, junto al municipio, elabora el proyecto definitivo a partir de la propuesta ganadora.
3. Implementación: Al momento de ejecutar, vecinos y vecinas colaboran y dan seguimiento a la implementación del proyecto.
4. Evaluación: Quienes se involucraron en el proceso evalúan qué se debe mantener, qué mejorar y qué experimentar.

### Experiencias destacadas

Han utilizado esta herramienta

- Municipalidad de Peñalolén, Chile (2017)
- Municipalidad de Independencia, Chile (2018)

Ejemplos de su uso: Comuna Peñalolén, Chile. abre.penalolen.cl


### Contáctanos

Si crees que esta herramienta es para ti y deseas llevarla a cabo ¡contáctanos!

Escríbenos a **herramientas@ciudadaniai.org**
