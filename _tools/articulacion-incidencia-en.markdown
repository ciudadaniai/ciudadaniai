---
ref: incidencia
lang: en
title: Articulation for advocacy
image: tools-01.gif
layout: tool
intro: We deliver tools for planning and monitoring. Our methodologies help to define advocacy objectives from diversity, and they also build networks of interdisciplinary collaboration to achieve goals.
---
