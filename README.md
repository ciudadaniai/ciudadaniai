# Ciudadanía Inteligente

[![pipeline status](https://gitlab.com/ciudadaniai/ciudadaniai/badges/master/pipeline.svg)](https://gitlab.com/ciudadaniai/ciudadaniai/-/commits/master)


## Índice

- [1. Instalación](https://gitlab.com/ciudadaniai/ciudadaniai#instalaci%C3%B3n)
- [2. Traducciones](https://gitlab.com/ciudadaniai/ciudadaniai#traducciones)
  - [2.1. Cómo agregar traducciones](https://gitlab.com/ciudadaniai/ciudadaniai#c%C3%B3mo-agregar-traducciones)
  - [2.2. Cómo utilizar url](https://gitlab.com/ciudadaniai/ciudadaniai#c%C3%B3mo-utilizar-url)
- [3. Edición](https://gitlab.com/ciudadaniai/ciudadaniai#edici%C3%B3n)
  - [3.1. Posts](https://gitlab.com/ciudadaniai/ciudadaniai#post)
  - [3.2. Proyectos](https://gitlab.com/ciudadaniai/ciudadaniai#proyectos)
  - [3.3. Páginas temáticas](https://gitlab.com/ciudadaniai/ciudadaniai#p%C3%A1ginas-tem%C3%A1ticas)
  - [3.4. Equipo](https://gitlab.com/ciudadaniai/ciudadaniai#equipo)
  - [3.5. Directorio](https://gitlab.com/ciudadaniai/ciudadaniai#directorio)
  - [3.6. Línea de tiempo](https://gitlab.com/ciudadaniai/ciudadaniai#l%C3%ADnea-de-tiempo)
  - [3.7. Colaboradores](https://gitlab.com/ciudadaniai/ciudadaniai#colaboradores)
  - [3.8. Hero](https://gitlab.com/ciudadaniai/ciudadaniai#hero)
  - [3.8. Pasantías](https://gitlab.com/ciudadaniai/ciudadaniai#pasant%C3%ADas)

## Instalación

- Install jekyll
```
gem install bundler jekyll
```
- Clone repository
```
git clone git@gitlab.com:ciudadaniai/ciudadaniai.git
```
- ` cd ciudadaniai` to enter the folder site
- ` bundle install` to install the gems
- Ready!
```
bundle exec jekyll serve --watch --baseurl=
```

## Traducciones

Documentación plugin utilizado: [Jekyll Multiple languages](https://github.com/kurtsson/jekyll-multiple-languages-plugin#2-features)

### Cómo agregar traducciones

* Ref: [Translating strings](https://github.com/kurtsson/jekyll-multiple-languages-plugin#51-translating-strings)

Para agregar strings al sitio se debe utilizar `{% t key %}` o `{% translate key %}`. Esto mostrará la traducción correspondiente de los archivos de traducción, ubicados en la carpeta _i18n.

Los archivos de traducción (en.yml, es.yml, pt.yml) están escritos en sintáxis YAML de la siguiente manera:

```
meta:
  description: Somos una organización latinoamericana inclusiva feminista sin fines de lucro que busca fortalecer las democracias de América Latina a través del uso innovador de la tecnología.
  keywords: democracia, tecnología
  title: Fundación Ciudadanía Inteligente
  projects: Proyectos de FCI. Hola!

navbar:
  fci: Ciudadanía Inteligente
  about_fci: Sobre Ciudadanía
```

Entonces, si se quiere obtener el title, se debe escribir `{% t meta.description %}`

### Cómo utilizar url

* Ref: [Link between languages](https://github.com/kurtsson/jekyll-multiple-languages-plugin#55-link-between-languages)

Hay páginas y archivos que se traducen, y otras que no. Los archivos que están excluidos de traducción son los que están dentro de la carpeta assets (esto se indica en el archivo _config.yml).

Si una página esta traducida, para agregar un link a esta se debe agregar `site.baseurl/<page>`, en caso que sea una referencia a una página o un archivo (como una imagen) que no esté traducido se usa `site.baseurl_root/<page>`.


| Name              | Value                                                         | Example                    |
| ----------------: | :------------------------------------------------------------ | :------------------------- |
| site.lang         | The language used in the current compilation stage            | ``` en ```                 |
| site.baseurl      | Points to the root of the site including the current language | ``` http://foo.bar/en ```  |
| site.baseurl_root | Points to the root of the page without the language path      | ``` http://foo.bar ```     |
| page.url          | The current page's relative URL to the baseurl                | ``` /a/sub/folder/page/ ```|


## Edición

### Post

Los posts están ubicados en la carpeta de traducciones (`ciudadaniai > _i18n > en/es/pt > _posts`)[https://gitlab.com/ciudadaniai/ciudadaniai/tree/master/_i18n/es]. Para agregar un post se debe crear un archivo, en la carpeta correspondiente al idioma del post, utilizando la siguiente nomenclatura de nombre `YYYY-MM-DD-nombre-post.markdown`.

Dentro del archivo del post, en markdown, se debe completar el head para cada post, y bajo esto sumar el contenido en markdown (acá se puede revisar cómo escribir). A continuación un ejemplo de esto:

```
---
layout: post
title: 'Título del post'
intro: 'Resumen del post. Esto se verá en la vista previa.'
day: 21
month: Abr
date: 2017-04-21 12:00:00-0300
categories:
  - "Política"
highlighted: false
share_message: 'Los últimos acontecimientos políticos te pueden haber quitado las ganas de vivir. Aquí cómo recuperarlas'
img: 21-04-17-3.jpg

---

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

### Título en markdown

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

```

### Proyectos

Los proyectos están ubicados en la carpeta `ciudadaniai > _projects `. Estos se muestran en la sección de [projectos de la web de FCI](https://ciudadaniai.org/projects.html), en el [home](https://ciudadaniai.org) y en la (línea de tiempo)[(https://ciudadaniai.org/timeline)].

Para agregar un proyecto debe crearse un archivo `nombre-proyecto.md`, con la siguiente estructura:

```
---
layout: project
lang: es
name: "Vota Inteligente"
description: "Crea y apoya propuestas, durante procesos electorales, que serán enviadas a las candidaturas para que las incorporen en sus programas de gobierno."
description-timeline: "Primer Proyecto de Ciudadanía inteligente. Herramienta digital para  informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política."
visible_timeline: true
visible_home: true
image: "/assets/images/proyectos/vota.png"
hero-image: "/assets/images/hero_projects/hero-vota.jpg"
site: "https://votainteligente.cl/"
category: Denuncia Cohecho
year: 2009
status: Activo
versions:
  - name: "Vota Inteligente Chile"
    year: 2009
    country: "Chile"
    description: "Primer Proyecto de Ciudadanía inteligente. Herramienta digital para  informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política."
  - name: "Vota Inteligente Chile"
    year: 2011
    country: "Chile"
    description: "Herramienta digital para  informar a la ciudadanía sobre las candidaturas presidenciales, poniendo la tecnología a disposición de la política."

```

Algunas consideraciones importantes:
* Es importante notar que los parámetros `visible_timeline` y `visible_home` indicarán si este proyecto debe aparecer en la línea de tiempo y en el home respectivamente. Es importante indicar el idioma del proyecto en el atributo `lang` (es, pt o en), y se debe crear un archivo distinto por cada idioma.
* Las versions de cada proyecto serán las que se mostrarán en la línea de tiempo.
* La imagen `hero-image` es la que se muestra en la página particular del proyeco, mientras que la imagen `image` es la que se muestra en la vista previa (donde se ven todos los proyectos).


### Páginas temáticas

Son onepages que son creadas generalmente sobre un tema contingente (Covid-19, Elecciones, etc.). Se guardan en la carpeta `ciudadaniai > _campaigns `. Se muestran, eventualmente, en la sección de [projectos de la web de FCI](https://ciudadaniai.org/projects.html) y/o en el [home](https://ciudadaniai.org).

Para agregar una página debe crearse un archivo `nombre.md`, con la siguiente estructura:

```
---
layout: campaigns/semaforoautoritario
version: Informe semáforo autoritario
title: '¿Está en riesgo la democracia en Chile?'
call: 'Revisa el informe'
summary: En tiempos donde cada vez más democracias están siendo amenazadas por el surgimiento de liderazgos autoritarios, es fundamental que la ciudadanía cuente con elementos claros para evaluar el riesgo que determinadas candidaturas pueden implicar en caso de acceder al poder.
img: slides/BoricKast.jpg
anchor: informe
update: Datos levantos entre el 7 y 13 de julio
---

```


### Equipo

El equipo está ubicado en la carpeta `ciudadaniai > _rockstars` ⭐. Para agregar una persona, se debe crear un archivo con la siguiente estructura:

```
---
ref: dani
lang: es
sede: 'País sede'
name: 'Nombre persona'
function: 'Cargo persona'
bio: 'Biografía'
image: nombre-archivo.jpg
email: email@ciudadaniai.org
network_twitter: https://twitter.com/account
network_linkedin: https://www.linkedin.com/in/account/
network_github: https://github.com/account
network_facebook: https://www.facebook.com/in/account/
network_instagram: https://www.instagram.com/in/account/
active: true
---

```

Algunas consideraciones:
* La imagen debe ser agregada en `assets > images > team`.
* No olvidar indicar el idioma en el parámetro `lang` (es, pt, en). Se debe agregar un archivo por cada idioma.


### Directorio

Para el directorio aplica lo mismo que para el equipo, a diferencia que el archivo debe agregarse en la carpeta `ciudadaniai > _board_members`, y la imagen en `assets > images > board`.


### Línea de tiempo

Cada uno de los años que se muestran en la línea de tiempo están en la carpeta `ciudadaniai > _timelines `. Para sumar uno debe crearse un archivo nuevo por cada idioma, con la siguiente estructura:

```
---
lang: es
name: "2009"
title: "¡Ciudadanía Inteligente nace en Santiago de Chile!"
description: "Nacimos como Ciudadano Inteligente con el objetivo de disminuir las asimetrías de información existentes entre las candidaturas y la ciudadanía. Creamos nuestro primer proyecto: Vota Inteligente."
---

```

### Colaboradores

Para sumar organizaciones colaboradoras deben sumarse a a lista del archivo `sponsors` ubicado en `ciudadaniai > _data > sponsors.yml ` de la siguiente forma:

```
- name: Nombre colaborador 1
  logo: "nombre-imagen"
- name: Nombre colaborador 2
  logo: "nombre-imagen"

```

La imagen debe ser agregada en `ciudadaniai > assets > images > sponsors`.



### Hero

Para sumar imágenes con texto y botones en el hero/carrusel del home debe sumarse a a lista del archivo `slides.yml` ubicado en `ciudadaniai > _data > slides.yml ` de la siguiente forma:

```
- title: "Texto que aparece más grande sobre la imagen"
  subtitle: "Texto de bajada que sale bajo el title"
  lang: es
  img: nombreimagen.jpg
  buttons:
    - text: "Texto que tiene el botón, es el call to action"
      link: https://link_a_donde_quieras_que_lleve_el_boton.org/
      style: border-hover-white

```

Algunas consideraciones:
* Es importante el atributo `lang`, ya que este indica si aperece en la página de FCI en el idioma correspondiente. Si se quiere mostrar un slide en diferentes idiomas debe crearse un slide para cada idioma.
* Las imagenes deben ser agregadas en assets/images/hero
* Las opciones de estilo para un botón son:
  - Fondos que tienen degradaciones: bkg-blue-pink, bkg-pink-orange, bkg-green-purple, bkg-pink-cyan, bkg-purple-red, bkg-purple-orange, bkg-green-blue, bkg-cyan-purple
  - Fondo transparente: border-hover-white


### Pasantías

Para sumar búsquedas de practicantes o pasantes deben sumarse a a lista del archivo `internship` ubicado en `ciudadaniai > _data > internship.yml ` de la siguiente forma:

```
- title: En Ciudadanía Inteligente estamos buscando practicantes en nuestra área de diseño
  lang: es
  img: searching-01.jpg
  summary: Buscamos estudiantes de diseño gráfico que tengan interés en mejorar las democracias en Latinoamérica y quieran cambiar el mundo.
  description: Si quieres hacer tu práctica en diseño de infografías, manuales informativos y gráficas comunicacionales ¡Postula!
  skills:
    - Pensamiento crítico y reflexivo
    - Trabajo en equipo
    - Capacidad de trabajar autónomamente
    - Energía y ganas de involucrarse en el proyecto
    - Manejo de software gráfico vectorial como Adobe Illustrator
    - Manejo de software gráfico editorial como Adobe InDesign (no excluyente)

```

* Es importante el atributo `lang`, ya que este indica si aperece en la página de FCI en el idioma correspondiente.
